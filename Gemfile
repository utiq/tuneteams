source 'http://rubygems.org'

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '4.0.3'
# ruby '2.1.7'


# Use SCSS for stylesheets
gem 'sass-rails', '~> 4.0.0'

# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'

# Use CoffeeScript for .js.coffee assets and views
gem 'coffee-rails', '~> 4.0.0'

# See https://github.com/sstephenson/execjs#readme for more supported runtimes
# gem 'therubyracer', platforms: :ruby

# Use jquery as the JavaScript library
gem 'jquery-rails'

# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 1.2'

group :doc do
  # bundle exec rake doc:rails generates the API under doc/api.
  gem 'sdoc', require: false
end

# Use ActiveModel has_secure_password
# gem 'bcrypt-ruby', '~> 3.1.2'

# Use unicorn as the app server
gem 'unicorn'

# Use Capistrano for deployment
# gem 'capistrano', group: :development

# Use debugger
# gem 'debugger', group: [:development, :test]

group :development, :test do
  # gem 'rspec-rails', '~> 2.14.0'
  # gem 'factory_girl_rails', '~> 4.2.1'
  # gem 'guard-rspec'
  # gem 'guard-teaspoon'
  # gem 'guard-spork'
  # gem 'spork-rails'
  gem 'teaspoon'
  gem 'teaspoon-mocha'
  gem 'phantomjs'
  # gem 'rb-fsevent'
  # gem 'rb-inotify', :require => false
  # gem 'awesome_print'
end

group :development do
  gem 'knife-solo', '~> 0.4.2'
  # Capistrano
  gem 'capistrano'
  gem 'capistrano-bundler'
  gem 'capistrano-rails'
  gem 'capistrano-faster-assets'
  gem 'capistrano-sidekiq'
  gem 'capistrano-rbenv', '~> 2.0'
end

# Mongo
gem 'mongoid', '4.0.0'
gem 'bcrypt-ruby', '3.0.1', :require => 'bcrypt' # Encryption
gem "unf", '0.1.3'

# Uploading files
gem "carrierwave", "0.8.0" # Image Uploading
gem "carrierwave-mongoid", '0.7.0'
gem 'carrierwave_direct', '0.0.13'
gem "mini_magick", "3.4" # Image Manipulation
gem 'fog', '1.18.0' # Cloud library for Amazon S3

# Another gems
gem 'haml', '3.1.7' # HTML Abstraction Markup Language - Lightweight Markup Language
gem 'httparty', '0.12.0' # Consume web services and APIs
gem 'thin', '1.6.0' # Server

gem "aws-sdk", '2.0.34'
gem 'aws-ses', '~> 0.6.0', :require => 'aws/ses' # Send Emails with Amazon SES
gem "exception_notification", '4.0.1'
gem 'angularjs-rails', '1.2.16'

gem 'geocoder', '1.2.10'#'1.1.8' # Geographic Data
gem 'ng-rails-csrf' # helper for handling of CSRF token
gem 'bourbon' # Sass mixin library Bourbon

gem 'angularjs-rails-resource', '~> 0.2.5'
gem 'i18n', '0.6.9'
gem 'active_model_serializers', '0.8.1'

gem 'kmts', '~> 2.0.0' # KISSMetrics

# To search using "Elastic Search"
gem 'elasticsearch-model'
gem 'elasticsearch-rails'

# To work with Sidekiq
gem 'sidekiq', '3.2.1'
gem 'sinatra', require: false
gem 'slim'
# gem 'mina-sidekiq', require: false

# Mem caching
gem 'dalli', '2.7.2'

# Pagination
# gem 'will_paginate', '3.0.7'

gem 'hellosign-ruby-sdk', '3.0.9' # E-Signing

# PDF generation
gem 'wicked_pdf', '0.11.0'
gem 'wkhtmltopdf-binary', '0.9.9.3'

gem 'numbers_and_words', '0.10.0' # Converts any number to words
gem 'prerender_rails', '1.1.2' # Used for SEO and social networks

gem 'private_pub' # To use Faye

gem 'whenever', :require => false # Cronjobs
gem 'stripe', '1.20.4' # Payment processor

gem 'twilio-ruby', '3.16.1' # Send SMSs ans make phone calls
gem 'rotp'
gem 'mongoid_orderable'
gem 'phony'
gem 'payoneer-ruby'

gem 'kaminari', '0.16.3' # Pagination
gem 'koala', '2.2.0' # Facebook Graph API
gem 'twitter'

gem 'soundcloud', '0.3.1'
gem 'next-big-sound-lite', '1.0.0'
gem 'time_difference'
