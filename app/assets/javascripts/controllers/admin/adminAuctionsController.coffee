angular.module('tuneTeams').controller 'AdminAuctionsController'
, [ '$log'
    '$scope'
    '$http'
    '$filter'
    '$modal'
    'SessionService'
    'ViewState'
    '$alert'
    'Page'
    'Auction'
    'AdminAuction'
    '$localStorage'
, ($log, $scope, $http, $filter, $modal, SessionService, ViewState, $alert, Page, Auction, AdminAuction, $localStorage) ->

  # init
  # ------------------------------------------------------------
  init = ->
    ViewState.current = 'admin-auctions'
    Page.setTitle('Auctions Admin')
    # $scope.currentStatus = 'published'
    $scope.pagination =
      current: 1
    $scope.auction = {}
    $scope.$storage = $localStorage.$default(
      sortType: 'name'
      sortDirection: 'ASC'
      currentStatus: 'published'
    )

    $scope.getAuctions($scope.$storage.currentStatus, 1)

  $scope.pageChanged = (status, newPage) ->
    $scope.getAuctions(status, newPage)

  $scope.getAuctions = (status, pageNumber, sorting) ->
    AdminAuction.query({status: status, order_by: "#{$scope.$storage.sortType} #{$scope.$storage.sortDirection}", page_number: pageNumber}
    , (response) ->
      console.log response
      #404 or bad
      $scope.auctions = response.auctions
      $scope.totalAuctions = response.total_count
      $scope.counter = response.counter
      $scope.$storage.currentStatus = status
    )

  $scope.setSorting = (sortType, sortDirection) ->
    $scope.$storage.sortType = sortType
    console.log sortType, sortDirection
    if sortDirection == 'ASC'
      $scope.$storage.sortDirection = 'DESC'
    else
      $scope.$storage.sortDirection = 'ASC'
    $scope.getAuctions($scope.$storage.currentStatus, 1)

  notesModal = $modal({scope: $scope, template: '/assets/modals/auctionNotes.html', show: false})
  $scope.showNotesModal = (auction) ->
    $scope.auction = auction
    if auction.notes == undefined
      $scope.auction.notes = ""

    notesModal.$promise.then(notesModal.show)

  $scope.saveNotes = ->
    adminAuction = new AdminAuction
    adminAuction.$update {id: $scope.auction.id, notes: $scope.auction.notes}, ((response) ->
      notesModal.$promise.then(notesModal.hide)
    )

  $scope.approve = (auction, index)->
    adminAuction = new AdminAuction
    adminAuction.$approve {id: auction.id}, ((response) ->
      $scope.auctions.splice(index, 1)
    )

  $scope.return_to_draft = (auction, index)->
    adminAuction = new AdminAuction
    adminAuction.$returnToDraft {id: auction.id}, ((response) ->
      $scope.auctions.splice(index, 1)
    )

  $scope.reject = (auction, index)->
    adminAuction = new AdminAuction
    adminAuction.$reject {id: auction.id}, ((response) ->
      $scope.auctions.splice(index, 1)
    )

  $scope.delete = (auction, index)->
    adminAuction = new AdminAuction
    adminAuction.$delete {id: auction.id}, ((response) ->
      $scope.auctions.splice(index, 1)
    )

  payoutModal = $modal({scope: $scope, template: '/assets/modals/auctionPayout.html', show: false})
  $scope.showPayoutModal = (auction, index) ->
    $scope.auction = auction
    $scope.currentAuction = Auction.get({id: auction.permalink}, ->
      console.log $scope.currentAuction
      $scope.winningAmount = $scope.currentAuction.total.sub_total
      $scope.commision = $scope.currentAuction.total.tuneteams_commision
      $scope.amountToPay = $scope.winningAmount - $scope.commision
      $scope.currentIndex = index
      payoutModal.$promise.then(payoutModal.show)
    )

  $scope.makePayout = ->
    adminAuction = new AdminAuction
    adminAuction.$payout {auction_id: $scope.auction.id}, ((data) ->
      $scope.auctions.splice($scope.currentIndex, 1)
      $alert({title: "Payout delivered.", content: "The payment was successfully delivered", placement: 'top', type: 'success', show: true, container: 'body', animation: 'am-fade-and-slide-top', duration: 6})
      payoutModal.$promise.then(payoutModal.hide)
    ), (error) ->
      $alert({title: error.data.title, content: error.data.message, placement: 'top', type: 'danger', show: true, container: 'body', animation: 'am-fade-and-slide-top', duration: 6})

  $scope.dateOrder = ->
    if $scope.$storage.currentStatus == 'rejected' || $scope.$storage.currentStatus == 'ended_no_bids' ||  $scope.$storage.currentStatus == 'draft'
      console.log "lol"
      return 'created_at'


  init()
]
