angular.module('tuneTeams').controller 'AdminBillboardController'
, [ '$log'
    '$scope'
    '$http'
    '$filter'
    'SessionService'
    'ViewState'
    '$alert'
    'Page'
    'Auction'
    'AuctionPosition'
    'ProfilePosition'
, ($log, $scope, $http, $filter, SessionService, ViewState, $alert, Page, Auction, AuctionPosition, ProfilePosition) ->

  # init
  # ------------------------------------------------------------
  init = ->
    ViewState.current = 'admin-arrange'
    Page.setTitle('Arrange elements')
    getAuctions()
    getProfiles()

  getAuctions = ->
    $scope.auctions = AuctionPosition.query({order_by: "position ASC, updated_at DESC"}
    , (response) ->
      #404 or bad
      $location.path("/404") if response.status is 404
    )

  getProfiles = ->
    $scope.profiles = ProfilePosition.query({order_by: "position ASC"}
    , (response) ->
      $location.path("/404") if response.status is 404
    )    

  $scope.deleteAuction = (auction, index) ->
    auctionPosition = new AuctionPosition
    auctionPosition.$delete {id: auction.id, length: $scope.auctions.length}, ((data) ->
      $scope.auctions.splice(index, 1)
      $log.debug data
      if data.bottom_auction
        $scope.auctions.push(data.bottom_auction)
    )

  $scope.auctionSortableOptions =
    containment: '#auctions-sortable-container'
    accept: (sourceItemHandleScope, destSortableScope) ->
      sourceItemHandleScope.itemScope.sortableScope.$id == destSortableScope.$id
    orderChanged: (event) ->
      auction = new AuctionPosition
      auction.$update({id: event.source.itemScope.modelValue.id, position: event.dest.index + 1}, ->
        $log.info "updated"
      )

  $scope.profileSortableOptions =
    containment: '#profiles-sortable-container'
    accept: (sourceItemHandleScope, destSortableScope) ->
      sourceItemHandleScope.itemScope.sortableScope.$id == destSortableScope.$id
    orderChanged: (event) ->
      auction = new ProfilePosition
      auction.$update({id: event.source.itemScope.modelValue.id, position: event.dest.index + 1}, ->
        $log.info "updated"
      )

  addAuction = (auction) ->
    newMovingAuction = new AuctionPosition({id: auction._id})
    newMovingAuction.$save({}, ->
      newAuction = 
        id: auction._id
        name: auction._source.name
        permalink: auction._source.permalink
        default_illustration: auction._source.default_illustration

      # Remove the auction from the list if is already included
      $scope.auctions = $filter('filter')($scope.auctions, (value, index) ->
        value.id != auction._id
      )
      $scope.auctions.splice(0, 0, newAuction)
    )

  addProfile = (profile) ->
    newMovingProfile = new ProfilePosition({id: profile._id})
    newMovingProfile.$save({}, ->
      newProfile = 
        id: profile._id
        name: profile._source.name
        permalink: profile._source.permalink
        default_avatar: profile._source.default_avatar

      # Remove the profile from the list if is already included
      $scope.profiles = $filter('filter')($scope.profiles, (value, index) ->
        value.id != profile._id
      )
      $scope.profiles.splice(0, 0, newProfile)
    )

  $scope.deleteProfile = (profile, index) ->
    profilePosition = new ProfilePosition
    profilePosition.$delete {id: profile.id, length: $scope.profiles.length}, ((data) ->
      $scope.profiles.splice(index, 1)
      $log.debug data
      if data.bottom_auction
        $scope.profiles.push(data.bottom_auction)
    )

  $scope.$watch "auctionSelected", ->
    if $scope.auctionSelected != undefined
      addAuction($scope.auctionSelected)

  $scope.$watch "profileSelected", ->
    console.log $scope.profileSelected
    if $scope.profileSelected != undefined
      addProfile($scope.profileSelected)

  init()
]