angular.module('tuneTeams').controller 'AdminCampaignsController'
, [ '$log'
    '$scope'
    '$http'
    'SessionService'
    'ViewState'
    '$alert'
    '$modal'
    'Page'
    'MailingCampaign'
    '$location'
, ($log, $scope, $http, SessionService, ViewState, $alert, $modal, Page, MailingCampaign, $location) ->

  # init
  # ------------------------------------------------------------
  init = ->
    ViewState.current = 'admin-mailing-campaigns'
    Page.setTitle('Admin Mailing - Campaigns')
    $scope.newCampaign = new MailingCampaign()
    $scope.mailingCampaigns = MailingCampaign.query()

  newMailingCampaignModal = $modal({scope: $scope, template: '/assets/modals/newMailingCampaign.html', show: false})
  $scope.showNewCampaignModal = ->
    $scope.newMessage = {}
    newMailingCampaignModal.$promise.then(newMailingCampaignModal.show)

  $scope.saveNewCampaign = ->
    $scope.newCampaign.$save (result) ->
      newMailingCampaignModal.$promise.then(newMailingCampaignModal.hide)
      $location.path("/admin/mailing/campaigns/#{result.id}")

  init()
]