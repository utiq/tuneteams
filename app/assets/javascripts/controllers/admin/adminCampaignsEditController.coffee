angular.module('tuneTeams').controller 'AdminCampaignsEditController'
, [ '$log'
    '$scope'
    '$http'
    '$stateParams'
    'SessionService'
    'ViewState'
    '$alert'
    '$modal'
    'Page'
    'MailingCampaign'
, ($log, $scope, $http, $stateParams, SessionService, ViewState, $alert, $modal, Page, MailingCampaign) ->

  # init
  # ------------------------------------------------------------
  init = ->
    ViewState.current = 'admin-mailing-campaigns'
    Page.setTitle('Admin Mailing - Campaigns')
    $scope.newCampaign = new MailingCampaign()
    $scope.customMenu = [
      ['bold', 'italic', 'underline', 'strikethrough']
      ['font-size'],
      ['remove-format'],
      ['ordered-list', 'unordered-list', 'outdent', 'indent'],
      ['left-justify', 'center-justify', 'right-justify'],
      ['code', 'quote', 'paragragh'],
      ['link', 'image']
    ]
    $scope.mailingCampaign = MailingCampaign.get({id: $stateParams.id})

  newMailingCampaignModal = $modal({scope: $scope, template: '/assets/modals/newMailingCampaign.html', show: false})
  $scope.showNewCampaignModal = ->
    $scope.newMessage = {}
    newMailingCampaignModal.$promise.then(newMailingCampaignModal.show)

  $scope.saveDraft = (action) ->
    $scope.mailingCampaign.mailing_action = action
    $scope.mailingCampaign.$update (result) ->
      if action == "send_test"
        $alert({title: 'Message sent!', content: 'Your message was successfully sent.', placement: 'top', type: 'success', show: true, container: 'body', animation: 'am-fade-and-slide-top', duration: 6})
      else if action == "save_draft"
        $alert({title: 'Draft saved!', content: 'Your draft was successfully saved.', placement: 'top', type: 'success', show: true, container: 'body', animation: 'am-fade-and-slide-top', duration: 6})
      else if action == "send"
        $alert({title: 'Mailing sent!', content: 'Your mailing was successfully sent.', placement: 'top', type: 'success', show: true, container: 'body', animation: 'am-fade-and-slide-top', duration: 6})

      $scope.mailingCampaign = MailingCampaign.get({id: result.mailing_campaign._id})
      # $scope.mailingCampaign = result.mailing_campaign

  # $scope.sendTest = ->


  init()
]
