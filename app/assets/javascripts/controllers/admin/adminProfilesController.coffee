angular.module('tuneTeams').controller 'AdminProfilesController'
, [ '$log'
    '$scope'
    '$http'
    'SessionService'
    'ViewState'
    '$alert'
    'Page'
    'AdminProfile'
    '$localStorage'
    '$window'
, ($log, $scope, $http, SessionService, ViewState, $alert, Page, AdminProfile, $localStorage, $window) ->

  # init
  # ------------------------------------------------------------
  init = ->
    ViewState.current = 'admin-profiles'
    Page.setTitle('Admin profiles')
    # $scope.currentStatus = 'published'
    $scope.pagination =
      current: 1
    $scope.$storage = $localStorage.$default(
      sortType: 'name'
      sortDirection: 'ASC'
    )
    getProfiles(1)

  getProfiles = (pageNumber) ->
    AdminProfile.query({order_by: "#{$scope.$storage.sortType} #{$scope.$storage.sortDirection}", page_number: pageNumber}
    , (response) ->
      console.log response
      #404 or bad
      $scope.profiles = response.profiles
      $scope.totalProfiles = response.total_count
    )

  $scope.pageChanged = (newPage) ->
    getProfiles(newPage)

  $scope.setSorting = (sortType, sortDirection) ->
    $scope.$storage.sortType = sortType
    console.log sortType, sortDirection
    if sortDirection == 'ASC'
      $scope.$storage.sortDirection = 'DESC'
    else
      $scope.$storage.sortDirection = 'ASC'
    getProfiles(1)

  $scope.updateRating = (profile) ->
    adminProfile = new AdminProfile(profile)
    adminProfile.$update {id: profile.id}, ((response) ->
      console.log response
    )


  $scope.delete = (profile, index) ->
    p = new AdminProfile()
    p.$delete({id: profile.id}, ->
      $alert({title: 'Profile deleted!', content: "The profile was successfully deleted.", placement: 'top', type: 'success', show: true, container: 'body', animation: 'am-fade-and-slide-top', duration: 6})
      $scope.profiles.splice(index, 1);
    )

  $scope.exportCsv = ->
    $scope.laddaLoading = true
    AdminProfile.download {}, ((data) ->
      # $window.open(data.download_url, '_blank')
      $alert({title: "Emails export is in process", content: "You will receive an email when the process finishes", placement: 'top', type: 'success', show: true, container: 'body', animation: 'am-fade-and-slide-top', duration: 6})
      $scope.laddaLoading = false
    ), (error) ->
      $alert({title: error.data.title, content: error.data.message, placement: 'top', type: 'danger', show: true, container: 'body', animation: 'am-fade-and-slide-top', duration: 6})
      $scope.laddaLoading = false

  init()
]
