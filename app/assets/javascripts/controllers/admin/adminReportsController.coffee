angular.module('tuneTeams').controller 'AdminReportsController'
, [ '$log'
    '$scope'
    '$http'
    'SessionService'
    'ViewState'
    '$alert'
    'Page'
    'ProfileService'
, ($log, $scope, $http, SessionService, ViewState, $alert, Page, ProfileService) ->

  # init
  # ------------------------------------------------------------
  init = ->
    ViewState.current = 'admin-reports-dashboard'
    Page.setTitle('Admin Reports - Dashboard')
    $scope.profilesPerIndustry = {}
    getProfiles()

  getProfiles = ->
    $http.get("/api/v1/admin/reports/dashboard",
    ).success (data, status) ->
      $scope.profilesPerIndustry = data
      console.log $scope.profilesPerIndustry

  init()
]