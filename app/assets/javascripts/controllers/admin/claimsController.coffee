angular.module('tuneTeams').controller 'ClaimsController'
, [ '$log'
    '$scope'
    '$http'
    'SessionService'
    'ViewState'
    '$alert'
    'Page'
, ($log, $scope, $http, SessionService, ViewState, $alert, Page) ->

  # init
  # ------------------------------------------------------------
  init = ->
    ViewState.current = 'admin-claims'
    Page.setTitle('Admin claims')
    $scope.claims = {}
    getClaims()

  getClaims = ->
    $http.get("/api/v1/claims",
    ).success (data, status) ->
      $scope.claims = data

  $scope.assign = (claim, index) ->
    claimToDelete = $scope.claims[index];
    $http(
      method: "POST"
      url: "/api/v1/claims/assign/#{claim.id}"
    ).then ((result) ->
      $alert({title: 'Claim assigned!', content: "The claim was successfully assigned to #{claim.email}.", placement: 'top', type: 'success', show: true, container: 'body', animation: 'am-fade-and-slide-top', duration: 6})
      $scope.claims.splice(index, 1);
    ), (error) ->
      console.log error
      $alert({title: 'Error assigning the profile.', content: "#{error.data.errors.user}.", placement: 'top', type: 'danger', show: true, container: 'body', animation: 'am-fade-and-slide-top', duration: 6})

  $scope.reject = (claim, index) ->
    claimDelete = $scope.claims[index];
    $http(
      method: "POST"
      url: "/api/v1/claims/reject/#{claim.id}"
    ).then ((result) ->
      $scope.claims.splice(index, 1);
    ), (error) ->
      console.log error

  init()
]