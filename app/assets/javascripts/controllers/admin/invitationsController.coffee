angular.module('tuneTeams').controller 'InvitationsController'
, [ '$log'
    '$scope'
    '$http'
    'SessionService'
    'ViewState'
    '$alert'
    'Page'
    '$state'
    '$modal'
, ($log, $scope, $http, SessionService, ViewState, $alert, Page, $state, $modal) ->

  # init
  # ------------------------------------------------------------
  init = ->
    ViewState.current = $state.current.name
    $scope.hostName = "#{window.location.protocol}//#{window.location.host}"
    Page.setTitle('Admin invitations')
    $scope.getInvitations('pending')

  $scope.getInvitations = (status) ->
    # $http.get("/api/v1/invitations",
    # ).success (data, status) ->
    #   $scope.invitations = data
    console.log status
    $scope.currentStatus = status
    $http(
      method: "GET"
      url: "/api/v1/invitations"
      params:
        status: status
    ).then ((result) ->
      $scope.invitations = result.data
    ), (error) ->
      console.log error

  $scope.invite = (invitation, index) ->
    console.log invitation.id
    invitationDelete = $scope.invitations[index];
    $http(
      method: "POST"
      url: "/api/v1/invitations/invite/#{invitation.id}"
    ).then ((result) ->
      $alert({title: 'Invitation sent!', content: "Your invitation to #{invitation.email} was successfully sent.", placement: 'top', type: 'success', show: true, container: 'body', animation: 'am-fade-and-slide-top', duration: 6})
      invitation.invitation_sent = true
      $scope.invitations.splice(index, 1);
    ), (error) ->
      console.log error

  $scope.ignore = (invitation, index) ->
    invitationDelete = $scope.invitations[index];
    $http(
      method: "PUT"
      url: "/api/v1/invitations/#{invitation.id}"
      data:
        status: "ignored"
    ).then ((result) ->
      $scope.invitations.splice(index, 1);
    ), (error) ->
      console.log error

  sendPersonalInvitationModal = $modal({scope: $scope, template: '/assets/modals/invitationPersonal.html', show: false})
  $scope.showSendPersonalInvitationModal = ->
    $scope.newInvitation = {}
    sendPersonalInvitationModal.$promise.then(sendPersonalInvitationModal.show)

  $scope.sendPersonalInvitation = () ->
    # invitationDelete = $scope.invitations[index];
    $http(
      method: "POST"
      url: "/api/v1/invitations/personal"
      data: {
        email: $scope.newInvitation.email
      }
    ).then ((result) ->
      if $scope.invitations is undefined
        $scope.invitations = []
      $scope.invitations.splice(0,0,result.data)
      # $scope.invitations.push(result.data, 0)
      sendPersonalInvitationModal.$promise.then(sendPersonalInvitationModal.hide)
    ), (error) ->
      $scope.newInvitation.errors = error.data.errors
      console.log error

  init()
]