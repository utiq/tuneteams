angular.module('tuneTeams').controller 'AuctionsAgreementController'
, [ '$log',
    '$scope',
    '$http',
    '$sce',
    '$stateParams',
    '$filter'
    'SessionService',
    'ViewState'
    '$cookieStore'
    '$modal'
    '$alert'
    '$upload'
    'Auth'
    'ProfileService'
    'Genre'
    'Country'
    'Message'
    '$location'
    'Page'
    'Auction'
    'AuctionSong'
    'AuctionSigner'
    'AuctionWatchlist'
    'AuctionAgreement'
    'ngAudio'
    'angularLoad'
, ($log, $scope, $http, $sce, $stateParams, $filter, SessionService, ViewState, $cookieStore, $modal, $alert, $upload, Auth, ProfileService, Genre, Country, Message, $location, Page, Auction, AuctionSong, AuctionSigner, AuctionWatchlist, AuctionAgreement, ngAudio, angularLoad) ->

  # init
  # ------------------------------------------------------------
  init = ->
    ViewState.current = 'auctions-agreement'
    $scope.documentSigned = false
    $scope.errorMessage = ""
    $scope.hasHellosignError = false
    getAuction()


  getAuction = ->
    $scope.auction = Auction.get({id: $stateParams.permalink}, ->
      Page.setTitle("#{$scope.auction.name} auction")

      if $scope.auction.competitive_term == 'advance'
        $scope.required_advance_or_royalty_rate_text = "#{$scope.auction.required_advance_or_royalty_rate}%"
      else
        $scope.required_advance_or_royalty_rate_text = "$#{$scope.auction.required_advance_or_royalty_rate} USD"

      # $scope.timeRemaining = $scope.auction.due_date -
      $scope.auction.agreement = {}

      if $stateParams.signerId != undefined
        loadSignGenerator()

    , (response) ->
      #404 or bad
      $location.path("/404") if response.status is 404
    )

  loadSignGenerator = ->
    angularLoad.loadScript('https://s3.amazonaws.com/cdn.hellofax.com/js/embedded.js').then( ->
      HelloSign.init($scope.auction.client_id)
      $scope.auction.agreement = AuctionAgreement.get {id: $stateParams.permalink, signer_id: $stateParams.signerId}, ((data) ->
        # console.log $scope.auction.agreement.sign_url
        $scope.connectionsLoading = false
        HelloSign.open
          url: $scope.auction.agreement.sign_url,
          allowCancel: false,
          skipDomainVerification: $scope.auction.agreement.skip_domain_verification,
          # container: document.getElementById('hellosign-frame'),
          # height: 640
          # # redirectUrl
          messageListener: (eventData) ->
            # console.log "HelloSign event received"
            console.log eventData.event
            if eventData.event is HelloSign.EVENT_SIGNED
              # console.log "was signed"
              # $scope.auction.agreement.$update({id: $stateParams.permalink, signer_id: $stateParams.signerId}, ->
              #   console.log "updated"
              # )
              $scope.$apply ->
                $scope.documentSigned = true
      ), (error) ->
        console.log error
        # console.log JSON.stringify(error.data.error)
        $scope.hasHellosignError = true
        $scope.errorMessage = error.data.message
    ).catch( ->
      console.log "load script error"
      # // There was some error loading the script. Meh
    )

  placeBidModal = $modal({scope: $scope, template: '/assets/modals/placeBid.html', show: false})
  $scope.showPlaceBidModal = ->
    placeBidModal.$promise.then(placeBidModal.show)

  init()
]
