angular.module('tuneTeams').controller 'AuctionsIndexController'
, [ '$log'
    '$scope'
    '$modal'
    '$alert'
    '$location'
    '$stateParams'
    'ViewState'
    'Page'
    'Genre'
    'Country'
    'Auction'
    'Mobile'
, ($log, $scope, $modal, $alert, $location, $stateParams, ViewState, Page, Genre, Country, Auction, Mobile) ->

  # init
  # ------------------------------------------------------------
  init = ->
    ViewState.current = 'auctions-index'


    Country.query { show_all: true }, ((data) ->
      data[0].name = "All Countries (Global Rights)"
      $scope.countriesList = data
    )

    if !Mobile.isMobile()
      $scope.genresList = Genre.query()
    else
      $scope.genresList = Genre.query(order_by: 'name ASC')

    $scope.filter = {}
    $scope.filter.auctionTypes = [
      {id: "recording_rights", text: 'Recording rights'},
      {id: "publishing_rights", text: 'Publishing rights'},
      {id: "sub_publishing_rights", text: 'Sub-Publishing rights'},
      {id: "recording_publishing_rights", text: 'Recording and Publishing rights'}
    ]

    # Variables related with searching
    $scope.searchResults = {}
    $scope.isLoading = false
    $scope.formSearch = {}

    $scope.itemsPerPage = 12
    $scope.currentPage = 0
    $scope.total = 0


    getAuctions()

  $scope.randomBids = ->
    Math.floor((Math.random()*20)+1)

  $scope.randomAmount = ->
    Math.floor((Math.random()*50000)+1)

  $scope.showDropdownFilter = (filter) ->
    console.log filter
    switch filter
      when "country" then $scope.showCountryFilter = true; $scope.showGenreFilter = false; $scope.showTypeFilter = false
      when "genre" then $scope.showCountryFilter = false; $scope.showGenreFilter = true; $scope.showTypeFilter = false
      when "type" then $scope.showCountryFilter = false; $scope.showGenreFilter = false; $scope.showTypeFilter = true

  $scope.clearFilters = ->
    $scope.showCountryFilter = false
    $scope.showGenreFilter = false
    $scope.showTypeFilter = false
    $scope.filter.country_ids = []
    $scope.filter.genre_ids = []
    $scope.filter.auctionType = []
    $scope.filter.name = ''
    $scope.getResults(false)

  getAuctions = ->
    Auction.query({}
    , (response) ->
      $scope.lastBidsAuctions = response.last_bids_auctions
      $scope.getResults(false)
      console.log response
      # $scope.auctions =
      #404 or bad
      $location.path("/404") if response.status is 404
    )

  $scope.getResults = (loadMore) ->

    $scope.isLoading = true
    if loadMore
      $scope.currentPage++
    else
      $scope.currentPage = 0
      $scope.auctions = {}

    params = {
      offset: $scope.currentPage * $scope.itemsPerPage,
      limit: $scope.itemsPerPage
    }

    params.name = $scope.filter.name if $scope.filter.name != undefined and $scope.filter.name != ''
    params.country = $scope.filter.country_ids.join(',') if $scope.filter.country_ids != undefined and $scope.filter.country_ids.length > 0
    params.genre = $scope.filter.genre_ids.join(',') if $scope.filter.genre_ids != undefined and $scope.filter.genre_ids.length > 0
    params.auction_type = $scope.filter.auctionType.join(',') if $scope.filter.auctionType != undefined and $scope.filter.auctionType.length > 0

    Auction.search params, ((data) ->
      console.log data
      $scope.total = data.total
      if loadMore
        $scope.auctions = $scope.auctions.concat(data.results);
      else
        $scope.auctions = data.results
      if data.total == 0
        $scope.defaultMessage = "There are no results with your search criteria."
      $scope.isLoading = false
    )

  $scope.nextPageDisabledClass = ->
    (if $scope.currentPage is $scope.pageCount() - 1 or $scope.isLoading or $scope.total == 0 then "hide" else "")

  $scope.pageCount = ->
    Math.ceil($scope.total/$scope.itemsPerPage)

  $scope.$watch 'filter.name', ->
    $scope.getResults(false)

  init()
]
