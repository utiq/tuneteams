angular.module('tuneTeams').controller 'AuctionsNewController'
, [ '$log',
    '$scope',
    '$state',
    '$stateParams',
    '$filter'
    'SessionService',
    'ViewState'
    '$cookieStore'
    '$modal'
    '$alert'
    '$upload'
    'Auth'
    'ProfileService'
    'Genre'
    'Country'
    'Message'
    '$location'
    'Page'
    'Auction'
    '$timeout'
, ($log, $scope, $state, $stateParams, $filter, SessionService, ViewState, $cookieStore, $modal, $alert, $upload, Auth, ProfileService, Genre, Country, Message, $location, Page, Auction, $timeout) ->

  # init
  # ------------------------------------------------------------
  init = ->
    ViewState.current = 'auctions-new'
    Page.setTitle("Create an Auction")
    console.log Auth.user
    if Auth.user.role.title is 'user'
      _kmq.push(['record', 'Create Auction Visited', {}])
    $scope.slide = "auctionDescription"
    # $scope.slide = "auctionType"
    # $scope.slide = "legal"
    $scope.auction = new Auction()
    # $scope.auction.auction_type = 'recording_publishing_rights'
    # $scope.laddaLoading = true

    $scope.genresList = Genre.query()
    $scope.countriesListSimple = Country.query()

    Country.query { show_all: true }, ((data) ->
      data[0].name = "All Countries (Global Rights)"
      $scope.countriesList = data
    )
    $scope.showErrorFields = false
    $scope.auctionPermalink = ''
    $scope.tooltip =
      auctionName: "You might include the artist or songwriter name(s), the song name(s) and the country(s) in the Territory."
      auctionType: "The type of rights that will be licensed."
      auctionLength: "The number of days the auction will be accepting bids."
      competitiveTerm: "The number in the contract that will be entered as an auction bid (e.g. advance amount)"
      reserveOnCompetitiveTermRecording: "For the competitive term (advance or royalty rate), the minimum bid you will accept."
      reserveOnCompetitiveTermPublishing: "Help for reserve On Competitive Term Publishing."
      requiredAdvancedOrRoyaltyRate: "For the term (advance or royalty rate) that bidders will NOT be bidding on, the percentage or monetary amount that will be in the contract."
      term: "The number of years the contract will last."
      advanceOption: "A cash payment made at the time of contract signature that is a non-refundable payment recoupable against future royalties."
      royaltyRateOption: "The revenue share percentage paid to the contractual party who is the Licensor or Rights Owner."
      legalName: "The name of Rights Owner that will be party to the agreement. Their name as specified in government documents (e.g. passport or company incorporation documents)."
      applicableLaw: "Country's law under which the contract will be interpreted."
      placeOfCourt: "The city, state or region (in country of Applicable Law) where any legal proceedings resulting from this auction and associated contract will take place."
      signersName: "The person who will be signing the contract (person's legal name)."
      auctionManagerSecondSigner: "Please note that the publishing rights owner will also have to sign the auction's contract electronically.<br/> The publishing rights owner will receive an email asking them to confirm their email address. You will not be able to publish this auction until they confirm it."
      publishingRightsOwnerHasDifferentInformation: "Please note that the publishing rights owner will also have to sign the auction's contract. The publishing rights owner will receive an email asking them to confirm their email. You will not be able to publish this auction until they confirm it."
      showPrivateInfo: "The privacy setting means that only approved bidders will be able to see the \"Required\" and \"Length\" information. The \"public\" setting may attract more interest to your auction."

    $scope.auction.recording_term = 0
    $scope.auction.publishing_term = 0
    $scope.options =
      from: 1
      to: 10
      step: 1
      dimension: ' years'
      css:
        background: 'background-color': 'silver'

    validateProfile()


  validateProfile = ->
    $scope.isLoading = true
    $timeout (->
      SessionService.get(0).then (response) ->
        $scope.isLoading = false
        $scope.emailConfirmed = response.email_confirmed
        if response.profiles.length > 0
          $scope.userHasProfile = true
        showNewAuctionModal()
    ), 5000

  $scope.submit = (form) ->
    if form.$invalid
      $scope.showErrorFields = true
    else
      $scope.laddaLoading = true
      $scope.auction.$save (result) ->
        $scope.laddaLoading = false
        $scope.auction.same_recording_and_publisher = false
        $scope.slide = "auctionType"
        $scope.auctionPermalink = result.permalink
        $scope.auction.licensor_signer_name = result.signers[0].name
        $scope.auction.licensor_contact_email = result.signers[0].email

  newAuctionModal = $modal({scope: $scope, template: '/assets/modals/auctionNew.html', show: false})
  showNewAuctionModal = ->
    $log.info $cookieStore.get("showAuctionNewModal")
    showNewModal = $cookieStore.get("showAuctionNewModal")
    if showNewModal == undefined || showNewModal != false
      newAuctionModal.$promise.then(newAuctionModal.show)

  $scope.hideNewAuctionModal = ->
    newAuctionModal.$promise.then(newAuctionModal.hide)

  $scope.dontShowNewAuctionModalAgain = ->
    $cookieStore.put("showAuctionNewModal", false)

  $scope.updateAuction = (form) ->
    console.log form
    if form.$invalid
      $scope.showErrorFields = true
    else
      $scope.laddaLoading = true
      $scope.auction.$update({id: $scope.auctionPermalink}, ->
        $scope.slide = "legal"
        if Auth.user.role.title is 'user'
          _kmq.push(['record', 'Auction Created (Step2) - Auction Type', {'Auction name': $scope.auction.name }])
        $scope.laddaLoading = false
        # $scope.auction.licensor_signer_name = result.signers[0].name
        # $scope.auction.licensor_contact_email = result.signers[0].email
      )

  $scope.goToEditAuctionPage = ->
    #go to auction
    $scope.laddaLoading = true
    $scope.auction.$update({id: $scope.auctionPermalink}, ->
      if Auth.user.role.title is 'user'
        _kmq.push(['record', 'Auction Created (Step3) - Legal info', {'Auction name': $scope.auction.name }])
      $scope.slide = "legal"
      $scope.laddaLoading = false
      # $scope.auction.applicable_law_for_license = $scope.auction.applicable_law_for_license.id
      $state.go("auctions-show", { permalink: $scope.auction.permalink })
    )

  $scope.songWriterRequired = ->
    return $scope.slide == 'auctionType' && ($scope.auction.auction_type == 'publishing_rights' || $scope.auction.auction_type == 'sub_publishing_rights' || ($scope.auction.auction_type == 'recording_publishing_rights' && $scope.auction.same_recording_and_publisher == false))

  init()
]
