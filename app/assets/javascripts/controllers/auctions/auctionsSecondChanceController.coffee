angular.module('tuneTeams').controller 'AuctionsSecondChanceController'
, [ '$log'
    '$scope'
    '$http'
    '$sce'
    '$stateParams'
    'Page'
    'Auction'
    'SecondChance'
, ($log, $scope, $http, $sce, $stateParams, Page, Auction, SecondChance) ->

  # init
  # ------------------------------------------------------------
  init = ->
    # ViewState.current = 'auctions-second-chance'
    $scope.errorMessage = ""
    console.log $stateParams
    $scope.second_chance =
      accepted: false
    getAuction()

  $scope.accept = ->
    secondChance = new SecondChance()

    $scope.isLoading = true
    secondChance.$accept { auction_bidder_id: $stateParams.bidderId, auction_id: $scope.auction.id }, ((data) ->
      $scope.second_chance.accepted = true
      $scope.isLoading = false
    ), (error) ->
      console.log error

  $scope.decline = ->
    secondChance = new SecondChance()

    $scope.isLoading = true
    secondChance.$decline { auction_bidder_id: $stateParams.bidderId, auction_id: $scope.auction.id }, ((data) ->
      $scope.second_chance.declined = true
      $scope.isLoading = false
    ), (error) ->
      console.log error

  getAuction = ->
    $scope.isLoading = true
    $scope.auction = Auction.get({id: $stateParams.permalink}, ->
      Page.setTitle("Second chace for #{$scope.auction.name} auction")
      SecondChance.get { id: $stateParams.bidderId, auction_id: $scope.auction.id }, ((data) ->
        console.log data
        $scope.second_chance.amount = data.max_amount
        $scope.second_chance.valid = data.is_valid
        $scope.second_chance.previously_accepted = data.was_accepted
        $scope.isLoading = false
      ), (error) ->
        console.log error

    , (response) ->
      #404 or bad
      $location.path("/404") if response.status is 404
    )

  init()
]
