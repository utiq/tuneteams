angular.module('tuneTeams').controller 'AuctionsShowController'
, [ '$log'
    '$scope'
    '$http'
    '$sce'
    '$modal'
    '$alert'
    '$location'
    '$stateParams'
    '$routeParams'
    'ViewState'
    '$upload'
    '$window'
    '$filter'
    'Auth'
    'SessionService'
    'Genre'
    'Country'
    'Page'
    'Auction'
    'AuctionSong'
    'AuctionSigner'
    'AuctionWatchlist'
    'AuctionBidder'
    'AuctionBid'
    'AuctionVerification'
    'ngAudio'
    '$timeout'
    'SecondChance'
, ($log, $scope, $http, $sce, $modal, $alert, $location, $stateParams, $routeParams, ViewState, $upload, $window, $filter, Auth, SessionService, Genre, Country, Page, Auction, AuctionSong, AuctionSigner, AuctionWatchlist, AuctionBidder, AuctionBid, AuctionVerification, ngAudio, $timeout, SecondChance) ->

  # init
  # ------------------------------------------------------------
  init = ->
    ViewState.current = 'auctions-show'
    $scope.selectedNBSArtistId = undefined
    $scope.uploadProgress = 0
    $scope.uploadSongProgress = 0
    $scope.loaded = 0
    $scope.agree_terms_and_conditions = false
    $scope.navClass = "nav-tabs"
    # $scope.required_advance_or_royalty_rate_text = ''
    # $scope.auction =
    #   recording_term: "0",
    #   publishing_term:"0"
    getAuction()
    getLists()
    $scope.lol = 0
    $scope.songs = []

    $scope.options =
      from: 1
      to: 10
      step: 1
      dimension: ' years'
      css:
        background: 'background-color': 'silver'

    $scope.tabs = []
    $scope.tabs.activeTab = 0
    if $location.search().tab && $location.search().tab == 'bids'
      $scope.tabs.activeTab = 1

    if Auth.isLoggedIn()
      SessionService.get(0).then (response) ->
        $log.info "current_profile", response.current_profile
        $scope.current_profile = response.current_profile

    $scope.tooltip =
      rights: "The type of rights that will be licensed (e.g. recording, publishing and/or sub-publishing)."
      competitiveTerm: "The contract term that is bid on in an auction. In other words, the number in the contract that will be entered as an auction bid."
      reserve: "For the Competitive Term, the minimum amount the auction owner will accept. Royalty rates shown are the Rights Owner's share."
      required: "For the Term (advance or royalty rate) that is NOT the Competitive Term, the amount that will be in the contract. Royalty rates shown are the Rights Owner's share."
      recordingLength: "The number of years the Recording License Agreement will last."
      publishingLength: "The number of years the Publishing or Sub-Publishing Agreement will last."
      length: "The number of years the contract will last."
      reviewFullLicense: "Review Full License"
      isWatching: "Remove from favorites"
      isNotWatching: "Add to favorites"
      uploadSample: "Please note that tuneteams requires that Auction Managers upload at least a one (1) minute MP3 320 kbps file for each song to be auctioned. We recommend that you do not upload lossless files and, for pre-release material, do not upload full songs. The size limit for audio file uploads is 15 MB"
      iHaveAsecondSigner: "You can add a second signer to the contract if, for example, there is a different signing authority in your company for recording and publishing rights.<br />Please note, your second signer will receive an email asking them to confirm their email address. You will not be able to bid until they do."
      applicableLaw: "Country's law under which the contract will be interpreted."
      placeOfCourt: "The city, state or region (in country of Applicable Law) where any legal proceedings resulting from this auction and associated contract will take place."
      showPrivateInfo: "The privacy setting means that only approved bidders will be able to see the \"Required\" and \"Length\" information. The \"public\" setting may attract more interest to your auction."
      infoIsHidden: "Only you can see this information."
      infoIsPrivate: "Viewable to Approved Bidders Only."
      firstBid: "This is the first amount you will bid. If this amount both meets the Reserve and is the highest bid, no further bids will be made. If this amount is either lower than the Reserve or lower than the highest bid by another user, your bids will be increased in $25 intervals until both the Reserve is met and you are the high bidder or until your Maximum Bid amount is reached."
      maximumBid: "This is the Maximum Bid you will make even if this amount is lower than the Reserve amount or lower than a bid by another user. Of course, you can come back and make a higher bid later if the auction has not ended."

    getNBSData()

  $scope.generateCharts = ->

    $scope.lolo.forEach (entry) ->
      if entry["service"]["name"] == 'YouTube'
        setChart(entry["service"]["name"], entry["metric"]["plays"])
      else if entry["service"]["name"] == 'Facebook'
        setChart(entry["service"]["name"], entry["metric"]["fans"])
      else if entry["service"]["name"] == 'Twitter'
        setChart(entry["service"]["name"], entry["metric"]["fans"])
      else if entry["service"]["name"] == 'Instagram'
        setChart(entry["service"]["name"], entry["metric"]["fans"])

  setChart = (serviceName, serviceData) ->

    i = 0
    tempData = []

    for k of serviceData
      date = new Date(k * 86400 * 1000)
      l = date.getMonth() + '/' + date.getDate()
      if i == 0
        if serviceName == 'YouTube'
          tempData = [['day', 'plays']]
        else if serviceName == 'Facebook'
          tempData = [['day', 'fans']]
        else if serviceName == 'Twitter'
          tempData = [['day', 'followers']]
        else if serviceName == 'Instagram'
          tempData = [['day', 'followers']]
      tempData.push([l,parseInt(serviceData[k])])
      i++

    NBSChart = {}
    NBSChart.type = 'LineChart'
    NBSChart.data = tempData#[['day', 'likes'], ['pero',1000], ['todo',2000]
    NBSChart.options =
      showValueLabels: true
      backgroundColor: '#F1F1F1'
      width: '100%'
      height: 150
      legend: 'none'
      # chartArea:
      #   # left: '12%'
      #   # bottom: 10px;
      #   width: '85%'
      #   # height: '100%'
      hAxis:
        showTextEvery: 12
      vAxis:
        showTextEvery: 2
      tooltip:
        showColorCode: true

    NBSChart.formatters = number: [ {
      columnNum: 1
      pattern: '#,##0'
    } ]

    if serviceName == 'YouTube'
      $scope.NBSYouTubeChartData = NBSChart
    else if serviceName == 'Facebook'
      $scope.NBSFacebookChartData = NBSChart
    else if serviceName == 'Twitter'
      $scope.NBSTwitterChartData = NBSChart
    else if serviceName == 'SoundCloud'
      $scope.NBSSoundCloudChartData = NBSChart
    else if serviceName == 'Instagram'
      $scope.NBSInstagramChartData = NBSChart

  getAuction = ->
    $scope.isLoading = true
    $scope.auction = Auction.get({id: $stateParams.permalink}, ->
      $log.info $scope.auction.bids.bidders
      Page.setTitle("#{$scope.auction.name} auction")
      Page.setImage("#{$scope.auction.illustration.illustration.large.url}")
      $scope.auction.need_outbid = false
      $scope.auction.confirmBidMessage = false
      checkParameters()
      needOutbid()

      if $scope.auction.description != null
        if $scope.auction.description.length > 600
          $scope.descriptionTextHight = "less"

      $timeout (->
        $scope.lol = $scope.auction.due_date.stamp
        $scope.$apply()
        document.getElementsByTagName('timer')[0].start();
        return
      ), 1000

      getSocialNetworkPosts()

      PrivatePub.subscribe "/bids", (data, channel) ->
        $scope.$apply ->
          $log.info data.bid
          if $scope.auction.permalink == data.bid.auction.permalink
            $scope.auction.bids.current.recording = data.bid.amount
            $scope.auction.bids.current.publishing = data.bid.amount_publishing
            $scope.auction.bids.higher.name = data.bid.profile.name
            $scope.auction.bids.higher.permalink = data.bid.profile.permalink
            $scope.auction.bids.higher.avatar.thumb = data.bid.profile.default_avatar.thumb
            $scope.auction.bids.total++

            lastBid = {profile: {name: data.bid.profile.name, permalink: data.bid.profile.permalink, default_avatar: data.bid.profile.default_avatar}, bid_time_ago_in_words: data.bid.bid_time_ago_in_words, amount: data.bid.amount, amount_publishing: data.bid.amount_publishing}
            $scope.auction.bids.bidders.unshift(lastBid)
            needOutbid()

      $scope.isLoading = false

      $scope.embedCode = '<iframe frameborder="no" height="400" width="400" scrolling="no" src="' + currentDomain + '/embed/auctions/' + $scope.auction.permalink + '"></iframe>'
      $scope.hostName = currentDomain

      # # Signers validator
      # angular.forEach $scope.auction.signers, (value, key) ->
      #   $scope.auction.signers[key].temporal_email = value.email

      Auction.related { 'curent_auction_id': $scope.auction.id }, ((data) ->
        $scope.related_auctions = data
      )

    , (response) ->
      #404 or bad
      $location.path("/404") if response.status is 404
    )

  cancelRestartConfirmModal = $modal({scope: $scope, template: '/assets/modals/restartAuctionConfirmation.html', show: false})
  $scope.showCancelRestartConfirmModal = ->
    cancelRestartConfirmModal.$promise.then(cancelRestartConfirmModal.show)

  sendSecondChanceOfferModal = $modal({scope: $scope, template: '/assets/modals/secondChanceOffer.html', show: false})
  $scope.showSendSecondOfferModal = ->
    sendSecondChanceOfferModal.$promise.then(sendSecondChanceOfferModal.show)

  $scope.sendSecondChanceOffer = ->
    secondChance = new SecondChance()

    secondChance.$save { auction_bidder: $scope.auction.second_offer_bidder, auction_id: $scope.auction.id }, ((data) ->
      $scope.auction = data.auction
      $alert({title: '', content: 'Your second chance offer was successfully sent.', placement: 'top', type: 'success', show: true, container: 'body', animation: 'am-fade-and-slide-top', duration: 6})
      $window.location.reload();
      sendSecondChanceOfferModal.$promise.then(sendSecondChanceOfferModal.hide)
    ), (error) ->
      console.log error

  $scope.restart = ->
    $scope.auction.$restart { }, ((data) ->
      $alert({title: '', content: 'Auction has been restarted.', placement: 'top', type: 'success', show: true, container: 'body', animation: 'am-fade-and-slide-top', duration: 6})
      $window.location.reload();
      # $scope.auction = data
    ), (error) ->
      $alert({title: error.data.title, content: error.data.error , placement: 'top', type: 'danger', show: true, container: 'body', animation: 'am-fade-and-slide-top', duration: 6})


  $scope.cancelSecondChanceOffer = ->
    secondChance = new SecondChance()
    secondChance.$delete { id: '0', auction_id: $scope.auction.id }, ((data) ->
      $scope.auction = data.auction
    ), (error) ->
      console.log error

  checkParameters = ->
    if $location.search().reason
      if $location.search().reason is 'cancel_restart'
        $scope.showCancelRestartConfirmModal()
      else if $location.search().reason is 'validate_second_signer'
        $scope.validateSecondSigner()

  $scope.cancelRestartAuction = ->
    Auction.cancelRestart { id: $scope.auction.id, token: $location.search().token }, ((data) ->
      $alert({title: 'Restarting cancelled', content: 'This Auction was not restarted.', placement: 'top', type: 'success', show: true, container: 'body', animation: 'am-fade-and-slide-top', duration: 6})
    ), (error) ->
      $alert({title: error.data.title, content: error.data.error , placement: 'top', type: 'danger', show: true, container: 'body', animation: 'am-fade-and-slide-top', duration: 6})

  needOutbid = ->
    console.log "lolas pedos"
    $log.info "needOutbid", $scope.auction.bids.current, $scope.auction.bids.last_bid_profile
    $log.info $scope.auction.bids.current.recording, $scope.auction.bids.last_bid_profile.recording
    if $scope.auction.bids.current.recording > $scope.auction.bids.last_bid_profile.recording && $scope.auction.profile_owner.permalink != $scope.auction.bids.higher.permalink && !$scope.auction.is_owner && $scope.auction.bid_before
      if $scope.current_profile != undefined && $scope.current_profile.permalink == $scope.auction.bids.higher.permalink
        $scope.auction.need_outbid = false
      else
        $scope.auction.need_outbid = true

  $scope.getBidAmount = (amount) ->
    parsed = parseFloat(amount, 10)

    if $scope.auction.auction_type != 'recording_publishing_rights'
      if $scope.auction.competitive_term == 'advance'
        return "USD$#{parsed.toFixed(0)}"
      else
        return "#{parsed.toFixed(2)}%"
    else
      return "USD$#{parsed.toFixed(0)}"

  getLists = ->
    $scope.countriesListSimple = Country.query()

    Country.query { show_all: true }, ((data) ->
      data[0].name = "All Countries (Global Rights)"
      $scope.countriesList = data
    )

    $scope.genresList = Genre.query()

  getSocialNetworkPosts = ->
    if $scope.auction.facebook_page != null && $scope.auction.facebook_page != ""
      Auction.facebook_feed { permalink: $stateParams.permalink }, ((data) ->
        $scope.facebookFeed = data
      ), (error) ->
        # error handler
        $log.error error

    if $scope.auction.twitter_username != null && $scope.auction.twitter_username != ""
      Auction.twitter_timeline { permalink: $stateParams.permalink }, ((data) ->
        $scope.twitterTimeline = data
      ), (error) ->
        # error handler
        $log.error error

    if $scope.auction.soundcloud_profile != null && $scope.auction.soundcloud_profile != ""
      Auction.soundcloud_info { permalink: $stateParams.permalink }, ((data) ->
        $scope.soundcloudProfile = data
      ), (error) ->
        # error handler
        $log.error error

  getNBSData = ->
    Auction.next_big_sound_metrics { permalink: $stateParams.permalink }, ((data) ->
      $scope.lolo = data
      $scope.lolo.forEach (entry) ->
        if entry["service"]["name"] == 'YouTube'
          $scope.youTubeTotalPlays =  entry["metric"]["plays"][Object.keys(entry["metric"]["plays"])[Object.keys(entry["metric"]["plays"]).length - 1]]
        if entry["service"]["name"] == 'Instagram'
          $scope.instagramTotalFollowers =  entry["metric"]["fans"][Object.keys(entry["metric"]["fans"])[Object.keys(entry["metric"]["fans"]).length - 1]]

    ), (error) ->
      # error handler
      $log.error error

  $scope.timeFinished = ->
    if $scope.auction.bids.length == 0
      $scope.auction.status.raw = 'ended_no_bids'
    # else
    #   $scope.auction.status.raw = 'ended_reserve_not_met'

  $scope.onChangeIllustrationSelect = ($files) ->
    i = 0
    while i < $files.length
      file = $files[i]
      $scope.upload = $upload.upload(
        url: "/api/v1/auctions/set_illustration"
        data:
          auction_permalink: $stateParams.permalink
        file: file
      ).progress((evt) ->
        $scope.uploadProgress = parseInt(100.0 * evt.loaded / evt.total)
      ).success((data, status, headers, config) ->
        $scope.uploadProgress = 0
        $scope.auction.validations = data.validations
        $scope.auction.illustration.illustration.medium.url = data.illustration.illustration.medium.url
        # $scope.getCoverPicture()
      )
      i++

  $scope.updateTerritory = ->
    $scope.auction.$update({id: $stateParams.permalink}, ->
      $scope.isEditingTerritory = false
    )

  $scope.updateGenre = ->
    $scope.auction.$update({id: $stateParams.permalink}, ->
      $scope.isEditingGenre = false
    )

  $scope.updateName = ->
    $scope.auction.$update({id: $stateParams.permalink}, ->
      $scope.isEditingName = false
      $stateParams.permalink = $scope.auction.permalink
      # $window.history.pushState(null, $scope.auction.name, "/auctions/#{$scope.auction.permalink}");
      newUrl = "/auctions/#{$scope.auction.permalink}"
      $location.url(newUrl)
      $location.replace()
      $window.history.pushState(null, $scope.auction.name, newUrl);
    )

  $scope.updateDescription = ->
    $scope.auction.$update({id: $stateParams.permalink}, ->
      $scope.isEditingDescription = false
    )

  $scope.updateMainDetails = (form) ->
    $log.info form
    if form.$invalid
      $scope.showErrorFields = true
    else
      $scope.auction.$update({id: $stateParams.permalink}, ->
        $scope.isEditingDetails = false
      )

  $scope.updateSocialNetworks = ->
    # $scope.auction.nbs_artist_id = $scope.selectedNBSArtistId
    # $scope.auction.nbs_artist_name = $scope.selectedNBSArtistName
    $scope.auction.$update({id: $stateParams.permalink}, ->
      $scope.isEditingSocialNetwork = false
      getSocialNetworkPosts()
      getNBSData()
    )

  $scope.updateVideos = ->
    $scope.isEditingVideos = false

  $scope.watchlist = ->
    newWatchlist = new AuctionWatchlist()
    if $scope.auction.is_watching
      newWatchlist.$delete({
        id: $stateParams.permalink
      }, (result) ->
        $scope.auction.is_watching = false
      )
    else
      newWatchlist.auction_permalink = $stateParams.permalink
      newWatchlist.$save (result) ->
        $scope.auction.is_watching = true

  $scope.updateLegalInformation = ->
    $scope.auction.$update({id: $stateParams.permalink}, (response) ->
      console.log response
      $scope.auction.applicable_law_for_license = $scope.auction.applicable_law_for_license.id
      $scope.isEditingLegalInformation = false
    )

  $scope.getCoverPicture = ->
    if $scope.auction.illustration.illustration.medium.url == null
      "url(/assets/icons/auctions/default_album.png)"
    else
      "url(#{$scope.auction.illustration.illustration.medium.url})"

  placeBidModal = $modal({scope: $scope, template: '/assets/modals/placeBid.html', show: false})
  $scope.showPlaceBidModal = ->
    if $scope.auction.auction_type == 'recording_publishing_rights' && $scope.auction.bidder_has_second_signer && $scope.auction.second_licensor_contact_email_confirmed == false
      $alert({title: '', content: "You cannot place a bid because your second signer with email #{$scope.auction.second_licensor_contact_email} did not validate the email address. Please validate the email address and try again", placement: 'top', type: 'danger', show: true, container: 'body', animation: 'am-fade-and-slide-top', duration: 6})
    else
      $scope.auction.confirmBidMessage = false
      placeBidModal.$promise.then(placeBidModal.show)

  embedAuctionModal = $modal({scope: $scope, template: '/assets/modals/auctionEmbed.html', show: false})
  $scope.showEmbedAuctionModal = ->
    embedAuctionModal.$promise.then(embedAuctionModal.show)

  $scope.placeBid = ->
    bidIsValid = true
    error_type = ""
    $log.info $scope.auction.bid

    if $scope.auction.competitive_term == 'advance'
      if $scope.auction.bids.bidders.length > 0
        $scope.auction.bid = $scope.auction.bids.current.recording + 25
      if $scope.auction.maximum_bid < $scope.auction.bids.current.recording + 25
        error_type = "max_lt_bid"
        bidIsValid = false
    else if $scope.auction.competitive_term == 'royalty_rate'
      if $scope.auction.bids.bidders.length > 0
        $scope.auction.bid = $scope.auction.bids.current.recording + 1
      if $scope.auction.maximum_bid < $scope.auction.bids.current.recording + 1
        error_type = "max_lt_bid"
        bidIsValid = false

    # Demo data
    options =
      id: $scope.auction.id
      amount: $scope.auction.bid
      maximum_amount: $scope.auction.maximum_bid
      amount_publishing: $scope.auction.bid_publishing
      maximum_amount_publishing: $scope.auction.maximum_bid_publishing

    if bidIsValid == true
      newBid = new AuctionBid(options)
      newBid.$save { }, ((result) ->
        $scope.auction.bids.last_bid_profile = result.profile
        # $scope.auction.bids.higher.avatar.thumb = result.profile.default_avatar.thumb
        $scope.auction.bid = null
        $scope.auction.bid_publishing = null
        $scope.auction.maximum_bid = null
        $scope.auction.maximum_bid_publishing = null
        $scope.auction.need_outbid = false
        $scope.auction.bid_before = true
        placeBidModal.$promise.then(placeBidModal.hide)
        $alert({title: 'Bid placed!', content: 'Thank you for bidding on this auction. We will message you if another user submits a higher bid.', placement: 'top', type: 'success', show: true, container: 'body', animation: 'am-fade-and-slide-top', duration: 6})
      ), (error) ->
        # error handler
        $alert({title: 'Error.', content: error.data.message, placement: 'top', type: 'danger', show: true, container: 'body', animation: 'am-fade-and-slide-top', duration: 6})
    else
      if error_type == "max_lt_bid"
        $alert({title: 'Maximum bid amount.', content: 'Maximum bid amount must be greater or equal than current bid, please edit it and try again.', placement: 'top', type: 'danger', show: true, container: 'body', animation: 'am-fade-and-slide-top', duration: 6})

  $scope.canPublish = ->
    if $scope.auction.validations is undefined
      return true
    else
      return $scope.auction.validations.length > 0

  $scope.publish = ->
    if not $scope.agree_terms_and_conditions
      $alert({title: 'Please agree to the terms and conditions.', content: 'You need to agree to tuneteams’ Terms of Use agreement before you can publish an auction.', placement: 'top', type: 'warning', show: true, container: 'body', animation: 'am-fade-and-slide-top', duration: 5})
    else
      $scope.publishingLoading = true
      Auction.publish {auction_id: $scope.auction.id}, ((response)->
        $log.debug response
        $scope.auction.status.raw = 'published'
        $scope.agree_terms_and_conditions = true
        $scope.publishingLoading = false
        $scope.lol = $scope.auction.due_date.stamp
        # $scope.$apply ->
        # document.getElementsByTagName('timer')[0].start()
        $scope.isEditingName = $scope.isEditingDescription = $scope.isEditingDetails = $scope.isEditingTerritory = $scope.isEditingGenre = $scope.isEditingSongs = $scope.isEditingLegalInformation = false
      ), (error) ->
        console.log error

  # Adding songs methods
  $scope.editSongsButtonText = ->
    if $scope.auction.auction_songs is undefined
      'Add'
    else
      if $scope.auction.auction_songs.length == 0
        'Add'
      else
        'Edit'

  $scope.addSong = ->
    newSong = new AuctionSong({auction_permalink: $stateParams.permalink})
    newSong.$save (result) ->
      $scope.auction.auction_songs.push result.song
      $scope.auction.validations = result.validations

  $scope.updateSongs = (song) ->
    songUpdating = new AuctionSong(song)
    songUpdating.$update({id: song.id, auction_permalink: $stateParams.permalink}, (result) ->
      # song = songUpdating
      song = result.song
      $scope.auction.validations = result.validations
    )

  $scope.closeUpdateSongs = (form) ->
    if form.$valid
      $scope.isEditingSongs = false
      $scope.showEditingSongsErrors = false
    else
      $scope.showEditingSongsErrors = true

  $scope.removeSong = (index) ->
    song_to_delete = $scope.auction.auction_songs[index]
    deletedSong = new AuctionSong
    deletedSong.$delete({
      id: song_to_delete.id,
      auction_permalink: $stateParams.permalink
    }, (result) ->
      $scope.auction.auction_songs.splice(index, 1)
      $scope.auction.auction_songs.errors = null
    )



  $scope.onChangeAudioSelect = ($files, song) ->
    $scope.updateSongs(song)
    $scope.currentUploadingSong = song
    i = 0
    while i < $files.length
      file = $files[i]
      $scope.upload = $upload.upload(
        url: "/api/v1/auction_songs/set_audio"
        data:
          auction_permalink: $stateParams.permalink
          id: song.id
        file: file
      ).progress((evt) ->
        $scope.uploadSongProgress = parseInt(100.0 * evt.loaded / evt.total)
      ).success((data, status, headers, config) ->
        $log.debug data
        $scope.uploadSongProgress = 0
        $scope.auction.validations = data.validations
        $scope.auction.auction_songs = data.songs
        $scope.lastUploadedSong = $scope.currentUploadingSong.id
        $scope.currentUploadingSong = null
      )
      i++

  $scope.checkPlaylist = ->
    $log.info $scope.playlist

  $scope.trustSrc = (src) ->
    $sce.trustAsResourceUrl(src)

  ###########################################
  ###### BEGIN REQUEST APPROVAL TO BID ######
  ###########################################
  requestApprovalToBidModal = $modal({scope: $scope, template: '/assets/modals/requestApprovalToBid.html', show: false})
  $scope.showRequestApprovalToBidModal = ->
    _kmq.push(['record', "Request Approval To Bid Clicked", {'Auction Name': $scope.auction.name}])
    if Auth.isLoggedIn()
      SessionService.get(0).then (response) ->
        if response.current_profile == null
          $alert({title: 'Profile not configured.', content: "You have to set your profile in order to request permission to bid.", placement: 'top', type: 'danger', show: true, container: 'body', animation: 'am-fade-and-slide-top', duration: 5})
        else
          requestApprovalToBidModal.$promise.then(requestApprovalToBidModal.show)
    else
      $alert({title: 'User not logged in.', content: "Please login in order to request permission to bid on this auction.", placement: 'top', type: 'danger', show: true, container: 'body', animation: 'am-fade-and-slide-top', duration: 5})

  $scope.sendSecondSignerValidationEmail = (email) ->
    AuctionSigner.send_validation_email { auction_id: $scope.auction.id, signer_email: email, party_from: 'auction_manager' }, ((data) ->
      $alert({title: 'Verify your email.', content: "An email was sent to #{email}. Please check this email's inbox for instructions on how to complete email verification.", placement: 'top', type: 'success', show: true, container: 'body', animation: 'am-fade-and-slide-top', duration: 5})
    ), (error) ->
      # error handler
      return

  $scope.validateSecondSigner = ->
    email = $location.search().email
    from = $location.search().from
    profile_id = $location.search().profile_id

    AuctionSigner.validate_second_signer { auction_id: $scope.auction.id, signer_email: email, token: $location.search().token, from: from, profile_id: profile_id }, ((data) ->
      $scope.auction = data
      $alert({title: 'Email verified.', content: "The email #{email} was successfully verified.", placement: 'top', type: 'success', show: true, container: 'body', animation: 'am-fade-and-slide-top', duration: 5})
    ), (error) ->
      # error handler
      return


  $scope.requestApprovalToBid = (form) ->
    $log.info form
    if form.$invalid
      $scope.showErrorFields = true
    else if !$scope.auction.current_bidder_profile.licensor_phone_number_verified
      $alert({title: 'Verify your phone.', content: "You need to verify your phone in order to request approval to bid.", placement: 'top', type: 'warning', show: true, container: 'body', animation: 'am-fade-and-slide-top', duration: 5})
    else
      $log.info "form without errors"
      params =
        licensor_legal_name: $scope.auction.current_bidder_profile.licensor_legal_name
        licensor_legal_address_line1: $scope.auction.current_bidder_profile.licensor_legal_address_line1
        licensor_legal_address_line2: $scope.auction.current_bidder_profile.licensor_legal_address_line2
        licensor_legal_address_country: $scope.auction.current_bidder_profile.licensor_legal_address_country
        licensor_signer_name: $scope.auction.current_bidder_profile.licensor_signer_name
        licensor_contact_email: $scope.auction.current_bidder_profile.licensor_contact_email
        has_second_signer: $scope.auction.current_bidder_profile.has_second_signer
        second_licensor_signer_name: $scope.auction.current_bidder_profile.second_licensor_signer_name
        second_licensor_contact_email: $scope.auction.current_bidder_profile.second_licensor_contact_email

      newBidder = new AuctionBidder(auction_permalink: $stateParams.permalink, requester_details: params)
      newBidder.$save {}, ((result) ->
        $alert({title: 'Request to bid sent.', content: "Your request was sent and is waiting for the auction manager approval.", placement: 'top', type: 'success', show: true, container: 'body', animation: 'am-fade-and-slide-top', duration: 5})
        requestApprovalToBidModal.$promise.then(requestApprovalToBidModal.hide)
        $scope.auction.bidder_status = "pending"
      ), (error) ->
        $alert({title: 'Error.', content: error.data.message, placement: 'top', type: 'danger', show: true, container: 'body', animation: 'am-fade-and-slide-top', duration: 5})

  $scope.sendVerificationCode = ->
    params =
      phone_number: $scope.auction.current_bidder_profile.licensor_phone_number
      calling_code: $scope.auction.current_bidder_profile.licensor_calling_code
      source: 'bidder'

    $scope.phoneVerification = new AuctionVerification(params)
    $scope.phoneVerification.$save {}, ((result) ->
      $scope.auction.current_bidder_profile.phone_number_complete = "+#{$scope.auction.current_bidder_profile.licensor_calling_code}#{$scope.auction.current_bidder_profile.licensor_phone_number}"
      $scope.verificationCodeSent = true
    ), (error) ->
      console.log error
      $alert({title: 'Error.', content: error.data.message, placement: 'top', type: 'danger', show: true, container: 'body', animation: 'am-fade-and-slide-top', duration: 5})

  $scope.verifyPhoneCode = ->
    if $scope.auction.newBidderPhoneVerificationCode is undefined
      $alert({title: 'Verification code required.', content: 'Please enter the verification code you received in your cell phone.', placement: 'top', type: 'danger', show: true, container: 'body', animation: 'am-fade-and-slide-top', duration: 5})
    else
      params =
        verification_code: $scope.auction.newBidderPhoneVerificationCode
        phone_number: $scope.auction.current_bidder_profile.licensor_phone_number
        calling_code: $scope.auction.current_bidder_profile.licensor_calling_code
        source: 'bidder'

      $scope.phoneVerification.$verify params, ((data) ->
        $log.debug data
        if data.verified == true
          $scope.auction.current_bidder_profile.licensor_phone_number_verified = true
          $alert({title: 'Phone verified.', content: "Thanks, your phone was successfully verified.", placement: 'top', type: 'success', show: true, container: 'body', animation: 'am-fade-and-slide-top', duration: 5})
        else
          $alert({title: 'Wrong validation code.', content: "The code you entered is wrong. Try sending a text message again.", placement: 'top', type: 'danger', show: true, container: 'body', animation: 'am-fade-and-slide-top', duration: 5})
      )

  ###########################################
  ###### END REQUEST APPROVAL TO BID ######
  ###########################################

  $scope.sendVerificationCodeLicensor = ->
    params =
      phone_number: $scope.auction.licensor_phone_number
      calling_code: $scope.auction.licensor_calling_code
      source: 'auction_manager'
      auction_id: $scope.auction.id

    if $scope.auction.licensor_calling_code == '' || $scope.auction.licensor_phone_number == '' || $scope.auction.licensor_calling_code == undefined || $scope.auction.licensor_phone_number == undefined
      $alert({title: 'Cell phone number.', content: "Select your country code and enter your cell phone number.", placement: 'top', type: 'warning', show: true, container: 'body', animation: 'am-fade-and-slide-top', duration: 5})
    else
      $scope.phoneVerification = new AuctionVerification(params)
      # $scope.phoneVerification.$save (result) ->
      #   $log.info result
      #   $scope.showEnterLicensorVerificationCode = true

      $scope.phoneVerification.$save { }, ((data) ->
        $log.info data
        $scope.showEnterLicensorVerificationCode = true
      ), (error) ->
        $alert({title: 'Wrong Cell phone number.', content: error.data.message, placement: 'top', type: 'danger', show: true, container: 'body', animation: 'am-fade-and-slide-top', duration: 5})

  $scope.verifyLicensorPhone = ->
    if $scope.auction.licensor_verification_code is undefined
      $alert({title: 'Verification code required.', content: 'Please enter the verification code you received in your cell phone.', placement: 'top', type: 'danger', show: true, container: 'body', animation: 'am-fade-and-slide-top', duration: 5})
    else
      params =
        verification_code: $scope.auction.licensor_verification_code
        phone_number: $scope.auction.licensor_phone_number
        calling_code: $scope.auction.licensor_calling_code
        source: 'auction_manager'

      $scope.phoneVerification.$verify params, ((data) ->
        $log.debug data
        $scope.auction.licensor_verification_code = ""
        if data.verified == true
          $scope.showEnterLicensorVerificationCode = false
          $scope.auction = data.auction
          # $scope.auction.licensor_phone_number_verified = true
          $alert({title: 'Phone verified.', content: "Thanks, your phone was successfully verified.", placement: 'top', type: 'success', show: true, container: 'body', animation: 'am-fade-and-slide-top', duration: 5})
        else
          $alert({title: 'Wrong validation code.', content: "The code you entered is wrong. Try sending a text message again.", placement: 'top', type: 'danger', show: true, container: 'body', animation: 'am-fade-and-slide-top', duration: 5})
      )

  $scope.noLongerAvailable = ->
    # true
    if $scope.auction.status
      if $scope.auction.is_owner || $scope.auction.is_admin
        return false
      else
        if $scope.auction.status.raw == 'ended' || $scope.auction.status.raw == 'ended_no_bids'
          if !Auth.isLoggedIn()
            return true
          else
            if $scope.auction.bidder_status != 'accepted'
              return true
            else
              return false
        else
          return false


  $scope.$watch "selectedNBSArtistId", ->
    if $scope.selectedNBSArtistId != undefined
      $scope.auction.nbs_artist_id = $scope.selectedNBSArtistId
      $scope.auction.nbs_artist_name = $scope.selectedNBSArtistName
      $scope.editNbsArtist = false

  $scope.$on 'track:loaded', (event, data) ->
    $scope.loaded = data

  $scope.$on 'track:id', (event, data) ->
    $scope.loaded = 0
    $scope.loadedId = data
    song = $filter('filter')($scope.auction.auction_songs, {id: data})[0]
    _kmq.push(['record', "Song played", {'Song Name': song.name, 'Auction Name': $scope.auction.name}])

  init()
]
