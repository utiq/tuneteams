angular.module('tuneTeams').controller 'AccountManagementController'
, ['$log'
   '$scope'
   '$http'
   '$alert'
   '$location'
   'SessionService'
   'Auth'
   'ViewState'
   'Page'
, ($log, $scope, $http, $alert, $location, SessionService, Auth, ViewState, Page) ->

  # init
  # ------------------------------------------------------------
  init = ->
    ViewState.current = 'account-management'
    Page.setTitle('Account management')
    $scope.accountSettings = []
    SessionService.get(0).then (response) ->
      $scope.accountSettings.email = response.email

  $scope.save = ->
    #TODO: Convert into service
    $http(
      method: "PUT"
      url: "/api/v1/users/0"
      data:
        user:
          email: $scope.accountSettings.email
    ).then ((result) ->
      $scope.accountSettings.errors = []
      $alert({title: 'Check your email.', content: "A confirmation email has been sent to #{$scope.accountSettings.email}. Click on the confirmation link in the email to confirm your new email address.", placement: 'top', type: 'success', show: true, container: 'body', animation: 'am-fade-and-slide-top', duration: 6})
    ), (error) ->
      $alert({title: '', content: error.data.message, placement: 'top', type: 'danger', show: true, container: 'body', animation: 'am-fade-and-slide-top', duration: 6})
      # $scope.accountSettings.errors = error.data.errors

  init()

]
