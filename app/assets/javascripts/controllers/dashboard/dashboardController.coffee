angular.module('tuneTeams').controller 'DashboardController'
, ['$log'
   '$scope'
   '$http'
   '$location'
   'SessionService'
   'SessionShared'
   'Auth'
   'ViewState'
   '$alert'
   'Page'
   '$rootScope'
   'ProfileService'
   '$route'
, ($log, $scope, $http, $location, SessionService, SessionShared, Auth, ViewState, $alert, Page, $rootScope, ProfileService, $route) ->

  # init
  # ------------------------------------------------------------
  init = ->
    Page.setTitle('Dashboard')

    SessionService.get(0).then (response) ->
      $scope.userInfo = response
      $rootScope.profilePermalink = response.current_profile.permalink

  $scope.switchProfile = (profilePermalink) ->
    ProfileService.switch { permalink: profilePermalink }, ((data) ->
      $rootScope.profilePermalink = profilePermalink
      $location.path('/dashboard/profiles')
      $route.reload()
      SessionShared.prepForBroadcast(profilePermalink)
    ), (error) ->
      $log.info error

  $scope.$on('handleBroadcast', ->
    $scope.userInfo = SessionShared.currentUser
  )

  init()

]
