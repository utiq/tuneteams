angular.module('tuneTeams').controller 'AuctionsManagementOwnerController'
, [ '$log'
    '$scope'
    '$http'
    '$filter'
    '$modal'
    'SessionService'
    'ViewState'
    '$alert'
    'Page'
    'Auction'
    'Country'
    'AuctionManagement'
    '$state'
    'AuctionAgreement'
    '$location'
    '$window'
    '$localStorage'
, ($log, $scope, $http, $filter, $modal, SessionService, ViewState, $alert, Page, Auction, Country, AuctionManagement, $state, AuctionAgreement, $location, $window, $localStorage) ->

  # init
  # ------------------------------------------------------------
  init = ->
    ViewState.current = 'auctions-management'
    Page.setTitle('Auctions Management')
    $scope.auction = {}
    $scope.pagination =
      current: 1
      itemsPerPage: 10

    $scope.countriesList = Country.query()
    $scope.$storage = $localStorage.$default(
      sortTypeOwner: 'name'
      sortDirectionOwner: 'ASC'
      filterAuctionNameOwner: ''
      filterIconActivated: true
    )
    $scope.filter = {}
    $scope.getAuctions(1)


  $scope.getAuctions = (pageNumer) ->
    group = "owner"
    $scope.filter.name = $scope.$storage.filterAuctionNameOwner

    $scope.isLoading = true
    $scope.auctions = AuctionManagement.query({group: group, items_per_page: $scope.pagination.itemsPerPage, page_number: pageNumer, sorting: "#{$scope.$storage.sortTypeOwner} #{$scope.$storage.sortDirectionOwner}", filter: $scope.filter}
    , (response) ->
      $scope.isLoading = false
      $scope.auctions = response.auctions
      $scope.totalAuctions = response.total_count
    )

  $scope.pageChanged = (newPage) ->
    $scope.getAuctions(newPage)

  $scope.setSorting = (sortType, sortDirection) ->
    $scope.$storage.sortTypeOwner = sortType
    if sortDirection == 'ASC'
      $scope.$storage.sortDirectionOwner = 'DESC'
    else
      $scope.$storage.sortDirectionOwner = 'ASC'
    $scope.getAuctions(1)

  $scope.setFilter = (e) ->
    $scope.$storage.filterAuctionNameOwner = $scope.tempFilterAuctionNameOwner
    $scope.pagination.current = 1
    $scope.getAuctions(1)

  auctionCopyModal = $modal({scope: $scope, template: '/assets/modals/auctionCopy.html', show: false})
  $scope.showAuctionCopyModal = (auctionId) ->
    $scope.auction.id = auctionId
    auctionCopyModal.$promise.then(auctionCopyModal.show)

  $scope.showAuctionDeleteModal = (auction, index) ->
    if confirm("Are you sure you wanna delete this auction?. This action cannot be undone")
      auctionToDelete = new Auction
      auctionToDelete.$delete {id: auction.id}, ((response) ->
        foundAuction = $filter('filter')($scope.auctions, { id: auction.id }, true)[0]
        #get the index
        index = $scope.auctions.indexOf(foundAuction)
        $scope.auctions.splice(index, 1)
      )

  $scope.downloadContract = (auction_id) ->
    AuctionAgreement.download {auction_id: auction_id}, ((data) ->
      $window.open(data.download_url, '_blank');
    ), (error) ->
      $alert({title: error.data.title, content: error.data.message, placement: 'top', type: 'danger', show: true, container: 'body', animation: 'am-fade-and-slide-top', duration: 6})

  $scope.cancelAuction = (auction) ->
    AuctionManagement.cancel { auction_id: auction.id }, ((data) ->
      console.log data
      auction.status = data.status
    ), (error) ->
      $alert({title: error.data.title, content: error.data.message, placement: 'top', type: 'danger', show: true, container: 'body', animation: 'am-fade-and-slide-top', duration: 6})

  $scope.options = (auction) ->
    actionsOptions = [
      {
        text: "Copy"
        click: "showAuctionCopyModal(auction.id)"
      }
    ]

    if auction.status.code == -1 || auction.status.code == 0
      actionsOptions.push({text: "Delete", click: "showAuctionDeleteModal(auction)"})
    if auction.status.code == 4
      actionsOptions.push({text: "Cancel", click: "cancelAuction(auction)"})
    if auction.status.code > 5
      actionsOptions.push({text: "Download Contract", click: "downloadContract(auction.id)"})

    return actionsOptions

  $scope.saveAuctionCopy = ->
    auction = new Auction
    auction.name = $scope.auction.copyName
    $log.info $scope.auction.copyName
    auction.$copy {id: $scope.auction.id, name: $scope.auction.copyName, country_ids: $scope.auction.country_ids.join(',')}, ((response) ->
      $log.info response.auction
      $scope.auction.copyName = null
      $scope.auction.country_ids = null
      $scope.auctions.splice(0, 0, response.auction)
      auctionCopyModal.$promise.then(auctionCopyModal.hide)
    ), (error) ->
      console.log error
      $alert({title: "", content: error.data.message, placement: 'top', type: 'danger', show: true, container: 'body', animation: 'am-fade-and-slide-top', duration: 6})

  $scope.materialReceived = (auction) ->
    AuctionManagement.materialReceived {auction_id: auction.id}, ((data) ->
      auction.status.raw = data.auction_status.raw
      $alert({title: "", content: "Thank you for confirming that you have received the contractual materials. We will now send the Advance to the Auction Owner.", placement: 'top', type: 'success', show: true, container: 'body', animation: 'am-fade-and-slide-top', duration: 6})
    ), (error) ->
      $log.debug error

  init()
]
