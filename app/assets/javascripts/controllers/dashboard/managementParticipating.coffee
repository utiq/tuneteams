angular.module('tuneTeams').controller 'AuctionsManagementParticipatingController'
, [ '$log'
    '$scope'
    '$http'
    '$filter'
    '$modal'
    'SessionService'
    'ViewState'
    '$alert'
    'Page'
    'Auction'
    'Country'
    'AuctionManagement'
    '$state'
    'AuctionAgreement'
    '$location'
    '$window'
    '$localStorage'
, ($log, $scope, $http, $filter, $modal, SessionService, ViewState, $alert, Page, Auction, Country, AuctionManagement, $state, AuctionAgreement, $location, $window, $localStorage) ->

  # init
  # ------------------------------------------------------------
  init = ->
    ViewState.current = 'auctions-management'
    Page.setTitle('Auctions Management')
    $scope.actionsOptions = [
      {
        text: "Copy"
        click: "showAuctionCopyModal(auction.id)"
      }
    ]
    $scope.auction = {}
    $scope.pagination =
      current: 1
      itemsPerPage: 10

    $scope.countriesList = Country.query()
    $scope.$storage = $localStorage.$default(
      sortTypeParticipating: 'name'
      sortDirectionParticipating: 'ASC'
      filterAuctionNameParticipating: ''
      filterIconActivatedParticipating: false
    )
    $scope.filter = {}
    $scope.getAuctions(1)


  $scope.getAuctions = (pageNumer) ->
    group = "participating"

    $scope.filter.name = $scope.$storage.filterAuctionNameParticipating

    $scope.isLoading = true
    $scope.auctions = AuctionManagement.query({group: group, items_per_page: $scope.pagination.itemsPerPage, page_number: pageNumer, sorting: "#{$scope.$storage.sortTypeParticipating} #{$scope.$storage.sortDirectionParticipating}", filter: $scope.filter}
    , (response) ->
      $scope.isLoading = false
      $scope.auctions = response.auctions
      $scope.totalAuctions = response.total_count
    )

  $scope.pageChanged = (newPage) ->
    $scope.getAuctions(newPage)

  $scope.setSorting = (sortType, sortDirection) ->
    $scope.$storage.sortTypeParticipating = sortType
    if sortDirection == 'ASC'
      $scope.$storage.sortDirectionParticipating = 'DESC'
    else
      $scope.$storage.sortDirectionParticipating = 'ASC'
    $scope.getAuctions(1)

  $scope.setFilter = () ->
    $scope.$storage.filterAuctionNameParticipating = $scope.tempFilterAuctionNameParticipating
    $scope.pagination.current = 1
    $scope.getAuctions(1)

  $scope.materialReceived = (auction) ->
    AuctionManagement.materialReceived {auction_id: auction.id}, ((data) ->
      auction.status.raw = data.auction_status.raw
      $alert({title: "", content: "Thank you for confirming that you have received the contractual materials. We will now send the Advance to the Auction Owner.", placement: 'top', type: 'success', show: true, container: 'body', animation: 'am-fade-and-slide-top', duration: 6})
    ), (error) ->
      $log.debug error

  init()
]
