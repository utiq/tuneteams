angular.module('tuneTeams').controller 'AuctionSingleManagementController'
, [ '$log'
    '$scope'
    '$http'
    '$filter'
    '$modal'
    '$stateParams'
    'SessionService'
    'ViewState'
    '$alert'
    'Page'
    'Auction'
    'Country'
    'AuctionManagement'
    '$state'
    'AuctionAgreement'
    '$location'
    '$window'
, ($log, $scope, $http, $filter, $modal, $stateParams, SessionService, ViewState, $alert, Page, Auction, Country, AuctionManagement, $state, AuctionAgreement, $location, $window) ->

  # init
  # ------------------------------------------------------------
  init = ->
    ViewState.current = 'auctions-management-single'
    getAuction()

  getAuction = ->
    $scope.auction = Auction.get({id: $stateParams.auctionPermalink}, ->
      Page.setTitle("#{$scope.auction.name} Management")
      console.log $scope.auction

    , (response) ->
      #404 or bad
      $location.path("/404") if response.status is 404
    )

  init()
]
