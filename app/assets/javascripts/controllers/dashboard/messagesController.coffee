angular.module('tuneTeams').controller 'MessagesController'
, [ '$log'
    '$scope'
    '$http'
    '$location'
    'SessionService'
    'ViewState'
    '$stateParams'
    '$state'
    '$window'
    'Message'
    'SessionShared'
    'Page'
, ($log, $scope, $http, $location, SessionService, ViewState, $stateParams, $state, $window, Message, SessionShared, Page) ->

  # init
  # ------------------------------------------------------------
  init = ->
    ViewState.current = 'messages-inbox'
    Page.setTitle('Messages')
    $scope.replyMessage = []
    $scope.messages = []
    $scope.currentProfilePermalink = $stateParams.profilePermalink

    if $stateParams.messageId
      getMessage()
    else
      $scope.getMessages('inbox')

  getMessage = ->
    $scope.message = Message.get({
      id: $stateParams.messageId
      permalink: $stateParams.profilePermalink
    }, ->
      SessionShared.prepForBroadcast($scope.currentProfilePermalink)
    )

  $scope.reply = ->
    $scope.repliedMessage = new Message
    $scope.repliedMessage.$reply({
      id: $scope.message.id
      body: $scope.replyMessage.body
    }, (result) ->
      $scope.message.conversations.push(result)
      $scope.replyMessage = []
    )

  $scope.archive = (messageId, index) ->
    $scope.archivedMessage = new Message
    $scope.archivedMessage.$archive({
      id: messageId
    }, (result) ->
      $scope.messages.splice(index, 1);
    )

  $scope.delete = (messageId, index) ->
    $scope.deletedMessage = new Message
    $scope.deletedMessage.$delete({
      id: messageId
    }, (result) ->
      $scope.messages.splice(index, 1);
      console.log result
    )

  $scope.back = ->
    $window.history.back()

  $scope.getMessages = (status) ->
    $scope.currentStatus = status
    $scope.isLoading = true
    Message.query
      permalink: $stateParams.profilePermalink
      status: status
    , ((data) ->
      $scope.messages = data
      $scope.isLoading = false
    ), (error) ->
      $location.path("/401") if error.status is 401


  init()
]
