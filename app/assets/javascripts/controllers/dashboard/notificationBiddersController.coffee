angular.module('tuneTeams').controller 'NotificationBiddersController'
, [ '$log'
    '$scope'
    '$stateParams'
    '$location'
    '$alert'
    'SessionService'
    'AuctionBidder'
    'ViewState'
    'SessionShared'
    'Page'
, ($log, $scope, $stateParams, $location, $alert, SessionService, AuctionBidder, ViewState, SessionShared, Page) ->

  # init
  # ------------------------------------------------------------
  init = ->
    ViewState.current = 'bidders-notifications'
    Page.setTitle('Notifications')
    getBidders()

  getBidders = ->
    $scope.isLoading = true
    AuctionBidder.query
      permalink: $stateParams.profilePermalink
    , ((data) ->
      $scope.bidders = data
      $scope.isLoading = false
      # SessionShared.prepForBroadcast($stateParams.profilePermalink)
    ), (error) ->
      $location.path("/401") if error.status is 401

  $scope.updateStatus = (bidder, action, index) ->
    bidder.$update({id: bidder.id, decision: action}, ->
      # console.log "updated"
      SessionShared.prepForBroadcast($stateParams.profilePermalink)
      $scope.bidders.splice(index, 1);
      $alert({title: "Requesting to bid #{action}.", content: "Requesting to bid was succesfully #{action}.", placement: 'top', type: 'success', show: true, container: 'body', animation: 'am-fade-and-slide-top', duration: 5})
    )

  init()
]
