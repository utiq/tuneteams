angular.module('tuneTeams').controller 'NotificationManagementController'
, ['$log'
   '$scope'
   '$http'
   '$alert'
   '$location'
   'SessionService'
   'Auth'
   'ViewState'
   'Page'
, ($log, $scope, $http, $alert, $location, SessionService, Auth, ViewState, Page) ->

  # init
  # ------------------------------------------------------------
  init = ->
    ViewState.current = 'notification-management'
    Page.setTitle('Notification management')
    $scope.accountSettings = []
    SessionService.get(0).then (response) ->
      $scope.accountSettings.notification_mailing = response.notification_mailing
      $scope.accountSettings.notification_messages_or_invitations = response.notification_messages_or_invitations
      $scope.accountSettings.notification_surveys = response.notification_surveys
      $scope.accountSettings.notification_auction_approval = response.notification_auction_approval
      $scope.accountSettings.notification_auction_restart = response.notification_auction_restart

  $scope.save = ->
    #TODO: Convert into service
    $http(
      method: "PUT"
      url: "/api/v1/users/0"
      data:
        user:
          notification_mailing: String($scope.accountSettings.notification_mailing)
          notification_messages_or_invitations: String($scope.accountSettings.notification_messages_or_invitations)
          notification_surveys: String($scope.accountSettings.notification_surveys)
          notification_auction_approval: String($scope.accountSettings.notification_auction_approval)
          notification_auction_restart: String($scope.accountSettings.notification_auction_restart)
    ).then ((result) ->
      $scope.accountSettings.errors = []
      $alert({title: 'Changes saved', content: 'Your changes were successfully updated.', placement: 'top', type: 'success', show: true, container: 'body', animation: 'am-fade-and-slide-top', duration: 6})
    ), (error) ->
      $scope.accountSettings.errors = error.data.errors

  init()

]
