angular.module('tuneTeams').controller 'NotificationsController'
, [ '$log'
    '$scope'
    '$stateParams'
    '$location'
    '$alert'
    'SessionService'
    'Notification'
    'ViewState'
    'SessionShared'
    'Page'
, ($log, $scope, $stateParams, $location, $alert, SessionService, Notification, ViewState, SessionShared, Page) ->

  # init
  # ------------------------------------------------------------
  init = ->
    ViewState.current = 'notifications'
    Page.setTitle('Notifications')
    getNotifations()

  getNotifations = ->
    $scope.isLoading = true
    Notification.query
      permalink: $stateParams.profilePermalink
    , ((data) ->
      $scope.notifications = data
      $scope.isLoading = false
      SessionShared.prepForBroadcast($stateParams.profilePermalink)
    ), (error) ->
      $location.path("/401") if error.status is 401

  $scope.updateStatus = (notification, action, index) ->
    notification.$update({id: notification.id, decision: action}, ->
      # console.log "updated"
      SessionShared.prepForBroadcast($stateParams.profilePermalink)
      $scope.notifications.splice(index, 1);
      $alert({title: "Invitation to connect #{action}.", content: "The invitation was succesfully #{action}.", placement: 'top', type: 'success', show: true, container: 'body', animation: 'am-fade-and-slide-top', duration: 5})
    )

  init()
]
