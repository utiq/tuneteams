angular.module('tuneTeams').controller 'PasswordManagementController'
, ['$log'
   '$scope'
   '$http'
   '$location'
   'SessionService'
   'Auth'
   'ViewState'
   '$alert'
   'Page'
, ($log, $scope, $http, $location, SessionService, Auth, ViewState, $alert, Page) ->

  # init
  # ------------------------------------------------------------
  init = ->
    ViewState.current = 'password-management'
    Page.setTitle('Password management')

  $scope.save = ->
    #TODO: Convert into service
    if $scope.accountSettings.new_password != $scope.accountSettings.confirm_new_password
      $scope.accountSettings.errors =
        confirm_new_password: "The new password and confirm new password fields should be the same."
    else
      $http(
        method: "PUT"
        url: "/api/v1/users/0"
        data:
          user:
            password: $scope.accountSettings.new_password
      ).then ((result) ->
        $scope.accountSettings = []
        $alert({title: 'Password changed', content: 'Your new password was successfully changed.', placement: 'top', type: 'success', show: true, container: 'body', animation: 'am-fade-and-slide-top', duration: 6})
      ), (error) ->
        $scope.accountSettings.errors = error.data.errors

  init()

]
