angular.module('tuneTeams').controller 'PayoutManagementController'
, ['$log'
   '$scope'
   '$http'
   '$alert'
   '$location'
   'SessionService'
   'Auth'
   'ViewState'
   'Page'
   'PayoneerSettings'
, ($log, $scope, $http, $alert, $location, SessionService, Auth, ViewState, Page, PayoneerSettings) ->

  # init
  # ------------------------------------------------------------
  init = ->
    ViewState.current = 'payout-management'
    Page.setTitle('Payout management')

    $scope.isLoading = true
    $scope.payoneerSettings = PayoneerSettings.get({id: '0'}, ->
      $log.info $scope.payoneerSettings
      $scope.isLoading = false
      $scope.showApplicationReceived = $scope.payoneerSettings.details.accept_ach == true || $scope.payoneerSettings.details.accept_iach == true || $scope.payoneerSettings.details.accept_card == true
    )

  # $scope.showApplicationReceived = ->
  #   # true
  #   return

  init()

]
