angular.module('tuneTeams').controller 'ProfileDashboardController'
, [ '$log'
    '$scope'
    '$http'
    '$filter'
    '$modal'
    'SessionService'
    'ViewState'
    '$alert'
    'Page'
    '$state'
    '$location'
    '$window'
    'ProfileService'
    'Auth'
, ($log, $scope, $http, $filter, $modal, SessionService, ViewState, $alert, Page, $state, $location, $window, ProfileService, Auth) ->

  # init
  # ------------------------------------------------------------
  init = ->
    ViewState.current = 'profiles-dashboard'
    # $scope.getProfiles($scope.currentStatus)
    $scope.isLoading = true
    $scope.profiles = ProfileService.query((data) ->
      $scope.userRoleTitle = Auth.user.role.title
      $scope.isLoading = false
    )

  $scope.getAuctions = (status) ->
    if $state.current.name == "auctions-management-participating"
      group = "participating"
    else
      group = "owner"

  init()
]
