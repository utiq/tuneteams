angular.module('tuneTeams').controller 'EventsLandingController'
, ['$log'
   '$scope'
   'ViewState'
   '$modal'
   'Auth'
, ($log, $scope, ViewState, $modal, Auth) ->

  # init
  # ------------------------------------------------------------
  init = ->
    ViewState.current = 'events-landing'
    $scope.tuneteamsProfileVideo = 'https://www.youtube.com/watch?v=u-t9efpayzU'
    $scope.tuneteamsAuctionsVideo = 'https://www.youtube.com/watch?v=1TVyezwNftY'
    $scope.playerVars =
      controls: 2,
      autoplay: 1,
      rel: 0

  homeVideoModal = $modal({scope: $scope, template: '/assets/modals/homeVideo.html', show: false})
  $scope.showHomeVideoModal = ->
    _kmq.push(['record', "Watch Video Profiles Clicked", {'source': 'Midem landing'}])
    homeVideoModal.$promise.then(homeVideoModal.show)

  auctionVideoModal = $modal({scope: $scope, template: '/assets/modals/auctionsVideo.html', show: false})
  $scope.showAuctionVideoModal = ->
    _kmq.push(['record', "Watch Video Auctions Clicked", {'source': 'Midem landing'}])
    auctionVideoModal.$promise.then(auctionVideoModal.show)

  $scope.authorized = ->
    Auth.isLoggedIn()

  init()

]
