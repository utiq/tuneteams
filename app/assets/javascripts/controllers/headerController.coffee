angular.module('tuneTeams').controller 'HeaderController'
, [ '$log'
    '$window'
    '$rootScope'
    '$scope'
    '$location'
    'SessionService'
    'SessionShared'
    'ViewState'
    'Auth'
    '$state'
    'ProfileService'
, ($log, $window, $rootScope, $scope, $location, SessionService, SessionShared, ViewState, Auth, $state, ProfileService) ->

  # init
  # ------------------------------------------------------------

  init = ->
    $scope.accessLevels = Auth.accessLevels
    if Auth.isLoggedIn()
      SessionService.get(0).then (response) ->
        $scope.userInfo = response
        $rootScope.profilePermalink = response.current_profile.permalink


    $scope.selectedTopSearchOption = "profiles" #if $rootScope.selectedTopSearchOption == undefined then "profiles" else $rootScope.selectedTopSearchOption
    $scope.icons = [
                    {"value":"profiles","label":"Profiles"},
                    {"value":"auctions","label":"Auctions"}
                   ];

  # user status
  # ------------------------------------------------------------
  $scope.authorized = ->
    Auth.isLoggedIn()

  # logout
  # ------------------------------------------------------------
  $scope.logout = ->
    # Auth.logout
    #   # user: $scope.user
    #   # rememberme: $scope.rememberme
    # , ((res) ->
    #   $location.path "/login"
    #   $scope.$parent.is_logged = false
    # ), (err) ->
    #   # $rootScope.error = "Failed to login"
    #   $scope.user.errors = err.errors
    #   console.log err
    Auth.logout (->
      $location.path "/login"
    ), ->
      console.log "Failed to logout"

  # Header search bar
  # ------------------------------------------------------------

  $scope.search = ->
    location = "/search?q=#{$scope.formSearch.query}"
    if $scope.formSearch.genre
      location += "&genre=#{$scope.formSearch.genreName}"
    if $scope.formSearch.country
      location += "&country=#{$scope.formSearch.countryName}"
    if $scope.formSearch.industryRole
      location += "&industry_role=#{$scope.formSearch.industryRoleName}"
    $window.location.href = location
    $scope.formSearch.show = false

  # $scope.$watch "selectedTopSearchOption", ->
  #   $rootScope.selectedTopSearchOption = $scope.selectedTopSearchOption
  #   console.log $rootScope.selectedTopSearchOption

  $scope.$watch "searchProfilePermalinkSelected", ->
    if $scope.searchProfilePermalinkSelected != undefined
      $window.location.href = "/#{$scope.searchProfilePermalinkSelected}"

  $scope.$watch "searchAuctionPermalinkSelected", ->
    if $scope.searchAuctionPermalinkSelected != undefined
      $window.location.href = "/auctions/#{$scope.searchAuctionPermalinkSelected}"

  $scope.$on('handleBroadcast', ->
    $scope.userInfo = SessionShared.currentUser
  )

  $scope.goToSearch = ->
    _kmq.push(['record', "Advanced Search Button Clicked", {'source': 'Top'}])
    $location.path("/profiles")

  $scope.goToAuctionsSearch = ->
    _kmq.push(['record', "Advanced Auctions Search Button Clicked", {'source': 'Top'}])
    $location.path("/auctions")

  init()
]
