angular.module('tuneTeams').controller 'HomeController'
, ['$log'
   '$scope'
   '$http'
   '$window'
   '$modal'
   'ViewState'
   '$state'
   '$alert'
   'Genre'
   'Country'
   'IndustryRoleProfile'
   '$location'
   'Auth'
   '$rootScope'
   'Page'
   'SessionService'
   'Auction'
   'ProfileService'
, ($log, $scope, $http, $window, $modal, ViewState, $state, $alert, Genre, Country, IndustryRoleProfile, $location, Auth, $rootScope, Page, SessionService, Auction, ProfileService) ->

  # init
  # ------------------------------------------------------------
  init = ->
    if Auth.isLoggedIn()
      SessionService.get(0).then (response) ->
        $scope.userInfo = response
        $rootScope.profilePermalink = response.current_profile.permalink

      if $rootScope.lastLocationPath == '/login'
        $window.location.href = "/#{Auth.user.default_profile}"
      else if Auth.user.default_profile == '' || Auth.user.default_profile == null || Auth.user.default_profile == undefined
        $window.location.href = "/profile/create"

    ProfileService.promoted { }, ((data) ->
      $scope.profiles = data.slice(0, 5)
    )
    $scope.auctions = Auction.promoted()
    $scope.carouselIndex = 0

    ViewState.current = 'home'
    Page.setTitle('Home')

    $scope.tuneteamsProfileVideo = 'https://www.youtube.com/watch?v=1TVyezwNftY'
    $scope.playerVars =
      controls: 2,
      autoplay: 1,
      rel: 0
    # console.log
    $alert({title: 'Error.', content: $rootScope.error, placement: 'top', type: 'danger', show: true, container: 'body', animation: 'am-fade-and-slide-top', duration: 6}) unless !$rootScope.error
    # getAuctions()

  # user status
  # ------------------------------------------------------------
  $scope.authorized = ->
    Auth.isLoggedIn()

  homeVideoModal = $modal({scope: $scope, template: '/assets/modals/homeVideo.html', show: false})
  $scope.showHomeVideoModal = ->
    _kmq.push(['record', "Watch Video Clicked", {'source': 'Home'}])
    homeVideoModal.$promise.then(homeVideoModal.show)

  $scope.logoView = (event, inview) ->
    if inview
      # TODO: It's not a good practice to control de DOM from here (provitional code)
      $(".landing-header").css("display", "none")
    else
      $(".landing-header").css("display", "")
    return tru


  init()
]
