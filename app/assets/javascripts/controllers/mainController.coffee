angular.module('tuneTeams').controller 'MainController'
, [ '$log'
    '$scope'
    'SessionService'
    'ViewState'
    'Auth'
    'Page'
, ($log, $scope, SessionService, ViewState, Auth, Page) ->

  # init
  # ------------------------------------------------------------
  init = ->
    $scope.viewState = ViewState
    $scope.Page = Page
    $scope.showHeaderSearch = false

  # user status
  # ------------------------------------------------------------
  $scope.authorized = ->
    Auth.isLoggedIn()

  init()
]
