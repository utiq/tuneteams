angular.module('tuneTeams').controller 'PaymentController'
, ['$log'
   '$scope'
   '$http'
   '$window'
   '$modal'
   'ViewState'
   '$state'
   '$alert'
   'Auction'
   '$location'
   'Auth'
   '$rootScope'
   'Page'
   'SessionService'
   '$stateParams'
, ($log, $scope, $http, $window, $modal, ViewState, $state, $alert, Auction, $location, Auth, $rootScope, Page, SessionService, $stateParams) ->

  # init
  # ------------------------------------------------------------
  init = ->
    ViewState.current = 'auctions-payment'
    Page.setTitle('Auctions Payment')
    # $scope.number = "4242424242424242"
    # $scope.number = "4000000000000002"
    # $scope.expiry = "01/2016"
    # $scope.cvc = 123
    getAuction()

  # user status
  # ------------------------------------------------------------

  getAuction = ->
    $scope.auction = Auction.get({id: $stateParams.permalink}, ->
      Page.setTitle("#{$scope.auction.name} payment")

    , (response) ->
      #404 or bad
      $location.path("/404") if response.status is 404
    )

  $scope.handleStripe = (status, response) ->
    $scope.processingOrder = true
    if response.error
      # $scope.errors = response.error.message
      $alert({title: 'Error in your transaction.', content: response.error.message, placement: 'top', type: 'danger', show: true, container: 'body', animation: 'am-fade-and-slide-top', duration: 5})
      $scope.processingOrder = false
    else
      # got stripe token, now charge it or smt
      token = response.id

      $scope.auction.$pay {id: $stateParams.permalink, stripe_token: token}, ((response) ->
        $scope.processingOrder = false
        $scope.paymentSuccesful = true
        console.log response
      ), (error) ->
        $scope.processingOrder = false
        $alert({title: 'Error in your transaction.', content: $scope.errors = error.data.errors.message, placement: 'top', type: 'danger', show: true, container: 'body', animation: 'am-fade-and-slide-top', duration: 5})


  init()
]
