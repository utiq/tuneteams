angular.module('tuneTeams').controller 'CreateProfileController'
, [ '$log'
    '$scope'
    '$http'
    '$resource'
    '$location'
    '$alert'
    '$stateParams'
    'SessionService'
    'ViewState'
    'Page'
    'Auth'
, ($log, $scope, $http, $resource, $location, $alert, $stateParams, SessionService, ViewState, Page, Auth) ->

  # init
  # ------------------------------------------------------------
  init = ->
    ViewState.current = 'profile-create'
    Page.setTitle('Create profile')
    getIndustryRoleProfiles()
    $scope.countries = $resource('/api/v1/countries').query()
    SessionService.get(0).then (response) ->
      $scope.userInfo = response

  $scope.showForm = ($event) ->
    $($event).css("top", "-30px");

  getIndustryRoleProfiles = ->
    $http(
      method: "GET"
      url: "/api/v1/industry_role_profiles"
    ).then ((result) ->
      $scope.industry_role_profiles = result.data
    ), (error) ->
      console.log error

  $scope.submit = (form) ->
    # console.log $scope.newProfile
    if $scope.userInfo.role.title == "user" && $scope.userInfo.profiles.length == 2
      $alert({title: 'Maximum number of profiles.', content: 'You can not create more than 2 profiles.', placement: 'top', type: 'danger', show: true, container: 'body', animation: 'am-fade-and-slide-top', duration: 5})
    else
      $http(
        method: "POST"
        url: "/api/v1/profiles"
        data:
          profile: $scope.newProfile
      ).then ((result) ->
        $location.path("/profile/create/#{result.data.permalink}/step2")
      ), (error) ->
        $scope.newProfile.errors = error.data.errors
        console.log $scope.newProfile.errors

  init()
]
