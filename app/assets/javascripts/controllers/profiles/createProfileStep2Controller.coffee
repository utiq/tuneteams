angular.module('tuneTeams').controller 'CreateProfileStep2Controller'
, ['$log'
   '$scope'
   '$http'
   '$resource'
   '$location'
   '$stateParams'
   '$upload'
   'SessionService'
   'ViewState'
   'Genre'
   'Country'
   'ProfileService'
   'IndustryRoleProfile'
   'Page'
   'Auth'
, ($log, $scope, $http, $resource, $location, $stateParams, $upload, SessionService, ViewState, Genre, Country, ProfileService, IndustryRoleProfile, Page, Auth) ->

  # init
  # ------------------------------------------------------------
  init = ->
    ViewState.current = "profile-create-step2"
    $scope.slide = "information"
    Page.setTitle('Create profile step 2')
    # $scope.slide = "profile_picture"
    $scope.accessLevels = Auth.accessLevels
    $scope.uploadProgress = 0
    $scope.has_avatar = false
    $scope.showErrorFields = false
    $scope.genres = Genre.query()

    $scope.newProfile = ProfileService.get({id: $stateParams.profile_permalink}, ->
      $scope.newProfile.interests = []
      $scope.newProfile.interestedCountries = []
      $scope.industryRoleProfileInterests = IndustryRoleProfile.interests({id: $scope.newProfile.industry_role_profile_id})
      $scope.countriesList = Country.query(
        show_all: true
      )
    )


  $scope.onFileSelect = ($files) ->
    #$files: an array of files selected, each file has name, size, and type.
    i = 0

    while i < $files.length
      file = $files[i]
      $scope.upload = $upload.upload(
        url: "/api/v1/profiles/set_profile_picture"
        data:
          profile_permalink: $stateParams.profile_permalink
        file: file
      ).progress((evt) ->
        $scope.uploadProgress = parseInt(100.0 * evt.loaded / evt.total)
      ).success((data, status, headers, config) ->
        if Auth.user.role.title is 'user'
          _kmq.push(['record', 'Profile Created (Step2) - Avatar Changed', {'Profile name': $scope.newProfile.name }])
        $scope.uploadProgress = 0
        $scope.newProfile.avatar = data.avatar
        $scope.has_avatar = true
      )
      i++

  $scope.goToEditProfilePage = ->
    if Auth.user.role.title is 'user'
      _kmq.push(['record', 'Profile Created (Step2) - Avatar Change Skipped', {'Profile name': $scope.newProfile.name }])
    $location.path("/#{$stateParams.profile_permalink}")

  $scope.updateProfileInterests = ->
    $scope.newProfile.$update({id: $stateParams.profile_permalink}, ->
      if Auth.user.role.title is 'user'
        _kmq.push(['record', 'Profile Created (Step2) - Interests', {'Profile name': $scope.newProfile.name }])
      $scope.slide = "profile_picture"
    )

  $scope.submit = (form) ->
    if form.$invalid
      $scope.showErrorFields = true
    else
      $scope.showErrorFields = true
      $scope.newProfile.has_owner = String($scope.newProfile.has_owner)
      $scope.newProfile.$update({id: $stateParams.profile_permalink}, ->
        if Auth.user.role.title is 'user'
          _kmq.push(['record', 'Profile Created (Step2) - Basic Information', {'Profile name': $scope.newProfile.name }])
        $scope.slide = "interests"
      )

  init()
]
