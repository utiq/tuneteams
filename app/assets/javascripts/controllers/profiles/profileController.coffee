angular.module('tuneTeams').controller 'ProfileController'
, [ '$log',
    '$scope',
    '$http',
    '$sce',
    '$stateParams',
    '$filter'
    'SessionService',
    'ViewState'
    '$cookieStore'
    '$modal'
    '$alert'
    '$upload'
    'Auth'
    'ProfileService'
    'Genre'
    'Country'
    'Message'
    '$location'
    'Page'
    'Auction'
, ($log, $scope, $http, $sce, $stateParams, $filter, SessionService, ViewState, $cookieStore, $modal, $alert, $upload, Auth, ProfileService, Genre, Country, Message, $location, Page, Auction) ->

  # init
  # ------------------------------------------------------------
  init = ->
    ViewState.current = 'edit-profile'
    getProfile()
    setTooltips()
    $scope.uploadProgress = 0
    $scope.connectionsDropdownOptions = [
      {
        text: "Block"
        click: "changeConnectionStatus(connection.id, 'blocked')"
      }
    ]
    $scope.connectionsDropdownOptionsBlocked = [
      {
        text: "Unblock"
        click: "changeConnectionStatus(connection.id, 'active')"
      }
    ]
    $scope.banners = [
      {
        img: "/assets/banners/new-auctions-banner.gif"
        url: "/auctions"
      }
    ]
    $scope.randomBanner = $scope.banners[Math.floor(Math.random() * $scope.banners.length)]
    $scope.activeTab = 1

    # Related profiles
    $scope.recommendedProfilesCarouselIndex = 0
    $scope.recommendedAuctionsCarouselIndex = 0


  $scope.next = ->
    $scope.carouselIndex++
    console.log $scope.carouselIndex

  intro = ->
    $scope.IntroOptions = {}
    # console.log "introFinished"
    # console.log $cookieStore.get("introFinished")
    if $scope.profile.is_owner
      if $scope.profile.tour_viewed
        $scope.IntroOptions = {}
      else
        $scope.IntroOptions =
          steps: [
            {
              intro: "<strong>Congratulations!</strong>, You created your profile. Now we will take you on a short tour."
            }
            {
              element: "#cover-inner"
              intro: "In the header, you can change your location, genres and website URL."
              position: "bottom"
            }
            {
              element: "#cover-header"
              intro: "You can also upload your own cover image."
              position: "bottom"
            }
            {
              element: "#avatar"
              intro: "You can also upload your own profile image."
              position: "left"
            }
            {
              element: "#left-pane"
              intro: "Our profile sections are customized for each job type to cover the most important information. Click <strong>add</strong> or <strong>edit</strong> next to each section to fill it in."
              position: "right"
            }
          ]
          showStepNumbers: false
          exitOnOverlayClick: true
          exitOnEsc: true
          nextLabel: "<strong>NEXT!</strong>"
          prevLabel: "<span style=\"color:green\">Previous</span>"
          skipLabel: "Exit"
          doneLabel: "Thanks"

  connectionRequestModal = $modal({scope: $scope, template: '/assets/modals/connectionRequest.html', show: false})
  $scope.showModal = ->
    console.log("test")
    if Auth.isLoggedIn()
      SessionService.get(0).then (response) ->
        if response.profiles.length > 0
          $scope.newConnectionRequest = {"profile_id": $scope.profile.id, "invitation_message": "I would like to add you to my professional connections so that we can message each other via tuneteams."}
          connectionRequestModal.$promise.then(connectionRequestModal.show)
        else
          $alert({title: 'Profile required.', content: 'You need to create a profile in order to connect with others.', placement: 'top', type: 'warning', show: true, container: 'body', animation: 'am-fade-and-slide-top', duration: 5})
    else
      $alert({title: 'Login required.', content: 'You need to be logged in in order to connect with another profile.', placement: 'top', type: 'warning', show: true, container: 'body', animation: 'am-fade-and-slide-top', duration: 5})

  sendMessageModal = $modal({scope: $scope, template: '/assets/modals/sendMessage.html', show: false})
  $scope.showSendMessageModal = ->
    $scope.newMessage = {}
    sendMessageModal.$promise.then(sendMessageModal.show)

  worksAtModal = $modal({scope: $scope, template: '/assets/modals/worksAt.html', show: false})
  $scope.showWorksAtModalModal = ->
    # $scope.newMessage = {}
    worksAtModal.$promise.then(worksAtModal.show)

  claimProfileModal = $modal({scope: $scope, template: '/assets/modals/claimRequest.html', show: false})
  $scope.showClaimProfileModal = ->
    $scope.newClaimProfile = {}
    claimProfileModal.$promise.then(claimProfileModal.show)

  changeCoverImageModal = $modal({scope: $scope, template: '/assets/modals/changeCoverImage.html', show: false})
  $scope.showChangeCoverImageModal = ->
    # $scope.newClaimProfile = {}
    changeCoverImageModal.$promise.then(changeCoverImageModal.show)

  editProfileDetailsHeaderModal = $modal({scope: $scope, template: '/assets/modals/editProfileDetailsHeader.html', show: false})
  $scope.showEditProfileDetailsHeaderModal = ->
    getGenres()
    getCountries()
    editProfileDetailsHeaderModal.$promise.then(editProfileDetailsHeaderModal.show)

  profileConnectionsModal = $modal({scope: $scope, template: '/assets/modals/profileConnections.html', show: false})
  $scope.showProfileConnectionsModal = ->
    showConnections()
    $scope.profileConnections = {}
    $scope.connectionsLoading = true
    profileConnectionsModal.$promise.then(profileConnectionsModal.show)

  $scope.sendMessage = ->
    $http(
      method: "POST"
      url: "/api/v1/messages"
      data:
        profile_id: $scope.profile.id
        subject: $scope.newMessage.subject
        body: $scope.newMessage.body
    ).then ((result) ->
      $alert({title: 'Message sent!', content: 'Your message was successfully sent.', placement: 'top', type: 'success', show: true, container: 'body', animation: 'am-fade-and-slide-top', duration: 6})
      sendMessageModal.$promise.then(sendMessageModal.hide)
    ), (error) ->
      console.log error

  $scope.sendMessageConnection = (connectionId) ->
    $scope.connectionMessage = new Message()
    $scope.connectionMessage =
      profile_id: connectionId
      subject: $scope.profile.currentConnectionMessage.subject
      body: $scope.profile.currentConnectionMessage.body
    # console.log $scope.profile.currentConnectionMessage.subject
    if $scope.profile.currentConnectionMessage.subject == "" || $scope.profile.currentConnectionMessage.subject == undefined || $scope.profile.currentConnectionMessage.body == "" || $scope.profile.currentConnectionMessage.body == undefined
      $alert({title: 'Incomplete fields.', content: 'Please add a subject and body to your message.', placement: 'top', type: 'warning', show: true, container: 'body', animation: 'am-fade-and-slide-top', duration: 6})
    else
      Message.save $scope.connectionMessage, (result) ->
        $scope.profile.currentConnectionMessage = {}
        $scope.profile.currentConnectionMessage.id = 0
        $alert({title: 'Message sent!', content: 'Your message was successfully sent.', placement: 'top', type: 'success', show: true, container: 'body', animation: 'am-fade-and-slide-top', duration: 6})

  $scope.connectionRequest = ->
    $http(
      method: "POST"
      url: "/api/v1/friendships"
      data:
        friendship_request = $scope.newConnectionRequest
    ).then ((result) ->
      $scope.profile.frienship_status = "pending"
      connectionRequestModal.$promise.then(connectionRequestModal.hide)
      $alert({title: 'Connection request was sent!', content: 'Please wait while your request is confirmed.', placement: 'top', type: 'success', show: true, container: 'body', animation: 'am-fade-and-slide-top', duration: 6})
    ), (error) ->
      $scope.newConnectionRequest.errors = error.data.errors

  $scope.claimProfile = ->
    $scope.newClaimProfile.profile_id = $scope.profile.id
    $http(
      method: "POST"
      url: "/api/v1/claims"
      data:
        claim: $scope.newClaimProfile
    ).then ((result) ->
      # console.log result
      _kmq.push(['record', "Claim Profile", {'name': $scope.newClaimProfile.name, 'email': $scope.newClaimProfile.email, 'Profile Name': $scope.profile.name}])
      claimProfileModal.$promise.then(claimProfileModal.hide)
      $alert({title: 'Claim request sent!', content: 'Please wait and we will contact by email.', placement: 'top', type: 'success', show: true, container: 'body', animation: 'am-fade-and-slide-top', duration: 6})
    ), (error) ->
      $scope.newClaimProfile.errors = error.data.errors

  $scope.introOnExit = () ->
    # localStorage.setItem("introFinished", "true")
    $scope.profile.tour_viewed = true
    $scope.profile.$update({id: $stateParams.profilePermalink}, ->
      # changeCoverImageModal.$promise.then(changeCoverImageModal.hide)
    )


  $scope.trustSrc = (src) ->
    $sce.trustAsResourceUrl(src)

  $scope.onChangeAvatarSelect = ($files) ->
    i = 0
    while i < $files.length
      file = $files[i]
      $scope.upload = $upload.upload(
        url: "/api/v1/profiles/set_profile_picture"
        data:
          profile_permalink: $stateParams.profilePermalink
        file: file
      ).progress((evt) ->
        $scope.uploadProgress = parseInt(100.0 * evt.loaded / evt.total)
      ).success((data, status, headers, config) ->
        $scope.uploadProgress = 0
        $scope.profile.avatar.avatar.medium.url = data.avatar.avatar.medium.url
        $scope.profile.avatar.is_default = false
      )
      i++

  $scope.onChangeCoverSelect = ($files) ->
    i = 0
    while i < $files.length
      file = $files[i]
      $scope.upload = $upload.upload(
        url: "/api/v1/profiles/set_profile_cover"
        data:
          profile_permalink: $stateParams.profilePermalink
        file: file
      ).progress((evt) ->
        $scope.uploadProgress = parseInt(100.0 * evt.loaded / evt.total)
      ).success((data, status, headers, config) ->
        $scope.uploadProgress = 0
        $scope.profile.cover.cover = data.cover.cover
        $scope.profile.cover_type = 'customized'
        changeCoverImageModal.$promise.then(changeCoverImageModal.hide)
      )
      i++

  $scope.getCoverPicture = ->
    if $scope.profile.cover_type == 'customized'
      "url(#{$scope.profile.cover.cover.large.url})"
    else
      "url(/assets/default-cover.jpg)"

  $scope.setCoverType = (type) ->
    # $scope.profile.cover_type = type
    $scope.profile.$update({id: $stateParams.profilePermalink, cover_type: type}, ->
      changeCoverImageModal.$promise.then(changeCoverImageModal.hide)
    )

  $scope.updateName = ->
    $scope.profile.$update({id: $stateParams.profilePermalink}, ->
      $scope.profile.isEditingProfileName = false
      window.location.href = $scope.profile.permalink
    )


  getProfile = ->
    $scope.isLoading = true
    $scope.profile = ProfileService.get({id: $stateParams.profilePermalink}, ->
      Page.setTitle($scope.profile.name)
      $scope.isLoading = false
      Page.setImage("#{$location.protocol()}://#{$location.host()}/assets/default/profile-social-sharing-1.jpg")

      $scope.currentUrl = $location.absUrl()

      $scope.profile.worksAtIdSelected = undefined
      $scope.profile.addingWorkAtManually = false
      $scope.profileDetailsHeader =
        web_page: $scope.profile.web_page
        country_id: $scope.profile.country.id
      intro()

      ProfileService.recommended { 'current_profile': $stateParams.profilePermalink }, ((data) ->
        $scope.recommendedProfiles = data
        $scope.recommendedProfiles.push({name: "See all", permalink: "profiles", default_cover: { small: "/assets/backgrounds/see-all-rectangle.jpg"}})
      )

      Auction.recommended { }, ((data) ->
        console.log data
        $scope.recommendedAuctions = data
        $scope.recommendedAuctions.push({name: "See all", permalink: "", default_illustration: { small: "/assets/backgrounds/see-all-square.jpg"}})
      )

    , (response) ->
      #404 or bad
      $location.path("/404") if response.status is 404
    )

  showConnections = ->
    $scope.profile.connections = ProfileService.connections({permalink: $stateParams.profilePermalink}, ->
      $scope.connectionsLoading = false
    )

  $scope.changeConnectionStatus = (connectionId, status) ->
    $scope.profileConnection = new ProfileService()
    $scope.profileConnection.$connectionStatus({permalink: $stateParams.profilePermalink, connectionId: connectionId, status: status}, ->
      connection = $filter("filter")($scope.profile.connections, (d) ->
        d.id is connectionId
      )[0]
      connection.status = status
    )

  setTooltips = ->
    $scope.connectTooltip =
      "title": "You can only message with users who have accepted you as a connection. Click here to send a connection request to this profile."

  $scope.saveProfileDetailsHeader = ->
    $scope.profile.web_page = $scope.profileDetailsHeader.web_page
    $scope.profile.country_id = $scope.profileDetailsHeader.country_id
    $scope.profile.$update({id: $stateParams.profilePermalink}, ->
      editProfileDetailsHeaderModal.$promise.then(editProfileDetailsHeaderModal.hide)
    )

  getGenres = ->
    $scope.genresList = Genre.query()

  $scope.goToSettings = ->
    $location.path("/#{$stateParams.profilePermalink}/settings")

  getCountries = ->
    $scope.countriesList = Country.query()

  #################### Profile working at ######################
  $scope.$watch "profile.worksAtIdSelected", ->
    # TODO: Alert when you select yourself
    if $scope.profile.worksAtIdSelected != undefined
      $scope.profile.addingWorkAtManually = false
      $http(
        method: "POST"
        url: "/api/v1/profile_working_ats"
        data:
          profile_id: $scope.profile.id
          profile_working_at_id: $scope.profile.worksAtIdSelected
      ).then ((result) ->
        if $scope.profile.profile_working_at == null
          $scope.profile.profile_working_at = {}
        $scope.profile.profile_working_at.profile_permalink = result.data.profile_permalink
        $scope.profile.profile_working_at.name = result.data.name
        $scope.profile.profile_working_at.added_manually = false
        worksAtModal.$promise.then(worksAtModal.hide)
      ), (error) ->
        console.log error

  $scope.addWorkAtManually = ->
    $http(
      method: "POST"
      url: "/api/v1/profile_working_ats"
      data:
        profile_id: $scope.profile.id
        name: $scope.profile.profile_working_at.name
    ).then ((result) ->
      if $scope.profile.profile_working_at == null
        $scope.profile.profile_working_at = {}
      $scope.profile.profile_working_at.name = result.data.name
      $scope.profile.profile_working_at.added_manually = true
      $scope.profile.addingWorkAtManually = false
      worksAtModal.$promise.then(worksAtModal.hide)
    ), (error) ->
      console.log error

  $scope.showWorksAt = ->
    # console.log $scope.profile.profile_working_at
    if $scope.profile.is_executive
      return true
    else
      if ($scope.profile.profile_working_at == undefined or $scope.profile.profile_working_at == null) and $scope.profile.is_executive
        if $scope.profile.is_owner
          return true
        else
          return false

  init()
]
