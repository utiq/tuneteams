angular.module('tuneTeams').controller 'ProfileSettingsController'
, ['$log'
   '$scope'
   '$http'
   '$alert'
   '$location'
   'SessionService'
   'Auth'
   'ViewState'
   'ProfileService'
   'IndustryRoleProfile'
   '$stateParams'
   'Page'
   '$window'
, ($log, $scope, $http, $alert, $location, SessionService, Auth, ViewState, ProfileService, IndustryRoleProfile, $stateParams, Page, $window) ->

  # init
  # ------------------------------------------------------------
  init = ->
    ViewState.current = 'profile-settings'
    $scope.activeTopTab = 2
    $scope.activeTab = 'connections'
    $scope.profile = ProfileService.get({id: $stateParams.profilePermalink}, ->
      Page.setTitle("#{$scope.profile.name} | Settings")
    )

  $scope.updateShowConnectionsList = ->
    $scope.profile.show_connections_list = String(!$scope.profile.show_connections_list)
    $scope.profile.$update({id: $stateParams.profilePermalink}, ->
      # console.log $scope.profile.show_connections_list
    )

  $scope.updateWhoCanSeeConnectionList = ->
    $scope.profile.$update({id: $stateParams.profilePermalink}, ->
      # console.log $scope.profile.who_can_see_connection_list
    )

  $scope.deleteProfile = ->
    $scope.profile.$delete({id: $scope.profile.id}, ->
      # _kmq.push(['record', "Deleted Profile", {'name': $scope.profile.name }])
      $window.location.href = "/"
    )

  $scope.goToProfile = ->
    $location.path("/#{$stateParams.profilePermalink}")

  init()

]