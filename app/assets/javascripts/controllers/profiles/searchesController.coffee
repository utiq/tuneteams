angular.module('tuneTeams').controller 'SearchesController'
, [ '$log'
    '$scope'
    '$http'
    '$location'
    '$filter'
    'SessionService'
    'ViewState'
    'Page'
    'Genre'
    'Country'
    'IndustryRoleProfile'
    'Mobile'
, ($log, $scope, $http, $location, $filter, SessionService, ViewState, Page, Genre, Country, IndustryRoleProfile, Mobile) ->

  # init
  # ------------------------------------------------------------
  init = ->
    ViewState.current = 'search'
    Page.setTitle('Profiles')
    $scope.filter = {}

    $scope.searchResults = {}
    $scope.isLoading = false

    $scope.itemsPerPage = 8
    $scope.currentPage = 0
    $scope.total = 0
    $scope.defaultMessage = "Use the filters to search for profiles."

    # if $location.search().industry_role or $location.search().genre or $location.search().country or $location.search().deal_type or $location.search().query or $location.search().q
    #   $scope.getResults(false)

    getIndustryRoleProfiles()
    getGenres()
    getCountries()

    if $location.search().genre
      $scope.filter.genre_ids = $location.search().genre.split(',')

    if $location.search().country
      $scope.filter.country_ids = $location.search().country.split(',')

    $log.info "industry_role", $location.search().industry_role
    if $location.search().industry_role
      $scope.filter.industryRoleProfileId = $location.search().industry_role
    $log.info $scope.filter.industryRoleProfileId

    if $location.search().deal_type
      $scope.filter.dealType = $location.search().deal_type.split(',')

    if $location.search().name
      $scope.filter.name = $location.search().name

    $scope.getResults(false)

  getIndustryRoleProfiles = ->
    $scope.industryRoleProfiles = IndustryRoleProfile.query()

  getGenres = ->
    if !Mobile.isMobile()
      $scope.genresList = Genre.query()
    else
      $scope.genresList = Genre.query(order_by: 'name ASC')

  getCountries = ->
    $scope.countriesList = Country.query()

  $scope.getDealTypes = ->
    console.log "getDealTypes"
    $scope.filter.industryRoleProfileDealTypes = IndustryRoleProfile.interests({id: $scope.filter.industryRoleProfileId}, ->
      # console.log $scope.industryRoleProfileDealTypes
    )

  $scope.search = ->
    location = $location.path("/profiles").search('q', $scope.formSearch.query)

    # Genre parameters
    if $scope.formSearch.genre
      location.search('genre', $scope.formSearch.genreName)
    else
      $location.search('genre', null)

    # Country parameters
    if $scope.formSearch.country
      location.search('country', $scope.formSearch.countryName)
    else
      $location.search('country', null)

    # Industry Role parameters
    if $scope.formSearch.industryRole
      location.search('industry_role', $scope.formSearch.industryRoleName)
    else
      $location.search('industry_role', null)
      $scope.formSearch.dealType = false

    # Industry Role parameters
    if $scope.formSearch.dealType and $scope.formSearch.industryRole
      location.search('deal_type', $scope.formSearch.industryRoleProfileDealTypeName)
    else
      $location.search('deal_type', null)

    # $scope.getResults(false)

  $scope.getResults = (loadMore) ->
    $scope.isLoading = true
    if loadMore
      $scope.currentPage++
    else
      $scope.currentPage = 0

    params = {
      offset: $scope.currentPage*$scope.itemsPerPage,
      limit: $scope.itemsPerPage
    }

    params.name = $scope.filter.name if $scope.filter.name != undefined and $scope.filter.name != ''
    params.country = $scope.filter.country_ids.join(',') if $scope.filter.country_ids != undefined and $scope.filter.country_ids.length > 0
    params.genre = $scope.filter.genre_ids.join(',') if $scope.filter.genre_ids != undefined and $scope.filter.genre_ids.length > 0
    params.industry_role = $scope.filter.industryRoleProfileId
    params.deal_type = $scope.filter.dealType.join(',') if $scope.filter.dealType != undefined and $scope.filter.dealType.length > 0

    $location.search('name', params.name)
    $location.search('country', params.country)
    $location.search('genre', params.genre)
    $location.search('industry_role', params.industry_role)
    $location.search('deal_type', params.deal_type)

    $http.get("/api/v1/profiles/search",
      params: params
    ).success (data, status) ->
      $scope.total = data.total
      if loadMore
        $scope.searchResults = $scope.searchResults.concat(data.results);
      else
        $scope.searchResults = data.results
      if data.total == 0
        $scope.defaultMessage = "There are no results with your search criteria."
      $scope.isLoading = false

    # $log.log $scope.pageCount()
    # $log.log $scope.currentPage
    # $log.log $scope.nextPageDisabledClass()

  $scope.nextPageDisabledClass = ->
    (if $scope.currentPage is $scope.pageCount() - 1 or $scope.isLoading or $scope.total == 0 then "hide" else "")

  $scope.pageCount = ->
    Math.ceil($scope.total/$scope.itemsPerPage)

  $scope.showDropdownFilter = (filter) ->
    # console.log filter
    switch filter
      when "country" then $scope.showCountryFilter = true; $scope.showGenreFilter = false; $scope.showDealTypeFilter = false; $scope.showIndustryRoleFilter = false
      when "genre" then $scope.showCountryFilter = false; $scope.showGenreFilter = true; $scope.showDealTypeFilter = false; $scope.showIndustryRoleFilter = false
      when "industry_role" then $scope.showCountryFilter = false; $scope.showGenreFilter = false; $scope.showDealTypeFilter = false; $scope.showIndustryRoleFilter = true
      when "deal_type" then $scope.showCountryFilter = false; $scope.showGenreFilter = false; $scope.showIndustryRoleFilter = false; $scope.showDealTypeFilter = true

  $scope.clearFilters = ->
    $scope.showCountryFilter = false
    $scope.showGenreFilter = false
    $scope.showIndustryRoleFilter = false
    $scope.showDealTypeFilter = false
    $scope.filter.country_ids = []
    $scope.filter.genre_ids = []
    $scope.filter.industryRoleProfileId = null
    $scope.filter.dealType = []
    $scope.filter.name = ''
    $scope.getResults(false)

  $scope.$watch 'filter.industryRoleProfileId', ->
    $scope.filter.dealType = []
    $scope.getDealTypes()

  $scope.$watch 'filter.name', (value) ->
    $scope.getResults(false)
    # $location.search('name', value)

  init()
]
