angular.module('tuneTeams').controller 'LoginController'
, ['$log'
   '$scope'
   '$location'
   'SessionService'
   'Auth'
   'ViewState'
   '$window'
   'Page'
   '$rootScope'
   '$route'
, ($log, $scope, $location, SessionService, Auth, ViewState, $window, Page, $rootScope, $route) ->

  # init
  # ------------------------------------------------------------
  init = ->
    # if Auth.isLoggedIn()
    #   if Auth.user.default_profile != ""
    #     $window.location.href = "/#{Auth.user.default_profile}"
    #   else
    #     $window.location.href = "/profile/create"

    ViewState.current = 'login'
    Page.setTitle('Login')

  # login
  # ------------------------------------------------------------
  $scope.rememberme = true
  $scope.submit = ->
    Auth.login
      user: $scope.user
      # rememberme: $scope.rememberme
    , ((res) ->
      # $location.path "/"
      _kmq.push(['identify', "User-#{res.user.id}"])
      _kmq.push(['set', {'email': res.user.email}])
      _kmq.push(['record', 'Logged In']);
      # TODO: change the way of redirection
      $scope.$parent.is_logged = true

      if $rootScope.lastLocationPath == null or $rootScope.lastLocationPath == undefined or $rootScope.lastLocationPath == '/'
        $window.location.href = "/#{res.user.default_profile}"
      else
        $location.path($rootScope.lastLocationPath)
        $route.reload()
        # $window.location.href = $rootScope.lastLocationPath
      # if res.user.default_profile != ""
      #   $window.location.href = "/#{res.user.default_profile}"
      # else
      #   $window.location.href = "/profile/create"

    ), (err) ->
      # $rootScope.error = "Failed to login"
      $scope.user.errors = err.errors
      console.log err

  $scope.loginOauth = (provider) ->
    $window.location.href = "/auth/" + provider

  init()

]
