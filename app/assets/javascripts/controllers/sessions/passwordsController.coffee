angular.module('tuneTeams').controller 'PasswordsController'
, ['$log',
   '$scope',
   '$location',
   '$http'
   'SessionService',
   'Auth'
   'ViewState'
   '$alert'
   '$stateParams'
   'Page'
, ($log, $scope, $location, $http, SessionService, Auth, ViewState, $alert, $stateParams, Page) ->

  # init
  # ------------------------------------------------------------
  init = ->
    Page.setTitle('Password recovery')
    if Auth.isLoggedIn()
      $alert({title: 'You are already logged in.', content: "If you want to change your password go to the menu: Account -> Settings", placement: 'top', type: 'warning', show: true, container: 'body', animation: 'am-fade-and-slide-top', duration: 6})
      $location.path "/"

  $scope.recoverPasswordRequest = ->
    $http(
      method: "POST"
      url: "/api/v1/users/recover_password"
      data:
        email: $scope.recoverPassword.email
    ).then ((result) ->
      $alert({title: 'Password reset request was sent.', content: "An email was sent to #{$scope.recoverPassword.email} to recover your password.", placement: 'top', type: 'success', show: true, container: 'body', animation: 'am-fade-and-slide-top', duration: 6})
      $scope.recoverPassword = {}
    ), (error) ->
      $scope.recoverPassword.errors = error.data.errors

  $scope.resetPasswordRequest = ->
    $http(
      method: "PUT"
      url: "/api/v1/user_password_resets/#{$stateParams.temporaryToken}"
      data:
        user:
          password: $scope.resetPassword.newPassword
    ).then ((result) ->
      # $alert({title: 'Password changed.', content: 'Now you can login with your new password.', placement: 'top', type: 'success', show: true, container: 'body', animation: 'am-fade-and-slide-top', duration: 6})
      # $location.path "login"
      Auth.login
        user:
          email: result.data.email
          password: $scope.resetPassword.newPassword
      , ((res) ->
        $alert({title: 'Password changed.', content: 'Your password was successfully updated.', placement: 'top', type: 'success', show: true, container: 'body', animation: 'am-fade-and-slide-top', duration: 6})
        $scope.resetPassword = {}
        $location.path "/"
        # $scope.$parent.is_logged = true
      ), (err) ->
        $alert({title: 'Error.', content: 'Internal server error, please try later', placement: 'top', type: 'danger', show: true, container: 'body', animation: 'am-fade-and-slide-top', duration: 6})
    ), (error) ->
      # $scope.resetPassword.errors = error.data.errors
      $alert({title: 'Error trying to reset your password.', content: error.data.errors.password, placement: 'top', type: 'danger', show: true, container: 'body', animation: 'am-fade-and-slide-top', duration: 6})

  init()

]
