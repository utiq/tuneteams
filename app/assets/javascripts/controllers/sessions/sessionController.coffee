angular.module('tuneTeams').controller 'SessionController'
, ['$log'
   '$scope'
   '$location'
   'SessionService'
   'Auth'
   'ViewState'
   '$window'
   'Page'
   '$stateParams'
   '$http'
, ($log, $scope, $location, SessionService, Auth, ViewState, $window, Page, $stateParams, $http) ->

  # init
  # ------------------------------------------------------------
  init = ->
    Page.setTitle('Confirm Email')
    confirmEmail()

  confirmEmail = ->
    $http(
      method: "POST"
      url: "/api/v1/users/confirm_email"
      data:
        email: $stateParams.email
        token: $stateParams.temporaryToken
    ).then ((result) ->
      # $alert({title: '', content: "Your email was confirmed.", placement: 'top', type: 'success', show: true, container: 'body', animation: 'am-fade-and-slide-top', duration: 6})
    ), (error) ->
      $scope.recoverPassword.errors = error.data.errors

  init()

]
