angular.module('tuneTeams').controller 'SignupController'
, ['$log',
   '$scope',
   '$location',
   'SessionService',
   'Auth'
   'ViewState'
   '$http'
   'Page'
   '$window'
, ($log, $scope, $location, SessionService, Auth, ViewState, $http, Page, $window) ->

  # init
  # ------------------------------------------------------------
  init = ->
    # $scope.user = SessionService.getCurrentUser()
    if Auth.isLoggedIn()
      $location.path "/"

    ViewState.current = 'signup'
    Page.setTitle('Sign up')
    $scope.newUser = {}
    $scope.newUser.invitationToken = $location.search().token
    $scope.newUser.email = $location.search().email
    $scope.newUser.source = $location.search().source

  $scope.submit = ->
    Auth.register
      user: $scope.newUser
    , ((res) ->
      _kmq.push(['identify', "User-#{res.user.id}"])
      _kmq.push(['set', {'email': res.user.email}])

      if $scope.newUser.source == "claim"
        _kmq.push(['record', 'Signed Up', {'source':'claim'}])
        # $location.path "/#{$location.search().profile}"
        $window.location.href = "/#{$location.search().profile}"
      else
        _kmq.push(['record', 'Signed Up', {'source':'invitation'}])
        $location.url("/profile/create")

    ), (err) ->
      console.log err.errors
      $scope.newUser.errors = err.errors

  $scope.loginOauth = (provider) ->
    $window.location.href = "/auth/" + provider
    return

  init()

]
