angular.module('tuneTeams').controller 'StaticPagesController'
, ['$log'
   '$scope'
   'ViewState'
, ($log, $scope, ViewState) ->

  # init
  # ------------------------------------------------------------
  init = ->
    ViewState.current = 'static_page'

  init()

]