angular.module('tuneTeams').controller 'SubscriberController', [ '$log',
    '$scope',
    '$http',
    'SubscriberService',
    'ViewState'
, ($log, $scope, $http, SubscriberService, ViewState) ->

  # init
  # ------------------------------------------------------------
  init = ->
    $scope.viewState = ViewState
    getIndustryRoles()
    # $scope.token = true

  $scope.addSubscriber = ->
    # subscriber = SubscriberService.save($scope.newSubscriber)
    # $scope.token = subscriber.token
    # console.log subscriber
    # serializedData = $.param({subscriber : {email:"lol@goo.com"}});
    $http(
      method: "POST"
      url: "/subscribers"
      data:
        $.param(subscriber: $scope.newSubscriber)
      headers:
        "Content-Type": "application/x-www-form-urlencoded; charset=UTF-8"
    ).then ((result) ->
      $scope.token = result.data.token
      console.log result
    ), (error) ->
      console.log "error"
      console.log error.data.errors
      message = "Error trying to subscribe: #{JSON.stringify(error.data.errors)})"
      alert message

  getIndustryRoles = ->
    $http(
      method: "GET"
      url: "/api/v1/industry_role_profiles"
      data: {}
      headers:
        "Content-Type": "application/x-www-form-urlencoded; charset=UTF-8"
    ).then ((result) ->
      $scope.industry_role_profiles = result.data
      console.log result.data
    ), (error) ->
      console.log error


  init()
]