angular.module('tuneTeams').directive 'addthisToolbox', [
  '$timeout', 'Page'
  ($timeout, Page) ->
    {
      restrict: 'A'
      transclude: true
      replace: true
      template: '<div ng-transclude></div>'
      link: ($scope, element, attrs) ->
        $timeout ->
          addthis.init()
          addthis.toolbox $(element).get(), {},
            url: attrs.url
            title: attrs.title
            description: Page.description()
          angular.element(".addthis-toolbox-container").css("display", "block")
    }
]
