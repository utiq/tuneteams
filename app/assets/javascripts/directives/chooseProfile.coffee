angular.module("tuneTeams").directive "facade", [->

  link: (scope, element, attrs) ->
    element.bind "click", ->
      angular.element(".facade").css("top", "3px")
      angular.element(".form-container").css("top", "265px").css("z-index", 1)

      element.css("top", "-265px")
      element.parent().find(".form-container").css("top", "3px").css("z-index", 2)

  restrict: 'C'
]
