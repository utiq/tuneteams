angular.module("tuneTeams").directive "discography", [->
  replace: true
  restrict: 'E'
  templateUrl: '/assets/directives/discography.html'
  scope:
    discographies : '='
    widgetId: '@'
    title: '@'
    showEditOption: '='
    options: '='
    helpText: '='

  link: (scope, element, attrs) ->
    scope.isEditing = false

  controller: [
    "$scope"
    "$http"
    '$sce'
    ($scope, $http, $sce) ->
      $scope.finish = ->
        isValid = true
        if $scope.discographies != undefined
          angular.forEach $scope.discographies, (value, key) ->
            if value.name == null or value.name == '' or value.external_url == null or value.external_url == ''
              $scope.discographies.errors =
                0: "You have a field in blank. Please fill it with a valid Song title, URI or remove it"
              isValid = false
              return
            else
              $scope.discographies.errors = []

        if isValid
          $scope.isEditing = false

      $scope.add = () ->
        $http(
          method: "POST"
          url: "/api/v1/widgets_content"
          data:
            widget_id: $scope.widgetId
            type: "discography"
        ).then ((result) ->
          if $scope.discographies is undefined
            $scope.discographies = []
          $scope.discographies.push(result.data.discography)
        ), (error) ->
          console.log error

      $scope.remove = (index) ->
        discographyToDelete = $scope.discographies[index];
        $http(
          method: "DELETE"
          url: "/api/v1/widgets_content/#{discographyToDelete._id}?type=discography&widget_id=#{$scope.widgetId}"
        ).then ((result) ->
          $scope.discographies.errors = []
          $scope.discographies.splice(index, 1);
        ), (error) ->
          console.log error

      $scope.update = (discography) ->
        console.log discography
        $http(
          method: "PUT"
          url: "/api/v1/widgets_content/#{discography._id}"
          data:
            external_url: discography.external_url
            name: discography.name
            type: "discography"
            widget_id: $scope.widgetId
        ).then ((result) ->
        ), (error) ->
          console.log error

      $scope.editButtonText = ->
        if $scope.discographies is undefined
          'Add'
        else
          if $scope.discographies.length == 0
            'Add'
          else
            'Edit'

      $scope.trustSrc = (src) ->
        $sce.trustAsResourceUrl(src)
  ]

]
