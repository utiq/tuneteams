angular.module("tuneTeams").directive "interest", [->
  replace: true
  restrict: 'E'
  templateUrl: '/assets/directives/interest.html'
  scope:
    interests: '='
    widgetId: '@'
    title: '@'
    showEditOption: '='
    helpText: '='
    industryRoleProfileId: '@'

  link: (scope, element, attrs) ->
    scope.isEditing = false

  controller: [
    "$scope"
    "$http"
    'Country'
    'IndustryRoleProfile'
    ($scope, $http, Country, IndustryRoleProfile) ->
      $scope.update = (interest) ->
        $http(
          method: "PUT"
          url: "/api/v1/widgets_content/#{interest._id}"
          data:
            widget_id: $scope.widgetId
            countries: $scope.interests.countries || []
            interests: $scope.interests.interests || []
            industry_role_profile_id: $scope.industryRoleProfileId
            type: 'interest'
        ).then ((result) ->
          $scope.interests.interest_names = result.data.interest_names
          $scope.interests.country_names = result.data.country_names
          $scope.isEditing = false
        ), (error) ->
          console.log error

      $scope.$watch "isEditing", ->
        $scope.countriesList = Country.query(
          show_all: true
        )
        $scope.interestsList = IndustryRoleProfile.interests({id: $scope.industryRoleProfileId})
  ]

]
