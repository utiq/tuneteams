angular.module("tuneTeams").directive "isNumber", [->

  restrict: 'A'
  link: (scope, element, attrs) ->

    keyCode = [8, 9, 46, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 96]
    element.bind 'keypress', (event) ->
      # console.log $.inArray(event.which, keyCode)
      console.log event.which
      if $.inArray(event.which, keyCode) == -1
        scope.$apply ->
          scope.$eval attrs.onlyNum
          event.preventDefault()
          return
        event.preventDefault()
]
