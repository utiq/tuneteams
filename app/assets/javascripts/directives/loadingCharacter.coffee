angular.module("tuneTeams").directive "loadingCharacter", [->
  # replace: true
  restrict: 'E'
  template: '<div class="loading-character"><div class="loading-image {{size}}"></div><div class="text-container">{{text}}</div></div>'
  scope:
    text: '@'
    size: '@'

  # link: (scope, element, attrs) ->
  #   console.log "lolas pedos"
  #   console.log scope
  #   # scope.isEditing = false
  #   # scope.newMemberManual =
  #   #   name: null
  #   #   role: null

]
