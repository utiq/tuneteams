angular.module("tuneTeams").directive "memberPeople", [->
  replace: true
  restrict: 'E'
  templateUrl: '/assets/directives/memberPeople.html'
  scope:
    members: '='
    profileIdSelected : '@'
    widgetId: '@'
    title: '@'
    showEditOption: '='
    options: '='
    helpText: '='

  link: (scope, element, attrs) ->
    scope.isEditing = false
    scope.newMemberManual =
      name: null
      role: null

  controller: [
    '$scope'
    '$http'
    '$sce'
    '$upload'
    ($scope, $http, $sce, $upload) ->

      if $scope.members != undefined
        if $scope.members.length > 8
          $scope.membersListHight = "less"

      $scope.finish = ->
        $scope.isEditing = false
        $scope.newMemberManual.errors = null

      $scope.add = (profileId) ->
        $http(
          method: "POST"
          url: "/api/v1/widgets_content"
          data:
            widget_id: $scope.widgetId
            profile_id: profileId
            type: "people"
        ).then ((result) ->
          if $scope.members is undefined
            $scope.members = []
          $scope.members.push(result.data.member_people)
        ), (error) ->
          console.log error

      $scope.edit = (index) ->
        $scope.uploadProgress = 0
        $scope.editMember = $scope.members[index]

      $scope.cancelEditing = ->
        $scope.editMember = [];
        $scope.addingManually = false;

      $scope.update = (index) ->
        if $scope.editMember.name == null or $scope.editMember.name == ''
          $scope.editMember.errors =
            0: "Please enter a name."
          return
        if ($scope.editMember.role == null or $scope.editMember.role == '') and $scope.options.requiredRole == true
          $scope.editMember.errors =
            0: "Please enter a role."
          return

        if $scope.file == null and $scope.options.requiredPhoto == true
          $scope.editMember.errors =
            0: "Please select an avatar to upload."
          return
        else
          $scope.editMember.errors = []
          if $scope.file == null
            $http(
              method: "PUT"
              url: "/api/v1/widgets_content/#{$scope.editMember._id}"
              data:
                widget_id: $scope.widgetId
                name: $scope.editMember.name
                role: $scope.editMember.role
                type: "people"
            ).then ((result) ->
              console.log result
              $scope.editMember = undefined
              # if $scope.members is undefined
              #   $scope.members = []
              # $scope.members.push(result.data.member_people)
            ), (error) ->
              console.log error
          else
            file = $scope.file
            $scope.upload = $upload.upload(
              url: "/api/v1/widgets_content/#{$scope.editMember._id}"
              method: "PUT"
              data:
                widget_id: $scope.widgetId
                name: $scope.editMember.name
                role: $scope.editMember.role
                type: "people"
              file: file
            ).progress((evt) ->
              $scope.uploadProgress = parseInt(100.0 * evt.loaded / evt.total)
              console.log $scope.uploadProgress
            ).success((data, status, headers, config) ->
              $scope.file = null
              $scope.uploadProgress = 0
              # if $scope.members is undefined
              #   $scope.members = []
              # $scope.members.push(data.member_people)
              # $scope.addingManually = false
              $scope.members[index] = data.people
              console.log data
              $scope.editMember = undefined

            )


      $scope.showAddManually = ->
        $scope.file = null
        $scope.uploadProgress = 0
        $scope.addingManually = true
        $scope.newMemberManual =
          name: null
          role: null

      $scope.addManually = ->
        if $scope.newMemberManual.name == null or $scope.newMemberManual.name == ''
          $scope.newMemberManual.errors =
            0: "Please enter a name."
          return
        if ($scope.newMemberManual.role == null or $scope.newMemberManual.role == '') and $scope.options.requiredRole == true
          $scope.newMemberManual.errors =
            0: "Please enter a role."
          return

        if $scope.file == null and $scope.options.requiredPhoto == true
          $scope.newMemberManual.errors =
            0: "Please select an avatar to upload for your new member."
          return
        else
          $scope.newMemberManual.errors = []
          if $scope.file == null
            $http(
              method: "POST"
              url: "/api/v1/widgets_content"
              data:
                widget_id: $scope.widgetId
                name: $scope.newMemberManual.name
                role: $scope.newMemberManual.role
                type: "people"
            ).then ((result) ->
              if $scope.members is undefined
                $scope.members = []
              $scope.members.push(result.data.member_people)
              $scope.addingManually = false
            ), (error) ->
              # TODO: Show the error message at the top
              console.log error
              $scope.addingManually = false
          else
            file = $scope.file
            $scope.upload = $upload.upload(
              url: "/api/v1/widgets_content"
              data:
                widget_id: $scope.widgetId
                name: $scope.newMemberManual.name
                role: $scope.newMemberManual.role
                type: "people"
              file: file
            ).progress((evt) ->
              $scope.uploadProgress = parseInt(100.0 * evt.loaded / evt.total)
            ).success((data, status, headers, config) ->
              $scope.file = null
              $scope.uploadProgress = 0
              if $scope.members is undefined
                $scope.members = []
              $scope.members.push(data.member_people)
              $scope.addingManually = false

            )

      $scope.onFileSelect = ($files) ->
        # console.log "selected"
        i = 0
        while i < $files.length
          $scope.file = $files[i]
          i++


      $scope.remove = (index) ->
        personToDelete = $scope.members[index]
        $http(
          method: "DELETE"
          url: "/api/v1/widgets_content/#{personToDelete._id}?type=people&widget_id=#{$scope.widgetId}"
          data: {}
        ).then ((result) ->
          $scope.members.splice(index, 1);
        ), (error) ->
          console.log error

      $scope.editButtonText = ->
        if $scope.members is undefined
          'Add'
        else
          if $scope.members.length == 0
            'Add'
          else
            'Edit'

      $scope.trustSrc = (src) ->
        $sce.trustAsResourceUrl(src)

      $scope.$watch "profileIdSelected", ->
        if $scope.profileIdSelected != undefined
          $scope.add($scope.profileIdSelected)
  ]

]
