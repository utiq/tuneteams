angular.module("tuneTeams").directive "onlyDigits", [->

  restrict: 'A'
  require: '?ngModel'
  link: (scope, element, attrs, ngModel) ->
    if !ngModel
      return
    ngModel.$parsers.unshift (inputValue) ->
      digits = inputValue.split('').filter((s) ->
        !isNaN(s) and s != ' '
      ).join('')
      ngModel.$viewValue = digits
      ngModel.$render()
      return digits
]
