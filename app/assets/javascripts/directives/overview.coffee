angular.module("tuneTeams").directive "overview", [->
  replace: true
  restrict: 'E'
  templateUrl: '/assets/directives/overview.html'
  scope:
    overview: '='
    widgetId: '@'
    title: '@'
    showEditOption: '='
    helpText: '='

  link: (scope, element, attrs) ->
    scope.isEditing = false
    scope.customMenu = [['bold', 'italic', 'underline', 'strikethrough']]

  controller: [
    "$scope"
    "$http"
    ($scope, $http) ->

      if $scope.overview.description != null
        if $scope.overview.description.length > 1300
          $scope.descriptionTextHight = "less"

      $scope.update = (overview) ->
        $http(
          method: "PUT"
          url: "/api/v1/widgets_content/#{overview._id}"
          data:
            widget_id: $scope.widgetId
            description: overview.description
            type: 'overview'
        ).then ((result) ->
          $scope.overview.description = result.data.overview.description
          $scope.isEditing = false
        ), (error) ->
          console.log error
  ]

]
