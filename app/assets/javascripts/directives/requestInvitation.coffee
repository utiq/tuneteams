angular.module("tuneTeams").directive "requestInvitation", [->
  replace: true
  restrict: 'E'
  template: "<button class=\"btn btn-primary {{size}}\" ng-click=\"showInvitationRequestModal()\">Request an invite</button>"
  scope:
    size : '@'
    id: '@'

  link: (scope, element, attrs) ->
    scope.isEditing = false

  controller: [
    '$scope'
    '$http'
    '$sce'
    '$modal'
    '$alert'
    '$state'
    'IndustryRoleProfile'
    ($scope, $http, $sce, $modal, $alert, $state, IndustryRoleProfile) ->

      $scope.industryRoleProfiles = IndustryRoleProfile.query()
      invitationRequestModal = $modal({scope: $scope, template: '/assets/modals/invitationRequest.html', show: false})

      $scope.showInvitationRequestModal = ->
        _kmq.push(['record', "Request Invitation Button Clicked", {'source': $state.current.name, 'id': $scope.id}])
        $scope.newInvitation = {}
        invitationRequestModal.$promise.then(invitationRequestModal.show)

      $scope.sendInvitationRequest = ->
        $http(
          method: "POST"
          url: "/api/v1/invitations"
          data:
            invitation: $scope.newInvitation
        ).then ((result) ->
          _kmq.push(['record', "Request Invitation", {'source': 'Home', 'email': $scope.newInvitation.email}])
          invitationRequestModal.$promise.then(invitationRequestModal.hide)
          $alert({title: 'Invitation sent!', content: 'Please wait and we will send you an email with the invitation link.', placement: 'top', type: 'success', show: true, container: 'body', animation: 'am-fade-and-slide-top', duration: 6})
        ), (error) ->
          $scope.newInvitation.errors = error.data.errors
  ]

]
