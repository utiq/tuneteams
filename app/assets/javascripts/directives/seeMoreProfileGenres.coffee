angular.module("tuneTeams").directive "SeeMoreProfileGenres", [->

  link: (scope, element, attrs) ->

    element.bind "click", ->
      console.log element.attr("data-is-collapsed")
      if element.attr("data-is-collapsed") == "true"
        element.text("See less").attr("data-is-collapsed", "false")
        angular.element(".name-container").toggleClass("see_more")
        angular.element(".collapsed").hide()
        angular.element(".not-collapsed").show()
      else
        element.text("See more").attr("data-is-collapsed", "true")
        angular.element(".name-container").removeClass("see_more")
        angular.element(".collapsed").show()
        angular.element(".not-collapsed").hide()

  restrict: 'C'
]
