angular.module("tuneTeams").directive "spotifyMusic", [->
  replace: true
  restrict: 'E'
  templateUrl: '/assets/directives/spotifyMusic.html'
  scope:
    links : '='
    widgetId: '@'
    title: '@'
    showEditOption: '='
    helpText: '='

  link: (scope, element, attrs) ->
    scope.isEditing = false

  controller: [
    "$scope"
    "$http"
    '$sce'
    ($scope, $http, $sce) ->
      $scope.save = ->
        isValid = true
        if $scope.links != undefined
          angular.forEach $scope.links, (value, key) ->
            if value.url == null or value.url == ''
              $scope.links.errors =
                0: "You have a field in blank. Please fill it with a valid Spotify URI or remove it"
              isValid = false
              return
            else
              $scope.links.errors = []

        if isValid
          $scope.isEditing = false

      $scope.add = () ->
        if $scope.links == undefined or $scope.links.length < 10
          $http(
            method: "POST"
            url: "/api/v1/widgets_content"
            data:
              widget_id: $scope.widgetId
              type: "spotify_music"
          ).then ((result) ->
            if $scope.links is undefined
              $scope.links = []
            $scope.links.push(result.data.spotify_music)
          ), (error) ->
            console.log error
        else
          $scope.links.errors =
            0: "You can not add more than 10 players"

      $scope.remove = (index) ->
        person_to_delete = $scope.links[index];
        $http(
          method: "DELETE"
          url: "/api/v1/widgets_content/#{person_to_delete._id}?type=spotify_music&widget_id=#{$scope.widgetId}"
          data: {}
        ).then ((result) ->
          $scope.links.errors = []
          $scope.links.splice(index, 1);
        ), (error) ->
          console.log error

      $scope.update = (link) ->
        console.log link.url
        $http(
          method: "PUT"
          url: "/api/v1/widgets_content/#{link._id}"
          data:
            widget_id: $scope.widgetId
            type: "spotify_music"
            url: link.url
        ).then ((result) ->
          # console.log result.data
        ), (error) ->
          console.log error

      $scope.editButtonText = ->
        if $scope.links is undefined
          'Add'
        else
          if $scope.links.length == 0
            'Add'
          else
            'Edit'

      $scope.trustSrc = (src) ->
        $sce.trustAsResourceUrl(src)
  ]

]
