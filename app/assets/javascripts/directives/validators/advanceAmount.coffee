angular.module('tuneTeams').directive 'advanceAmount', [
  '$log'
  ($log) ->
    {
      restrict: 'A'
      require: 'ngModel'
      link: (scope, element, attrs, ngModel) ->

        maximumPercent = 99.99
        minimumAmount = 250
        validate = (value) ->
          if scope.auction.auction_type == "recording_rights" || scope.auction.auction_type == "publishing_rights" || scope.auction.auction_type == "sub_publishing_rights"
            ngModel.$setValidity 'minimumAdvanceZero', true

            if scope.auction.competitive_term == "advance"
              ngModel.$setValidity 'minimumAdvance', true
              if value > maximumPercent
                ngModel.$setValidity 'maximumAdvance', false
              else
                ngModel.$setValidity 'maximumAdvance', true


              if value <= 0
                ngModel.$setValidity 'minimumAdvanceZero', false
              else
                ngModel.$setValidity 'minimumAdvanceZero', true
            if scope.auction.competitive_term == "royalty_rate"
              ngModel.$setValidity 'maximumAdvance', true
              if value < minimumAmount
                ngModel.$setValidity 'minimumAdvance', false
              else
                ngModel.$setValidity 'minimumAdvance', true

          else if scope.auction.auction_type == "recording_publishing_rights"
            if value > maximumPercent
              ngModel.$setValidity 'maximumAdvance', false
            else
              ngModel.$setValidity 'maximumAdvance', true
            if value <= 0
              ngModel.$setValidity 'minimumAdvanceZero', false
            else
              ngModel.$setValidity 'minimumAdvanceZero', true

        scope.$watch (->
          ngModel.$viewValue
        ), validate

        scope.$watch 'auction.competitive_term', ->
          validate(ngModel.$viewValue)

        scope.$watch 'auction.auction_type', ->
          validate(ngModel.$viewValue)
    }
]
