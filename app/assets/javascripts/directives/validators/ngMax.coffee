angular.module('tuneTeams').directive 'ngMax', [
  '$log'
  ($log) ->
    {
      restrict: 'A'
      require: 'ngModel'
      link: (scope, element, attr, ctrl) ->
        scope.$watch attr.ngMax, ->
        ctrl.$setViewValue ctrl.$viewValue

        isEmpty = (value) ->
          angular.isUndefined(value) or value == '' or value == null or value != value

        maxValidator = (value) ->
          max = scope.$eval(attr.ngMax) or Infinity
          if !isEmpty(value) and value > max
            ctrl.$setValidity 'ngMax', false
            undefined
          else
            ctrl.$setValidity 'ngMax', true
            value

        ctrl.$parsers.push maxValidator
        ctrl.$formatters.push maxValidator

      }
]