angular.module('tuneTeams').directive 'ngMin', [
  '$log'
  ($log) ->
    {
      restrict: 'A'
      require: 'ngModel'
      link: (scope, element, attr, ctrl) ->
        scope.$watch attr.ngMin, ->
        ctrl.$setViewValue ctrl.$viewValue

        isEmpty = (value) ->
          angular.isUndefined(value) or value == '' or value == null or value != value

        minValidator = (value) ->
          min = scope.$eval(attr.ngMin) or 0
          if !isEmpty(value) and value < min
            ctrl.$setValidity 'ngMin', false
            undefined
          else
            ctrl.$setValidity 'ngMin', true
            value

        ctrl.$parsers.push minValidator
        ctrl.$formatters.push minValidator

      }
]