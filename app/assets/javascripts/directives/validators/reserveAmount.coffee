angular.module('tuneTeams').directive 'reserveAmount', [
  '$log'
  ($log) ->
    {
      restrict: 'A'
      require: 'ngModel'
      link: (scope, element, attrs, ngModel) ->
        
        maximumPercent = 99.99
        minimumAmount = 250
        validate = (value) ->
          if scope.auction.auction_type == "recording_rights" || scope.auction.auction_type == "publishing_rights" || scope.auction.auction_type == "sub_publishing_rights"
            ngModel.$setValidity 'minimumReserveZero', true

            if scope.auction.competitive_term == "royalty_rate"
              ngModel.$setValidity 'minimumReserve', true
              if value > maximumPercent
                ngModel.$setValidity 'maximumReserve', false
              else
                ngModel.$setValidity 'maximumReserve', true

              console.log (value <= 0)
              if value <= 0 || value == null || value == undefined
                ngModel.$setValidity 'minimumReserveZero', false
              else
                ngModel.$setValidity 'minimumReserveZero', true
            if scope.auction.competitive_term == "advance"
              ngModel.$setValidity 'maximumReserve', true
              if value < minimumAmount
                ngModel.$setValidity 'minimumReserve', false
              else
                ngModel.$setValidity 'minimumReserve', true

          else if scope.auction.auction_type == "recording_publishing_rights"
            if value < minimumAmount
              ngModel.$setValidity 'minimumReserve', false
            else
              ngModel.$setValidity 'minimumReserve', true

        scope.$watch (->
          ngModel.$viewValue
        ), validate

        scope.$watch 'auction.competitive_term', ->
          validate(ngModel.$viewValue)

        scope.$watch 'auction.auction_type', ->
          validate(ngModel.$viewValue)
    }
]