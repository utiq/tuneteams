angular.module("tuneTeams").directive "youtubeVideos", [->
  replace: true
  restrict: 'E'
  templateUrl: '/assets/directives/youtubeVideos.html'
  scope:
    youtubeVideos : '='
    widgetId: '@'
    title: '@'
    showEditOption: '='
    helpText: '='
    apiController: '@'

  link: (scope, element, attrs) ->
    scope.isEditing = false

  controller: [
    '$scope'
    '$http'
    '$sce'
    '$log'
    '$alert'
    ($scope, $http, $sce, $log, $alert) ->
      $scope.save = () ->
        isValid = true
        if $scope.youtubeVideos != undefined
          angular.forEach $scope.youtubeVideos, (value, key) ->
            if value.url == null or value.url == '' or value.url is undefined
              $scope.youtubeVideos.errors =
                0: "You have a field in blank or invalid. Please fill it with a valid YouTube URI or remove it"
              isValid = false
              return
            else
              $scope.youtubeVideos.errors = []

        if isValid
          $scope.isEditing = false

      $scope.add = () ->
        if $scope.youtubeVideos is undefined
          $scope.youtubeVideos = []

        if $scope.youtubeVideos.length > 3
          $scope.youtubeVideos.errors =
              0: "You can not add more than 4 videos."
            isValid = false
            return
        else
          $scope.youtubeVideos.errors = null
          $http(
            method: "POST"
            # url: "/api/v1/widgets_content"
            url: "/api/v1/#{$scope.apiController}"
            data:
              widget_id: $scope.widgetId
              type: "youtube_video"
          ).then ((result) ->
            $scope.youtubeVideos.push(result.data.youtube_video)
          ), (error) ->
            $alert({title: 'Error', content: "Internal Server Error. Please try again later", placement: 'top', type: 'danger', show: true, container: 'body', animation: 'am-fade-and-slide-top', duration: 6})

      $scope.remove = (index) ->
        video_to_delete = $scope.youtubeVideos[index];
        $http(
          method: "DELETE"
          url: "/api/v1/#{$scope.apiController}/#{video_to_delete._id}?type=youtube_video&widget_id=#{$scope.widgetId}"
        ).then ((result) ->
          $scope.youtubeVideos.splice(index, 1);
          $scope.youtubeVideos.errors = null
        ), (error) ->
          $alert({title: 'Error', content: "Internal Server Error. Please try again later", placement: 'top', type: 'danger', show: true, container: 'body', animation: 'am-fade-and-slide-top', duration: 6})

      $scope.update = (video) ->
        $log.debug video.url
        if video.url is undefined or video.url is null
          $scope.youtubeVideos.errors =
            0: "You have a field in blank or invalid. Please fill it with a valid YouTube URI or remove it"
        else
          $http(
            method: "PUT"
            url: "/api/v1/#{$scope.apiController}/#{video._id}"
            data:
              url: video.url
              widget_id: $scope.widgetId
              type: "youtube_video"
          ).then ((result) ->
            video.youtube_id = result.data.youtube_video.youtube_id
          ), (error) ->
            $alert({title: 'Error', content: "Internal Server Error. Please try again later", placement: 'top', type: 'danger', show: true, container: 'body', animation: 'am-fade-and-slide-top', duration: 6})

      $scope.editButtonText = ->
        if $scope.youtubeVideos is undefined
          'Add'
        else
          if $scope.youtubeVideos.length == 0
            'Add'
          else
            'Edit'

      $scope.swapVideo = (video, index) ->
        $log.debug video
        # console.log element.css("background-image")
        # console.log $(".thumbnail:eq(#{index})").css("background-image")
        angular.element("#video-playing").attr("src", "https://www.youtube.com/embed/#{video.youtube_id}")
        return

      $scope.trustSrc = (src) ->
        $sce.trustAsResourceUrl(src)
  ]

]
