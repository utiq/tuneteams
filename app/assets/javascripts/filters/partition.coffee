angular.module("tuneTeams.services").filter("partition", [
  '$sce'
  ($sce) ->
    cache = {}

    filter = (arr, size) ->
      if !arr
        return
      newArr = []
      i = 0
      while i < arr.length
        newArr.push arr.slice(i, i + size)
        i += size
      arrString = JSON.stringify(arr)
      fromCache = cache[arrString + size]
      if JSON.stringify(fromCache) == JSON.stringify(newArr)
        return fromCache
      cache[arrString + size] = newArr
      newArr

    filter
])
