angular.module("tuneTeams.services").filter("unsafe", [
  '$sce'
  ($sce) ->
    (val) ->
      $sce.trustAsHtml val
])
