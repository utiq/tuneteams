# register all modules
angular.module('tuneTeams.services', [
  'ngRoute'
  'ngAnimate'
  'ngResource'
  'ngCookies'
  'ngSanitize'
  'ngAudio'
  'rails'
  'ng-rails-csrf'
  'mgcrea.ngStrap'
  'localytics.directives'
  'ui.router'
  'angularFileUpload'
  'angucomplete-alt'
  'angular-intro'
  'angularytics'
  'checklist-model'
  'toggle-switch'
  'colorpicker.module'
  'wysiwyg.module'
  'youtube-embed'
  'angular-inview'
  'angularLoad'
  'ngSlider'
  'timer'
  'angularPayments'
  'sticky'
  'ui.sortable'
  'ladda'
  'angularSoundManager'
  'angular-carousel'
  'angularUtils.directives.dirPagination'
  'ngStorage'
  'angular.filter'
  'googlechart'
])

angular.module('tuneTeams', ['tuneTeams.services'])

# Access configuration
.run ([
  "$rootScope"
  "$state"
  "Auth"
  ($rootScope, $state, Auth) ->
    $rootScope.$on "$stateChangeStart", (event, toState, toParams, fromState, fromParams) ->
      if toState.access != undefined
        unless Auth.authorize(toState.access)
          $rootScope.error = "Seems like you tried to access to a route you don't have privileges to..."
          event.preventDefault()
          if fromState.url is "^"
            if Auth.isLoggedIn()
              $state.go "home"
            else
              $rootScope.error = null
              $state.go "login"
    $rootScope.$state = $state
    # $rootScope.selectedTopSearchOption = "profiles"
    $rootScope.$on '$stateChangeSuccess', ->
      console.log $state.current
      if $state.current.name == 'auctions-show' || $state.current.name == 'profile'
        $('html, body').animate { scrollTop: 0 }, 2000
])

.config ([
  '$logProvider'
  ($logProvider) ->
    $logProvider.debugEnabled !isProduction
])

# Angular Strap Module configuration
.config([
  '$modalProvider'
  '$tooltipProvider'
  ($modalProvider, $tooltipProvider) ->
    angular.extend($modalProvider.defaults, {
      html: true
    });
    angular.extend($tooltipProvider.defaults, {
      html: true
    });
])

# Angularytics configuration
.config((AngularyticsProvider) ->
  AngularyticsProvider.setEventHandlers [
    "Console"
    "GoogleUniversal"
  ]
).run (Angularytics) ->
  Angularytics.init()
