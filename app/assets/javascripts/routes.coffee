angular.module('tuneTeams').config ([
  '$stateProvider'
  '$urlRouterProvider'
  '$locationProvider'
  ($stateProvider, $urlRouterProvider, $locationProvider) ->

    access = routingConfig.accessLevels

    # $stateProvider.state("mailbox",
    #   url: ""
    #   templateUrl: "/assets/layouts/mailbox.html"
    #   controller: ['$scope', '$state',  ($scope, $state) ->
    #     $scope.profilePermalink = $state.$current.locals.globals.$stateParams.profilePermalink
    #   ]
    # )
    $stateProvider.state("dashboard",
      url: ""
      templateUrl: "/assets/layouts/dashboard.html"
      controller: 'DashboardController'
    )
    .state("auctions-new",
      url: "/auctions/new"
      templateUrl: "/assets/auctions/new.html"
      controller: 'AuctionsNewController'
      access: access.user
    )
    .state("auctions-management-parent",
      url: ""
      templateUrl: "/assets/layouts/auctionsManagement.html"
    )
    .state("auctions-management-participating",
      parent: "dashboard"
      url: "/dashboard/auctions/participating"
      templateUrl: "/assets/dashboard/managementParticipating.html"
      controller: 'AuctionsManagementParticipatingController'
      access: access.user
    )
    .state("auctions-management-owner",
      parent: "dashboard"
      url: "/dashboard/auctions/owner"
      templateUrl: "/assets/dashboard/managementOwner.html"
      controller: 'AuctionsManagementOwnerController'
      access: access.user
    )
    .state("auctions-management-single",
      parent: "dashboard"
      url: "/dashboard/auctions/:auctionPermalink"
      templateUrl: "/assets/dashboard/managementAuctionSingle.html"
      controller: 'AuctionSingleManagementController'
      access: access.user
    )
    .state("auctions-show",
      url: "/auctions/:permalink"
      templateUrl: "/assets/auctions/show.html"
      controller: 'AuctionsShowController'
    )
    .state("auctions-index",
      url: "/auctions"
      templateUrl: "/assets/auctions/index.html"
      controller: 'AuctionsIndexController'
    )
    .state("auctions-payment",
      url: "/auctions/:permalink/payment"
      templateUrl: "/assets/auctions/payment.html"
      controller: 'PaymentController'
      access: access.user
    )
    .state("auctions-agreement",
      url: "/auctions/:permalink/agreement/:signerId"
      templateUrl: "/assets/auctions/agreement.html"
      controller: 'AuctionsAgreementController'
      access: access.user
    )
    .state("auctionsSecondChance",
      url: "/auctions/:permalink/second-chance/:bidderId"
      templateUrl: "/assets/auctions/secondChance.html"
      controller: 'AuctionsSecondChanceController'
      access: access.user
    )
    .state("messages",
      parent: 'dashboard',
      url: "/dashboard/:profilePermalink/messages"
      templateUrl: "/assets/dashboard/messages.html"
      controller: 'MessagesController'
      access: access.user
    )
    .state("messages-show",
      parent: 'dashboard',
      url: "/dashboard/:profilePermalink/messages/:messageId"
      templateUrl: "/assets/dashboard/messagesShow.html"
      controller: 'MessagesController'
      access: access.user
    )
    .state("notifications",
      parent: 'dashboard',
      url: "/dashboard/:profilePermalink/notifications"
      templateUrl: "/assets/dashboard/notifications.html"
      controller: 'NotificationsController'
      access: access.user
    )
    .state("bidders-notifications",
      parent: 'dashboard',
      url: "/dashboard/:profilePermalink/bidders"
      templateUrl: "/assets/dashboard/biddersNotifications.html"
      controller: 'NotificationBiddersController'
      access: access.user
    )
    .state("forgot-password",
      url: "/forgot-password"
      templateUrl: "/assets/sessions/forgotPassword.html"
      controller: 'PasswordsController'
    )
    .state("reset-password",
      url: "/reset-password/:temporaryToken"
      templateUrl: "/assets/sessions/resetPassword.html"
      controller: 'PasswordsController'
    )
    .state("confirm-email",
      url: "/confirm-email/:temporaryToken/:email"
      templateUrl: "/assets/sessions/confirmEmail.html"
      controller: 'SessionController'
    )
    .state("terms-of-use-agreement",
      url: "/terms-of-use-agreement"
      templateUrl: "/assets/staticPages/termsOfUseAgreement.html"
      controller: 'StaticPagesController'
    )
    .state("about-us",
      url: "/about-us"
      templateUrl: "/assets/staticPages/aboutUs.html"
      controller: 'StaticPagesController'
    )
    .state("faqs",
      url: "/faqs"
      templateUrl: "/assets/staticPages/faqs.html"
      controller: 'StaticPagesController'
    )
    .state("privacy-policy",
      url: "/privacy-policy"
      templateUrl: "/assets/staticPages/privacyPolicy.html"
      controller: 'StaticPagesController'
    )
    .state("events-midem",
      url: "/events/midem"
      templateUrl: "/assets/staticPages/events/midem.html"
      controller: 'EventsLandingController'
    )
    .state("management-parent",
      url: ""
      templateUrl: "/assets/layouts/management.html"
    )
    .state("account-management",
      parent: 'dashboard',
      url: "/dashboard/management/account"
      templateUrl: "/assets/dashboard/management/accountManagement.html"
      controller: 'AccountManagementController'
      access: access.user
    )
    .state("payout-management",
      parent: 'dashboard',
      url: "/dashboard/management/payout"
      templateUrl: "/assets/dashboard/management/payoutManagement.html"
      controller: 'PayoutManagementController'
      access: access.user
    )
    .state("notification-management",
      parent: 'dashboard',
      url: "/dashboard/management/notification"
      templateUrl: "/assets/dashboard/management/notificationManagement.html"
      controller: 'NotificationManagementController'
      access: access.user
    )
    .state("password-management",
      parent: 'dashboard',
      url: "/dashboard/management/password"
      templateUrl: "/assets/dashboard/management/passwordManagement.html"
      controller: 'PasswordManagementController'
      access: access.user
    )
    .state("profiles",
      url: "/profiles"
      templateUrl: "/assets/profiles/search.html"
      controller: 'SearchesController'
    )
    .state("profiles-dashboard",
      parent: 'dashboard',
      url: "/dashboard/profiles"
      templateUrl: "/assets/dashboard/profiles.html"
      controller: 'ProfileDashboardController'
      access: access.user
    )
    .state("login",
      url: "/login",
      templateUrl: "/assets/sessions/login.html",
      controller: 'LoginController'
    )
    .state("signup",
      url: "/signup",
      templateUrl: "/assets/sessions/signup.html",
      controller: 'SignupController'
    )
    .state("home",
      url: "/",
      templateUrl: "/assets/home.html",
      controller: 'HomeController'
    )
    .state("profile-create",
      url: "/profile/create",
      templateUrl: "/assets/profiles/profileCreate.html",
      controller: 'CreateProfileController',
      access: access.user
    )
    .state("profile-create-step2",
      url: "/profile/create/:profile_permalink/step2",
      templateUrl: "/assets/profiles/profileCreateStep2.html",
      controller: 'CreateProfileStep2Controller'
    )
    .state("admin-invitations-parent",
      url: ""
      templateUrl: "/assets/layouts/invitation.html"
    )
    .state("admin-invitations-requests",
      parent: 'admin-invitations-parent',
      url: "/admin/invitations/requests",
      templateUrl: "/assets/admin/invitations.html",
      controller: 'InvitationsController'
      access: access.admin
    )
    .state("admin-invitations-personal",
      parent: 'admin-invitations-parent',
      url: "/admin/invitations/personal",
      templateUrl: "/assets/admin/invitationsPersonal.html",
      controller: 'InvitationsController'
      access: access.admin
    )
    .state("admin-claims",
      url: "/admin/claims",
      templateUrl: "/assets/admin/claims.html",
      controller: 'ClaimsController'
      access: access.admin
    )
    .state("admin-profiles",
      url: "/admin/profiles",
      templateUrl: "/assets/admin/profiles.html",
      controller: 'AdminProfilesController'
      access: access.admin
    )
    .state("admin-billboard",
      url: "/admin/billboard",
      templateUrl: "/assets/admin/billboard.html",
      controller: 'AdminBillboardController'
      access: access.admin
    )
    .state("admin-auctions",
      url: "/admin/auctions",
      templateUrl: "/assets/admin/auctions.html",
      controller: 'AdminAuctionsController'
      access: access.admin
    )
    .state("admin-reports-parent",
      url: ""
      templateUrl: "/assets/layouts/reports.html"
    )
    .state("admin-reports-dashboard",
      parent: 'admin-reports-parent',
      url: "/admin/reports/dashboard",
      templateUrl: "/assets/admin/reportsDashboard.html",
      controller: 'AdminReportsController'
      access: access.admin
    )
    .state("admin-mailing-parent",
      url: ""
      templateUrl: "/assets/layouts/mailing.html"
    )
    .state("admin-mailing-campaigns",
      parent: 'admin-mailing-parent',
      url: "/admin/mailing/campaigns",
      templateUrl: "/assets/admin/mailingCampaigns.html",
      controller: 'AdminCampaignsController'
      access: access.admin
    )
    .state("admin-mailing-campaigns-edit",
      parent: 'admin-mailing-parent',
      url: "/admin/mailing/campaigns/:id",
      templateUrl: "/assets/admin/mailingCampaignsEdit.html",
      controller: 'AdminCampaignsEditController'
      access: access.admin
    )
    .state("pageNotFound",
      url: "/404",
      templateUrl: "/assets/404.html"
    )
    .state("unauthorized-access",
      url: "/401",
      templateUrl: "/assets/401.html"
    )
    .state("profile",
      url: "/:profilePermalink",
      templateUrl: "/assets/profiles/show.html",
      controller: 'ProfileController',
      access: access.public
    )
    .state("profile-settings",
      url: "/:profilePermalink/settings",
      templateUrl: "/assets/profiles/profileSettings.html",
      controller: 'ProfileSettingsController',
      access: access.user
    )

    $locationProvider.html5Mode true
    # For any unmatched url, send to /
    $urlRouterProvider.otherwise "/"

])
