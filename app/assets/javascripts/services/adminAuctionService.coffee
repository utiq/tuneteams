angular.module('tuneTeams.services').factory('AdminAuction', [
  '$log'
  '$resource'
  ($log, $resource) ->

    $resource "/api/v1/admin_auctions/:id", id: "@_id",
      query:
        method: "GET"
        url: "/api/v1/admin_auctions"
        isArray: false
      update:
        method: "PUT"
      approve:
        method: "POST"
        url: "/api/v1/admin_auctions/approve"
      reject:
        method: "POST"
        url: "/api/v1/admin_auctions/reject"
      disapprove:
        method: "POST"
        url: "/api/v1/admin_auctions/disapprove"
      payout:
        method: "POST"
        url: "/api/v1/admin_auctions/payout"
      returnToDraft:
        method: "POST"
        url: "/api/v1/admin_auctions/return_to_draft"

])
