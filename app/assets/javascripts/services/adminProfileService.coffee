angular.module('tuneTeams.services').factory('AdminProfile', [
  '$log'
  '$resource'
  ($log, $resource) ->

    $resource "/api/v1/admin_profiles/:id", id: "@_id",
      query:
        method: "GET"
        url: "/api/v1/admin_profiles"
        isArray: false
      update:
        method: "PUT"
      download:
        isArray: false
        method: "GET"
        url: "/api/v1/admin_profiles/download_profiles"

])
