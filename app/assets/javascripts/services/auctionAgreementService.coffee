angular.module('tuneTeams.services').factory('AuctionAgreement', [
  '$log'
  '$resource'
  ($log, $resource) ->

    $resource "/api/v1/auction_agreements/:id", id: "@_id",
      update:
        method: "PUT"
      download:
        isArray: false
        method: "GET"
        url: "/api/v1/auction_agreements/download"

])