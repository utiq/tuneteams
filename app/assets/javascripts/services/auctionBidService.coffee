angular.module('tuneTeams.services').factory('AuctionBid', [
  '$log'
  '$resource'
  ($log, $resource) ->

    $resource "/api/v1/auction_bids/:id", id: "@_id",
      update:
        method: "PUT"
])
