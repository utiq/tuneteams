angular.module('tuneTeams.services').factory('AuctionBidder', [
  '$log'
  '$resource'
  ($log, $resource) ->

    $resource "/api/v1/auction_bidders/:id", id: "@_id",
      update:
        method: "PUT"
])
