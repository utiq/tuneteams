angular.module('tuneTeams.services').factory('AuctionManagement', [
  '$log'
  '$resource'
  ($log, $resource) ->

    $resource "/api/v1/auction_management/:id", id: "@_id",
      update:
        method: "PUT"
      query:
        method: "GET"
        url: "/api/v1/auction_management"
        isArray: false
      cancel:
        method: "POST"
        url: "/api/v1/auction_management/cancel"
      materialReceived:
        method: "POST"
        url: "/api/v1/auction_management/materials_received"
])
