angular.module('tuneTeams.services').factory('AuctionPayout', [
  '$log'
  '$resource'
  ($log, $resource) ->

    $resource "/api/v1/auction_payouts/:id", id: "@_id",
      update:
        method: "PUT"
])
