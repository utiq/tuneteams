angular.module('tuneTeams.services').factory('AuctionPosition', [
  '$log'
  '$resource'
  ($log, $resource) ->

    $resource "/api/v1/auction_positions/:id", id: "@_id",
      update:
        method: "PUT"
])
