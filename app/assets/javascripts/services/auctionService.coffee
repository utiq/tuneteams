angular.module('tuneTeams.services').factory('Auction', [
  '$log'
  '$resource'
  ($log, $resource) ->

    $resource "/api/v1/auctions/:id", id: "@_id",
      # overriding the query method because is returning an object
      query:
        method: "GET"
        url: "/api/v1/auctions"
        isArray: false
      promoted:
        method: "GET"
        url: "/api/v1/auctions/promoted"
        isArray: true
      recommended:
        method: "GET"
        url: "/api/v1/auctions/recommended"
        isArray: true
      update:
        method: "PUT"
      cancelRestart:
        method: "GET"
        url: "/api/v1/auctions/:id/cancel_restart/:token"
      copy:
        method: "POST"
        url: "/api/v1/auctions/:id/copy"
      agreement:
        method: "GET"
        url: "/api/v1/auctions/:id/agreement/:signer_id"
      search:
        method: "GET"
        url: "/api/v1/auctions/search"
      publish:
        method: "POST"
        url: "/api/v1/auctions/publish"
      pay:
        method: "POST"
        url: "/api/v1/auctions/:id/pay"
      related:
        method: "GET"
        url: "/api/v1/auctions/related"
        isArray: true
      facebook_feed:
        method: "GET"
        url: "/api/v1/auctions/:permalink/facebook_feed"
        isArray: false
      twitter_timeline:
        method: "GET"
        url: "/api/v1/auctions/:permalink/twitter_timeline"
        isArray: false
      soundcloud_info:
        method: "GET"
        url: "/api/v1/auctions/:permalink/soundcloud_info"
        isArray: false
      next_big_sound_metrics:
        method: "GET"
        url: "/api/v1/auctions/:permalink/next_big_sound_metrics"
        isArray: true
      restart:
        method: "POST"
        url: "/api/v1/auctions/:id/restart"

])
