angular.module('tuneTeams.services').factory('AuctionSigner', [
  '$log'
  '$resource'
  ($log, $resource) ->

    $resource "/api/v1/auction_signers/:id", id: "@_id",
      update:
        method: "PUT"
      send_validation_email:
        url: "/api/v1/auction_signers/send_validation_email"
        method: "POST"
      validate_second_signer:
        url: "/api/v1/auction_signers/validate_second_signer"
        method: "POST"
])
