angular.module('tuneTeams.services').factory('AuctionSong', [
  '$log'
  '$resource'
  ($log, $resource) ->

    $resource "/api/v1/auction_songs/:id", id: "@_id",
      update:
        method: "PUT"
])