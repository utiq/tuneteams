angular.module('tuneTeams.services').factory('AuctionVerification', [
  '$log'
  '$resource'
  ($log, $resource) ->

    $resource "/api/v1/auction_verifications/:id", id: "@_id",
      update:
        method: "PUT"
      verify:
        method: "POST"
        url: "/api/v1/auction_verifications/verify"
])