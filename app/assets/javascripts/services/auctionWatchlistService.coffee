angular.module('tuneTeams.services').factory('AuctionWatchlist', [
  '$log'
  '$resource'
  ($log, $resource) ->

    $resource "/api/v1/auction_watchlists/:id", id: "@_id"

])