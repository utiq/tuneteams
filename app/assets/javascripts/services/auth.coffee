# "use strict"
angular.module("tuneTeams.services").factory("Auth", [
  '$http'
  '$cookieStore'
  '$log'
  ($http, $cookieStore, $log) ->
    changeUser = (user) ->
      angular.extend currentUser, user

    accessLevels = routingConfig.accessLevels
    userRoles = routingConfig.userRoles
    currentUser = $cookieStore.get("user") or { user: {role: userRoles.public} }
    $log.debug currentUser

    # $cookieStore.remove "user"
    authorize: (accessLevel, role) ->
      role = currentUser.user.role if role is undefined
      accessLevel.bitMask & role.bitMask
      # accessLevel.bitMask is role.bitMask

    isLoggedIn: (user) ->
      user = currentUser.user if user is undefined
      user.role.title is userRoles.user.title or user.role.title is userRoles.admin.title

    register: (user, success, error) ->
      $http.post("/api/v1/users", user).success((res) ->
        $cookieStore.put("user", res) #this is new
        changeUser res
        # success()
        success res
        return
      ).error error
      return

    login: (user, success, error) ->
      $http.post("/api/v1/sessions", user).success((user) ->
        $log.debug user
        $cookieStore.put("user", user) #this is new
        changeUser user
        success user
        return
      ).error error
      return

    logout: (success, error) ->
      $log.debug "user logued out"
      $http(
        method: "DELETE"
        url: "/api/v1/sessions/0"
        data: {}
      ).then ((result) ->
        changeUser
          user: {role: userRoles.public}

        $cookieStore.remove("user") #this is new
        success()
        return
      ), (error) ->
        console.log error

    accessLevels: accessLevels
    userRoles: userRoles
    user: currentUser.user
])

angular.module("tuneTeams.services").factory "Users", ($http) ->
  getAll: (success, error) ->
    $http.get("/users").success(success).error error
    return
