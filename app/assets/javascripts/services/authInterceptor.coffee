# Intercepting HTTP calls.
angular.module("tuneTeams.services").config [
  '$provide'
  '$httpProvider'
  '$locationProvider'
  ($provide, $httpProvider, $locationProvider) ->

    # Intercept http calls.
    $provide.factory "AuthInterceptor", [
      '$log'
      '$q'
      '$cookieStore'
      '$location'
      '$rootScope'
      ($log, $q, $cookieStore, $location, $rootScope) ->

        # On request success
        request: (config) ->
          if $location.path() != '/login'
            $rootScope.lastLocationPath = $location.path()
          config.headers = config.headers or {}
          current_user = $cookieStore.get("user")
          if current_user
            config.headers.Authorization = "Token token=#{current_user.user.access_token}"
          return config

        # On response success
        response: (response) ->
          # console.log response.status
          if response.status is 401
            $location.path "/login"
            $rootScope.location = $location

          response or $q.when(response)

        # On response failture
        responseError: (rejection) ->
          # if rejection.status is 401
          #   $location.path "/login"
          #   console.log "Unauthorized"
          $q.reject rejection
    ]
    # Add the interceptor to the $httpProvider.
    $httpProvider.interceptors.push "AuthInterceptor"
]
