angular.module('tuneTeams.services').factory('Country', [
  '$log'
  '$resource'
  ($log, $resource) ->

    $resource "/api/v1/countries/:id"

])
