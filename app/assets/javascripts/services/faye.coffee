angular.module('tuneTeams.services').factory('Faye', [
  '$log'
  '$faye'
  ($log, $faye) ->

    # $faye("/faye")
    $faye "/faye", (client) ->
    	client.disable("websocket")

])