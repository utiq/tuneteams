angular.module('tuneTeams.services').factory('Genre', [
  '$log'
  '$resource'
  ($log, $resource) ->

    $resource "/api/v1/genres/:id", id: "@_id",
      update:
        method: "PUT"
      
])