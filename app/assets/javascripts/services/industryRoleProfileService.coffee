angular.module('tuneTeams.services').factory('IndustryRoleProfile', [
  '$log'
  '$resource'
  ($log, $resource) ->

    $resource "/api/v1/industry_role_profiles/:id", id: "@_id",
      interests:
        method: "GET"
        url: "/api/v1/industry_role_profiles/:id/interests"
        isArray: true

])
