angular.module('tuneTeams.services').factory('MailingCampaign', [
  '$log'
  '$resource'
  ($log, $resource) ->

    $resource "/api/v1/mailing_campaigns/:id", id: "@_id",
      update:
        method: "PUT"
      
])