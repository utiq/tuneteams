angular.module('tuneTeams.services').factory('Message', [
  '$log'
  '$resource'
  ($log, $resource) ->

    $resource "/api/v1/messages/:id", id: "@_id",
      update:
        method: "PUT"
      reply:
        method: "POST"
        url: "/api/v1/messages/reply/:id"
      archive:
        method: "POST"
        url: "/api/v1/messages/archive/:id"
      
])