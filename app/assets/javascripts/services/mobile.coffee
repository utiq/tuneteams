# "use strict"
angular.module("tuneTeams.services").factory("Mobile", [
  '$http'
  '$cookieStore'
  '$log'
  ($http, $cookieStore, $log) ->
    isMobile: ->
      if window.innerWidth <= 800 and window.innerHeight <= 600
        return true
      else
        return false
])
