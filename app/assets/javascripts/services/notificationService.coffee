angular.module('tuneTeams.services').factory('Notification', [
  '$log'
  '$resource'
  ($log, $resource) ->

    $resource "/api/v1/notifications/:id", id: "@_id",
      update:
        method: "PUT"
        
])