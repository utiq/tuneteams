###
  Simple Service to share data of current view state, which is acting as a domain model.
  The view state set by controllers.


  Current view states are:
    'login'
    'employees'
    'edit'
    'create'

###
angular.module('tuneTeams.services').factory 'Page'
, ['$log', '$location'
, ($log, $location) ->

    title = 'tuneteams'
    description = 'In a digital world, music companies need local promotion partners more than ever. tuneteams is the new way to create music partnerships around the world.'
    image = 'http://tuneteams.com/assets/logo-header.png'

    title: ->
      title

    description: ->
      description

    image: ->
      image

    url: ->
      $location.absUrl()

    setTitle: (newTitle) ->
      title = "#{newTitle} - tuneteams"

    setDescription: (newDescription) ->
      description = "#{newDescription}"

    setImage: (newImage) ->
      image = "#{newImage}"
]