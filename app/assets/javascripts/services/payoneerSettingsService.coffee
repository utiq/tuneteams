angular.module('tuneTeams.services').factory('PayoneerSettings', [
  '$log'
  '$resource'
  ($log, $resource) ->

    $resource "/api/v1/payoneer_settings/:id"
      
])