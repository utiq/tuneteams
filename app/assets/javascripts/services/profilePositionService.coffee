angular.module('tuneTeams.services').factory('ProfilePosition', [
  '$log'
  '$resource'
  ($log, $resource) ->

    $resource "/api/v1/profile_positions/:id", id: "@_id",
      update:
        method: "PUT"
])