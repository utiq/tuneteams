angular.module('tuneTeams.services').factory('ProfileService', [
  '$log'
  '$resource'
  ($log, $resource) ->

    $resource "/api/v1/profiles/:id", id: "@_id",
      update:
        method: "PUT"
      connections:
        method: "GET"
        url: "/api/v1/profiles/:permalink/connections"
        isArray: true
      switch:
        method: "PUT"
        url: "/api/v1/profiles/:permalink/switch"
        params:
          permalink: '@permalink'
      connectionStatus:
        method: "POST"
        url: "/api/v1/profiles/:permalink/connections/:connectionId/status/:status"
      promoted:
        method: "GET"
        url: "/api/v1/profiles/promoted"
        isArray: true
      recommended:
        method: "GET"
        url: "/api/v1/profiles/recommended"
        isArray: true
])
