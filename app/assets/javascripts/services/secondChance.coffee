angular.module('tuneTeams.services').factory('SecondChance', [
  '$log'
  '$resource'
  ($log, $resource) ->

    $resource "/api/v1/second_chances/:id", id: "@_id",
      update:
        method: "PUT"
      accept:
        method: "POST"
        url: "/api/v1/second_chances/accept/:auction_id/:auction_bidder_id"
      decline:
        method: "POST"
        url: "/api/v1/second_chances/decline/:auction_id/:auction_bidder_id"
])
