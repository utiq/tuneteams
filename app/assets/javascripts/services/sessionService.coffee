angular.module('tuneTeams.services').factory('SessionService', [
  '$log'
  '$resource'
  '$http'
  ($log, $resource, $http)->

    currentUser = {}
    User = $resource "/api/v1/users/:id"

    get: (id) ->
      User.get({id: id}).$promise.then ((user) ->
        currentUser = user
      ), (errResponse) ->
        $log.log errResponse

    # service = $resource '/api/v1/sessions/:param', {},
    #   'login':
    #     method: 'POST'
    #   'logout':
    #     method: 'DELETE'

    # # login
    # # ------------------------------------------------------------

    # authorized = ->
    #   # $log.info getCurrentUser()
    #   # getCurrentUser().authorized is 'true'
    #   true

    # login = (newUser)->
    #   promise = service.login(newUser).$promise
    #   promise.then((result)->
    #     updateCurrentUser(result.user, result.authorized)
    #   )
    #   promise

    # # logout
    # # ------------------------------------------------------------

    # logout = ->
    #   promise = service.logout(param: currentUser.id).$promise
    #   updateCurrentUser {}, false
    #   promise

    # # user
    # # ------------------------------------------------------------

    # currentUser = {}

    # getCurrentUser = ->
    #   currentUser

    # # helper method to update currentUser
    # updateCurrentUser = (user, authorized)->
    #   currentUser.id = user.id
    #   currentUser.name= user.name
    #   currentUser.authorized = authorized

    # {
    #   login
    #   logout
    #   authorized
    #   getCurrentUser
    # }
])