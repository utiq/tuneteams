angular.module('tuneTeams.services').factory('SessionShared', [
  '$log'
  '$resource'
  '$http'
  'SessionService'
  '$rootScope'
  'ProfileService'
  ($log, $resource, $http, SessionService, $rootScope, ProfileService)->

    SessionShared = {}
    SessionShared.currentUser = {}

    SessionShared.prepForBroadcast = (profilePermalink) ->
      profile = new ProfileService
      profile.$switch({permalink: profilePermalink}, ->
        SessionService.get(0).then (response) ->
          SessionShared.currentUser = response
          SessionShared.broadcastItem()
      )

    SessionShared.broadcastItem = ->
      $rootScope.$broadcast('handleBroadcast')

    return SessionShared
])
