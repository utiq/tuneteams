###
  RESTful service to handle data of employees
  using angularjs-rails-resource
  @see: http://ngmodules.org/modules/angularjs-rails-resource
###
angular.module('tuneTeams.services').factory('SubscriberService', [
  '$log'
  'railsResourceFactory'
  ($log, railsResourceFactory) ->
    resource = railsResourceFactory
      url: '/'
      name: 'subscriber'
])