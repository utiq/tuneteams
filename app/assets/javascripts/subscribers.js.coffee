app = angular.module("Subscriber", ["ngResource", "ui.bootstrap"])

app.factory "Subscriber", ["$resource", ($resource) ->
  $resource("/subscribers/:id", {id: "@id"}, {update: {method: "PUT"}})
]

@SubscriberCtrl = ["$scope", "$http", "Subscriber", ($scope, $http, Subscriber) ->
  # Subscriber = $resource("/subscribers", {email: "@email"}, {update: {method: "POST"}})
  # console.log Subscriber.query()
  # $scope.subscribers = Subscriber.query()

  $scope.addSubscriber = ->
    # subscriber = Subscriber.save($scope.newSubscriber)
    # $scope.token = subscriber.token
    # console.log subscriber
    # serializedData = $.param({subscriber : {email:"lol@goo.com"}});
    $http(
      method: "POST"
      url: "/subscribers"
      data:
        $.param(subscriber: $scope.newSubscriber)
      headers:
        "Content-Type": "application/x-www-form-urlencoded; charset=UTF-8"
    ).then ((result) ->
      $scope.token = result.data.token
      console.log result
    ), (error) ->
      console.log error

]


@AccordionDemoCtrl = ["$scope", "Subscriber", ($scope, Subscriber) ->
  $scope.oneAtATime = true
  $scope.groups = [
    {
      title: "Dynamic Group Header - 1"
      content: "Dynamic Group Body - 1"
    }
    {
      title: "Dynamic Group Header - 2"
      content: "Dynamic Group Body - 2"
    }
  ]
  $scope.items = [
    "Item 1"
    "Item 2"
    "Item 3"
  ]
  $scope.addItem = ->
    newItemNo = $scope.items.length + 1
    $scope.items.push "Item " + newItemNo
    console.log $scope
]