module Api::V1
  class AdminAuctionsController < Api::V1::ApplicationController
    respond_to :json
    before_filter :restrict_access

    def index
      if params.has_key?(:order_by)
        @auctions = Auction.where(:status => params[:status]).page(params[:page_number]).per(10).order_by(params[:order_by])
        # @auctions = Auction.all.order_by(params[:order_by])
      else
        # @auctions = Auction.all.order_by("position ASC, updated_at DESC")
        @auctions = Auction.where(:status => params[:status]).page(params[:page_number]).per(10).order_by("created_at DESC")
      end

      auctions_result = ActiveModel::ArraySerializer.new(@auctions, each_serializer: AuctionIndexSerializer, scope: @current_user).serializable_array
      total_count = Auction.where(:status => params[:status]).count
      # TODO: Create a cache for this
      counter = {
        rejected: Auction.where(:status => 'rejected').count,
        ended_no_bids: Auction.where(:status => 'ended_no_bids').count,
        draft: Auction.where(:status => 'draft').count,
        published: Auction.where(:status => 'published').count,
        approved: Auction.where(:status => 'approved').count,
        waiting_sign: Auction.where(:status => 'waiting_sign').count,
        waiting_payment: Auction.where(:status => 'waiting_payment').count,
        paid: Auction.where(:status => 'paid').count,
        materials_received: Auction.where(:status => 'materials_received').count,
        payout_made: Auction.where(:status => 'payout_made').count
      }

      render json: {auctions: auctions_result, total_count: total_count, counter: counter}
    end

    def update
      if params.has_key? (:notes)
        logger.debug "has notes"
        @auction = Auction.find(params[:id])
        @auction.update_attributes(notes: params[:notes])

      end
      render :json => { message: 'ok' }, :status => 201
    end

    def approve
      @auction = Auction.find(params[:id])
      @auction.approve
      if @auction.profile.user.notification_auction_approval
        AuctionsMailer.delay.approve(@auction.id)
      end

      render json: @auction, scope: @current_user, serializer: AuctionSerializer, :status => 201, location: nil
    end

    def reject
      @auction = Auction.find(params[:id])
      @auction.update_attributes(status: "rejected")
      AuctionsMailer.delay.reject(@auction.id)

      render json: @auction, scope: @current_user, serializer: AuctionSerializer, :status => 201
    end

    def return_to_draft
      @auction = Auction.find(params[:id])
      @auction.update_attributes(status: "draft")
      # AuctionsMailer.delay.reject(@auction.id)

      render json: @auction, scope: @current_user, serializer: AuctionSerializer, :status => 201
    end

    def disapprove
      @auction = Auction.find(params[:id])
      @auction.update_attributes(status: 'disapproved')
      AuctionsMailer.delay.disapprove(@auction.id)

      render json: @auction, scope: @current_user, serializer: AuctionSerializer, :status => 201, location: nil
    end

    def destroy
      @auction = Auction.find(params[:id])
      @auction.auction_bids.delete_all
      @auction.auction_payouts.delete_all
      @auction.auction_bidders.delete_all
      @auction.auction_watchlist.delete_all
      @auction.delete
      render :json => { message: "ok" }, status: :ok
    end

    def payout
      @auction = Auction.find(params[:auction_id])

      begin
        if @auction.profile.user.payoneer_info.nil?
          raise PayoneerPaymentMethodRegisteredException.new, "The Auction Manager didn't sign up for a Payoneer account."
        end

        if (@auction.profile.user.payoneer_info.accept_ach == false &&
           @auction.profile.user.payoneer_info.accept_iach == false &&
           @auction.profile.user.payoneer_info.accept_card == false)
          raise PayoneerPaymentMethodRegisteredException.new, "The Auction Manager doesn't have any payment method configured."
        end

        if @auction.profile.user.payoneer_info.status == 'payment_method_requested'
          raise PayoneerPaymentMethodRegisteredException.new, "Payoneer has not approved the user yet. Wait for a confirmation."
        end

        if @auction.profile.user.payoneer_info.status == 'declined'
          raise PayoneerUserDeclinedException.new, "Payoneer has declined the user application. Communicate with them."
        end

        payee_id = @auction.profile.user.id.to_s
        logger.debug payee_id
        amount_to_pay = @auction.sub_total - @auction.tuneteams_commision
        @auction_payout = AuctionPayout.new(amount: amount_to_pay, auction: @auction)

        payout_response = Payoneer::Payout.create(
          program_id: 'TUNETEAMS100043560-PAY',
          payment_id: @auction_payout.id,
          payee_id: payee_id,
          amount: amount_to_pay,
          description: "Payment for #{@auction.name} auction",
          payment_date: Time.now,
          currency: 'USD'
        )

        logger.debug "payout_response.body"
        logger.debug payout_response.body

        if payout_response.ok?
          if @auction_payout.save
            @auction.update_attributes(status: 'payout_made')
            render :json => { message: "ok" }, :status => 201
          end
        else
          raise PayoneerResponseException.new, payout_response.body
        end

      rescue PayoneerResponseException => e
        render :json => {title: "Payoneer server response error.", message: e.message }, status: :unprocessable_entity
      rescue PayoneerUserDeclinedException => e
        render :json => {title: "Payoneer user configuration error.", message: e.message }, status: :unprocessable_entity
      rescue PayoneerPaymentMethodRegisteredException => e
        render :json => {title: "Payoneer user configuration error.", message: e.message }, status: :unprocessable_entity
      end
      # TODO: Send the email with the Standard error
    end

    private

    def restrict_access
      authenticate_or_request_with_http_token do |token, options|
        @current_user = User.where(access_token: token, role: "admin").first
        if not @current_user.nil?
          return true
        else
          render :json => { :status => :unauthorized }
        end
      end
    end

  end

  class PayoneerPaymentMethodRegisteredException < StandardError; end
  class PayoneerUserDeclinedException < StandardError; end
  class PayoneerResponseException  < StandardError; end
end
