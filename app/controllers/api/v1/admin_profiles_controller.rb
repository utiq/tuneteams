# encoding: utf-8
module Api::V1
  class AdminProfilesController < Api::V1::ApplicationController
    respond_to :json
    before_filter :restrict_access

    def index
      @profiles = Profile.all.page(params[:page_number]).per(10).order_by(params[:order_by])

      profiles_result = ActiveModel::ArraySerializer.new(@profiles, each_serializer: ProfileIndexSerializer, scope: @current_user).serializable_array
      total_count = @profiles.count#Pr.where(:status => params[:status]).count

      render json: {profiles: profiles_result, total_count: total_count}
    end

    def update
      @profile = Profile.find(params[:id])
      @profile.update_attributes(profile_update_params)
      render :json => { message: 'ok' }, status: :ok
    end

    def destroy
      DeleteProfileWorker.perform_async(params[:id])
      render :json => { message: "ok" }, :status => 201
    end

    def download_profiles
      ExportEmailsWorker.perform_async

      render :json => { message: "ok" }, status: :ok
    end

    private

    def profile_update_params
      params.permit(:rating)
    end

    def restrict_access
      authenticate_or_request_with_http_token do |token, options|
        @current_user = User.where(access_token: token, role: "admin").first
        if not @current_user.nil?
          return true
        else
          render :json => { :status => :unauthorized }
        end
      end
    end

  end
end
