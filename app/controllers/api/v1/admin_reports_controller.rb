module Api::V1
  class AdminReportsController < Api::V1::ApplicationController
    respond_to :json
    before_filter :restrict_access

    def dashboard
      @results = []
      if @current_user.role == "admin"
        industry_role_profiles = IndustryRoleProfile.all.order_by('name ASC')
        industry_role_profiles.each do |industry_role_profile|
          @results << {industry_role: industry_role_profile.industry_role.name,
                   industry_role_profile_name: industry_role_profile.name,
                   count_with_owner: industry_role_profile.profiles.where(has_owner: true).count,
                   count_without_owner: industry_role_profile.profiles.where(has_owner: false).count}
        end
      end
      render json: @results
    end

    private

    def restrict_access
      authenticate_or_request_with_http_token do |token, options|
        @current_user = User.where(access_token: token).first
        if not @current_user.nil?
          return true
        else
          render :json => { :status => :unauthorized }
        end
      end
    end

  end
end