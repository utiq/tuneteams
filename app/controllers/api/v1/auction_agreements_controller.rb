module Api::V1
  class AuctionAgreementsController < Api::V1::ApplicationController
    respond_to :json
    before_filter :restrict_access, :only => [:show, :update]

    def show
      @auction = Auction.find_by(permalink: params[:id])
      @auction_signer = @auction.auction_signers.find(params[:signer_id])

      sign_url = ""

      begin
        client = HelloSign.client
        response = client.get_embedded_sign_url :signature_id => @auction_signer.signature_id
        sign_url = response.sign_url
        skip_domain_verification = if ENV["CURRENT_ENV"] == 'production' then false else true end

        render :json => { sign_url: sign_url,
                          skip_domain_verification: skip_domain_verification }, :status => 201


      rescue HelloSign::Error::NotFound => e
        ExceptionNotifier.notify_exception(e)
        render :json => { title: "HelloSign Warning.", message: "This document was not found." }, :status => :not_found
      rescue HelloSign::Error::Conflict => e
        render :json => { title: "HelloSign Warning.", message: "Seems like this document was already signed." }, :status => :conflict
      rescue StandardError => e
        ExceptionNotifier.notify_exception(e)
        render :json => { message: 'Internal Server Error' }, status: :internal_server_error
      end

    end

    def update

    end

    # download the contract from HelloSign
    def download
      # TODO: Now there is a method in the model, use that.
      @auction = Auction.find(params[:auction_id])
      begin
        client = HelloSign.client
        file_bin = client.signature_request_files :signature_request_id => @auction.signature_request_id, :file_type => 'pdf'
        random_name = SecureRandom.hex
        path = "uploads/tmp/#{random_name}.pdf"

        open("public/#{path}", "wb") do |file|
          file.write(file_bin)
        end

        render :json => { download_url: "#{ENV['HOST_NAME']}/#{path}" }

      rescue HelloSign::Error::Error => e
        render :json => { title: "HelloSign Error.", message: "There is an error in your request" }, :status => :unprocessable_entity and return
      rescue StandardError => e
        render :json => { title: "Internal Server Error", message: e.message }, :status => :unprocessable_entity and return
      end
    end

    def hellosign_callback
      hash = JSON.parse params[:json]

      if hash["event"]["event_type"] == "signature_request_sent"
        @auction = Auction.find_by(permalink: hash["signature_request"]["metadata"]["auction_permalink"])
        if @auction.update_attributes(signature_request_id: hash["signature_request"]["signature_request_id"], signature_is_complete: hash["signature_request"]["is_complete"])
          hash["signature_request"]["signatures"].each do |signature|
            # logger.debug signature
            auction_signer = @auction.auction_signers.find_by(email: signature["signer_email_address"])
            auction_signer.update_attributes(signature_id: signature["signature_id"]) if not auction_signer.nil?
          end
        end
      end

      if hash["event"]["event_type"] == "signature_request_signed"
        @auction = Auction.find_by(permalink: hash["signature_request"]["metadata"]["auction_permalink"])
        hash["signature_request"]["response_data"].each do |response|
          @auction_signer = @auction.auction_signers.find_by(signature_id: response["signature_id"])
          @auction_signer.update_attributes(signed: true)
        end
      end

      render json: "Hello API Event Received", status: :ok
      # render :json => { message: "ok" }, :status => 201
      # HelloSign.client.create_embedded_signature_request(test_mode: 1, subject: "test1", message: "Message for test 1", signers: [{email_address: "cesguty@gmail.com", name: "Cesar"}], files: ["public/pdfs/licenses/54873eb3636573ef30000000.pdf"], metadata: {auction_permalink: "outkast"})
    end

    private

    def restrict_access
      authenticate_or_request_with_http_token do |token, options|
        @current_user = User.where(access_token: token).first
        if not @current_user.nil?
          return true
        else
          head :unauthorized
        end
      end
    end

  end
end
