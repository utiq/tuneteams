module Api::V1
  class AuctionBiddersController < Api::V1::ApplicationController
    respond_to :json
    before_filter :restrict_access#, :only => [:create]

    def index
      @auction_bidders = []
      @current_user.default_profile.auctions.each do |auction|
        if auction.status == 'approved'
          auction.auction_bidders.where(status: 'pending').each do |auction_bidder|
            @auction_bidders << AuctionBidderSerializer.new(auction_bidder)
          end
        end
      end
      respond_with @auction_bidders
    end

    def create

      @auction = Auction.find_by(permalink: params[:auction_permalink])

      has_second_signer = false
      if params[:requester_details][:has_second_signer]
        has_second_signer = params[:requester_details][:has_second_signer]
      end

      begin
        raise Tuneteams::Error::EmailNotConfirmed.new "You can not request to bid because your email address is not verified. Please verify your email or change it via My Dashboard and then check your email for instructions." if @current_user.default_profile.user.email_confirmed == false

        raise Tuneteams::Error::NoProfileConfiguredException.new "You don't have any default profile configured." if @current_user.default_profile.nil?

        @auction_bidder = AuctionBidder.new(auction: @auction,
                                          profile: @current_user.default_profile,
                                          has_second_signer: has_second_signer)

        if @auction_bidder.save
          @current_user.default_profile.update_attributes(
            licensor_legal_name: params[:requester_details][:licensor_legal_name],
            licensor_legal_address_line1: params[:requester_details][:licensor_legal_address_line1],
            licensor_legal_address_line2: params[:requester_details][:licensor_legal_address_line2],
            licensor_legal_address_country: params[:requester_details][:licensor_legal_address_country],
            licensor_signer_name: params[:requester_details][:licensor_signer_name],
            # licensor_contact_email: params[:requester_details][:licensor_contact_email],
            licensor_contact_email: @current_user.email,
            second_licensor_signer_name: params[:requester_details][:second_licensor_signer_name],
            second_licensor_contact_email: params[:requester_details][:second_licensor_contact_email]
          )

          if @auction.recording_publishing_different_info && has_second_signer && @current_user.default_profile.second_licensor_contact_email_confirmed == false
            AuctionSignerMailer.send_validation_email(@auction.id, @current_user.default_profile.second_licensor_contact_email, 'bidder', @current_user.default_profile.id.to_s).deliver
          end

          BidsMailer.delay.request_approval_to_bid(@auction_bidder.profile.name,
                                                   @auction_bidder.profile.permalink,
                                                   @auction.profile.name,
                                                   @auction.profile.permalink,
                                                   @auction.name,
                                                   @auction.permalink,
                                                   @auction_bidder.profile.licensor_legal_name,
                                                   @auction.profile.user.email)

          kiss_identifier = "User-#{@current_user.id.to_s}"
          KissmetricsWorker.perform_async(kiss_identifier, "Approval to bid", {'Action' => 'Requested', 'Auction name' => @auction.name, 'Bidder Profile' => @current_user.default_profile.name }) if @current_user.role != 'admin'

          render :json => { message: "ok"}, :status => :created, location: nil
        end

      rescue Tuneteams::Error::EmailNotConfirmed => e
        render :json => { message: e.message }, status: :precondition_failed
      rescue Tuneteams::Error::NoProfileConfiguredException => e
        render :json => { message: e.message }, status: :precondition_failed
      rescue StandardError => e
        ExceptionNotifier.notify_exception(e)
        render :json => { message: 'Internal Server Error' }, status: :internal_server_error
      end

    end

    def update
      @bidder = AuctionBidder.find(params[:id])
      if @bidder.update_attributes(status: params[:decision])

        kiss_identifier = "User-#{@current_user.id.to_s}"

        if @bidder.status == "accepted"
          BidsMailer.delay.request_approved(@bidder.auction.name,
                                            @bidder.auction.permalink,
                                            @bidder.profile.user.email)


          KissmetricsWorker.perform_async(kiss_identifier, "Approval to bid", {'Action' => 'Accepted', 'Auction name' => @bidder.auction.name, 'Bidder Profile' => @bidder.profile.name })

        elsif @bidder.status == "rejected"
          BidsMailer.delay.request_rejected(@bidder.auction.name,
                                            @bidder.auction.permalink,
                                            @bidder.profile.user.email)
          KissmetricsWorker.perform_async(kiss_identifier, "Approval to bid", {'Action' => 'Rejected', 'Auction name' => @bidder.auction.name, 'Bidder Profile' => @bidder.profile.name })
        end
      end

      render :json => { "message" => "ok" }, :status => 201
    end

    private

    def restrict_access
      authenticate_or_request_with_http_token do |token, options|
        @current_user = User.where(access_token: token).first
        if not @current_user.nil?
          return true
        else
          head :unauthorized
        end
      end
    end

  end
end
