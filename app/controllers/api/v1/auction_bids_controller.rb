module Api::V1
  class AuctionBidsController < Api::V1::ApplicationController
    respond_to :json
    before_filter :restrict_access, :only => [:create]

    def index

    end

    def create

      @auction = Auction.find(params[:id])
      minimum_amount_dollars = 250
      minimum_amount_percentage = 2
      if @auction.auction_bids.count == 0
        increment_money = 0
        increment_percentage = 0
      else
        increment_money = 25
        increment_percentage = 1
      end

      begin

        @auction_bidder = AuctionBidder.find_by(auction: @auction, profile: @current_user.default_profile, status: "accepted")
        raise InvalidBidderException.new "You are not allowed to bid in this auction." if @auction_bidder.nil?

        last_bidder_bid_amount = @auction.auction_bids.last.amount rescue 0

        raise OutOfTimeException.new, "The time for bidding has ended." if Time.zone.now > @auction.due_date

        ################################ First Bid validations #########################
        if @auction.auction_bids.length == 0
          if @auction.auction_type != 'recording_publishing_rights'
            if @auction.competitive_term == 'advance'
              if params[:amount].to_f < minimum_amount_dollars #@auction.reserve_on_competitive_term
                raise FirstBidAmountException.new, "First bid amount is greater or equal than $250. Please enter a larger amount in order to place a bid."
              end
            elsif @auction.competitive_term == 'royalty_rate'
              if params[:amount].to_f < minimum_amount_percentage #@auction.reserve_on_competitive_term
                raise FirstBidAmountException.new,  "First bid amount is greater or equal than 1%. Please enter a larger amount in order to place a bid."
              end
            end
          else
            if params[:amount].to_f < minimum_amount_dollars#@auction.reserve_on_competitive_term
              raise FirstBidAmountException.new, "First bid amount is greater or equal than $250. Please enter a larger amount in order to place a bid."
            elsif params[:amount_publishing].to_f < minimum_amount_percentage #@auction.reserve_on_competitive_term_publishing
              raise FirstBidAmountException.new, "First bid amount is greater or equal than 1%. Please enter a larger amount in order to place a bid."
            end
          end
        end

        ################################ Amount increment validations #########################
        if @auction.auction_type == "recording_rights" || @auction.auction_type == "publishing_rights" || @auction.auction_type == "sub_publishing_rights"

          if @auction.competitive_term == "advance"
            last_amount = if @auction.auction_bids.last.nil? then minimum_amount_dollars else @auction.auction_bids.last.amount end

            required_amount = last_amount + increment_money
            if params[:amount].to_f < required_amount
              raise BidAmountIncrementException.new, "Bidding increment is USD$#{increment_money}, bid USD$#{'%.2f' % required_amount} or more to participate in this bid."
            end
          elsif @auction.competitive_term == "royalty_rate"
            last_amount = if @auction.auction_bids.last.nil? then minimum_amount_percentage else @auction.auction_bids.last.amount end

            required_amount = last_amount + increment_percentage
            if params[:amount].to_f < required_amount
              raise BidAmountIncrementException.new, "Percentage increment is #{increment_percentage}%, bid #{'%.2f' % required_amount.round(2)}% or more to participate in this bid."
            end
          end
        elsif @auction.auction_type == "recording_publishing_rights"
          last_amount = if @auction.auction_bids.last.nil? then minimum_amount_dollars else @auction.auction_bids.last.amount end
          last_amount_publishing = if @auction.auction_bids.last.nil? then minimum_amount_percentage else @auction.auction_bids.last.amount_publishing end

          required_amount = last_amount + increment_money
          if params[:amount].to_f < required_amount
            raise BidAmountIncrementException.new, "Bidding increment for recording is USD$#{'%.2f' % increment_money}, bid USD$#{'%.2f' % required_amount} or more to participate in this bid."
          end

          required_publishing_amount = last_amount_publishing + increment_money
          if params[:amount_publishing].to_f < required_publishing_amount
            raise BidAmountIncrementException.new, "Bidding increment for publishing is USD$#{'%.2f' % increment_money}, bid USD$#{'%.2f' % required_publishing_amount} or more to participate in this bid."
          end
        end

        reserve_met = false
        amount = params[:amount].to_f
        maximum_amount = params[:maximum_amount].to_f rescue nil

        comparative_increment = 1
        if @auction.competitive_term == 'advance'
          comparative_increment = 25
        end

        if not params.has_key?(:maximum_amount)
          maximum_amount = amount
        else
          Rails.logger.debug { "enter 1" }
          last_bidder_maximum = @auction.auction_bids.where(:auction_bidder.ne => @auction_bidder, :maximum_amount.gte => amount + comparative_increment).order_by('maximum_amount DESC').limit(1).first

          if last_bidder_maximum != nil && maximum_amount > last_bidder_maximum.maximum_amount && last_bidder_maximum.maximum_amount > @auction.reserve_on_competitive_term
            amount = last_bidder_maximum.maximum_amount + comparative_increment
            # if @auction.auction_bids.count == 1
            #   Rails.logger.debug { "enter 2" }
            #   amount = last_bidder_maximum.maximum_amount + 25
            # else
            #   Rails.logger.debug { "enter 3" }
            #   amount = maximum_amount
            # end
            drive_up = true

          elsif maximum_amount > @auction.reserve_on_competitive_term
            Rails.logger.debug { "enter 4" }
            if amount < @auction.reserve_on_competitive_term
              amount = @auction.reserve_on_competitive_term
              drive_up = true
            end
          elsif maximum_amount < @auction.reserve_on_competitive_term
            Rails.logger.debug { "enter 5" }
              amount = maximum_amount
          end
        end

        if drive_up && amount >= @auction.reserve_on_competitive_term
          reserve_met = true
        end

        @auction_bid = AuctionBid.new(auction: @auction,
                                      auction_bidder: @auction_bidder,
                                      amount: amount,
                                      maximum_amount: maximum_amount,
                                      amount_publishing: params[:amount_publishing],
                                      maximum_amount_publishing: params[:maximum_amount_publishing],
                                      drive_up: drive_up,
                                      reserve_met: reserve_met)

        if @auction_bid.save
          # AutomatedBids.automated_bid(@auction.permalink, true)
          # BidCompetitionWorker.perform_in(5.seconds, @auction.permalink)
          BidCompetitionWorker.perform_async(@auction.permalink)
          OutbidWorker.perform_async(@auction_bid.id.to_s)

          # Init kissmetrics log
          if params[:amount_publishing].nil?
            kiss_params = {'Auction name' => @auction.name, 'Recording rights' => params[:amount]}
          else
            kiss_params = {'Auction name' => @auction.name, 'Recording rights' => params[:amount], 'Publishing rights' => params[:amount_publishing]}
          end

          KissmetricsWorker.perform_async("User-#{@current_user.id.to_s}", "Bid", kiss_params) if @current_user.role != 'admin'
          # End kissmetrics log

          render :json => { message: "ok", profile: {name: @auction_bidder.profile.name, permalink: @auction_bidder.profile.permalink, default_avatar: @auction_bidder.profile.default_avatar, recording: last_bidder_bid_amount}}, :status => 201, location: nil
        end

      rescue InvalidBidderException => e
        render :json => { message: e.message }, status: :unprocessable_entity
      rescue OutOfTimeException => e
        render :json => { message: e.message }, status: :unprocessable_entity
      rescue FirstBidAmountException => e
        render :json => { message: e.message }, status: :unprocessable_entity
      rescue BidAmountIncrementException => e
        render :json => { message: e.message }, status: :unprocessable_entity
      rescue StandardError => e
        Rails.logger.debug { e }
        ExceptionNotifier.notify_exception(e)
        render :json => { message: 'Internal Server Error' }, status: :internal_server_error
      end

    end

    def show
    end

    private

    def restrict_access
      authenticate_or_request_with_http_token do |token, options|
        @current_user = User.where(access_token: token).first
        if not @current_user.nil?
          return true
        else
          head :unauthorized
        end
      end
    end

  end

  class OutOfTimeException < StandardError
    # attr_reader :object

    # def initialize(object)
    #   @object = object
    # end
  end
  class BidAmountIncrementException < StandardError; end
  class FirstBidAmountException < StandardError; end
  class InvalidBidderException < StandardError; end
end
