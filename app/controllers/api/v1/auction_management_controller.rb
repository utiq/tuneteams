module Api::V1
  class AuctionManagementController < Api::V1::ApplicationController
    respond_to :json
    before_filter :restrict_access#, :except => [:create]

    def index
      sleep 5
      filter = {}
      filter_hash = JSON.parse params[:filter]
      if filter_hash["name"] != ''
        filter[:name] = /.*#{filter_hash["name"]}.*/i
      end

      if params[:group] == 'participating'
        @auction_bidders = AuctionBidder.where(:profile_id.in => @current_user.profiles.distinct(:id), status: 'accepted')
        @auctions = Auction.where(:_id.in => @auction_bidders.distinct(:auction_id)).where(filter).page(params[:page_number]).per(params[:items_per_page]).order_by(params[:sorting])
      elsif params[:group] == 'owner'
        @auctions = Auction.where(:profile_id.in => @current_user.profiles.distinct(:id)).where(filter).page(params[:page_number]).per(params[:items_per_page]).order_by(params[:sorting])
        total_count = @auctions.total_count
      end

      auctions_result = ActiveModel::ArraySerializer.new(@auctions, each_serializer: AuctionIndexSerializer, scope: @current_user).serializable_array


      counter = 0
      render json: {auctions: auctions_result, total_count: total_count, counter: counter}
      # render json: @auctions, each_serializer: AuctionIndexSerializer, scope: @current_user
    end

    def cancel
      @auction = Auction.find(params[:auction_id])
      if @auction.status == 'approved' # TODO: change for the statuses code
        @auction.update_attributes(status: 'cancelled')
        CancelAuctionWorker.perform_async(@auction.id)
      end
      render :json => { message: 'ok', status: @auction.auction_status }, :status => 201
    end

    def create

    end

    def show
    end

    def materials_received
      @auction = Auction.find(params[:auction_id])
      if @auction.status == 'paid' # TODO: change for the statuses code
        @auction.update_attributes(status: 'materials_received')
      end
      render :json => { message: 'ok', auction_status: @auction.auction_status }, :status => 201
    end

    private

    def restrict_access
      authenticate_or_request_with_http_token do |token, options|
        @current_user = User.where(access_token: token).first
        if not @current_user.nil?
          return true
        else
          head :unauthorized
        end
      end
    end

  end

end
