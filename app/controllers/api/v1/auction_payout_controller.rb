module Api::V1
  class AuctionPayoutsController < Api::V1::ApplicationController
    respond_to :json
    before_filter :restrict_access#, :only => [:create]

    def index
    end

    def create
      
    end

    def update
      
    end

    private

    def restrict_access
      authenticate_or_request_with_http_token do |token, options|
        @current_user = User.where(access_token: token).first
        if not @current_user.nil?
          return true
        else
          head :unauthorized
        end
      end
    end
    
  end
end