module Api::V1
  class AuctionPositionsController < Api::V1::ApplicationController
    respond_to :json
    before_filter :restrict_access##, :only => [:create]

    def index
      if params.has_key?(:order_by)
        @auctions = Auction.where(status: 'approved').all.order_by(params[:order_by]).limit(20)
      else
        @auctions = Auction.where(status: 'approved').order_by("position ASC, updated_at DESC")
      end
      render json: @auctions, each_serializer: AuctionIndexSerializer, scope: @current_user
    end

    def create
      @auction = Auction.find(params[:id])
      @auction.move_to! :top
      render :json => { message: "ok" }, :status => 201
    end

    def update
      @auction = Auction.find(params[:id])
      @auction.move_to! params[:position]
      render :json => { message: "ok" }, :status => 201
    end

    def destroy
      @auction = Auction.find(params[:id])
      @auction.move_to! :bottom
      bottom_auction = Auction.find_by(position: params[:length])
      render :json => { message: "ok", bottom_auction: bottom_auction.as_json(:methods => [:default_illustration])}, :status => 201
    end

    private

    def restrict_access
      authenticate_or_request_with_http_token do |token, options|
        @current_user = User.where(access_token: token, role: "admin").first
        if not @current_user.nil?
          return true
        else
          head :unauthorized
        end
      end
    end

  end
end
