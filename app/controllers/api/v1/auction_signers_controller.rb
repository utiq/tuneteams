module Api::V1
  class AuctionSignersController < Api::V1::ApplicationController
    respond_to :json
    before_filter :restrict_access, :except => [:validate_second_signer]

    def index
    end

    def update
      auction = Auction.find_by(permalink: params[:auction_permalink])
      @signer = auction.auction_signers.find_by(email: params[:email])
      if @signer.update_attributes(name: params[:name], email: params[:temporal_email])
        render json: @signer
      else
        render :json => { "errors" => @signer.errors }, :status => :unprocessable_entity
      end
    end

    def send_validation_email
      AuctionSignerMailer.send_validation_email(params[:auction_id], params[:signer_email], params[:party_from]).deliver
      render json: { message: "ok" }, status: :ok
    end

    def validate_second_signer
      @auction = Auction.find_by(temporary_token: params[:token])
      logger.debug "lolas"
      logger.debug params
      if params[:from] == 'auction_manager'
        second_signer = @auction.auction_signers.where(email: params[:signer_email]).first
        second_signer.update_attributes(email_confirmed: true)
      elsif params[:from] == 'bidder'
        profile = Profile.find(params[:profile_id])
        profile.update_attributes(second_licensor_contact_email_confirmed: true)
      end

      render json: @auction, scope: @current_user
      # "auction_id"=>"55b16e75636573080b0c0000", "signer_email"=>"cesguty@gmail.com", "token"=>"f5HMMcEVrmwsGsEgCK63pA", "auction_signer"=>{}
    end

    private

    def restrict_access
      authenticate_or_request_with_http_token do |token, options|
        @current_user = User.where(access_token: token).first
        if not @current_user.nil?
          return true
        else
          head :unauthorized
        end
      end
    end

  end
end
