module Api::V1
  class AuctionSongsController < Api::V1::ApplicationController
    respond_to :json
    # before_filter :restrict_access, :only => [:index, :assign]

    def index
    end

    def create
      @auction = Auction.find_by(permalink: params[:auction_permalink])
      new_song = AuctionSong.new
      @song = @auction.auction_songs.push(new_song)

      render json: { song: new_song, validations: @auction.validations }, location: nil

      # else
      #   render :json => { "errors" => @song.errors }, :status => :unprocessable_entity
      # end
    end

    def update
      auction = Auction.find_by(permalink: params[:auction_permalink])
      @song = auction.auction_songs.find(params[:id])
      if @song.update_attributes(name: params[:name],
                                 writer: params[:writer],
                                 publisher: params[:publisher],
                                 recording_rights: params[:recording_rights],
                                 publishing_rights: params[:publishing_rights],
                                 external_audio_url: params[:external_audio_url])

        render json: { song: @song, validations: auction.validations }, :status => 201
      else
        render :json => { "errors" => @song.errors }, :status => :unprocessable_entity
      end
    end

    def show
      current_user = nil
      @auction = Auction.find_by(permalink: params[:id])
      if @auction.nil?
        render :json => { message: "404 Error - Page not found" }, :status => 404 and return
      end

      if get_header_user_token
        current_user = User.cached_find_by_access_token(get_header_user_token)
        kiss_identifier = "User-#{current_user.id.to_s}"
      else
        kiss_identifier = cookies[:km_ai]
      end
      # KissmetricsWorker.perform_async(kiss_identifier, "auction Viewed", {'auction Name' => @auction.name})

      render json: @auction and return#, scope: current_user and return
    end

    def destroy
      @auction = Auction.find_by(permalink: params[:auction_permalink])
      @song = @auction.auction_songs.find(params[:id])
      @auction.auction_songs.delete(@song)
      render :json => { "message" => "ok" }, :status => 201
    end

    def set_audio
      auction = Auction.find_by(permalink: params[:auction_permalink])
      @song =  auction.auction_songs.find(params[:id])
      @song.audio = params[:file]

      if @song.save
        # KissmetricsWorker.perform_async("User-#{@current_user.id.to_s}", "Auction Illustration Changed", {'auction Name' => @auction.name})
        # render :json => { "audio" => @song.audio }, :status => 201
        # render json: @auction, serializer: AuctionSerializer, :status => 201
        render json: { song: AuctionSongSerializer.new(@song), validations: auction.validations, songs: ActiveModel::ArraySerializer.new(auction.auction_songs, each_serializer: AuctionSongSerializer).serializable_array }, :status => 201
      else
        render :json => { "errors" => @song.errors }, :status => :unprocessable_entity
      end
    end

    private

    # def auction_params
    #   params.require(:auction).permit(:name, :description, :genre_ids => [], :country_ids => [])
    # end

    def get_header_user_token
      text = nil
      if request.headers.include?("HTTP_AUTHORIZATION")
        text = request.headers["HTTP_AUTHORIZATION"]
        text = text.split('=')[1]
      end
      return text
    end

    def restrict_access
      authenticate_or_request_with_http_token do |token, options|
        @current_user = User.where(access_token: token).first
        if not @current_user.nil?
          return true
        else
          head :unauthorized
        end
      end
    end

  end
end
