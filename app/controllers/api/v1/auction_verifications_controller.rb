module Api::V1
  class AuctionVerificationsController < Api::V1::ApplicationController
    respond_to :json
    before_filter :restrict_access

    def create
      totp = ROTP::TOTP.new("tuneteamsiscool")
      code = totp.now
      phone_number = "+#{params[:calling_code]}#{params[:phone_number]}"

      begin

        raise NoProfileConfiguredException.new "You don't have any default profile configured." if @current_user.default_profile.nil?

        twilio_response = TWILIO.account.messages.create({
          :from => '+19177461003',
          :to => phone_number,
          :body => "Hello from tuneteams. Your verification code is #{code}"
        })

        if params[:source] == 'bidder'
          @current_user.default_profile.update_attributes(
            licensor_phone_number_verification_code: code,
            licensor_calling_code: params[:calling_code],
            licensor_phone_number: params[:phone_number]
          )
        else params[:source] == 'auction_manager'
          @auction = Auction.find(params[:auction_id])
          @auction.update_attributes(
            licensor_phone_number_verification_code: code
          )
        end

        render json: { message: "ok" }, status: :ok
        # TODO: use this to validate users phones https://github.com/joost/phony_rails

      rescue NoProfileConfiguredException => e
        render :json => { message: e.message }, status: :precondition_failed
      rescue Twilio::REST::RequestError => e
        ExceptionNotifier.notify_exception(e)
        render :json => { message: e.message }, status: :unprocessable_entity
      rescue StandardError => e
        ExceptionNotifier.notify_exception(e)
        render :json => { message: 'Internal Server Error' }, status: :internal_server_error
      end

    end

    def verify
      if params[:source] == 'bidder'
        profile = @current_user.profiles.where(
          licensor_phone_number_verification_code: params[:verification_code],
          licensor_calling_code: params[:calling_code],
          licensor_phone_number: params[:phone_number]).first

        if profile.nil?
          render :json => { verified: false }, :status => 201
        else
          render :json => { verified: true }, :status => 201
          @current_user.default_profile.update_attributes(licensor_phone_number_verified: true)
        end
      else params[:source] == 'auction_manager'
        auction = Auction.where(
          licensor_phone_number_verification_code: params[:verification_code],
          licensor_calling_code: params[:calling_code],
          licensor_phone_number: params[:phone_number]).first

        if auction.nil?
          render :json => { verified: false }, :status => 201
        else

          # Validates all the profiles that have the same number
          profiles = @current_user.profiles.where(
            licensor_calling_code: params[:calling_code],
            licensor_phone_number: params[:phone_number]
          )
          profiles.update_all(licensor_phone_number_verified: true)

          # Validates the auction phone number
          auction.update_attributes(licensor_phone_number_verified: true)
          auction_result = AuctionSerializer.new(auction, scope: @current_user)
          render :json => { verified: true, auction: auction_result }, :status => 201
        end
      end
    end

    private

    def restrict_access
      authenticate_or_request_with_http_token do |token, options|
        @current_user = User.where(access_token: token).first
        if not @current_user.nil?
          return true
        else
          head :unauthorized
        end
      end
    end

  end
end

class NoProfileConfiguredException < StandardError; end
