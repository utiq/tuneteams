module Api::V1
  class AuctionVideosController < Api::V1::ApplicationController
    respond_to :json
    before_filter :restrict_access, :only => [:create, :update, :destroy]

    def index
    end

    def create
      # TODO: Exeptions
      @auction = Auction.find(params[:widget_id])
      @youtube_video = AuctionVideo.new(url: params[:url])
      @auction.auction_videos.push(@youtube_video)
      render :json => { "youtube_video" => @youtube_video }, :status => 201, location: nil
    end

    def update
      @auction = Auction.find(params[:widget_id])
      @auction_video = @auction.auction_videos.find(params[:id])
      @auction_video.update_attributes(url: params[:url])
      render :json => { "youtube_video" => @auction_video }, :status => 201
    end

    def destroy
      @auction = Auction.find(params[:widget_id])
      @youtube_video = @auction.auction_videos.find(params[:id])
      @auction.auction_videos.delete(@youtube_video)
      render :json => { "message" => "ok" }, :status => 201
    end

    private

    def restrict_access
      authenticate_or_request_with_http_token do |token, options|
        @current_user = User.where(access_token: token).first
        if not @current_user.nil?
          return true
        else
          head :unauthorized
        end
      end
    end
    
  end
end