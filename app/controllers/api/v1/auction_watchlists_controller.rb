module Api::V1
  class AuctionWatchlistsController < Api::V1::ApplicationController
    respond_to :json
    before_filter :restrict_access

    def create
      auction = Auction.find_by(permalink: params[:auction_permalink])
      @auction_watchlist = AuctionWatchlist.new(user: @current_user, auction: auction)
      if @auction_watchlist.save
        respond_with @auction_watchlist.as_json(:only => [:id]), location: nil
      else
        render :json => { "errors" => @auction_watchlist.errors }, :status => :unprocessable_entity
      end
    end

    def destroy
      auction = Auction.find_by(permalink: params[:id])
      @auction_watchlist = AuctionWatchlist.where(user: @current_user, auction: auction)
      if @auction_watchlist.destroy
        render :json => { "message" => "ok" }, :status => 201
      else
        render :json => { "errors" => @auction_watchlist.errors }, :status => :unprocessable_entity
      end
    end

    private

    def restrict_access
      authenticate_or_request_with_http_token do |token, options|
        @current_user = User.where(access_token: token).first
        if not @current_user.nil?
          return true
        else
          head :unauthorized
        end
      end
    end

  end
end
