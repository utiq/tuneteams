module Api::V1
  class AuctionsController < Api::V1::ApplicationController
    respond_to :json
    before_filter :restrict_access, :only => [:create, :update, :publish, :pay, :copy, :next_big_sound_search_artist, :restart]

    def index
      current_user = nil

      if get_header_user_token
        # TODO: Implement caching
        current_user = User.cached_find_by_access_token(get_header_user_token)
        kiss_identifier = "User-#{current_user.id.to_s}"
      else
        kiss_identifier = cookies[:km_ai]
      end

      # @last_bids_auctions = Auction.where(status: 'approved').limit(6).order_by(:'bids_count'.desc)
      @last_bids_auctions = Auction.where(status: 'approved', :recent_bid_position.gt => 0).order_by('recent_bid_position ASC').limit(6)
      results = ActiveModel::ArraySerializer.new(@last_bids_auctions, each_serializer: AuctionIndexSerializer, scope: @current_user).serializable_array

      render :json => { last_bids_auctions: results }

    end

    def promoted
      @auctions = Auction.promoted
      render json: @auctions, each_serializer: AuctionIndexSerializer
    end

    def related
      @auctions = Auction.where(status: "approved", copy_from: nil, :id.ne => params[:curent_auction_id]).order_by("position ASC").limit(4)
      render json: @auctions, each_serializer: AuctionIndexSerializer
    end

    def create
      @auction = Auction.new(auction_params)
      default_profile = @current_user.default_profile

      @auction.licensor_legal_name = default_profile.licensor_legal_name if default_profile.licensor_legal_name
      @auction.licensor_legal_address_line1 = default_profile.licensor_legal_address_line1 if default_profile.licensor_legal_address_line1
      @auction.licensor_legal_address_line2 = default_profile.licensor_legal_address_line2 if default_profile.licensor_legal_address_line2
      @auction.licensor_phone_number = default_profile.licensor_phone_number if default_profile.licensor_phone_number
      @auction.licensor_calling_code = default_profile.licensor_calling_code if default_profile.licensor_calling_code
      @auction.licensor_phone_number_verified = default_profile.licensor_phone_number_verified if default_profile.licensor_phone_number_verified
      @auction.licensor_legal_address_country = default_profile.licensor_legal_address_country if default_profile.licensor_legal_address_country

      # @auction.licensor_contact_email = default_profile.licensor_contact_email if default_profile.licensor_contact_email

      @auction.publishing_date = Time.now
      @auction.profile = default_profile



      if @auction.save
        licensor_contact_email = if default_profile.licensor_contact_email.nil? then @current_user.email else default_profile.licensor_contact_email end
        licensor_signer_name = if default_profile.licensor_signer_name then default_profile.licensor_signer_name else nil end
        @auction.auction_signers.push(AuctionSigner.new(email: licensor_contact_email,
                                                        name: licensor_signer_name))
        # if ENV["CURRENT_ENV"] == 'production'
        #   auctionsMailer.delay.new_auction_created_admin(@auction.permalink)
        # end

        payoneer = @current_user.payoneer_payee_url
        if @current_user.payoneer_info.accept_ach == false and @current_user.payoneer_info.accept_card == false and @current_user.payoneer_info.accept_iach == false
          AuctionsMailer.delay.set_up_payoneer(@current_user.id)
        end

        kiss_identifier = "User-#{@current_user.id.to_s}"
        KissmetricsWorker.perform_async(kiss_identifier, "Auction Created (Step1)", {'Auction name' => @auction.name }) if @current_user.role != 'admin'

        # respond_with @auction, location: nil
        render json: @auction, scope: @current_user, location: nil
      else
        render :json => { "errors" => @auction.errors }, :status => :unprocessable_entity
      end
    end

    def update

      @auction = Auction.find_by(permalink: params[:id])
      # kiss_identifier = "User-#{@current_user.id.to_s}"
      if params[:name] != @auction.name
        @auction.update_attributes(name: params[:name])
        @auction.generate_permalink
        # RecommendedIndex.recommended_profiles(@auction.permalink)
      end

      if auction_update_params[:auction_type] == "recording_publishing_rights"

        auction_update_params[:competitive_term] = nil

        # Add a singer for publishing rights if the info is diferent that the recording
        if params[:auction][:recording_publishing_different_info] == true
          if @auction.auction_signers[1].nil?
            @auction.auction_signers.push(AuctionSigner.new(email: params[:publisher_contact_email], name: params[:publisher_signer_name]))
          else
            @auction.auction_signers[1].update_attributes(email: params[:publisher_contact_email], name: params[:publisher_signer_name], email_confirmed: false)
          end
        end
      end


      # Update profile legal information for the first time. If the user wants to change the info, it has to do it in the Profile settings
      default_profile = @current_user.default_profile
      if default_profile.licensor_legal_name.nil?
        default_profile.update_attributes(licensor_legal_name: params[:licensor_legal_name])
      end
      if default_profile.licensor_legal_address_line1.nil?
        default_profile.update_attributes(licensor_legal_address_line1: params[:licensor_legal_address_line1])
      end
      if default_profile.licensor_legal_address_line2.nil?
        default_profile.update_attributes(licensor_legal_address_line2: params[:licensor_legal_address_line2])
      end
      if default_profile.licensor_legal_address_country.nil?
        default_profile.update_attributes(licensor_legal_address_country: params[:licensor_legal_address_country])
      end
      if default_profile.licensor_signer_name.nil?
        default_profile.update_attributes(licensor_signer_name: params[:licensor_signer_name])
      end
      if default_profile.licensor_contact_email.nil?
        default_profile.update_attributes(licensor_contact_email: params[:licensor_contact_email])
      end
      if default_profile.licensor_phone_number.nil?
        default_profile.update_attributes(licensor_phone_number: params[:licensor_phone_number])
      end
      if default_profile.licensor_calling_code.nil?
        default_profile.update_attributes(licensor_calling_code: params[:licensor_calling_code])
      end

      if not @auction.licensor_phone_number.nil?
        if params[:licensor_phone_number] != @auction.licensor_phone_number
          @auction.update_attributes(licensor_phone_number_verified: false)
        end
      end

      # Update Signer in case updated
      licensor_signer = @auction.auction_signers.find_by(email: params[:licensor_contact_email])
      licensor_signer.update_attributes(email: params[:licensor_contact_email],
                                        name: params[:licensor_signer_name])

      # @auction.auction_signers[0].update_attributes(email: params[:licensor_contact_email], name: params[:licensor_signer_name])
      logger.debug auction_update_params
      @auction.update(auction_update_params)

      kiss_identifier = "User-#{@current_user.id.to_s}"
      KissmetricsWorker.perform_async(kiss_identifier, "Auction Updated", {'Auction name' => @auction.name }) if @current_user.role != 'admin'

      # render json: @auction, scope: @current_user
      response = AuctionSerializer.new(@auction, scope: @current_user)
      render json: response
    end

    def show
      current_user = nil
      @auction = Auction.find_by(permalink: params[:id])
      if @auction.nil?
        render :json => { message: "404 Error - Page not found" }, :status => 404 and return
      end

      if get_header_user_token
        current_user = User.cached_find_by_access_token(get_header_user_token)
        kiss_identifier = "User-#{current_user.id.to_s}"
      else
        kiss_identifier = cookies[:km_ai]
      end
      KissmetricsWorker.perform_async(kiss_identifier, "Auction Viewed", {'Auction Name' => @auction.name})

      render json: @auction, scope: current_user and return
    end

    def set_illustration
      @auction = Auction.find_by(permalink: params[:auction_permalink])
      @auction.illustration = params[:file]

      if @auction.save
        # KissmetricsWorker.perform_async("User-#{@current_user.id.to_s}", "Auction Illustration Changed", {'auction Name' => @auction.name})
        render json: @auction, serializer: AuctionSerializer, :status => 201
      else
        render json: { "errors" => @auction.errors }, :status => :unprocessable_entity
      end
    end

    def publish
      @auction = Auction.find(params[:auction_id])
      current_time = Time.now
      @auction.update_attributes(status: "published", publishing_date: current_time)
      GenerateLicenseWorker.perform_async(@auction.id)
      RelatedCountriesCopyWorker.perform_async(@auction.id)
      # @auction.generate_license(@auction.id)

      kiss_identifier = "User-#{@current_user.id.to_s}"
      KissmetricsWorker.perform_async(kiss_identifier, "Auction Published", {'Auction name' => @auction.name }) if @current_user.role != 'admin'

      render json: @auction, scope: @current_user, serializer: AuctionSerializer, :status => 201, location: nil
    end

    def search
      # query = "(status:approved)"
      query = ''

      if params.has_key?(:name) and params[:name] != ''
        query += "name:*#{params[:name]}*"
      end

      params.each do |key, value|
        if key == "country" || key == "genre" || key == "auction_type"
          key = "genres._id" if key == "genre"
          key = "countries._id" if key == "country"
          if query == ''
            query += '('
          else
            query += ' AND ('
          end
          auction_types = value.split(',')
          auction_types.each_with_index do |at, index|
            query += "#{key}:#{at}"
            if index < auction_types.length - 1
              query += ' OR '

            end
            # Hardcoded because of the "All Countries" selection
            if key == 'countries._id' && !auction_types.include?('5420a193636573852c000000')
              query += ' OR countries._id:5420a193636573852c000000'
            end
            # Finish hardcode
          end
          query += ')'
        end
      end

      total_records = 0

      if query.empty?
        query += '(status:approved)'
      else
        query += ' AND (status:approved)'
      end

      if not params.has_key?(:q)
        @auctions = Auction.search query, "and", params[:offset], params[:limit]
        total_records = @auctions.records.total
      else
        if params[:q].nil?
          @auctions = []
        else
          logger.debug query
          @auctions = Auction.search "*#{params[:q]}*", "complex", params[:offset], params[:limit]
          total_records = @auctions.records.total
        end
      end
      kissmetrics_params_log

      render :json => {total: total_records, results: @auctions.as_json(), query: query }
      # render :json => {query: query}
    end

    def kissmetrics_params_log
      if get_header_user_token
        current_user = User.cached_find_by_access_token(get_header_user_token)
        kiss_identifier = "User-#{current_user.id.to_s}"
      else
        kiss_identifier = cookies[:km_ai]
      end

      kiss = {}
      params.each do |key, value|
        if key == "country" || key == "genre" || key == "auction_type" || key == "name"
          if key == 'country'
            kiss[:country] = Country.where(:_id.in => value.split(',')).map(&:name).join(",")
          end
          if key == 'genre'
            kiss[:genre] = Genre.where(:_id.in => value.split(',')).map(&:name).join(",")
          end
          if key == 'auction_type'
            kiss[:auction_type] = value.gsub("_", " ").split.map(&:capitalize)*' '
          end
          if key == 'name'
            kiss[:name] = value
          end
        elsif key == "q"
          kiss[:q] = value
        end
      end
      KissmetricsWorker.perform_async(kiss_identifier, "Auction Search", kiss)
    end

    def pay
      @auction = Auction.find_by(permalink: params[:id])

      # Get the credit card details submitted by the form
      token = params[:stripe_token]
      price = @auction.sub_total + @auction.transaction_fee[:credit_card]

      # Create the charge on Stripe's servers - this will charge the user's card
      begin
        if @current_user.stripe_customer_token.present?
          customer = Stripe::Customer.retrieve(@current_user.stripe_customer_token)
          # TODO: It's possible to save many cards for an user
          # card = customer.sources.create(:card => token)
          customer.card = token
          customer.save
          # Make the charge
          source = customer.id
        else
          customer = Stripe::Customer.create(
            :card => token,
            :email => @current_user.email,
            :description => @current_user.id.to_s
          )
          source = customer.id
          @current_user.update_attributes(:stripe_customer_token => customer.id)
        end

        charge = Stripe::Charge.create(
          :amount => (price * 100).to_i,
          :currency => "usd",
          :description => "#{@auction.name} auction payment",
          :customer => source
        )

        kiss_identifier = "User-#{@current_user.id.to_s}"
        KissmetricsWorker.perform_async(kiss_identifier, "Auction Paid", {'Auction name' => @auction.name }) if @current_user.role != 'admin'

        if @auction.update_attributes(payed_date: Time.now, status: 'paid')
          AuctionsMailer.delay.order_confirmation(@auction.id)
          AuctionsMailer.delay.order_confirmation_auction_manager(@auction.id)
        end

        render :json => { "charge" => charge }

      rescue Stripe::CardError => e
        # The card has been declined
        render :json => { "errors" => e }, :status => :unprocessable_entity
      end
    end

    def copy

      begin
        auction = Auction.find(params[:id])
        @auction_copy = auction.dup
        @auction_copy.copy_from = auction.id
        @auction_copy.status = 'draft'
        @auction_copy.due_date = nil
        @auction_copy.name = params[:name]

        # Check if old countries contains "Global Rights", global rights id is hardcoded with id "5420a193636573852c000000"
        raise Tuneteams::Error::CopyGlobalRightsSelected.new "You used All Countries (Global Rights) in the Territory of another copy of this auction. tuneteams agreements are exclusive agreements and can only be auctioned to one Licensee in each country." if auction.countries.where(:id => "5420a193636573852c000000").exists?

        # Check if new countries are in the list of old countries
        old_countries = auction.countries.map { |x| x.id.to_s }
        new_countries_id = params[:country_ids].split(',')
        new_countries_id.each do |new_country|
          raise Tuneteams::Error::CopyHaveSameCountries.new "You used <strong>#{Country.find(new_country).name}</strong> in the Territory of another copy of this auction. tuneteams agreements are exclusive agreements and can only be auctioned to one Licensee in each country." if old_countries.include?(new_country)
        end
        @auction_copy.country_ids = new_countries_id
        @auction_copy.created_at = Time.now
        @auction_copy.auction_signers.where(party_from: 'bidder').delete_all
        @auction_copy.auction_bids.delete_all
        @auction_copy.auction_payouts.delete_all
        @auction_copy.auction_bidders.delete_all

        # Copy illustration S3 Content
        filename = @auction_copy.illustration_filename
        if not filename.nil?
          bucket_name = ENV['AWS_S3_BUCKET']
          s3 = Aws::S3::Resource.new
          bucket = s3.bucket(bucket_name)
          if bucket.object("uploads/auction/illustration/#{auction.id}/#{filename}").exists?
            s3_client = Aws::S3::Client.new
            sizes = ["large_", "medium_", "small_", "thumb_", ""]
            sizes.each do |size|
              old_key = "#{bucket_name}/uploads/auction/illustration/#{auction.id}/#{size + filename}"
              new_key = "uploads/auction/illustration/#{@auction_copy.id}/#{size + filename}"
              s3_client.copy_object(bucket:bucket_name, key:new_key, copy_source:old_key, acl:'public-read')
            end
          end
        end

        if @auction_copy.save
          kiss_identifier = "User-#{@current_user.id.to_s}"
          KissmetricsWorker.perform_async(kiss_identifier, "Auction Copied", {'Auction name' => auction.name, 'New Auction Name' => @auction_copy.name }) if @current_user.role != 'admin'
          render json: {auction: AuctionSerializer.new(@auction_copy, scope: @current_user)}
        end

      rescue Tuneteams::Error::CopyGlobalRightsSelected => e
        render :json => { message: e.message }, status: :precondition_failed
      rescue Tuneteams::Error::CopyHaveSameCountries => e
        render :json => { message: e.message }, status: :precondition_failed
      rescue StandardError => e
        ExceptionNotifier.notify_exception(e)
        render :json => { message: 'Internal Server Error' }, status: :internal_server_error
      end
    end

    def restart
      @auction = Auction.find(params[:id])
      @auction.restart
      @auction.auction_bidders.each do |bidder|
        BiddersMailer.restart_from_manager(@auction.id, bidder.profile.user.email)
      end
      render json: @auction, scope: @current_user, serializer: AuctionSerializer, :status => 201, location: nil
    end

    def cancel_restart
      @auction = Auction.find(params[:id])
      if params[:token] == @auction.temporary_token
        if @auction.status == 'ended_no_bids'
          render json: { title: 'Error.', error: 'This action was already performed.' }, :status => :unprocessable_entity
        else
          @auction.update_attributes(status: 'ended_no_bids', due_date: Time.now)
          render json: { message: 'ok' }, :status => 201
        end
      else
        render json: { title: 'Invalid token.', error: 'Your token is invalid.' }, :status => :unprocessable_entity
      end
    end

    def destroy
      @auction = Auction.find(params[:id])
      # @auction = Auction.find_by(permalink: auction_permalink)
      @auction.auction_bids.delete_all
      @auction.auction_payouts.delete_all
      @auction.auction_bidders.delete_all
      @auction.auction_watchlist.delete_all
      if @auction.destroy
        render :json => { message: 'ok' }, status: :ok
      end
    end

    def recommended

      # @current_user = User.find_by(email: "cesguty@gmail.com")
      if get_header_user_token
        @current_user = User.cached_find_by_access_token(get_header_user_token)
      end

      if @current_user.nil? || @current_user == ''
        # If the visitor is a non-logged user
        country_code = request.location.country_code rescue 'US'
        recommended_auctions = []
        # country_code = 'US'
        if country_code.nil?
          recommended_auctions = ActiveModel::ArraySerializer.new(Auction.promoted, each_serializer: AuctionIndexSerializer).serializable_array
        else
          country = Country.find_by(iso2: country_code)
          if country.nil?
            recommended_auctions = ActiveModel::ArraySerializer.new(Auction.promoted, each_serializer: AuctionIndexSerializer).serializable_array
          else
            recommended_auctions_same_country = Auction.where(:country_ids.in => Array.new(1, country.id.to_s), status: 'approved').limit(8).order_by('position ASC')
            recommended_auctions_same_country.each do |recommended_auction_same_country|
              logger.debug recommended_auction_same_country.permalink
              recommended_auctions <<  AuctionIndexSerializer.new(recommended_auction_same_country)
            end
            recommended_auctions_different_country = Auction.where(:country_ids.nin => Array.new(1, country.id.to_s), status: 'approved').limit(8).order_by('position ASC')
            recommended_auctions_different_country.each do |recommended_auction_different_country|
              recommended_auctions <<  AuctionIndexSerializer.new(recommended_auction_different_country)
            end

            if recommended_auctions.length == 0
              recommended_auctions = ActiveModel::ArraySerializer.new(Auction.promoted, each_serializer: AuctionIndexSerializer).serializable_array
            end
          end
        end
      else
        # If the visitor is a logged-in user
        if @current_user.default_profile.nil? || @current_user.default_profile.related_auctions.count == 0
          recommended_auctions = ActiveModel::ArraySerializer.new(Auction.promoted, each_serializer: AuctionIndexSerializer).serializable_array
        else
          results = Auction.where(:id.in => @current_user.default_profile.related_auctions).limit(13)
          recommended_auctions = ActiveModel::ArraySerializer.new(results, each_serializer: AuctionIndexSerializer).serializable_array
        end
      end
      render json: recommended_auctions
    end

    def facebook_feed
      token = "1151709568177077|fu91uQCLELSkK41Qo54hM1Ya2Ao"
      @auction = Auction.find_by(permalink: params[:permalink])
      if @auction.nil?
        render :json => { message: "404 Error - Page not found" }, :status => 404 and return
      end

      @graph = Koala::Facebook::API.new(token)

      if @auction.facebook_page.nil? || @auction.facebook_page == ''
        render :json => { message: "Facebook page not configured" }, :status => 404 and return
      end

      page_name = @auction.facebook_page.gsub("https://www.facebook.com/", "").gsub("https://facebook.com/", "").gsub("http://www.facebook.com/", "").gsub("http://facebook.com/", "")

      if page_name.include? '-'
        if page_name.scan(/\d/).join('').is_number?
          page_name = page_name.scan(/\d/).join('')
        end
      end

      posts = @graph.get_connection(page_name, "posts",
                                    {limit: 10,
                                     fields: ['message', 'id', 'from', 'type', 'picture', 'link', 'created_time', 'updated_time']})

      fans = @graph.get_object(page_name, {fields: ["likes"]})
      render json: {posts: posts, fans: fans}
    end

    def twitter_timeline
      @auction = Auction.find_by(permalink: params[:permalink])
      if @auction.nil?
        render :json => { message: "404 Error - Page not found" }, :status => 404 and return
      end

      if @auction.twitter_username.nil? || @auction.twitter_username == ''
        render :json => { message: "Twitter not configured" }, :status => 404 and return
      end

      username = @auction.twitter_username.gsub("@", "")
      timeline = TWITTER.user_timeline(username, {count: 20, exclude_replies: true})
      followers_count = TWITTER.user(username).followers_count

      render json: {timeline: timeline, followers_count: followers_count}
    end

    def soundcloud_info

      @auction = Auction.find_by(permalink: params[:permalink])
      if @auction.nil?
        render :json => { message: "404 Error - Page not found" }, :status => 404 and return
      end

      if @auction.soundcloud_profile.nil? || @auction.soundcloud_profile == ''
        render :json => { message: "SoundCloud not configured" }, :status => 404 and return
      end

      client = Soundcloud.new(:client_id => '766463c050ecc487e8f595df3e460836')
      soundcloud_profile = client.get('/resolve', :url => @auction.soundcloud_profile)
      soundcloud_profile_id = soundcloud_profile.id
      tracks = client.get("/users/#{soundcloud_profile_id}/tracks")
      playback_counts = tracks.map(&:playback_count).compact
      sum_playback_counts = playback_counts.inject(:+)


      render json: {playback_counts: sum_playback_counts, track_count: soundcloud_profile.track_count}

    end

    def next_big_sound_metrics
      @auction = Auction.find_by(permalink: params[:permalink])
      if @auction.nil?
        render :json => { message: "404 Error - Page not found" }, :status => 404 and return
      end
      data = {}
      if !@auction.nbs_artist_id.nil? && @auction.nbs_artist_id != ''
        data = NextBigSoundLite::Metric.artist(@auction.nbs_artist_id, :start => 1.year.ago)
        # Begin Kissmetrics log
        if get_header_user_token
          current_user = User.cached_find_by_access_token(get_header_user_token)
          kiss_identifier = "User-#{current_user.id.to_s}"
        else
          kiss_identifier = cookies[:km_ai]
        end
        KissmetricsWorker.perform_async(kiss_identifier, "NBS API call", {'Query' => 'Artist Metrics' })
        # End Kissmetrics log
      end
      respond_with data
    end

    def next_big_sound_search_artist
      kiss_identifier = "User-#{@current_user.id.to_s}"
      KissmetricsWorker.perform_async(kiss_identifier, "NBS API call", {'Query' => 'Artist Search' })
      data = NextBigSoundLite::Artist.search(params[:q])
      respond_with data
    end

    private

    def auction_params
      params.require(:auction).permit(:name, :description, :genre_ids => [], :country_ids => [])
    end

    def auction_update_params
      params.require(:auction).permit(:description,
                                      {:genre_ids => []},
                                      {:country_ids => []},
                                      :auction_type,
                                      :competitive_term,
                                      :reserve_on_competitive_term,
                                      :reserve_on_competitive_term_publishing,
                                      :required_advance_or_royalty_rate,
                                      :required_advance_or_royalty_rate_publishing,
                                      :auction_length,
                                      :recording_term,
                                      :publishing_term,
                                      :publishing_agreement_type,
                                      :licensor_legal_name,
                                      :licensor_legal_address_line1,
                                      :licensor_legal_address_line2,
                                      :licensor_legal_address_country,
                                      :licensor_phone_number,
                                      :licensor_signer_name,
                                      :licensor_contact_email,
                                      :licensor_calling_code,
                                      :recording_publishing_different_info,
                                      :same_recording_and_publisher,
                                      :songwriter_name,
                                      :recording_artist_name,
                                      :publisher_legal_name,
                                      :publisher_legal_address_line1,
                                      :publisher_legal_address_line2,
                                      :publisher_legal_address_country,
                                      :publisher_phone_number,
                                      :publisher_signer_name,
                                      :publisher_contact_email,
                                      :publisher_calling_code,
                                      :applicable_law_for_license,
                                      :place_of_court,
                                      :notes,
                                      :facebook_page,
                                      :twitter_username,
                                      :spotify_monthly_listeners,
                                      :spotify_total_plays,
                                      :soundcloud_profile,
                                      :nbs_artist_id,
                                      :nbs_artist_name,
                                      :youtube_channel,
                                      :youtube_video_views,
                                      :youtube_video_views_info,
                                      :show_private_info)
    end

    def get_header_user_token
      text = nil
      if request.headers.include?("HTTP_AUTHORIZATION")
        text = request.headers["HTTP_AUTHORIZATION"]
        text = text.split('=')[1]
      end
      return text
    end

    def restrict_access
      authenticate_or_request_with_http_token do |token, options|
        @current_user = User.where(access_token: token).first
        if not @current_user.nil?
          return true
        else
          head :unauthorized
        end
      end
    end

  end
end

class String
  def is_number?
    true if Float(self) rescue false
  end
end
