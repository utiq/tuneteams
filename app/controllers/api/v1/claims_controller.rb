module Api::V1
  class ClaimsController < Api::V1::ApplicationController
    respond_to :json
    before_filter :restrict_access, :only => [:index, :assign]

    def index
      respond_with Claim.where(status: 'claimed')
    end

    def create
      # TODO: move this validation to model
      if Claim.where(email: params[:claim][:email], profile_id: params[:claim][:profile_id]).exists?
        render :json => { errors: JSON.parse("{\"email\": \"A request to claim this profile was received with this email\"}") }, :status => :unprocessable_entity and return
      end

      @claim = Claim.new(claim_params)
      if @claim.save
        respond_with @claim.as_json(:only => [:_id]), location: nil and return
      else
        render :json => { errors: @claim.errors }, :status => :unprocessable_entity and return
      end
    end

    def reject
      @claim = Claim.find(params[:id])
      if @claim.update_attributes(status: 'rejected')
        ClaimsMailer.delay.reject(@claim.id)
        render :json => { "message" => "ok" }, :status => 201
      else
        render :json => { "errors" => @claim.errors }, :status => :unprocessable_entity
      end
    end

    def assign
      # TODO: Check atomicity
      @claim = Claim.find(params[:id])

      @user = User.where(email: @claim.email).first
      if @user.nil?
        @claim.update_attributes(status: 'assigned')
        ClaimsMailer.assign_and_invite(@claim).deliver
      else
        if @user.profiles.length > 0
          render :json => { errors: JSON.parse("{\"user\": \"The user has a profile in tuneteams\"}") }, :status => :unprocessable_entity and return
        else
          @claim.update_attributes(status: 'assigned')
          @claim.profile.update_attributes(user: @user, has_owner: true, is_default: true)
          ClaimsMailer.assign(@claim).deliver
        end
      end

      respond_with @claim.as_json(:only => [:_id]), location: nil and return
    end

    private

    def claim_params
      params.require(:claim).permit(:name, :email, :profile_id)
    end

    def restrict_access
      authenticate_or_request_with_http_token do |token, options|
        @current_user = User.where(access_token: token).first
        if not @current_user.nil?
          return true
        else
          head :unauthorized
        end
      end
    end
    
  end
end