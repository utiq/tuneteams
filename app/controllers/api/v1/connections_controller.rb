module Api::V1
  class ConnectionsController < Api::V1::ApplicationController
    respond_to :json
    # before_filter :restrict_access, :only => [:show]

    def update_status
      @profile = Profile.find_by(permalink: params[:permalink])
      @connection = @profile.connections.find(params[:id])
      if @connection.update_attributes(status: params[:status])
        render :json => { message: "ok" }, :status => 201
      else
        render :json => { "errors" => @connection.errors }, :status => :unprocessable_entity
      end
    end

    private

    def restrict_access
      authenticate_or_request_with_http_token do |token, options|
        @user = User.where(access_token: token).first
        if @user.exists?
          return true
        else
          head :unauthorized
        end
      end
    end
  end
end