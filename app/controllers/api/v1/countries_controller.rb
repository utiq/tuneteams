module Api::V1
  class CountriesController < Api::V1::ApplicationController
    respond_to :json

    def index
      if params.has_key?("show_all")
        respond_with Country.where(status: "active").order_by("position ASC")
      else
        respond_with Country.where(:position.gt => 0, status: "active").order_by("position ASC")
      end
    end

  end
end
