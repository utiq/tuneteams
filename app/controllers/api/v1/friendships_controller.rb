module Api::V1
  class FriendshipsController < Api::V1::ApplicationController
    respond_to :json
    before_filter :restrict_access, :except => [:show]

    def index

    end

    def show
      # @profile = Profile.find(params[:id])
      # @profiles_result = []

      # @profile.friendships.where(status: 'accepted').each do |friendship|
      #   @profiles_result << friendship.friend
      # end
      
      # respond_with @profiles_result
    end

    def create
      # TODO: catch errors
      # TODO: check duplicity
      logger.debug params[:profile_id]
      @profile = Profile.find(params[:profile_id])
      @current_profile = @current_user.profiles.where(is_default: true).first
      @friend = @current_profile.friendships.build(friend: @profile, invitation_message: params[:invitation_message])
      @friend.save

      KissmetricsWorker.perform_async("User-#{@current_user.id.to_s}", "Invitation To Connect Sent", {'Profile Name' => @profile.name, 'Profile Invitation From' => @current_profile.name})

      if not @profile.has_owner
        # TODO: What happen if the admin forgot to put the email?
        FriendshipsMailer.delay.send_owner_notice(@profile.owner_contact_email, @profile.permalink)
      else
        FriendshipsMailer.delay.send_invitation(@current_profile.name, @current_profile.industry_role_profile.name, @profile.user.email, @profile.name, @profile.permalink, @friend.invitation_message)
      end
      
      render :json => { "message" => "ok" }, :status => 201
    end

    private

    def friendship_params
      params.require(:friendship_request).permit(:profile_id, :invitation_message)
    end

    def restrict_access
      authenticate_or_request_with_http_token do |token, options|
        @current_user = User.where(access_token: token).first
        if not @current_user.nil?
          return true
        else
          render :json => { :status => :unauthorized }
        end
      end
    end
    
  end
end