module Api::V1
  class GenresController < Api::V1::ApplicationController
    respond_to :json
    before_filter :restrict_access, :only => [:show]

    def index
      if params.has_key?(:order_by)
        respond_with Genre.all.order_by(params[:order_by])
      else
        respond_with Genre.all.order_by([[ :name_length, 1 ], [ :name, 1 ] ])
      end
    end

    def show
      respond_with @user
    end

    private

    def restrict_access
      authenticate_or_request_with_http_token do |token, options|
        @user = User.where(access_token: token).first
        if @user.exists?
          return true
        else
          head :unauthorized
        end
      end
    end
  end
end
