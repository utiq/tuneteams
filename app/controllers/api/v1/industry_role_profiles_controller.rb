module Api::V1
  class IndustryRoleProfilesController < Api::V1::ApplicationController
    respond_to :json

    def index
      result = []
      @industry_roles = IndustryRole.all.order_by('name ASC')
      @industry_roles.each do |industry_role|
        industry_role.industry_role_profiles.order_by('position ASC').each do |industry_role_profile|
          result << { id: industry_role_profile.id.to_s,
                      name: industry_role_profile.name,
                      industry_role: {id: industry_role.id.to_s, name: industry_role.name},
                      industry_role_id: industry_role.id.to_s
                    }
        end
      end
      render json: result
    end

    def show
      respond_with IndustryRoleProfile.find(params[:id]).as_json(:except => [:status, :industry_role_id])
    end

    def interests
      industry_role_profile = IndustryRoleProfile.find(params[:id])
      respond_with industry_role_profile.industry_role_profile_interests.as_json(:except => [:related_roles])
    end

  end
end
