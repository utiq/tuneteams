module Api::V1
  class IndustryRolesController < Api::V1::ApplicationController
    respond_to :json

    def index
      respond_with IndustryRole.all.as_json(:except => [:status])
    end

  end
end