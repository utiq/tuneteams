module Api::V1
  class InvitationsController < Api::V1::ApplicationController
    respond_to :json
    before_filter :restrict_access, :only => [:index, :update, :invite]

    def index
      logger.debug "params"
      logger.debug params[:status]
      if params[:status] == "invited"
        @invitations = Invitation.where(invitation_sent: true, status: params[:status]).order_by("created_at DESC")
      else
        @invitations = Invitation.where(invitation_sent: false, status: params[:status]).order_by("created_at DESC")
      end
      respond_with @invitations
    end

    def create
      @invitation = Invitation.new(invitation_params)
      @invitation.current_ip = if request.remote_ip.nil? then nil else request.remote_ip end
      # @invitation.city = if request.location.nil? then nil else request.location.city end
      # @invitation.country = if request.location.nil? then nil else request.location.country_code end
      if @invitation.save
        # kiss_identifier = cookies[:km_ai]
        # KissmetricsWorker.perform_async(kiss_identifier, "Invitation Request Sent", {'Email' => @invitation.email})
        respond_with @invitation.as_json(:only => [:token]), location: nil
      else
        render :json => { "errors" => @invitation.errors }, :status => :unprocessable_entity
      end
    end

    def update
      @invitation = Invitation.find(params[:id])
      if not params[:status].nil?
        if @invitation.update_attributes(status: params[:status])
          KissmetricsWorker.perform_async("User-#{@current_user.id.to_s}", "Invitation Request #{params[:status]}", {'Email' => @invitation.email})
          render :json => { "message" => "ok" }, :status => 201
        else
          render :json => { "errors" => @invitation.errors }, :status => :unprocessable_entity
        end
      end

    end

    def invite
      @invitation = Invitation.find(params[:id])
      if @invitation.update_attributes(invitation_sent: true, status: 'invited')
        KissmetricsWorker.perform_async("User-#{@current_user.id.to_s}", "Invitation Request Invited", {'Email' => @invitation.email})
        InvitationsMailer.delay.invite(@invitation.id)
        render :json => { "message" => "ok" }, :status => 201
      else
        render :json => { "errors" => @invitation.errors }, :status => :unprocessable_entity
      end
    end

    def personal
      @invitation = Invitation.new(email: params[:email], category: "personal", invitation_sent: true)
      if @invitation.save
        respond_with @invitation.as_json, location: nil
      else
        render :json => { "errors" => @invitation.errors }, :status => :unprocessable_entity
      end
    end

    private

    def invitation_params
      params.require(:invitation).permit(:email, :industry_role_profile_id)
    end

    def restrict_access
      authenticate_or_request_with_http_token do |token, options|
        @current_user = User.where(access_token: token).first
        if not @current_user.nil?
          return true
        else
          head :unauthorized
        end
      end
    end
  end
end
