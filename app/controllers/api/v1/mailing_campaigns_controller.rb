module Api::V1
  class MailingCampaignsController < Api::V1::ApplicationController
    respond_to :json
    # before_filter :restrict_access, :only => [:index, :assign]

    def index
      # mailchimp = Mailchimp::API.new(ENV['MAILCHIMP_API_KEY'])
      respond_with MailingCampaign.all.order_by("created_at DESC")
    end

    def create
      @mailing_campaign = MailingCampaign.new(mailing_campaign_create_params)
      if @mailing_campaign.save
        render :json => { message: "ok", id: @mailing_campaign.id.to_s }, :status => 201
      else
        render :json => { "errors" => @mailing_campaign.errors }, :status => :unprocessable_entity
      end
    end

    def show
      respond_with MailingCampaign.find(params[:id])
    end

    def update
      @mailing_campaign = MailingCampaign.find(params[:id])
      if @mailing_campaign.update_attributes(mailing_campaign_update_params)
        if params[:mailing_action] == "send_test"
          MailingCampaignsMailer.send_test(@mailing_campaign.subject, @mailing_campaign.body, @mailing_campaign.test_emails).deliver
        elsif params[:mailing_action] == "send"
          if @mailing_campaign.update_attributes(status: 'sent')
            MailingCampaignWorker.perform_async(@mailing_campaign.id.to_s)
          end
        end
        render :json => { message: "ok", mailing_campaign: @mailing_campaign}, :status => 201
      else
        render :json => { "errors" => @mailing_campaign.errors }, :status => :unprocessable_entity
      end
    end

    private

    def mailing_campaign_create_params
      params.require(:mailing_campaign).permit(:name)
    end

    def mailing_campaign_update_params
      params.require(:mailing_campaign).permit(:name, :subject, :body, :test_emails)
    end

    def restrict_access
      authenticate_or_request_with_http_token do |token, options|
        @current_user = User.where(access_token: token).first
        if not @current_user.nil?
          return true
        else
          head :unauthorized
        end
      end
    end
    
  end
end