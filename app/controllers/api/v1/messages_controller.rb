module Api::V1
  class MessagesController < Api::V1::ApplicationController
    respond_to :json
    before_filter :restrict_access

    def index
      # TODO: Optimize this
      last = ""
      messages_response = Array.new
      @current_profile = @current_user.profiles.where(permalink: params[:permalink]).first
      if not @current_profile.nil?

        if params[:status] == 'inbox'
          @conversations = @current_profile.received_messages.where(status: 'inbox').order_by("created_at DESC")
        elsif params[:status] == 'sent'
          # And they don't have response
          @conversations = @current_profile.sent_messages.order_by("created_at DESC")
        elsif params[:status] == 'archived'
          # The another person could have the message in the inbox
          @conversations = @current_profile.received_messages.where(status: 'archived').order_by("created_at DESC")
        end

        @conversations.each_with_index do |conversation, index|
          if conversation.message_id != last
            # messages_response << message
            avatar = conversation.sender.default_avatar rescue ''
            recipient_avatar = conversation.recipient.default_avatar rescue ''
            sender_name = conversation.sender.name rescue ''
            recipient_name = conversation.recipient.name rescue ''
            messages_response << { message_id: conversation.message.id,
                                   subject: conversation.message.subject,
                                   avatar: avatar,
                                   recipient_avatar: recipient_avatar,
                                   recipient_name: recipient_name,
                                   sender_name: sender_name,
                                   last_message: conversation.message.conversations.last,
                                   last_message_replied: ((conversation.message.conversations.last.sender_id == @current_profile.id))
                                  }
          end
          last = conversation.message_id
        end

        respond_with messages_response
      else
        render :json => { "errors" => "You can not read messages from another account" }, :status => :unauthorized
      end
    end

    def show
      @message = Message.find(params[:id])
      @current_profile = Profile.where(permalink: params[:permalink]).first

      @message.conversations.where(is_read: false, recipient: @current_profile).each do |conversation|
        conversation.update_attributes(is_read: true)
      end
      respond_with @message
    end

    def create
      @recipient_profile = Profile.find(params[:profile_id])
      @message = Message.create(subject: params[:subject])
      if params.has_key?(:sender_id) and params[:sender_id] != ""
        @sender_profile = Profile.find(params[:sender_id])
      else
        @sender_profile = @current_user.profiles.where(is_default: true).first
      end

      if @sender_profile.sent_messages.create(message: @message, body: params[:body], recipient: @recipient_profile)
        if @recipient_profile.user.notification_messages_or_invitations
          KissmetricsWorker.perform_async("User-#{@current_user.id.to_s}", "Conversation Started", {'Subject' => @message.subject, 'Recipient Profile' => @recipient_profile.name, 'Sender Profile' => @sender_profile.name})
          MessagesMailer.delay.message_sent(@recipient_profile.user.email, @recipient_profile.name, @sender_profile.name, params[:body], @message.id, @message.subject, @recipient_profile.permalink)
        end
        render :json => { "message" => "ok" }, :status => 201
      else
        render :json => { "errors" => @message.errors }, :status => :unprocessable_entity
      end
    end

    def reply
      # TODO: Validate if the user who created is part of the conversation
      @message = Message.find(params[:id])
      sender = @current_user.profiles.where(is_default: true).first

      if @message.conversations.first.sender == sender
        recipient = @message.conversations.first.recipient
      else
        recipient = @message.conversations.first.sender
      end

      body = params[:body]
      @conversation = {sender: sender, recipient: recipient, body: body}
      if @message.conversations.create(@conversation)
        if recipient.user.notification_messages_or_invitations
          KissmetricsWorker.perform_async("User-#{@current_user.id.to_s}", "Conversation Replied", {'Subject' => @message.subject, 'Recipient Profile' => recipient.name, 'Sender Profile' => sender.name})
          MessagesMailer.delay.message_sent(recipient.user.email, recipient.name, sender.name, body, @message.id, @message.subject, recipient.permalink)
        end
        render :json => @message.conversations.last
      else
        render :json => { "errors" => @message.errors }, :status => :unprocessable_entity
      end
    end

    def archive
      # TODO: check if the user who created is archiving
      @message = Message.find(params[:id])
      recipient = @current_user.profiles.where(is_default: true).first
      @message.conversations.where(recipient: recipient).update_all(status: 'archived')
      render :json => { "message" => "ok" }, :status => 201
    end

    def destroy
      @message = Message.find(params[:id])
      @message.conversations.delete_all
      if @message.destroy
        render :json => { "message" => "ok" }, :status => 201
      end
    end

    private

    def restrict_access
      authenticate_or_request_with_http_token do |token, options|
        @current_user = User.where(access_token: token).first
        if not @current_user.nil?
          return true
        else
          render :json => { :status => :unauthorized }
        end
      end
    end

  end
end
