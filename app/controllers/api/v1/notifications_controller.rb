module Api::V1
  class NotificationsController < Api::V1::ApplicationController
    respond_to :json
    before_filter :restrict_access

    def index
      @current_profile = @current_user.profiles.where(permalink: params[:permalink]).first
      if not @current_profile.nil?
        friendships = Friendship.where(:friend => @current_profile, status: "pending").order_by("created_at DESC")
        respond_with friendships
      else
        render :json => { "errors" => "You can not read notifications from another account" }, :status => :unauthorized
      end
    end

    def update
      @friendship = Friendship.find(params[:id])
      @friend = @friendship.friend #Profile.find(@friendship.friend_id)
      if @friendship.update_attributes(status: params[:decision])
        if @friendship.status == "accepted"
          KissmetricsWorker.perform_async("User-#{@current_user.id.to_s}", "Invitation To Connect Accepted", {'Profile Name' => @friend.name, 'Profile Invitation From' => @friendship.owner.name})
          FriendAcceptWorker.perform_async(@friendship.owner.id.to_s, @friendship.friend.id.to_s)
        elsif @friendship.status == "ignored"
          KissmetricsWorker.perform_async("User-#{@current_user.id.to_s}", "Invitation To Connect Ignored", {'Profile Name' => @friend.name, 'Profile Invitation From' => @friendship.owner.name})
        end
      end
      
      render :json => { "message" => "ok" }, :status => 201
    end

    private

    def restrict_access
      authenticate_or_request_with_http_token do |token, options|
        @current_user = User.where(access_token: token).first
        if not @current_user.nil?
          return true
        else
          render :json => { :status => :unauthorized }
        end
      end
    end

  end
end