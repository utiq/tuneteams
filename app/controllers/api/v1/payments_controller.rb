# require 'openssl'
# require 'Base64' # This is not working on linux server, check why

module Api::V1
  class PaymentsController < Api::V1::ApplicationController
    respond_to :json
    # before_filter :restrict_access

    def index
      key = "secret-key"
      data = "some data to be signed"
      lol = Base64.encode64(OpenSSL::HMAC.digest(OpenSSL::Digest.new('sha256'), key, data)).strip()
      respond_with lol
    end

    def bluesnap_response
      render :json => { message: "Hello" } and return
    end
  end
end