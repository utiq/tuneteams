module Api::V1
  class PayoneerSettingsController < Api::V1::ApplicationController
    respond_to :json
    before_filter :restrict_access, :except => [:payoneer_listener]

    def index
    end

    def show
      url = @current_user.payoneer_payee_url
      details = @current_user.payoneer_info
      render :json => { signup_url: url, details: details }
    end

    def create

    end

    def update

    end

    def payoneer_listener
      @user = User.find(params[:payeeid])
      if params.has_key?(:payout_method_registered)
        case params[:payout_method_registered]
        when 'ach'
          @user.payoneer_info.update_attributes(accept_ach: true)
        when 'iach'
          @user.payoneer_info.update_attributes(accept_iach: true)
        when 'card'
          @user.payoneer_info.update_attributes(accept_card: true)
        end
      end
      if params.has_key?(:status)
        @user.payoneer_info.update_attributes(status: params[:status])
      end

      render :json => { "message" => "ok" }, :status => 201
    end

    private

    def restrict_access
      authenticate_or_request_with_http_token do |token, options|
        @current_user = User.where(access_token: token).first
        if not @current_user.nil?
          return true
        else
          render :json => { :status => :unauthorized }
        end
      end
    end
  end
end
