module Api::V1
  class ProfilePositionsController < Api::V1::ApplicationController
    respond_to :json
    before_filter :restrict_access

    def index
      if params.has_key?(:order_by)
        @profiles = Profile.all.order_by(params[:order_by]).limit(20)
      else
        @profiles = Profile.all.order_by("position ASC, updated_at DESC").limit(20)
      end
      render json: @profiles, each_serializer: ProfileIndexSerializer, scope: @current_user
    end

    def create
      @profile = Profile.find(params[:id])
      @profile.move_to! :top
      render :json => { message: "ok" }, :status => 201
    end

    def update
      @profile = Profile.find(params[:id])
      @profile.move_to! params[:position]
      render :json => { message: "ok" }, :status => 201
    end

    def destroy
      @profile = Profile.find(params[:id])
      @profile.move_to! :bottom
      bottom_auction = Profile.find_by(position: params[:length])
      render :json => { message: "ok", bottom_auction: bottom_auction.as_json(:methods => [:default_illustration])}, :status => 201
    end

    private

    def restrict_access
      authenticate_or_request_with_http_token do |token, options|
        @current_user = User.where(access_token: token, role: "admin").first
        if not @current_user.nil?
          return true
        else
          head :unauthorized
        end
      end
    end

  end
end
