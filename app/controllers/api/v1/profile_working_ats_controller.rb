module Api::V1
  class ProfileWorkingAtsController < Api::V1::ApplicationController
    respond_to :json
    before_filter :restrict_access

    def index
    end

    def show
    end

    def create
      @profile = Profile.find(params[:profile_id])
      if params.has_key?(:profile_working_at_id)
        @new_profile = Profile.find(params[:profile_working_at_id])
        @profile_working_at = ProfileWorkingAt.new(profile_id: @new_profile.id.to_s,
                                                   name: @new_profile.name,
                                                   profile_permalink: @new_profile.permalink,
                                                   added_manually: false)
      else
        @profile_working_at = ProfileWorkingAt.new(name: params[:name],
                                                   added_manually: true)
      end
      
      @profile.profile_working_at = @profile_working_at
      render :json => { "message" => "ok",
                        name: @profile.profile_working_at.name,
                        profile_permalink: @profile.profile_working_at.profile_permalink }, :status => 201
    end

    private

    def restrict_access
      authenticate_or_request_with_http_token do |token, options|
        @current_user = User.where(access_token: token).first
        if not @current_user.nil?
          return true
        else
          render :json => { :status => :unauthorized }
        end
      end
    end
    
  end
end