module Api::V1
  class ProfilesController < Api::V1::ApplicationController
    respond_to :json
    before_filter :restrict_access, :only => [:index, :create, :update, :destroy, :set_profile_picture, :set_profile_cover, :switch]

    def index
      @profiles = @current_user.profiles.order_by('created_at ASC')
      render json: @profiles, each_serializer: ProfileIndexSerializer
    end

    def promoted
      @profiles = Profile.promoted
      render json: @profiles, each_serializer: ProfileIndexSerializer
    end

    def show
      current_user = nil
      @profile = Profile.find_by(permalink: params[:id])
      if @profile.nil?
        render :json => { message: "404 Error - Page not found" }, :status => 404 and return
      end

      if get_header_user_token
        current_user = User.cached_find_by_access_token(get_header_user_token)
        kiss_identifier = "User-#{current_user.id.to_s}"
      else
        kiss_identifier = cookies[:km_ai]
      end
      KissmetricsWorker.perform_async(kiss_identifier, "Profile Viewed", {'Profile Name' => @profile.name})

      render json: @profile, scope: current_user and return
    end

    def create
      @profile = Profile.new(profile_params)
      @profile.user_id = @current_user.id
      kiss_identifier = "User-#{@current_user.id.to_s}"

      if @profile.save
        if ENV["CURRENT_ENV"] == 'production'
          ProfilesMailer.delay.new_profile_created_admin(@profile.permalink)
        end
        # Set the new profile as a default profile
        if @current_user.profiles.count > 1
          if @current_user.profiles.update_all(is_default: false)
            @profile.update_attributes(is_default: true)
          end
        else
          @profile.update_attributes(is_default: true)
        end
        if @current_user.role != 'admin'
          KissmetricsWorker.perform_async(kiss_identifier, "Profile Created (Step1)", {'Profile name' => @profile.name })
        end
        ProfilesMailer.delay.new_profile_created(@current_user.email, @profile.name, @profile.permalink, @profile.country.name, @profile.industry_role_profile.name)
        respond_with @profile.as_json(:only => [:permalink]), location: nil
      else
        render :json => { "errors" => @profile.errors }, :status => :unprocessable_entity
      end
    end

    def update

      @profile = Profile.find_by(permalink: params[:id])
      kiss_identifier = "User-#{@current_user.id.to_s}"

      # This is part of a section
      if params[:overview]
        @section = Section.where(industry_role_profile_id: @profile.industry_role_profile_id, content_type: "overview").first
        @widget = Widget.where(profile_id: @profile.id, section_id: @section.id).first
        @widget.widget_overview.update_attributes(description: params[:overview])
        # Kissmetrics
        KissmetricsWorker.perform_async(kiss_identifier, "Profile Overview Updated", {'Profile Name' => @profile.name}) if @current_user.role != 'admin'
      end

      # This is part of a section
      if params[:interests]
        @section = Section.where(industry_role_profile_id: @profile.industry_role_profile_id, content_type: "interest").first
        @widget = Widget.where(profile_id: @profile.id, section_id: @section.id).first
        @widget.widget_interest.update_attributes(interests: params[:interests], countries: params[:interestedCountries])
        # Kissmetrics
        KissmetricsWorker.perform_async(kiss_identifier, "Profile Interests Updated", {'Profile Name' => @profile.name}) if @current_user.role != 'admin'
      end

      if params[:web_page]
        @profile.update_attributes(web_page: params[:web_page])
        # Kissmetrics
        KissmetricsWorker.perform_async(kiss_identifier, "Profile Web Page Updated", {'Profile Name' => @profile.name}) if @current_user.role != 'admin'
      end

      if params[:country_id]
        @profile.update_attributes(country_id: params[:country_id])
        # Kissmetrics
        KissmetricsWorker.perform_async(kiss_identifier, "Profile Country Updated", {'Profile Name' => @profile.name}) if @current_user.role != 'admin'
      end

      if params[:genres_ids]
        @profile.update_attributes(genre_ids: params[:genres_ids])
        # Kissmetrics
        KissmetricsWorker.perform_async(kiss_identifier, "Profile Genres Updated", {'Profile Name' => @profile.name}) if @current_user.role != 'admin'
      end

      if params[:has_owner]
        @profile.update_attributes(has_owner: params[:has_owner])
      end

      if params[:cover_type]
        @profile.update_attributes(cover_type: params[:cover_type])
        # Kissmetrics
        KissmetricsWorker.perform_async(kiss_identifier, "Profile Cover changed to #{params[:cover_type]}", {'Profile Name' => @profile.name}) if @current_user.role != 'admin'
      end

      if params[:show_connections_list]
        @profile.update_attributes(show_connections_list: params[:show_connections_list])
        # Kissmetrics
        KissmetricsWorker.perform_async(kiss_identifier, "Profile Show Connections List Updated", {'Profile Name' => @profile.name}) if @current_user.role != 'admin'
      end

      if params[:who_can_see_connection_list]
        @profile.update_attributes(who_can_see_connection_list: params[:who_can_see_connection_list])
      end

      if params[:owner_contact_email]
        @profile.update_attributes(owner_contact_email: params[:owner_contact_email])
      end

      if params[:tour_viewed]
        @profile.update_attributes(tour_viewed: params[:tour_viewed])
      end

      if params[:name]
        @profile.update_attributes(name: params[:name])
      end

      if get_header_user_token
        current_user = User.find_by(access_token: get_header_user_token)
      end
      render json: @profile, scope: current_user
    end

    def destroy
      # TODO: Atomicity
      if @current_user.role == 'admin' or @current_user.profiles.where(id: params[:id]).exists?
        DeleteProfileWorker.perform_async(params[:id])
        render :json => { message: "ok" }, :status => 201
      end
    end

    def search2
      comma_params = ""
      params.each do |key, value|
        if key == "genre" || key == "country" || key == "industry_role"
          comma_params += value + ","
        end
      end
      comma_params = comma_params[0, comma_params.length-1]
      total_records = 0

      if params.has_key?(:deal_type)
        # TODO: See the way of index the content text in elastic search
        @industry_role_profile = IndustryRoleProfile.where(name: params[:industry_role]).first if params.has_key?(:industry_role)
        @industry_role_profile_interest = @industry_role_profile.industry_role_profile_interests.where(:alias => params[:deal_type]).first
        comma_params += "," if comma_params != ""
        comma_params += @industry_role_profile_interest.id
      end

      if params.has_key?(:genre) || params.has_key?(:country) || params.has_key?(:industry_role) || params.has_key?(:deal_type)
        @profiles = Profile.search comma_params, "and", params[:offset], params[:limit]
        total_records = @profiles.records.total
      elsif params.has_key?(:type)
        if params[:q].nil?
          @profiles = []
        else
          @profiles = Profile.search "*#{params[:q]}*", params[:type], 0, 0
        end
      else
        if params[:q].nil?
          @profiles = []
        else
          @profiles = Profile.search "*#{params[:q]}*", "complex", params[:offset], params[:limit]
          total_records = @profiles.records.total
        end
      end
      kissmetrics_params_log
      render :json => {total: total_records, results: @profiles.as_json() }
    end

    def search
      # query = "(status:approved)"
      query = ''

      if params.has_key?(:name) and params[:name] != ''
        query += "name:*#{params[:name]}*"
      end

      params.each do |key, value|
        if key == 'country' || key == 'genre' || key == 'industry_role' || key == 'deal_type'
          key = 'genres._id' if key == 'genre'
          key = 'country._id' if key == 'country'
          key = 'industry_role_profile._id' if key == 'industry_role'
          key = 'widgets.widget_interest.interests' if key == 'deal_type'
          if query == ''
            query += '('
          else
            query += ' AND ('
          end
          auction_types = value.split(',')
          auction_types.each_with_index do |at, index|
            query += "#{key}:#{at}"
            if index < auction_types.length - 1
              query += ' OR '
            end
          end
          query += ')'
        end
      end

      total_records = 0

      # For empty query
      if query.empty?
        query += '(_exists_:name)'
      else
        query += ' AND (_exists_:name)'
      end

      logger.debug query

      if not params.has_key?(:q)
        @profiles = Profile.search query, "and", params[:offset], params[:limit]
        total_records = @profiles.records.total
      else
        if params[:q].nil?
          @profiles = []
        else
          logger.debug query
          @profiles = Profile.search "*#{params[:q]}*", "complex", params[:offset], params[:limit]
          total_records = @profiles.records.total
        end
      end
      kissmetrics_params_log

      render :json => {total: total_records, results: @profiles.as_json(), query: query }
    end

    def switch
      logger.debug "Enter Switch"
      @current_user.profiles.update_all(is_default: false)
      @default_profile = @current_user.profiles.find_by(permalink: params[:permalink])
      if @default_profile.update_attributes(is_default: true)
        if @current_user.role != 'admin'
          KissmetricsWorker.perform_async("User-#{@current_user.id.to_s}", "Profile Switched", {'Profile Name' => @default_profile.name})
        end
        render :json => { message: "ok" }, :status => 201
      end
    end

    def get_connections
      @connections_result = []
      @profile = Profile.find_by(permalink: params[:permalink])

      if get_header_user_token
        current_user = User.cached_find_by_access_token(get_header_user_token)
        kiss_identifier = "User-#{current_user.id.to_s}"
      else
        kiss_identifier = cookies[:km_ai]
      end
      KissmetricsWorker.perform_async(kiss_identifier, "Viewed Connections List", {'Profile Name' => @profile.name})

      @profile.connections.each do |connection|
        if current_user
          @default_profile = current_user.profiles.where(is_default: true).first
          if @default_profile.are_friends? (connection.profile_id)
            connection["can_send_message"] = true
          else
            connection["can_send_message"] = false
          end
        else
          connection["can_send_message"] = false
        end
        @connections_result << connection
      end
      respond_with @connections_result
    end

    def set_profile_picture
      @profile = Profile.find_by(permalink: params[:profile_permalink])
      @profile.avatar = params[:file]
      @profile.avatar_type = 'customized'

      if @profile.save
        UpdateProfileAvatarWorker.perform_async(@profile.id.to_s)
        if @current_user.role != 'admin'
          KissmetricsWorker.perform_async("User-#{@current_user.id.to_s}", "Profile Avatar Changed", {'Profile Name' => @profile.name})
        end
        render :json => { "avatar" => @profile.avatar }, :status => 201
      else
        render :json => { "errors" => @profile.errors }, :status => :unprocessable_entity
      end
    end

    def set_profile_cover
      @profile = Profile.find_by(permalink: params[:profile_permalink])
      @profile.cover = params[:file]
      @profile.cover_type = 'customized'

      if @profile.save
        if @current_user.role != 'admin'
          KissmetricsWorker.perform_async("User-#{@current_user.id.to_s}", "Profile Cover Changed", {'Profile Name' => @profile.name})
        end
        render :json => { "cover" => @profile.cover }, :status => 201
      else
        render :json => { "errors" => @profile.errors }, :status => :unprocessable_entity
      end
    end

    def recommended

      current_profile_permalink = params[:current_profile]
      # @current_user = User.find_by(email: "cesguty@gmail.com")
      if get_header_user_token
        @current_user = User.cached_find_by_access_token(get_header_user_token)
      end

      if @current_user.nil? || @current_user == ''
        # If the visitor is a non-logged user
        country_code = request.location.country_code rescue 'US'
        recommended_profiles = []
        # country_code = 'US'
        if country_code.nil?
          recommended_profiles = ActiveModel::ArraySerializer.new(Profile.promoted, each_serializer: ProfileIndexSerializer).serializable_array
        else
          country = Country.find_by(iso2: country_code)
          if country.nil?
            recommended_profiles = ActiveModel::ArraySerializer.new(Profile.promoted, each_serializer: ProfileIndexSerializer).serializable_array
          else
            recommended_profiles_same_country = Profile.where(country: country).limit(8)#.order_by('rating DESC')
            recommended_profiles_same_country.each do |recommended_profile_same_country|
              logger.debug recommended_profile_same_country.permalink
              if recommended_profile_same_country.permalink != current_profile_permalink
                recommended_profiles <<  ProfileIndexSerializer.new(recommended_profile_same_country)
              end
            end
            recommended_profiles_different_country = Profile.where(:country.ne => country).limit(8)#.order_by('rating DESC')
            recommended_profiles_different_country.each do |recommended_profile_different_country|
              if recommended_profile_different_country.permalink != current_profile_permalink
                recommended_profiles <<  ProfileIndexSerializer.new(recommended_profile_different_country)
              end
            end
          end
        end
      else
        # If the visitor is a logged-in user
        if @current_user.default_profile.nil?
          recommended_profiles = ActiveModel::ArraySerializer.new(Profile.promoted, each_serializer: ProfileIndexSerializer).serializable_array
        else
          if @current_user.default_profile.related_profiles.count == 0
            recommended_profiles = ActiveModel::ArraySerializer.new(Profile.promoted(@current_user.default_profile.id.to_s), each_serializer: ProfileIndexSerializer).serializable_array
          else
            results = Profile.where(:id.in => @current_user.default_profile.related_profiles).limit(13)
            recommended_profiles = ActiveModel::ArraySerializer.new(results.shuffle, each_serializer: ProfileIndexSerializer).serializable_array
          end
        end
      end

      # respond_with recommended_profiles
      render json: recommended_profiles
    end

    private

    def get_header_user_token
      text = nil
      if request.headers.include?("HTTP_AUTHORIZATION")
        text = request.headers["HTTP_AUTHORIZATION"]
        text = text.split('=')[1]
      end
      return text
    end

    def profile_params
      params.require(:profile).permit(:name, :country_id, :industry_role_profile_id, :user_id, :has_owner, :owner_contact_email)
    end

    def kissmetrics_params_log
      if get_header_user_token
        current_user = User.cached_find_by_access_token(get_header_user_token)
        kiss_identifier = "User-#{current_user.id.to_s}"
      else
        kiss_identifier = cookies[:km_ai]
      end

      kiss = {}
      params.each do |key, value|
        if key == "genre" || key == "country" || key == "industry_role" || key == "deal_type" || key == "q"
          if key == "country"
            value = Country.any_in(id: value.split(',')).map(&:name).join(",")
          end
          if key == "industry_role"
            value = IndustryRoleProfile.find(value).name
          end
          if key == "genre"
            value = Genre.any_in(id: value.split(',')).map(&:name).join(",")
          end
          if key == "deal_type"
            temp_value = []
            values = value.split(',')
            values.each do |v|
              # TODO: All_in is not working, and this solution is bad
              temp_value << IndustryRoleProfile.where("industry_role_profile_interests._id" => BSON::ObjectId.from_string(v)).first.industry_role_profile_interests.find(v).alias
            end
            value = temp_value.join(",")
          end

          logger.debug value
          kiss.store(key.gsub("_", " ").split.map(&:capitalize)*' ', value)
        end
      end
      KissmetricsWorker.perform_async(kiss_identifier, "Profile Search", kiss)
    end

    def restrict_access
      authenticate_or_request_with_http_token do |token, options|
        @current_user = User.where(access_token: token).first
        if not @current_user.nil?
          return true
        else
          render :json => { :status => :unauthorized }
        end
      end
    end

  end
end
