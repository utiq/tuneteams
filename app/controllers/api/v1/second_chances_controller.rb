module Api::V1
  class SecondChancesController < Api::V1::ApplicationController
    respond_to :json
    before_filter :restrict_access

    def index

    end

    def show
      @auction = Auction.find(params[:auction_id])
      @auction_bidder = AuctionBidder.find(params[:id])

      max_amount = @auction.auction_bids.where(auction_bidder: @auction_bidder).order_by('amount DESC').limit(1).first.amount

      render :json => { max_amount: max_amount, is_valid: @auction_bidder.second_offer_sent, second_offer_status: @auction_bidder.second_offer_status }, status: :ok
    end

    def create
      @auction = Auction.find(params[:auction_id])
      @auction.auction_bidders.update_all(second_offer_sent: false)

      if @auction.number_restart > 0
        @auction.update_attributes(status: "ended_no_bids", due_date: Time.zone.now)
      end

      @auction_bidder = AuctionBidder.find(params[:auction_bidder])

      max_amount = @auction.auction_bids.where(auction_bidder: @auction_bidder).order_by('amount DESC').limit(1).first.amount

      @auction_bidder.update_attributes(second_offer_sent: true)

      BiddersMailer.second_chance_offer(@auction.id, @auction_bidder.profile.id.to_s, max_amount, @auction_bidder.id.to_s).deliver

      auction_response = AuctionSerializer.new(@auction, scope: @current_user)
      render :json => { auction: auction_response }, status: :ok
    end

    def accept
      # This is the bidder who accepted the second chance offer
      @auction = Auction.find(params[:auction_id])

      @auction_bidder = AuctionBidder.find(params[:auction_bidder_id])

      if @auction_bidder.second_offer_sent
        @auction_bidder.update_attributes(is_winner: true, second_offer_status: 'accepted')
        @auction.update_attributes(status: 'waiting_sign')


        SecondChanceAcceptedWorker.perform_async(@auction.id, @auction_bidder.id)
      end

      render :json => { message: "ok" }, status: :ok
    end

    def decline
      # This is the bidder who accepted the second chance offer
      @auction = Auction.find(params[:auction_id])

      @auction_bidder = AuctionBidder.find(params[:auction_bidder_id])
      @auction_bidder.update_attributes(second_offer_status: 'declined', second_offer_sent: false)

      BiddersMailer.declined_second_chance_offer(@auction.id, @auction_bidder.profile.id).deliver

      render :json => { message: "ok" }, status: :ok
    end

    def destroy
      @auction = Auction.find(params[:auction_id])
      @auction.auction_bidders.update_all(second_offer_sent: false)
      auction_response = AuctionSerializer.new(@auction, scope: @current_user)

      render :json => { auction: auction_response }, status: :ok
    end

    private

    def restrict_access
      authenticate_or_request_with_http_token do |token, options|
        @current_user = User.where(access_token: token).first
        if not @current_user.nil?
          return true
        else
          render :json => { :status => :unauthorized }
        end
      end
    end

  end
end
