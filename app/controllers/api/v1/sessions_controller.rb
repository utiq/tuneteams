module Api::V1
  class SessionsController < Api::V1::ApplicationController
    respond_to :json
    before_filter :restrict_access, :only => [:destroy]

    def create
      user = User.where(email: params[:user][:email].downcase).first

      if user.nil?
        render json: {errors: JSON.parse("{\"email\": \"Email is not registered in our records\"}"), authorized: 'false' }, :status => :unprocessable_entity and return
      end

      if user && user.authenticate(params[:user][:password])
        # session[:user_id] = user.id
        # render json: { user: UserSerializer.new(user), authorized: 'true' } and return
        # render json: { user: user.as_json(:only => [:email, :role, :access_token]), authorized: 'true' } and return
        @default_profile = ""
        if not user.profiles.where(is_default: true).first.nil?
          @default_profile = user.profiles.where(is_default: true).first.permalink
        end

        if user.role == "user"
          render json: {user: JSON.parse("{\"id\": \"#{user.id.to_s}\", \"email\": \"#{user.email}\", \"role\": {\"title\": \"#{user.role}\", \"bitMask\": 2}, \"access_token\": \"#{user.access_token}\", \"default_profile\": \"#{@default_profile}\"}"), authorized: 'true' } and return
        elsif user.role == "admin"
          render json: {user: JSON.parse("{\"id\": \"#{user.id.to_s}\", \"email\": \"#{user.email}\", \"role\": {\"title\": \"#{user.role}\", \"bitMask\": 4}, \"access_token\": \"#{user.access_token}\", \"default_profile\": \"#{@default_profile}\"}"), authorized: 'true' } and return
        end
        user.update_attributes(last_login: Time.now, login_count: user.login_count + 1)

      else
        render json: {errors: JSON.parse("{\"password\": \"Incorrect password\"}"), authorized: 'false' }, :status => :unprocessable_entity and return
      end
    end

    def destroy
      @current_user = nil
      render json: { authorized: 'false' }
    end

    private

    def restrict_access
      authenticate_or_request_with_http_token do |token, options|
        @current_user = User.where(access_token: token).first
        if not @current_user.nil?
          return true
        else
          render :json => { :status => :unauthorized }
        end
      end
    end

  end
end
