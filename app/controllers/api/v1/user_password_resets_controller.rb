module Api::V1
  class UserPasswordResetsController < Api::V1::ApplicationController
    respond_to :json
    before_filter :restrict_access, :only => [:edit]

    def create
      user = User.where(email: params[:email].downcase).first
      if user
        user.temporary_token = Utility.create_token
        user.password_reset_sent_at = Time.zone.now
        user.save
        UserMailer.delay.forgot_password(user.first_name, user.email, user.temporary_token, current_host)
      end
      redirect_to forgot_password_path
    end

    def update
      user = User.where(temporary_token: params[:id]).first
      if user.nil?
        render json: {errors: JSON.parse("{\"password\": \"The link are you using was used or is invalid. Try another password reset request.\"}")}, :status => :unprocessable_entity and return
      else
        if user.password_reset_sent_at < 24.hours.ago
          render json: {errors: JSON.parse("{\"password\": \"Password reset has expired.\"}")}, :status => :unprocessable_entity and return
        elsif user.update_attributes!(password: params[:user][:password], temporary_token: nil)
          render :json => { "email" => user.email }, :status => 201
        else
          render json: {errors: JSON.parse("{\"password\": \"Unexpected server error.\"}")}, :status => :unprocessable_entity and return
        end
      end
    end
  end
end
