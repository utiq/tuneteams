module Api::V1
  class UsersController < Api::V1::ApplicationController
    respond_to :json
    before_filter :restrict_access, :only => [:show, :update], :except => [:confirm_email]

    def create


      # Validate source
      # logger.debug params[:user][:source]
      if params[:user][:source] != "invitation" && params[:user][:source] != "claim"
        render json: {errors: JSON.parse("{\"source\": \"Invalid source\"}"), authorized: 'false' }, :status => :unprocessable_entity and return
      end

      # Validate invitation
      if params[:user][:source] == "invitation"
        if not params[:user].has_key?(:invitationToken)
          render json: {errors: JSON.parse("{\"token\": \"You need a token to create an account\"}"), authorized: 'false' }, :status => :unprocessable_entity and return
        end
        invitation = Invitation.where(token: params[:user][:invitationToken], email: params[:user][:email].downcase).first
        if invitation.nil?
          render json: {errors: JSON.parse("{\"token\": \"Token not valid or already used\"}"), authorized: 'false' }, :status => :unprocessable_entity and return
        else
          if invitation.invitation_signed_up
            render json: {errors: JSON.parse("{\"token\": \"Token already used\"}"), authorized: 'false' }, :status => :unprocessable_entity and return
          else
            invitation.update_attributes(invitation_signed_up: true)
          end
        end
      end

      # Validate Claim

      @user = User.new(user_params)
      @user.email_confirmed = true
      if @user.save
        if params[:user][:source] == "claim"
          @claim = Claim.find_by(email: params[:user][:email].downcase)
          @claim.update_attributes(status: 'assigned')
          @profile = Profile.find_by(permalink: params[:user][:profile])
          @claim.profile.update_attributes(user: @user, has_owner: true, is_default: true)
        end
        # render json: { user: UserSerializer.new(@user), authorized: 'true', from_claim: true } and return
        # TODO: change this
        if @user.role == "user"
          response = JSON.parse("{\"id\": \"#{@user.id.to_s}\", \"email\": \"#{@user.email}\", \"role\": {\"title\": \"#{@user.role}\", \"bitMask\": 2}, \"access_token\": \"#{@user.access_token}\"}")
          # cookies.permanent[:user] = response
          render json: {user: response, authorized: 'true' } and return
        elsif @user.role == "admin"
          response = JSON.parse("{\"id\": \"#{@user.id.to_s}\", \"email\": \"#{@user.email}\", \"role\": {\"title\": \"#{@user.role}\", \"bitMask\": 4}, \"access_token\": \"#{@user.access_token}\"}")
          # cookies.permanent[:user] = response
          render json: {user: response, authorized: 'true' } and return
        end
      else
        render json: {"errors" => @user.errors, authorized: 'false' }, :status => :unprocessable_entity and return
      end
    end

    def show
      respond_with @current_user
    end

    def update
      begin
        current_email = @current_user.email.downcase
        if @current_user.update_attributes(update_params)
          if params[:user][:email] != nil && params[:user][:email] != current_email
            raise Tuneteams::Error::EmailNotConfirmed.new "You cannot change your email because you have active/approved auctions." if @current_user.default_profile.auctions.where(status: 'approved').exists?

            @current_user.update_attributes(temporary_token: Utility.create_token, email_confirmed: false)
            UsersMailer.confirm_email(@current_user.id.to_s).deliver
          end
          render :json => { "message" => "ok" }, :status => 201
        else
          render json: {"errors" => @current_user.errors}, :status => :unprocessable_entity and return
        end
      rescue Tuneteams::Error::EmailNotConfirmed => e
        render :json => { message: e.message }, status: :precondition_failed
      rescue StandardError => e
        ExceptionNotifier.notify_exception(e)
        render :json => { message: 'Internal Server Error' }, status: :internal_server_error
      end
    end

    def confirm_email
      user = User.find_by(temporary_token: params[:token])
      user.update_attributes(email_confirmed: true)
      render :json => { "message" => "ok" }, :status => 201
    end

    def recover_password
      @email = params[:email]

      user = User.where(email: @email.downcase).first
      logger.debug user.nil?
      if user.nil?
        render json: {errors: JSON.parse("{\"email\": \"The email that you entered is not in our records.\"}")}, :status => :unprocessable_entity and return
      else

        user.temporary_token = Utility.create_token
        user.password_reset_sent_at = Time.zone.now
        if user.save
          UsersMailer.delay.forgot_password(user.id)
          render json: { "message" => "ok" }, :status => 201
        else
          render json: {errors: JSON.parse("{\"error\": \"Internal server error.\"}")}, :status => :unprocessable_entity and return
        end

      end

    end

    private

    def user_params
      params.require(:user).permit(:email, :password, :token)
    end

    def update_params
      params.require(:user).permit(:email, :password, :token, :notification_messages_or_invitations, :notification_mailing, :notification_surveys,:notification_auction_approval,
      :notification_auction_restart)

    end

    def restrict_access
      authenticate_or_request_with_http_token do |token, options|
        @current_user = User.where(access_token: token).first
        if not @current_user.nil?
          return true
        else
          render :json => { :status => :unauthorized }
        end
      end
    end

  end
end
