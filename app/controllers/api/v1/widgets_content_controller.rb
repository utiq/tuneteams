module Api::V1
  class WidgetsContentController < Api::V1::ApplicationController
    respond_to :json
    before_filter :restrict_access, :only => [:create, :update, :destroy]

    def index
      render :json => { "message" => "ok" }, :status => 201
    end

    def create
      @widget = Widget.find(params[:widget_id])
      
      if params[:type] == "youtube_video"
        @youtube_video = WidgetYoutube.new(url: params[:url])
        @widget.widget_youtubes.push(@youtube_video)
        render :json => { "youtube_video" => @youtube_video }, :status => 201, location: nil
      elsif params[:type] == "spotify_music"
        @spotify_music = WidgetSpotify.new(url: params[:url])
        @widget.widget_spotifies.push(@spotify_music)
        render :json => { "spotify_music" => @spotify_music }, :status => 201, location: nil
      elsif params[:type] == "people"
        if params.has_key?(:profile_id)
          @profile = Profile.find(params[:profile_id])
          @memberPeople = WidgetPeople.new(name: @profile.name,
                                           avatar_url_copy: @profile.default_avatar["small"],
                                           profile_id: @profile.id.to_s,
                                           profile_permalink: @profile.permalink,
                                           added_manually: false)
        else
          @memberPeople = WidgetPeople.new(name: params[:name],
                                           avatar: params[:file],
                                           role: params[:role],
                                           added_manually: true)
          # logger.debug "memberPeople result"
          # logger.debug @memberPeople
        end
        @widget.widget_peoples.push(@memberPeople)
        render :json => { "member_people" => @memberPeople }, :status => 201, location: nil
      elsif params[:type] == "discography"
        @discography = WidgetDiscography.new(name: params[:name], external_url: params[:external_url])
        @widget.widget_discographies.push(@discography)
        render :json => { "discography" => @discography }, :status => 201, location: nil
      end
    end

    def update

      @widget = Widget.find(params[:widget_id])

      if params[:type] == "youtube_video"
        @widget_youtube = @widget.widget_youtubes.find(params[:id])
        @widget_youtube.update_attributes(url: params[:url])
        render :json => { "youtube_video" => @widget_youtube }, :status => 201
      elsif params[:type] == "spotify_music"
        @widget_spotify = @widget.widget_spotifies.find(params[:id])
        @widget_spotify.update_attributes(url: params[:url])
        render :json => { "widget_spotify" => @widget_spotify }, :status => 201
      elsif params[:type] == "people"
        @widget_people = @widget.widget_peoples.find(params[:id])
        if params[:file]
          @widget_people.update_attributes(name: params[:name], role: params[:role], avatar: params[:file])
        else
          @widget_people.update_attributes(name: params[:name], role: params[:role])
        end
        render :json => { "people" => @widget_people }, :status => 201
      elsif params[:type] == "discography"
        @widget_discography = @widget.widget_discographies.find(params[:id])
        @widget_discography.update_attributes(name: params[:name], external_url: params[:external_url])
        render :json => { "discography" => @widget_discography }, :status => 201
      elsif params[:type] == "overview"
        @widget_overview = @widget.widget_overview.update_attributes(description: params[:description])
        render :json => { overview: {description: params[:description]} }, :status => 201
      elsif params[:type] == "interest"
        @widget.widget_interest.update_attributes(interests: params[:interests], countries: params[:countries])
        
        interest_names = []
        if not params[:interests].nil?
          IndustryRoleProfile.find(params[:industry_role_profile_id]).industry_role_profile_interests.find(params[:interests]).each do |interest|
            interest_names << interest.alias
          end
        end

        country_names = []
        if not params[:countries].nil?
          Country.find(params[:countries]).each do |country|
            country_names << country.name
          end
        end

        render :json => { "interest_names" => interest_names, "country_names" => country_names }, :status => 201
      end
    end

    def destroy
      
      @widget = Widget.find(params[:widget_id])
      logger.debug params[:type]
      if params[:type] == "youtube_video"
        @widget_youtube = @widget.widget_youtubes.find(params[:id])
        @widget.widget_youtubes.delete(@widget_youtube)
        render :json => { "message" => "ok" }, :status => 201
      elsif params[:type] == "spotify_music"
        @widget_spotify = @widget.widget_spotifies.find(params[:id])
        @widget.widget_spotifies.delete(@widget_spotify)
        render :json => { "message" => "ok" }, :status => 201
      elsif params[:type] == "people"
        @widget_people = @widget.widget_peoples.find(params[:id])
        @widget.widget_peoples.delete(@widget_people)
        render :json => { "message" => "ok" }, :status => 201
      elsif params[:type] == "discography"
        @widget_discography = @widget.widget_discographies.find(params[:id])
        @widget.widget_discographies.delete(@widget_discography)
        render :json => { "message" => "ok" }, :status => 201
      end
    end

    private

    def restrict_access
      authenticate_or_request_with_http_token do |token, options|
        @current_user = User.where(access_token: token).first
        if not @current_user.nil?
          return true
        else
          render :json => { :status => :unauthorized }
        end
      end
    end
  end
end