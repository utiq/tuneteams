class EmbedsController < ApplicationController
  after_filter :allow_iframe, only: [:auctions]
  layout 'embed'

  def auctions
    @auction = Auction.find_by(permalink: params[:permalink])
    @expired = Time.now > @auction.due_date
  end

  def test

  end
end
