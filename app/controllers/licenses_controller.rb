class LicensesController < ApplicationController
  layout 'pdf'

  def show
    if params.key?(:id)
      @auction = Auction.find(params[:id])
    end

    # For generated contracts logic can not be here, it has to be in the view

    # @advance_amount = "US$______________"
    # if @auction.status == "waiting_sign"
        
    #   # @publishing_advance_amount = "US$ #{@auction.max_bid.amount_publishing} (#{@auction.max_bid.amount_publishing} US Dollars)"
    #   if @auction.competitive_term == "advance"
    #     # The amount is the winning bid
    #     # Amount in $USD
    #     ##{number_to_currency(@auction.reserve_on_competitive_term)} USD (#{@auction.reserve_on_competitive_term.to_words} US Dollars)
    #     # @advance_amount = "US$ #{@auction.reserve_on_competitive_term} (#{@auction.reserve_on_competitive_term.to_words} US Dollars)" #"US$ #{3000.to_s}"#@auction.winner_amount
    #     @advance_amount = "US$ #{@auction.max_bid.amount} (#{@auction.max_bid.amount.to_words} US Dollars)"
    #   elsif @auction.competitive_term == "royalty_rate"
    #     # Amount in Percentaje (%)
    #     @advance_amount = "US$ #{@auction.required_advance_or_royalty_rate} (#{@auction.reserve_on_competitive_term.to_words} US Dollars)"
    #   end
    # end

    # @royalty_rate_amount = "______________%"
    # if @auction.status == "waiting_sign"
    #   # @royalty_rate_amount = "#{@auction.max_bid.amount}% (#{@auction.max_bid.amount} percent)"
    #   if @auction.competitive_term == "advance"
    #     # The amount is the winning bid
    #     # Amount in $USD
    #     @royalty_rate_amount = "#{@auction.required_advance_or_royalty_rate} % (#{@auction.required_advance_or_royalty_rate.to_words} percent)"
    #   elsif @auction.competitive_term == "royalty_rate"
    #     # Amount in Percentaje (%)
    #     @royalty_rate_amount = "#{@auction.max_bid.amount}% (#{@auction.max_bid.amount.to_words} percent)"
    #     # @royalty_rate_amount = "#{@auction.reserve_on_competitive_term}% (#{@auction.reserve_on_competitive_term.to_words} percent)"
    #   end
    # end

    respond_to do |format|
      format.html
      format.pdf do
        render :pdf => 'file_name',
               :template => "licenses/show.html",
               :layout => "pdf.html",
               :encoding => "UTF-8",
               :disable_internal_links => false,
               :show_as_html => params[:debug].present?
      end
    end
  end

end
