class SubscribersController < ApplicationController
  respond_to :json
  skip_before_action :verify_authenticity_token#, :only => [:index, :show]
  layout 'clean'

  def index
    # respond_with Subscriber.all
  end

  def show
    respond_with Subscriber.find(params[:id])
  end

  def create
    logger.info params
    @subscriber = Subscriber.new(subscriber_params)
    @subscriber.current_ip = request.remote_ip    
    @subscriber.city = request.location.city
    @subscriber.country = request.location.country_code
    if @subscriber.save
      respond_with @subscriber.as_json(:only => [:token]), location: nil
    else
      respond_to do |format|
        format.json { render :json => { "errors" => @subscriber.errors }, :status => :unprocessable_entity }
      end
    end
  end

  def update
    respond_with Subscriber.update(params[:id], params[:subscriber])
  end

  def destroy
    respond_with Subscriber.destroy(params[:id])
  end

  private

  def subscriber_params
    params.require(:subscriber).permit(:email, :industry_role_profile_id)
  end
end
