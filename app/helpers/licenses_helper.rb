module LicensesHelper

  def competitive_term(auction, contract_type)
    value = ""
    if auction.status == 'waiting_sign'
      if auction.auction_type == 'recording_publishing_rights'
        if contract_type == 'recording'
          value = "US$ #{'%.2f' % auction.max_bid.amount} (#{auction.max_bid.amount.to_words} US Dollars)"
        elsif contract_type == 'publishing'
          value = "US$ #{'%.2f' % auction.max_bid.amount_publishing} (#{auction.max_bid.amount_publishing.to_words} US Dollars)"
        end
      else
        if auction.competitive_term == 'advance'
          value = "US$ #{'%.2f' % auction.max_bid.amount} (#{auction.max_bid.amount.to_words} US Dollars)"
        else
          value = "US$ #{'%.2f' % auction.required_advance_or_royalty_rate} (#{auction.required_advance_or_royalty_rate.to_words} US Dollars)"
        end
      end
    else
      if auction.competitive_term == 'royalty_rate'
        value = "US$ #{'%.2f' % auction.required_advance_or_royalty_rate}  (#{auction.required_advance_or_royalty_rate.to_words} US Dollars)"
      else
        value = "USD$______________"
      end
    end

    return value
  end

  def required_advance_or_royalty_rate(auction, contract_type)
    value = ""
    if auction.status == 'waiting_sign'
      if auction.auction_type == 'recording_publishing_rights'
        if contract_type == 'recording'
          value = "#{'%.2f' % auction.required_advance_or_royalty_rate} % (#{auction.required_advance_or_royalty_rate.to_words} percent)"
        elsif contract_type == 'publishing'
          value = "#{'%.2f' % auction.required_advance_or_royalty_rate_publishing} % (#{auction.required_advance_or_royalty_rate_publishing.to_words} percent)"
        end
      else
        if auction.competitive_term == 'advance'
          value = "#{'%.2f' % auction.required_advance_or_royalty_rate} % (#{auction.required_advance_or_royalty_rate.to_words} percent)"
        else
          value = "#{'%.2f' % auction.max_bid.amount}% (#{auction.max_bid.amount.to_words} percent)"
        end
      end
    else
      if auction.auction_type == 'recording_publishing_rights'
        if contract_type == 'recording'
          value = "#{'%.2f' % auction.required_advance_or_royalty_rate} % (#{auction.required_advance_or_royalty_rate.to_words} percent)"
        elsif contract_type == 'publishing'
          value = "#{'%.2f' % auction.required_advance_or_royalty_rate_publishing} % (#{auction.required_advance_or_royalty_rate_publishing.to_words} percent)"
        end
      else
        if auction.competitive_term == 'advance'
          value = "#{'%.2f' % auction.required_advance_or_royalty_rate} % (#{auction.required_advance_or_royalty_rate.to_words} percent)"
        else
          value = "______________%"
        end
      end
    end

    return value

    # {if @auction.status == 'waiting_sign' then (if @auction.auction_type == 'recording_publishing_rights' then "#{'%.2f' % @auction.required_advance_or_royalty_rate_publishing} % (#{@auction.required_advance_or_royalty_rate_publishing.to_words} percent)" else (if @auction.competitive_term == 'advance' then "#{'%.2f' % auction.required_advance_or_royalty_rate} % (#{@auction.required_advance_or_royalty_rate.to_words} percent)" else "#{'%.2f' % @auction.max_bid.amount}% (#{@auction.max_bid.amount.to_words} percent)" end) end) else (if @auction.competitive_term == 'advance' || @auction.auction_type == 'recording_publishing_rights' then "#{'%.2f' % @auction.required_advance_or_royalty_rate_publishing} % (#{@auction.required_advance_or_royalty_rate_publishing.to_words} percent)" else "______________%" end) end}
  end
end
