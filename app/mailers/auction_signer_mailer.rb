class AuctionSignerMailer < ActionMailer::Base
  layout "email"
  default :from => '"tuneteams" <no-reply@tuneteams.com>',
          :return_path => 'no-reply@tuneteams.com',
          'reply-to' => 'no-reply@tuneteams.com',
          'X-No-Spam' => 'True'

  def send_validation_email(auction_id, signer_email, party_from, profile_id = nil)
    @auction = Auction.find(auction_id)
    @url = "#{ENV["HOST_NAME"]}/auctions/#{@auction.permalink}?reason=validate_second_signer&token=#{@auction.temporary_token}&email=#{signer_email}&from=#{party_from}"
    @party_from = party_from

    if @party_from == 'bidder'
      profile = Profile.find(profile_id)
      @url += "&profile_id=#{profile_id}"
      @user_name = profile.name
    elsif @party_from == 'auction_manager'
      @user_name = @auction.profile.name
    end

    mail(:to => signer_email,
         :subject => "Validate your email")
  end
end
