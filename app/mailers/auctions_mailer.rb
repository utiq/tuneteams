class AuctionsMailer < ActionMailer::Base
  layout "email"
  default :from => '"tuneteams" <no-reply@tuneteams.com>',
          :return_path => 'no-reply@tuneteams.com',
          'reply-to' => 'no-reply@tuneteams.com',
          'X-No-Spam' => 'True'

  def cancel(auction_name, bidder_email)

    @auction_name = auction_name
    mail(:to => bidder_email,
         :subject => "Auction cancelled")
  end

  def approve(auction_id)
    @auction = Auction.find(auction_id)

    mail(:to => @auction.profile.user.email,
         :subject => "Auction approved")
  end

  def reject(auction_id)
    @auction = Auction.find(auction_id)

    mail(:to => @auction.profile.user.email,
         :subject => "Auction not approved")
  end

  def disapprove(auction_id)
    @auction = Auction.find(auction_id)
    mail(:to => @auction.profile.user.email,
         :subject => "Auction disapproved")
  end

  def contract_not_signed_four_days(auction_id,
                                    signer_id,
                                    signer_name,
                                    signer_email)

    @auction = Auction.find(auction_id)
    @auction_url = "#{ENV["HOST_NAME"]}/auctions/#{@auction.permalink}"
    @due_date = (@auction.due_date + 7.days).strftime("%B %e at %H:%M")
    @contract_url = "#{ENV["HOST_NAME"]}/auctions/#{@auction.permalink}/agreement/#{signer_id}"
    mail(:to => signer_email,
         :subject => "Sign contract reminder")

  end

  def restart(auction_id)
    @auction = Auction.find(auction_id)
    auction_manager_email = @auction.profile.user.email
    @cancel_restart_url = "#{ENV["HOST_NAME"]}/auctions/#{@auction.permalink}?reason=cancel_restart&token=#{@auction.temporary_token}"

    mail(:to => auction_manager_email,
         :subject => "Auction restarted")

  end

  def contract_not_signed_six_days(auction_id,
                                   signer_id,
                                   signer_name,
                                   signer_email)

    @auction = Auction.find(auction_id)
    @auction_url = "#{ENV["HOST_NAME"]}/auctions/#{@auction.permalink}"
    @due_date = (@auction.due_date + 7.days).strftime("%B %e at %H:%M")
    @contract_url = "#{ENV["HOST_NAME"]}/auctions/#{@auction.permalink}/agreement/#{signer_id}"

    mail(:to => signer_email,
         :subject => "[URGENT] Sign contract reminder")

  end

  def contract_not_signed_admin(auction_id, signers_not_signed)

    @auction = Auction.find(auction_id)
    @auction_url = "#{ENV["HOST_NAME"]}/auctions/#{@auction.permalink}"
    @signers_not_signed = signers_not_signed.to_sentence

    mail(:to => ENV["ADMIN_EMAIL"].split(','),
         :subject => "[URGENT] Signers are not signing")
  end

  def order_confirmation(auction_id)

    @auction = Auction.find(auction_id)

    @winner_profile_name = @auction.max_bid.auction_bidder.profile.name
    @auction_manager_profile_name = @auction.profile.name
    winner_email = @auction.max_bid.auction_bidder.profile.user.email
    @url = "#{ENV["HOST_NAME"]}/dashboard/auctions/participating"
    # TODO: Add the url to the auction details

    @sub_total = @auction.sub_total
    @transaction_fee = @auction.transaction_fee[:credit_card]
    @total = @sub_total + @transaction_fee

    mail(:to => winner_email,
         :subject => "Next steps and payment receipt")
  end

  def order_confirmation_auction_manager(auction_id)

    @auction = Auction.find(auction_id)
    @winner_profile_name = @auction.max_bid.auction_bidder.profile.name
    @auction_manager_profile_name = @auction.profile.name
    auction_manager_email = @auction.profile.user.email
    @due_date = (Time.now + 7.days).strftime("%B %e at %H:%M")
    @payoneer_url = "#{ENV["HOST_NAME"]}/dashboard/management/payout"

    mail(:to => auction_manager_email,
         :subject => "Next steps and payment receipt")

  end

  def materials_not_sent_admin(auction_id)
    @auction = Auction.find(auction_id)

    mail(:to => ENV["ADMIN_EMAIL"].split(','),
         :subject => "[URGENT] Materials were not sent")
  end

  def set_up_payoneer(user_id)
    @user = User.find(user_id)
    @url = "#{ENV["HOST_NAME"]}/dashboard/management/payout"

    mail(:to => @user.email,
         :subject => "Set up your Payoneer account")
  end
end
