class BiddersMailer < ActionMailer::Base
  layout "email"
  default :from => '"tuneteams" <no-reply@tuneteams.com>',
          :return_path => 'no-reply@tuneteams.com',
          'reply-to' => 'no-reply@tuneteams.com',
          'X-No-Spam' => 'True'

  def second_chance_offer(auction_id, profile_id, max_amount, auction_bidder_id)
    @auction = Auction.find(auction_id)
    @profile = Profile.find(profile_id)
    @bidder_profile_name = @profile.name
    @max_amount = max_amount
    @offer_url = "#{ENV["HOST_NAME"]}/auctions/#{@auction.permalink}/second-chance/#{auction_bidder_id}"

    mail(:to => @profile.user.email,
         :subject => "Second chance offer")
  end

  def declined_second_chance_offer(auction_id, bidder_profile_id)
    @auction = Auction.find(auction_id)
    @profile = Profile.find(bidder_profile_id)
    @bidder_profile_name = @profile.name
    @auction_url = "#{ENV["HOST_NAME"]}/auctions/#{@auction.permalink}?tab=bids"

    mail(:to => @auction.profile.user.email,
         :subject => "[Declined] Second chance offer")
  end

  # When an auction-owner decides to re-start an auction instead of making a second-chance offer, any unsuccessful bidders from the previous round of the auction should reserve this email
  def restart_from_manager(auction_id, bidder_email)
    @auction = Auction.find(auction_id)
    @auction_url = "#{ENV["HOST_NAME"]}/auctions/#{@auction.permalink}"

    mail(:to => bidder_email,
         :subject => "Auction Restarted")
  end

end
