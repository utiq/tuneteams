class BidsMailer < ActionMailer::Base
  layout "email"
  default :from => '"tuneteams" <no-reply@tuneteams.com>',
          :return_path => 'no-reply@tuneteams.com',
          'reply-to' => 'no-reply@tuneteams.com',
          'X-No-Spam' => 'True'

  def request_approval_to_bid(bidder_profile_name,
  													  bidder_profile_permalink,
  													  auction_manager_profile_name,
  													  auction_manager_profile_permalink,
  													  auction_name,
  													  auction_permalink,
  													  legal_party_name,
  													  user_email)
    @user_email = user_email
    @bidder_profile_name = bidder_profile_name
    @auction_name = auction_name
    @auction_manager_profile_name = auction_manager_profile_name
    @auction_permalink = auction_permalink
    @legal_party_name = legal_party_name
    @url = "#{ENV["HOST_NAME"]}/dashboard/#{auction_manager_profile_permalink}/bidders"
    @auction_url = "#{ENV["HOST_NAME"]}/auctions/#{auction_permalink}"
    @bidder_profile_url = "#{ENV["HOST_NAME"]}/#{bidder_profile_permalink}"
    @auction_manager_profile_url = "#{ENV["HOST_NAME"]}/#{auction_manager_profile_permalink}"
    mail(:to => user_email,
         :subject => "Request approval to bid")
  end

  def request_rejected(auction_name,
  										 auction_permalink,
  										 user_email)

  	@auction_name = auction_name
  	@auction_url = "#{ENV["HOST_NAME"]}/auctions/#{auction_permalink}"
  	mail(:to => user_email,
         :subject => "Request to bid rejected")

  end

  def request_approved(auction_name,
  										 auction_permalink,
  										 user_email)

  	@auction_name = auction_name
  	@auction_url = "#{ENV["HOST_NAME"]}/auctions/#{auction_permalink}"
  	mail(:to => user_email,
         :subject => "Request to bid approved")

  end

  def outbid(auction_name,
  					 auction_permalink,
  					 bidder_profile_name,
  					 bidder_profile_permalink,
  					 amount,
             amount_publishing,
  					 user_email,
             due_date)

  	@auction_name = auction_name
  	@auction_url = "#{ENV["HOST_NAME"]}/auctions/#{auction_permalink}"
  	@bidder_profile_name = bidder_profile_name
  	@bidder_profile_permalink = bidder_profile_permalink
  	@amount = amount
    @amount_publishing = amount_publishing
    @due_date = due_date.strftime("%B %e at %H:%M")

  	mail(:to => user_email,
         :subject => "You have been outbid")

  end

  def sign_contract_auction_manager(auction_id,
  																	winner_profile_name,
  																	winner_profile_permalink,
  																	auction_manager_profile_name,
  													  			auction_manager_profile_permalink,
  																	amount,
  																	signer_id,
  																	user_email)

  	@auction = Auction.find(auction_id)
  	@auction_url = "#{ENV["HOST_NAME"]}/auctions/#{@auction.permalink}"
  	@winner_profile_name = winner_profile_name
  	@winner_profile_url = "#{ENV["HOST_NAME"]}/#{winner_profile_permalink}"
  	@auction_manager_profile_name = auction_manager_profile_name
  	@auction_manager_profile_permalink = auction_manager_profile_permalink
  	@amount = amount
  	@contract_url = "#{ENV["HOST_NAME"]}/auctions/#{@auction.permalink}/agreement/#{signer_id}"
  	mail(:to => user_email,
         :subject => "Your auction has a winner")
  end

  def sign_contract_winner(auction_id,
  												 winner_profile_name,
  												 winner_profile_permalink,
  												 auction_manager_profile_name,
  												 auction_manager_profile_permalink,
  												 amount,
  												 signer_id,
  												 user_email)

  	@auction = Auction.find(auction_id)
  	@auction_url = "#{ENV["HOST_NAME"]}/auctions/#{@auction.permalink}"
  	@winner_profile_name = winner_profile_name
  	@winner_profile_url = "#{ENV["HOST_NAME"]}/#{winner_profile_permalink}"
  	@amount = amount
  	@contract_url = "#{ENV["HOST_NAME"]}/auctions/#{@auction.permalink}/agreement/#{signer_id}"
  	mail(:to => user_email,
         :subject => "You won the auction")

  end

  def pay_auction(auction_id,
                  profile_name,
                  profile_permalink,
                  licensee_name,
                  licensee_email,
                  signed_date)

    @auction = Auction.find(auction_id)
    @auction_url = "#{ENV["HOST_NAME"]}/auctions/#{@auction.permalink}"
    @payment_url = "#{ENV["HOST_NAME"]}/auctions/#{@auction.permalink}/payment"
    @licensee_name = licensee_name
    @due_date = (signed_date + 7.days).strftime("%B %e at %H:%M")

    attachments['contract.pdf'] = File.read("public/pdfs/contracts/#{@auction.hellosign_file_name}.pdf")

    mail(:to => licensee_email,
         :subject => "Payment required")
  end

  def contract_to_licensor(auction_id,
                           licensee_name)

    @auction = Auction.find(auction_id)
    @licensee_name = licensee_name

    attachments['contract.pdf'] = File.read("public/pdfs/contracts/#{@auction.hellosign_file_name}.pdf")

    mail(:to => @auction.profile.user.email,
         :subject => "Contract signed")

  end

  def losers(auction_id, bidder_email)
    @auction = Auction.find(auction_id)
    mail(:to => bidder_email,
         :subject => "You did not win the auction")
  end

  def reserve_not_met(auction_id)
    @auction = Auction.find(auction_id)
    @auction_url = "#{ENV["HOST_NAME"]}/auctions/#{@auction.permalink}?tab=bids"
    mail(:to => @auction.profile.user.email,
         :subject => "Reserve not met")
  end

  def reserve_not_met_bidder(auction_id, user_email)
    @auction = Auction.find(auction_id)
    @auction_url = "#{ENV["HOST_NAME"]}/auctions/#{@auction.permalink}"
    mail(:to => user_email,
         :subject => "Reserve not met")
  end

end
