class ClaimsMailer < ActionMailer::Base
  layout "email"
  default :from => '"tuneteams" <no-reply@tuneteams.com>',
          :return_path => 'no-reply@tuneteams.com',
          'reply-to' => 'no-reply@tuneteams.com',
          'X-No-Spam' => 'True'

  def assign_and_invite(claim)
    @user_email = claim.email
    @url = "#{ENV["HOST_NAME"]}/signup?source=claim&email=#{claim.email}&profile=#{claim.profile.permalink}"
    mail(:to => claim.email,
         :subject => "Profile Claim Approved")
  end

  # Occurs if the user has registered previously
  def assign(claim)
    @user_email = claim.email
    @url = "#{ENV["HOST_NAME"]}/#{claim.profile.permalink}"
    mail(:to => claim.email,
         :subject => "Profile claim approved and asigned to your account")
  end

  def reject(claim_id)
    claim = Claim.find(claim_id)
    # @user_email = claim.email
    mail(:to => claim.email,
         :subject => "Your profile claim was rejected",
         "reply-to" => 'help@tuneteams.com')
  end
end