class FriendshipsMailer < ActionMailer::Base
  layout "email"
  default :from => '"tuneteams" <no-reply@tuneteams.com>',
          :return_path => 'no-reply@tuneteams.com',
          'reply-to' => 'no-reply@tuneteams.com',
          'X-No-Spam' => 'True'

  def send_invitation(owner_name, owner_industry_role_profile_name, friend_email, friend_name, friend_permalink, invitation_message)
    @owner_name = owner_name
    @friend_name = friend_name
    @owner_industry_role_profile_name = owner_industry_role_profile_name
    @url = "#{ENV["HOST_NAME"]}/dashboard/#{friend_permalink}/notifications"
    @invitation_message = invitation_message
    mail(:to => friend_email,
         :subject => "#{@owner_name} wants to connect with you")
  end

  def accept_invitation(owner_name, owner_email, friend_email, friend_name, friend_profile_permalink)
    @owner_name = owner_name
    @friend_name = friend_name
    @url = "#{ENV["HOST_NAME"]}/#{friend_profile_permalink}"
    mail(:to => owner_email,
         :subject => "#{friend_name} accepted your connection request")
  end

  def send_owner_notice(profile_owner_contact_email, profile_permalink)
    @url = "#{ENV["HOST_NAME"]}/#{profile_permalink}"
    mail(:to => profile_owner_contact_email,
         :subject => "Someone was trying to contact you via tuneteams")
  end

end
