class InvitationsMailer < ActionMailer::Base
  layout "email"
  default :from => '"tuneteams" <no-reply@tuneteams.com>',
          :return_path => 'no-reply@tuneteams.com',
          'reply-to' => 'no-reply@tuneteams.com',
          'X-No-Spam' => 'True'

  def invite(invitation_id)
    invitation = Invitation.find(invitation_id)
    @user_email = invitation.email
    @current_host = ENV["HOST_NAME"]
    # @url = @current_host + confirm_email_path(user.temporary_token)
    @url = "#{@current_host}/signup?source=invitation&token=#{invitation.token}&email=#{invitation.email}"
    # @main_message_subject = "We need to confirm your email address."
    mail(:to => invitation.email,
         :subject => "Welcome to tuneteams")
  end
end
