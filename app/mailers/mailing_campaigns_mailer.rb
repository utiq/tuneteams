class MailingCampaignsMailer < ActionMailer::Base
  layout "mailing"
  default :from => '"tuneteams" <newsletter@tuneteams.com>',
          :return_path => 'no-reply@tuneteams.com',
          'reply-to' => 'no-reply@tuneteams.com',
          'X-No-Spam' => 'True'

  def send_test(subject, body, test_emails)
    @body = body
    emails = test_emails.split(',')
    mail(:to => emails,
         :subject => "[TEST] #{subject}")
  end

  def send_mailing(subject, body, email)
    @body = body
    mail(:to => email,
         :subject => "#{subject}")
  end
end
