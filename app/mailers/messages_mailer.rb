class MessagesMailer < ActionMailer::Base
  layout "email"
  default :from => '"tuneteams" <no-reply@tuneteams.com>',
          :return_path => 'no-reply@tuneteams.com',
          'reply-to' => 'no-reply@tuneteams.com',
          'X-No-Spam' => 'True'

  def message_sent(recipient_email, recipient_name, sender_name, body, message_id, message_subject, profile_permalink)
    @current_host = ENV["HOST_NAME"]
    @body = body
    @recipient_name = recipient_name
    @sender_name = sender_name
    @message_subject = message_subject
    @url = "#{@current_host}/dashboard/#{profile_permalink}/messages/#{message_id}"
    mail(:to => recipient_email,
         :subject => "#{sender_name} sent you a message")
  end
end
