class ProfilesMailer < ActionMailer::Base
  layout "email"
  default :from => '"tuneteams" <no-reply@tuneteams.com>',
          :return_path => 'no-reply@tuneteams.com',
          'reply-to' => 'no-reply@tuneteams.com',
          'X-No-Spam' => 'True'

  def new_profile_created_admin(profile_permalink)
    @current_host = ENV["HOST_NAME"]
    # @body = body
    @url = "#{@current_host}/#{profile_permalink}"
    mail(:to => "ian@tuneteams.com, julia@tuneteams.com",
         :subject => "A new profile was created")
  end

  # Email sent to the user when create a new profile
  def new_profile_created(user_email, profile_name, profile_permalink, profile_country, profile_industry_role)
    @current_host = ENV["HOST_NAME"]
    @profile_name = profile_name
    @profile_country = profile_country
    @profile_industry_role = profile_industry_role
    @url = "#{@current_host}/#{profile_permalink}"
    mail(:to => user_email,
         :subject => "Your new profile was created")
  end

  def emails_exported(user_email, path)
    @current_host = ENV["HOST_NAME"]
    @url = "#{@current_host}/#{path}"
    mail(:to => user_email,
         :subject => "Profiles exported")
  end
end
