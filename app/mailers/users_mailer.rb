class UsersMailer < ActionMailer::Base
  layout "email"
  default :from => '"tuneteams" <no-reply@tuneteams.com>',
          :return_path => 'no-reply@tuneteams.com',
          'reply-to' => 'no-reply@tuneteams.com',
          'X-No-Spam' => 'True'

  def forgot_password(user_id)
    @user = User.find(user_id)
    # @user.update_attributes(temporary_token: Utility.create_token, email_confirmed: false)
    @url = "#{ENV["HOST_NAME"]}/reset-password/#{@user.temporary_token}"
    mail(:to => @user.email,
         :subject => "How to reset your tuneteams password.")
  end

  def confirm_email(user_id)
    @user = User.find(user_id)
    @url = "#{ENV["HOST_NAME"]}/confirm-email/#{@user.temporary_token}/#{@user.email}"
    mail(:to => @user.email,
         :subject => "Confirm your email")
  end
end
