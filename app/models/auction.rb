class Auction
  include Mongoid::Document
  include Mongoid::Timestamps
  include Elasticsearch::Model
  include Elasticsearch::Model::Callbacks
  include Mongoid::Orderable

  # Attributes
  field :name
  field :description
  field :permalink
  field :length, type: Integer
  field :status, default: 'draft' #published, won
  field :auction_type, type: String #recording_rights, publishing_rights, sub_publishing_rights, recording_publishing_rigths
  field :competitive_term #advance, royalty_rate

  field :reserve_on_competitive_term, type: Float #Can be USD$ or a percentage
  field :required_advance_or_royalty_rate, type: Float #Can be USD$ or a percentage
  field :reserve_on_competitive_term_publishing, type: Float #Can be USD$ or a percentage
  field :required_advance_or_royalty_rate_publishing, type: Float #Can be USD$ or a percentage
  field :show_private_info, type: Boolean, default: true
  field :reserve_not_met_notification_sent, type: Boolean, default: false

  field :auction_length, type: Integer
  field :publishing_date, type: DateTime
  field :approving_date, type: DateTime
  field :due_date, type: DateTime
  field :signed_date, type: DateTime
  field :payed_date, type: DateTime
  field :recording_term#, type: Integer
  field :publishing_term
  field :publishing_agreement_type
  field :place_of_court
  field :position, type: Integer
  field :recent_bid_position, type: Integer, default: 0
  field :number_restart, type: Integer, default: 0
  field :temporary_token

  # Attributes related with legal details
  field :licensor_legal_name
  field :licensor_legal_address_line1
  field :licensor_legal_address_line2
  field :licensor_phone_number
  field :licensor_calling_code
  field :licensor_phone_number_verified
  field :licensor_phone_number_verification_code
  field :licensor_legal_address_country

  # Attributes related with legal details if recording is different than publishing
  field :recording_publishing_different_info, type: Boolean, default: false
  field :same_recording_and_publisher, type: Boolean, default: false
  field :songwriter_name
  field :recording_artist_name

  field :publisher_legal_name
  field :publisher_legal_address_line1
  field :publisher_legal_address_line2
  field :publisher_phone_number
  field :publisher_calling_code
  field :publisher_phone_number_verified
  field :publisher_legal_address_country

  # attributes related with signature (hellosign)
  field :signature_request_id, type: String
  field :signature_is_complete, type: Boolean, default: false
  field :signature_notification_sent, type: Integer, default: 0
  field :hellosign_file_name
  field :related_countries, type: Array

  # Social Networks and Metrics
  field :facebook_page
  field :twitter_username
  field :spotify_monthly_listeners, type: Integer
  field :spotify_total_plays, type: Integer
  field :youtube_channel
  field :youtube_video_views, type: Integer
  field :youtube_video_views_info
  field :soundcloud_profile
  field :nbs_artist_id
  field :nbs_artist_name

  #attributes related with notifications
  field :materials_received_admin_notification, type: Integer, default: 0

  # field :full_license
  field :notes, type: String

  # Uploaders
  mount_uploader :illustration, AuctionIllustrationUploader, mount_on: :illustration_filename

  # Associations
  belongs_to :profile
  has_many :auction_bids
  has_many :auction_payouts
  has_many :auction_bidders
  has_many :auction_watchlist
  embeds_many :auction_songs
  embeds_many :auction_signers
  embeds_many :auction_videos
  has_and_belongs_to_many :genres, inverse_of: nil
  has_and_belongs_to_many :countries, inverse_of: nil
  belongs_to :country, :foreign_key => 'applicable_law_for_license', :class_name => "Country", inverse_of: nil
  orderable

  belongs_to :auction, :foreign_key => 'copy_from', :class_name => "Auction", inverse_of: nil

  # Actions
  before_create :generate_permalink, :generate_temporary_token
  after_save :update_indexes
  after_update :update_indexes
  after_create :move_to_bottom
  # after_destroy :deleted_document
  # after_update :generate_permalink, :if => :name_changed?

  index({ permalink: 1 }, { unique: true})
  index status: 1

  def update_indexes
    self.__elasticsearch__.index_document
  end

  def deleted_document
    self.__elasticsearch__.delete_document
  end

  def move_to_bottom
    self.move_to! :bottom
  end

  # index_name "auctions-index" # elasticsearch index
  def as_indexed_json(options={})
    as_json(
        methods: [:default_illustration, :bids_count],
           only: [:name, :permalink, :auction_type, :position, :status],
        include: { methods: [:default_illustration, :bids_count],
                   countries: { only: [:_id, :name] },
                   genres: { only: [:_id, :name] }
                 }
    )
  end

  def self.search(query, type, offset, limit)
    # Results related with the dropdown and autocomplete
    if type == "simple"
      __elasticsearch__.search(
        {
          from: 0, size: 5,
          query: {
            multi_match: {
              query: query,
              type: "phrase_prefix",
              fields: ['name^20', 'genres.name^10', 'country.name^5']
            }
          }
        }
      )
    # Results related with the total searching results
    elsif type == "complex"
      __elasticsearch__.search(
        {
          from: offset, size: limit,
          query: {
            filtered: {
              query: {
                multi_match: {
                  query: query,
                  type: "phrase_prefix",
                  fields: ['name^10', 'genres.name', 'country.name']
                }
              },
              filter: {
                term: {
                  status: "approved"
                }
              }
            }
            # multi_match: {
            #   query: query,
            #   type: "phrase_prefix",
            #   fields: ['name^10', 'genres.name', 'country.name']
            # }
          }
        }
      )
    elsif type == "and"
      __elasticsearch__.search(
        from: offset, size: limit,
        query: {
          query_string: {
            query: query
          }
        },
        sort: [{ position: {order: 'asc'}}]
      )
    end

  end

  def generate_permalink
    i = 0
    begin
      i += 1
      if i == 1
        self.permalink = Utility.create_permalink(self.name)
      else
        self.permalink = Utility.create_permalink(self.name) + "-" + i.to_s
      end
    end while self.class.where(permalink: permalink).exists?
  end

  def generate_temporary_token
    self.temporary_token = Utility.create_token
  end

  def default_illustration
    default_illustration = {}
    if self.illustration_filename.nil?
      @default_illustration_name = "default_illustration"
      default_illustration = {thumb: "#{ENV["HOST_NAME"]}/assets/icons/auctions/#{@default_illustration_name}_thumb.jpg",
                              small: "#{ENV["HOST_NAME"]}/assets/icons/auctions/#{@default_illustration_name}_small.jpg",
                              medium: "#{ENV["HOST_NAME"]}/assets/icons/auctions/#{@default_illustration_name}_medium.jpg",
                              large: "#{ENV["HOST_NAME"]}/assets/icons/auctions/#{@default_illustration_name}_large.jpg"}
    else
      default_illustration = {thumb: self.illustration_url(:thumb),
                              small: self.illustration_url(:small),
                              medium: self.illustration_url(:medium),
                              large: self.illustration_url(:large)}
    end
    return default_illustration

  end

  def self.promoted
    self.where(status: "approved").order_by("position ASC").limit(9)
  end

  def validations
    validations = []
    if self.countries.count == 0
      validations << "Add the countries where your rights could be used"
    end
    if self.genres.count == 0
      validations << "Add genres related"
    end
    if self.auction_songs.count == 0
      validations << "You have to add at least one song"
    end

    if self.auction_type.nil?
      validations << "Select an Auction Type"
    end

    if self.reserve_on_competitive_term.nil? || self.reserve_on_competitive_term == 0
      validations << "Enter a Reserve on Competitive Term"
    end

    if self.required_advance_or_royalty_rate.nil? || self.required_advance_or_royalty_rate == 0
      validations << "Enter a Required Advance"
    end

    # Term
    if self.auction_type == 'recording_publishing_rights'
      if self.recording_term == "" || self.recording_term == "0"
        validations << "Recording term cannot be zero"
      end
      if self.publishing_term == "" || self.publishing_term == "0"
        validations << "Publishing term cannot be zero"
      end
    elsif self.auction_type == 'recording_rights'
      if self.recording_term == "" || self.recording_term == "0"
        validations << "Recording term cannot be zero"
      end
      if self.competitive_term.nil?
        validations << "Select an Competitive Term"
      end
    elsif self.auction_type == 'publishing_rights' || self.auction_type == 'sub_publishing_rights'
      if self.publishing_term == "" || self.publishing_term == "0"
        validations << "Publishing term cannot be zero"
      end
      if self.competitive_term.nil?
        validations << "Select an Competitive Term"
      end
    end

    # Signers
    if self.auction_type == 'recording_publishing_rights' && self.recording_publishing_different_info
      if self.publisher_contact_email_confirmed == false
        validations << "Validate publisher's email address"
      end
    end

    # Validate if each song has music uploaded
    self.auction_songs.each do |song|
      if song.audio_filename.nil?
        validations << "Add a music sample for the song #{song.name}"
      end
    end
    # Validate if the illustration was setted
    if self.illustration_filename.nil?
      validations << "Upload an illustration"
    end
    if self.auction_signers.count == 0
      validations << "You have to add at least one signer"
    end
    if self.licensor_legal_name == '' or self.licensor_legal_name.nil?
      validations << "Enter a licensor legal name"
    end
    if self.licensor_legal_address_line1 == '' or self.licensor_legal_address_line1.nil?
      validations << "Enter a legal address"
    end
    if self.licensor_legal_address_country == '' or self.licensor_legal_address_country.nil?
      validations << "Select an address country"
    end
    if self.licensor_phone_number == '' or self.licensor_phone_number.nil?
      validations << "Enter a phone number"
    end

    if self.recording_publishing_different_info
      if self.publisher_legal_name == '' or self.publisher_legal_name.nil?
        validations << "Enter a publisher legal name"
      end
      if self.publisher_legal_address_line1 == '' or self.publisher_legal_address_line1.nil?
        validations << "Enter a legal address for publisher"
      end
      if self.publisher_legal_address_country == '' or self.publisher_legal_address_country.nil?
        validations << "Select an address country for publisher"
      end
      if self.publisher_phone_number == '' or self.publisher_phone_number.nil?
        validations << "Enter a phone number for publisher"
      end
    end

    if self.applicable_law_for_license == '' or self.applicable_law_for_license.nil?
      validations << "Enter an applicable national law for the agreement"
    end

    if self.place_of_court == '' or self.place_of_court.nil?
      validations << "Enter a Place of Court"
    end

    if not self.licensor_phone_number_verified
      validations << "Verify licensor phone number"
    end
    return validations
  end

  def bids_count
    auction_bids = AuctionBid.where(:auction_bidder_id.in => self.auction_bidders.distinct(:id))
    return auction_bids.count
  end

  def max_bid
    auction_bids = AuctionBid.where(:auction_bidder_id.in => self.auction_bidders.distinct(:id))
    max_bid = auction_bids.last
  end

  def sub_total
    sub_total = 0
    if self.auction_type == 'recording_publishing_rights'
      sub_total = (self.max_bid.amount.round + self.max_bid.amount_publishing.round) rescue 0
    else
      if self.competitive_term == 'advance'
        sub_total = self.max_bid.amount.round rescue 0
      elsif self.competitive_term == 'royalty_rate'
        sub_total = self.required_advance_or_royalty_rate.round rescue 0
      end
    end
    return sub_total
  end

  def transaction_fee
    amount = self.sub_total
    result = {:credit_card => (amount/(1-0.03)-amount).round, bitcoin: (amount/(1-0.005)-amount).round}
  end

  def tuneteams_commision
    amount = self.sub_total
    result = (amount * 0.07).round
  end

  # If nobody bid, restart the auction
  def self.check_restart
    puts "run restart at #{Time.zone.now}"
    Auction.where(:due_date.lte => Time.zone.now, status: "approved").each do |auction|

      if auction.auction_bids.count == 0
        status = "ended_no_bids"

        case auction.auction_length
        when 7
          if auction.number_restart < 3
            auction.restart
            auction.send_restart_notification
          else
            auction.update_attributes(status: status)
          end
        when 14
          if auction.number_restart < 2
            auction.restart
            auction.send_restart_notification
          else
            auction.update_attributes(status: status)
          end
        when 30
          if auction.number_restart < 1
            auction.restart
            auction.send_restart_notification
          else
            auction.update_attributes(status: status)
          end
        end
      else
        # status = "ended_reserve_not_met"
        #
        # auction.update_attributes(status: status)
      end
    end

  end

  def restart
    if self.auction_bids.count > 0
      self.auction_bids.delete_all
    end
    self.approve
    self.update_attributes(number_restart: self.number_restart + 1, reserve_not_met_notification_sent: false)
    self.auction_bidders.update_all(second_offer_sent: false)
  end

  def send_restart_notification
    if self.profile.user.notification_auction_restart
      AuctionsMailer.restart(self.id).deliver
    end
  end

  # Check all the published Auctions and sends the contract if there is a winner
  # This method is consumed by a cronjob configured in the server with whenever
  def self.check_winner
    puts "run check_winner at #{Time.zone.now}"

    Auction.where(:due_date.lte => Time.zone.now, status: "approved").each do |auction|
      puts "Enter here"

      auction_bids = auction.auction_bids #AuctionBid.where(:auction_bidder_id.in => auction.auction_bidders.distinct(:id))

      if auction_bids.count == 1
        max_bid = auction_bids.last
        if max_bid.amount > auction.reserve_on_competitive_term
          auction.buid_license_contract(auction, max_bid, max_bid.auction_bidder)
          break
        end
      end

      if auction_bids.count > 0
        max_bid = auction_bids.last
        auction_bidder = AuctionBidder.find(max_bid.auction_bidder_id)

        if max_bid.amount < auction.reserve_on_competitive_term
          # If the reserve was not met, send an email to the auction manager with the option to make an offer
          if !auction.reserve_not_met_notification_sent
            if not auction.auction_bidders.where(second_offer_sent: true).exists?
              # Send notification to auction manager
              BidsMailer.reserve_not_met(auction.id).deliver
              auction.update_attributes(reserve_not_met_notification_sent: true, status: 'ended_reserve_not_met')

              # Send notification to bidders
              bidders = auction.auction_bidders
              bidders.each do |bidder|
                BidsMailer.reserve_not_met_bidder(auction.id, bidder.profile.user.email).deliver
              end
            end
          end
        else
          auction.buid_license_contract(auction, max_bid, auction_bidder)
        end

      end
    end
  end

  def buid_license_contract(auction, max_bid, auction_bidder)
    hellosign_signers = []
    # Add the las bidder (max bidder) as a signer in the contract
    auction.auction_signers.push(AuctionSigner.new(email: auction_bidder.profile.user.email,
                                                   name: auction_bidder.profile.name,
                                                   party_from: "bidder"))
    # If the bidder has more than one signer they have to sign too
    if auction_bidder.has_second_signer
      auction.auction_signers.push(AuctionSigner.new(email: auction_bidder.profile.second_licensor_contact_email,
                                                     name: auction_bidder.profile.second_licensor_signer_name,
                                                     party_from: "bidder"))
    end

    # Put all the signers in an array
    auction.auction_signers.each do |auction_signer|
      hellosign_signers.push({:email_address => auction_signer.email, :name => auction_signer.name})
    end


    random_name = "temp_#{SecureRandom.hex}"
    auction.update_attributes(status: "waiting_sign")
    if auction.generate_license(random_name)
      Rails.logger.debug { "generate_license" }
      contract_generated = auction.generate_hellosign_contract(hellosign_signers, random_name)
      Rails.logger.debug { contract_generated }
      if contract_generated
        Rails.logger.debug { "contract_generated" }
        # Send notification to respective signers
        auction.auction_signers.each do |auction_signer|
          Rails.logger.debug { "auction_signer" }
          Rails.logger.debug { auction_signer }
          if auction_signer.party_from == "auction_manager"
            BidsMailer.sign_contract_auction_manager(auction.id,
                                                     auction_bidder.profile.name,
                                                     auction_bidder.profile.permalink,
                                                     auction.profile.name,
                                                     auction.profile.permalink,
                                                     max_bid.amount,
                                                     auction_signer.id,
                                                     auction.profile.user.email).deliver
          elsif auction_signer.party_from == "bidder"
            BidsMailer.sign_contract_winner(auction.id,
                                            auction_bidder.profile.name,
                                            auction_bidder.profile.permalink,
                                            auction.profile.name,
                                            auction.profile.permalink,
                                            max_bid.amount,
                                            auction_signer.id,
                                            auction_signer.email).deliver
          end
        end
        # Send a message to all the bidders who participated and didn't win
        lower_bidders = AuctionBidder.where(:id.ne => auction_bidder.id, :auction => auction)
        lower_bidders.each do |lower_bidder|
          BidsMailer.losers(auction.id, lower_bidder.profile.user.email).deliver
        end
      else
        auction.update_attributes(status: "approved")
      end
    end

  end

  # Generates the HelloSign contract
  def generate_hellosign_contract (hellosign_signers, file_name)
    Rails.logger.debug { "generate_hellosign_contract" }
    begin
      files_paths = []
      files_paths.push(Rails.root.join("public/pdfs/licenses", "#{file_name}.pdf").to_s)
      # TODO: generate the contract with the headers for licesee and licensor

      case self.auction_type
      when "recording_rights"
        subject = "License Agreement For Recording Rights"
      when "publishing_rights"
        subject = "Music Publishing Agreement"
      when "sub_publishing_rights"
        subject = "Sub-Publishing Agreement"
      end

      test_mode = if ENV["CURRENT_ENV"] == 'production' then 0 else 1 end

      client = HelloSign.client
      client.create_embedded_signature_request(:test_mode => test_mode,
                                               :files => files_paths,
                                               :signers => hellosign_signers,
                                               :subject => subject,
                                               :message => "This is your tuneteams generated contract for the auction #{self.name}",
                                               :metadata => {:auction_permalink => self.permalink})
      return true
    rescue HelloSign::Error::Error => e
      Rails.logger.debug { e }
      ExceptionNotifier.notify_exception(e)
      return false
    rescue StandardError => e
      Rails.logger.debug { e }
      ExceptionNotifier.notify_exception(e)
      return false
    end
  end

  # Check all the Auctions that are waiting for signature
  # This method is consumed by a cronjob configured in the server with whenever
  def self.check_signatures
    puts "run check_signatures at #{Time.zone.now}"
    Auction.where(status: "waiting_sign").each do |auction|
      everyone_signed = false
      auction.auction_signers.each do |auction_signer|
        everyone_signed = true
        if auction_signer.signed == false
          everyone_signed = false
          break
        end
      end

      # if everyone signed send a email with the payment link to the licensor
      if everyone_signed
        signed_date = Time.now
        if auction.update_attributes(status: "waiting_payment", signed_date: signed_date)
          licensee = auction.auction_signers.where(party_from: "bidder").first
          auction.download_hellosign_contract
          BidsMailer.pay_auction(auction.id,
                                 auction.profile.name,
                                 auction.profile.permalink,
                                 licensee.name,
                                 licensee.email,
                                 signed_date).deliver

          BidsMailer.contract_to_licensor(auction.id, licensee.name).deliver

        end
      else
        # Send emails reminding that they have to sign the contract
        elapsed_time = Time.now - auction.due_date
        if elapsed_time > Utility.days_or_minutes(4).seconds
          if auction.signature_notification_sent == 0
            auction.auction_signers.where(signed: false).each do |auction_signer|
              auction.update_attributes(signature_notification_sent: 4)
              AuctionsMailer.contract_not_signed_four_days(auction.id,
                                                           auction_signer.id,
                                                           auction_signer.name,
                                                           auction_signer.email).deliver
            end
          end
        end
        if elapsed_time > Utility.days_or_minutes(6).seconds
          if auction.signature_notification_sent == 4
            auction.auction_signers.where(signed: false).each do |auction_signer|
              auction.update_attributes(signature_notification_sent: 6)
              AuctionsMailer.contract_not_signed_six_days(auction.id,
                                                          auction_signer.id,
                                                          auction_signer.name,
                                                          auction_signer.email).deliver
            end
          end
        end
        if elapsed_time > Utility.days_or_minutes(7).seconds
          if auction.signature_notification_sent == 6
            signers_not_signed = []
            auction.auction_signers.each do |auction_signer|
              if not auction_signer.signed
                signers_not_signed << auction_signer.name
              end
            end
            if signers_not_signed.length > 0
              auction.update_attributes(signature_notification_sent: 7)
              AuctionsMailer.contract_not_signed_admin(auction.id,
                                                       signers_not_signed).deliver
            end
          end
        end
      end
    end
  end

  # Send an email to the admin if the auction manager didn't send the materials
  def self.check_materials_received
    puts "run check_materials_received at #{Time.zone.now}"
    Auction.where(status: "paid").each do |auction|
      elapsed_time = Time.now - auction.due_date
      # 6 days, in delevopment/staging is 6 minutes
      if elapsed_time > Utility.days_or_minutes(6).seconds
        if auction.materials_received_admin_notification == 0
            auction.update_attributes(materials_received_admin_notification: 1)
            AuctionsMailer.materials_not_sent_admin(auction.id).deliver
        end
      end
    end
  end

  # download the contract from HelloSign
  def download_hellosign_contract
    begin
      client = HelloSign.client
      file_bin = client.signature_request_files :signature_request_id => self.signature_request_id, :file_type => 'pdf'
      random_name = SecureRandom.hex
      path = "pdfs/contracts/#{random_name}.pdf"

      open("public/#{path}", "wb") do |file|
        file.write(file_bin)
      end

      self.update_attributes(hellosign_file_name: random_name)

    rescue HelloSign::Error::Error => e
      ExceptionNotifier.notify_exception(e)
    rescue StandardError => e
      ExceptionNotifier.notify_exception(e)
    end

  end

  def auction_status

    case self.status
    when 'rejected'
      status_code = -1
      name = "Not Approved"
    when 'draft'
      status_code = 0
      name = "Draft"
    when 'ended_no_bids'
      status_code = 1
      name = "Ended no bids"
    when 'ended_reserve_not_met'
      status_code = 1
      name = "Ended Reserve Not Met"
    when 'cancelled'
      status_code = 2
      name = "Cancelled"
    when 'published'
      status_code = 3
      name = "Published"
    when 'approved'
      status_code = 4
      name = 'Approved'
    when 'waiting_sign'
      status_code = 5
      name = 'Waiting Sign'
    when 'waiting_payment'
      status_code = 6
      name = 'Waiting Payment'
    when 'paid'
      status_code = 7
      name = 'Paid'
    when 'materials_received'
      status_code = 8
      name = 'Materials Received'
    when 'payout_made'
      status_code = 9
      name = 'Payout Made'
    end

    return { name: name, raw: self.status, code: status_code }
  end

  def auction_type_name
    name = ''
    if self.auction_type == 'recording_rights'
      name = "Recording"
    elsif self.auction_type == 'publishing_rights'
      name = "Publishing"
    elsif self.auction_type == 'sub_publishing_rights'
      name = "Sub Publishing"
    elsif self.auction_type == 'recording_publishing_rights' && self.publishing_agreement_type == 'publishing'
      name = "Recording & Publishing"
    elsif self.auction_type == 'recording_publishing_rights' && self.publishing_agreement_type == 'sub_publishing'
      name = "Recording & Sub-Publishing"
    end

    return name
  end

  def bidder_status(profile)
    self.auction_bidders.find_by(profile: profile).status rescue "guest"
  end

  def approve
    current_time = Time.now
    if ENV["CURRENT_ENV"] == 'production'
      due_date = current_time + self.auction_length.days
    else
      due_date = current_time + self.auction_length.minutes
    end
    self.update_attributes(status: "approved", approving_date: current_time, due_date: due_date)
  end

  def licensor_signer_name
    self.auction_signers[0].name rescue nil
  end

  def licensor_contact_email
    self.auction_signers[0].email rescue nil
  end

  def publisher_signer_name
    self.auction_signers[1].name rescue nil
  end

  def publisher_contact_email
    self.auction_signers[1].email rescue nil
  end

  def publisher_contact_email_confirmed
    self.auction_signers[1].email_confirmed rescue false
  end

  def generate_license(file_name)
    # pdf = WickedPdf.new.pdf_from_string(render_to_string(:pdf => "license", :template => "mailers/invitation.html.haml", :encoding => "UTF-8"))
    pdf = WickedPdf.new.pdf_from_string(ApplicationController.new.render_to_string(:template => "licenses/show.html", :layout => "pdf.html", :encoding => "UTF-8", :extra => 'pin', :locals => { :@auction => self }, :title => "License Agreement", :disable_javascript => true))

    # save PDF to a file
    save_path = Rails.root.join('public/pdfs/licenses', "#{file_name}.pdf")
    File.open(save_path, 'wb') do |file|
      file << pdf
    end
    return true
  end
end
