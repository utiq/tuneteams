class AuctionBid
  include Mongoid::Document
  include Mongoid::Timestamps

  # Attributes
  field :amount, type: Float
  field :maximum_amount, type: Float
  field :amount_publishing, type: Float
  field :maximum_amount_publishing, type: Float

  field :automated_bids_count, type: Integer, default: 0
  field :drive_up, type: Boolean, default: false
  field :reserve_met, type: Boolean, default: false

  # Associations
  # belongs_to :profile
  belongs_to :auction
  belongs_to :auction_bidder

  after_create :send_message

  def send_message
    # TODO: if the private_pub server doesn't respond continue with the process
		# PrivatePub.publish_to "/bids", :bid => self.as_json
    PrivatePub.publish_to "/bids", :bid => AuctionBidSerializer.new(self)
  end

  def as_json(options={})
    #TODO; Replace this for the serializer
   super( only: [:amount, :amount_publishing],
   		 include: { auction_bidder: {only: [:created_at],
   															include: { profile: { only: [:name, :permalink], methods: [:default_avatar] },
   											 							 		 auction: { only: [:name, :permalink]}
   										 							 		 }
   																}
   							})
	end
end
