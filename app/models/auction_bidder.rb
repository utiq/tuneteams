class AuctionBidder
  include Mongoid::Document
  include Mongoid::Timestamps

  # Attributes
  field :status, default: 'pending'
  field :has_second_signer, type: Boolean, default: false
  field	:is_winner, type: Boolean, default: false
  field :second_offer_sent, type: Boolean, default: false
  field :second_offer_status, default: 'none'
  # field :contract_read, type: Boolean, default: false

  # Associations
  belongs_to :profile
  belongs_to :auction
  has_many :auction_bids
end
