class AuctionPayout
  include Mongoid::Document
  include Mongoid::Timestamps

  # Attributes
  field :amount
  field :method, default: 'payoneer'
  field :status, default: 'pending'

  # Associations
  # belongs_to :user
  belongs_to :auction
end