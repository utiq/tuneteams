class AuctionSigner
  include Mongoid::Document
  include Mongoid::Timestamps

  # Attributes
  field :name
  field :email
  field :email_confirmed, type: Boolean, default: false
  field :signature_id
  field :party_from, type: String, default: "auction_manager"
  field :sign_url
  field :signed, type: Boolean, default: false

  # Associations
  embedded_in :auction
  before_save :email_is_confirmed

  def email_is_confirmed

    # Check in the Users database if the email exists and is confirmed
    if self.email_confirmed == false
      if User.where(email: self.email, email_confirmed: true).exists?
        self.email_confirmed = true
      end
    end

    # Check in the Auctions database if the email exists and is confirmed
    if self.email_confirmed == false
      if Auction.where('auction_signers.email' => self.email, 'auction_signers.email_confirmed' => true).exists?
        self.email_confirmed = true
      end
    end
  end
end
