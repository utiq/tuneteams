class AuctionSong
  include Mongoid::Document
  include Mongoid::Timestamps

  # Attributes
  field :name
  field :writer
  field :publisher
  field :recording_rights, type: Boolean
  field :publishing_rights, type: Float
  field :external_audio_url
  # field :file

  # Associations
  embedded_in :auction

  # Uploaders
  mount_uploader :audio, AuctionSongUploader, mount_on: :audio_filename
end
