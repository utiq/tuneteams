class AuctionVideo
  include Mongoid::Document

  # Attributes
  field :url
  field :name
  field :duration
  field :youtube_id, type: String
  field :position, default: 0, type: Integer

  # Associations
  embedded_in :auction
  before_save :get_youtube_data

  def get_youtube_data
    if self.url != nil
      uri = URI.parse(self.url)
      uri_params = CGI.parse(uri.query)
      self.youtube_id = uri_params['v'][0]
    end
  end
end
