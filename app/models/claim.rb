class Claim
  include Mongoid::Document
  include Mongoid::Timestamps

  # Attributes
  field :name
  field :email
  field :status, type: String, default: 'claimed'

  # Relations
  belongs_to :profile

  # Validations
  validates :name,
            :presence => {:message => "Enter your name"}
  validates :email,   
            :presence => {:message => "Email cannot be blank"},   
            :format => { :with => /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i, :message => "Invalid email" }
end
