class Connection
  include Mongoid::Document
  include Mongoid::Timestamps

  # Attributes
  field :profile_id, type: String
  field :name, type: String
  field :default_avatar, type: Hash
  field :permalink, type: String
  field :industry_role, type: String
  field :country, type: String
  field :genres, type: Array
  field :status, type: String, default: 'active'

  # Associations
  embedded_in :profile
end
