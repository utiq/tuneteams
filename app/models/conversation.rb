class Conversation
  include Mongoid::Document
  include Mongoid::Timestamps

  # Attributes
  field :body
  field :is_read, type: Boolean, default: false
  field :status, type: String, default: 'inbox'

  # Associations
  # The relation is with conversation is with the default profile because for the future is prepared to send messages from any profile that you are owner
  belongs_to :recipient, :class_name => "Profile", :inverse_of => :received_messages
  belongs_to :sender, :class_name => "Profile", :inverse_of => :sent_messages
  belongs_to :message

  def self.attribute_names
    fields.keys | ["country_names", "interest_names"]
  end

end
