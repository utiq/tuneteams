require 'elasticsearch/model'

class Country
  include Mongoid::Document
  include Elasticsearch::Model
  include Elasticsearch::Model::Callbacks

  # Attributes
  field :name, localize: true, type: String
  field :position
  field :iso2
  field :calling_code

  # Associations
  has_many :profiles #after_touch() { tire.update_index }
  # after_update { self.profiles.each(&:touch) }
  # after_touch [ lambda { |a,c| a.__elasticsearch__.index_document } ]

  #Indexes
  index({ name: 1 })
end
