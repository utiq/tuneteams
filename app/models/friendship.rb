class Friendship
  include Mongoid::Document
  include Mongoid::Timestamps

  # Attributes
  field :invitation_message
  field :status, :type => String, :default => 'pending' #pending, ignored, deleted, accepted

  # Associations
  belongs_to :owner, :class_name => "Profile"
  belongs_to :friend, :class_name => "Profile"
  

  # TODO: Raise validator
end
