class Genre
  include Mongoid::Document

  # Attributes
  field :name
  field :permalink
  field :name_length, type: Integer

  #Indexes
  index({ name: 1 })

  before_create :generate_permalink

  def generate_permalink
    i = 0
    begin
      i += 1
      if i == 1
        self.permalink = create_permalink(self.name)
      else
        self.permalink = create_permalink(self.name) + "-" + i.to_s
      end
    end while self.class.where(permalink: permalink).exists?
  end

  def create_permalink(string, separator = '-', max_size = 160)
    ignore_words = ['a', 'an', 'the']
    ignore_re = String.new
    ignore_words.each{ |word| ignore_re << word + '\b|\b' }
    ignore_re = '\b' + ignore_re + '\b'
    permalink = string.gsub("'", separator)
    permalink.gsub!(ignore_re, '')
    permalink = permalink.parameterize
    permalink.gsub!(/[^a-z0-9]+/, separator)
    permalink = permalink.to(max_size)
    return permalink.gsub(/^\\#{separator}+|\\#{separator}+$/, '')
  end
end
