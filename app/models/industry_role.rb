class IndustryRole
  include Mongoid::Document

  # Attributes
  field :name
  field :status, type: String, default: "active"
  field :default_avatar, type: String

  # Associations
  has_many :subscribers
  has_many :industry_role_profiles
  
end
