class IndustryRoleProfile
  include Mongoid::Document

  # Attributes
  field :name
  field :title, default: 'singular'
  field :status, type: String, default: 'active'
  field :is_executive, type: Boolean, default: false
  field :position

  # Associations
  has_many :subscribers
  has_many :sections
  has_many :profiles
  belongs_to :industry_role

  embeds_many :industry_role_profile_interests
end
