class IndustryRoleProfileInterest
  include Mongoid::Document

  field :alias, type: String
  field :related_roles, type: Array

  embedded_in :industry_role_profile
end
