class Invitation
  include Mongoid::Document
  include Mongoid::Timestamps
  
  # Attributes
  field :email
  field :current_ip
  field :token
  field :referred
  field :country
  field :city
  field :invitation_sent, type: Boolean, default: false
  field :invitation_token, type: String 
  field :invitation_signed_up, type: Boolean, default: false
  field :status, type: String, default: 'pending'
  field :category, type: String, default: 'request'
  
  # Associations
  before_create :generate_token
  belongs_to :industry_role_profile
  
  # Validations
  validates :industry_role_profile_id,
            :presence => {:message => "Enter your job type"}, :if => :is_request?
  validates :email,   
            :presence => {:message => "Email cannot be blank"},   
            :uniqueness => {:message => "A request for invitation was received with this email"},
            :format => { :with => /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i, :message => "Invalid email" }
         
  def is_request?
    category == 'request' 
  end

  private

  def generate_token
    begin
      self.token = SecureRandom.hex(3)
    end while self.class.where(token: token).exists?
  end
end
