class MailingCampaign
  include Mongoid::Document
  include Mongoid::Timestamps

  # Attributes
  field :name
  field :subject
  field :body
  field :test_emails
  field :status, type: String, default: 'created'
end
