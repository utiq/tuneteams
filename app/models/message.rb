class Message
  include Mongoid::Document
  include Mongoid::Timestamps::Created

  # Attributes
  field :subject

  # Associations
  has_many :conversations

  #Indexes
  index({ subject: 1 })
end
