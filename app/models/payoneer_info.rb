class PayoneerInfo
  include Mongoid::Document
  include Mongoid::Timestamps::Created

  # Attributes
  field :sign_up_url
  field :status, default: 'link_generated'
  field :accept_ach, type: Boolean, default: false
  field :accept_iach, type: Boolean, default: false
  field :accept_card, type: Boolean, default: false

  # Associations
  embedded_in :user

end


#link_generated: Every time a user is created in the Model payoneer_payee_url is called