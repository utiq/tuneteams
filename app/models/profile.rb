require 'elasticsearch/model'

class Profile
  include Mongoid::Document
  include Mongoid::Timestamps
  include Elasticsearch::Model
  include Elasticsearch::Model::Callbacks
  include Mongoid::Orderable

  # Attributes
  field :name, type: String
  field :web_page, type: String
  field :permalink, type: String
  field :has_owner, type: Boolean, default: true
  field :owner_contact_email, type: String
  field :is_default, type: Boolean
  field :show_connections_list, type: Boolean, default: false
  field :who_can_see_connection_list, type: String, default: 'all'
  field :cover_type, type: String, default: 'default'
  field :avatar_type, type: String, default: 'default'
  field :tour_viewed, type: Boolean, default: false
  field :rating, type: Integer, default: 1
  field :show_in_main_page, type: Boolean, default: false
  field :num_views, type: Integer, default: 0
  # Attributes related with legal details to auction
  field :licensor_legal_name
  field :licensor_legal_address_line1
  field :licensor_legal_address_line2
  field :licensor_signer_name
  field :licensor_contact_email
  field :licensor_phone_number
  field :licensor_calling_code
  field :licensor_phone_number_verification_code
  field :licensor_phone_number_verified, type: Boolean, default: false
  field :licensor_legal_address_country
  field :position, type: Integer

  field :related_profiles, type: Array, default: []
  field :related_auctions, type: Array, default: []

  # TODO: See in the future if this can be changed for a collection, now we don't know if there will be more than 1 aditional signers (move to bidders collection)
  field :second_licensor_signer_name
  field :second_licensor_contact_email
  field :second_licensor_contact_email_confirmed, type: Boolean, default: false

  # Associations
  belongs_to :user
  belongs_to :country
  belongs_to :industry_role_profile
  has_many :widgets
  has_many :claims
  has_and_belongs_to_many :genres, inverse_of: nil
  has_many :friendships, :inverse_of => :owner
  has_many :auctions
  # has_many :auction_bids
  has_many :auction_bidders
  embeds_many :connections
  embeds_one :profile_working_at
  orderable

  # Messages
  has_many :sent_messages, :class_name => 'Conversation', :inverse_of => :sender
  has_many :received_messages, :class_name => 'Conversation', :inverse_of => :recipient

  # Uploaders
  mount_uploader :avatar, ProfileAvatarUploader, mount_on: :avatar_filename
  mount_uploader :cover, ProfileCoverUploader, mount_on: :cover_filename

  # Validations
  validates :industry_role_profile_id,
            :presence => {:message => "Select your job type"}
  validates :country_id,
            :presence => {:message => "Select your country"}
  validates :name,
            :presence => {:message => "Name cannot be blank"},
            :uniqueness => {:message => "Name was already registered", :case_sensitive => false}

  #Indexes
  # index({ name: 1 })
  index({ permalink: 1 }, { unique: true})

  before_create :generate_widgets
  before_create :generate_permalink
  before_create :assign_default
  before_update :generate_permalink, :if => :name_changed?
  after_save :update_indexes
  before_save :second_licensor_contact_email_is_confirmed

  def update_indexes
    self.__elasticsearch__.index_document# if self.genre_ids_changed? || self.country_id_changed? || self.industry_role_profile_id_changed? || self.permalink_changed?
  end

  def index_images
    if (self.avatar_url(:medium))
      self.__elasticsearch__.index_document
    end
  end

  # index_name "profiles-index" # elasticsearch index
  def as_indexed_json(options={})
    # as_json(except: [:id, :_id, :is_default, :has_owner])
    as_json(
        methods: [:default_avatar, :default_cover],
           only: [:name, :permalink, :position],
        include: { methods: [:default_avatar, :default_cover],
                   # widgets: { only: [:widget_discographies, :widget_overview]},
                   # widgets: { include: [:widget_discographies]},
                   # include: [:widget_interest]
                   widgets: { },
                   country: { only: [:_id, :name] },
                   industry_role_profile: { only: [:_id, :name] },
                   genres: { only: [:_id, :name] }
                 }
    )
  end

  def self.search(query, type, offset, limit)
    if type == "simple"
      __elasticsearch__.search(
        {
          from: 0, size: 5,
          query: {
            multi_match: {
              query: query,
              type: "phrase_prefix",
              fields: ['name^20', 'genres.name^10', 'country.name^5', 'industry_role_profile.name']
            }
          }
        }
      )
    elsif type == "complex"
      __elasticsearch__.search(
        {
          from: offset, size: limit, #TODO: Implement pagination
          query: {
            multi_match: {
              query: query,
              type: "phrase_prefix",
              fields: ['name^10', 'genres.name', 'country.name', 'industry_role_profile.name', 'widgets.widget_peoples.name']
            }
          }
        }
      )
    elsif type == "and"
      Rails.logger.debug "poto"
      # __elasticsearch__.search(
      #   {
      #     from: offset, size: limit, #TODO: Implement pagination
      #     query: {
      #       multi_match: {
      #         query: query,
      #         type: "cross_fields",
      #         fields: ['country.name', 'genres.name', 'industry_role_profile.name', 'widgets.widget_interest.interests'],
      #         # fields: ['country.name', 'genres.name', 'industry_role_profile.name', 'widgets.widget_interest.interests.interest_names'],
      #         operator: "and"
      #       }
      #     }
      #   }
      # )
      __elasticsearch__.search(
        from: offset, size: limit,
        query: {
          query_string: {
            query: query
          }
        },
        sort: [{ position: {order: 'asc'}}]
      )
    end

  end

  def generate_widgets
    # sections = Section.where(:industry_role_profile_id => self.industry_role_profile_id, :content_type.ne => "overview")
    sections = Section.where(:industry_role_profile_id => self.industry_role_profile_id)
    sections.each do |section|
      widget = Widget.new(profile_id: self.id, section_id: section.id)
      if section.content_type == "overview"
        widget.widget_overview = WidgetOverview.new()
      end
      if section.content_type == "interest"
        widget.widget_interest = WidgetInterest.new()
      end
      widget.save
    end
  end

  def generate_permalink
    i = 0
    begin
      i += 1
      if i == 1
        self.permalink = Utility.create_permalink(self.name)
      else
        self.permalink = Utility.create_permalink(self.name) + "-" + i.to_s
      end
    end while self.class.where(permalink: permalink).exists?
  end

  def assign_default
    self.is_default = false
    if self.user.profiles.count == 0
      self.is_default = true
    end
  end

  def self.promoted(profile_id = nil)
    if profile_id.nil?
      self.all.order_by("position ASC").limit(21)
    else
      current_profile = self.find(profile_id)
      self.nin(:_id => current_profile.connections.map { |x| x.profile_id.to_s }).limit(21).order_by("position ASC")
    end
  end

  def default_avatar
    default_avatar = {}
    if self.avatar_filename.nil?
      @default_avatar_name = self.industry_role_profile.industry_role.default_avatar
      default_avatar = {"thumb" => "#{ENV["HOST_NAME"]}/assets/icons/#{@default_avatar_name}.png",
                        "small" => "#{ENV["HOST_NAME"]}/assets/icons/#{@default_avatar_name}.png",
                        "medium" => "#{ENV["HOST_NAME"]}/assets/icons/#{@default_avatar_name}.png",
                        "large" => "#{ENV["HOST_NAME"]}/assets/icons/#{@default_avatar_name}.png"}
    else
      default_avatar = {"thumb" => self.avatar_url(:thumb),
                        "small" => self.avatar_url(:small),
                        "medium" => self.avatar_url(:medium),
                        "large" => self.avatar_url(:large)}
    end
    return default_avatar

  end

  def default_cover
    default_cover = {}
    if self.cover_filename.nil?
      # resize the cover
      default_cover = {"thumb" => "#{ENV["HOST_NAME"]}/assets/default-cover.jpg",
                        "small" => "#{ENV["HOST_NAME"]}/assets/default-cover.jpg",
                        "medium" => "#{ENV["HOST_NAME"]}/assets/default-cover.jpg",
                        "large" => "#{ENV["HOST_NAME"]}/assets/default-cover.jpg"}
    else
      default_cover = {"thumb" => self.cover_url(:thumb),
                        "small" => self.cover_url(:small),
                        "medium" => self.cover_url(:medium),
                        "large" => self.cover_url(:large)}
    end
    return default_cover

  end

  def are_friends? (visitor_profile_id)
    return self.connections.where(profile_id: visitor_profile_id).exists?
  end

  # Begin fields related with auctions
  def phone_number_complete
    "+#{self.licensor_calling_code}#{self.licensor_phone_number}"
  end

  def licensor_contact_email
    if self[:licensor_contact_email].present? and !self[:licensor_contact_email].nil?
      return self[:licensor_contact_email]
    else
      return self.user.email
    end
  end

  def second_licensor_contact_email_is_confirmed

    # Check in the Users database if the email exists and is confirmed
    if self.second_licensor_contact_email_confirmed == false
      if User.where(email: self.second_licensor_contact_email, email_confirmed: true).exists?
        self.second_licensor_contact_email_confirmed = true
      end
    end

    # Check in the Auctions database if the email exists and is confirmed
    if self.second_licensor_contact_email_confirmed == false
      if Auction.where('auction_signers.email' => self.second_licensor_contact_email, 'auction_signers.email_confirmed' => true).exists?
        self.second_licensor_contact_email_confirmed = true
      end
    end
  end

end
