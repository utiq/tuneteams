class ProfileWorkingAt
  include Mongoid::Document

  # Attributes
  field :profile_id
  field :name
  field :profile_permalink
  field :added_manually, type: Boolean

  # Associations
  embedded_in :profile
end
