class Section
  include Mongoid::Document

  # Attributes
  field :title
  field :type
  field :content_type
  field :position, type: Integer, default: 0
  field :options, type: Hash, default: {}
  field :help_text, type: Hash, default: {}

  # Associations
  belongs_to :industry_role_profile
  has_many :widgets

end
