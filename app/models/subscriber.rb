class Subscriber
  include Mongoid::Document
  include Mongoid::Timestamps
  
  # Attributes
  field :email
  field :current_ip
  field :token
  field :referred
  field :country
  field :city
  field :invitation_sent, type: Boolean, default: false
  field :invitation_token, type: String 
  field :invitation_signed_up, type: Boolean, default: false
  
  # Associations
  before_create :generate_token
  # belongs_to :industry_role
  belongs_to :industry_role_profile
  
  # Validations
  validates :industry_role_profile_id,
            :presence => {:message => "enter your job type"}
  validates :email,   
            :presence => {:message => "cannot be blank"},   
            :uniqueness => {:message => "already registered"}
            # :format => { :with => /^([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})$/i, :message => "invalid" }
            
  private

  def generate_token
    begin
      self.token = SecureRandom.hex(3)
    end while self.class.where(token: token).exists?
  end
  
end
