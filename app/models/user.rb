class User
  include Mongoid::Document
  include Mongoid::Timestamps
  include ActiveModel::SecurePassword

  # Attributes
  field :first_name
  field :last_name
  field :email
  field :coordinates, type: Array
  field :last_login, type: DateTime
  field :login_count, type: Integer, default: 0
  field :email_confirmed, type: Boolean, default: false
  field :password_reset_sent_at, type: DateTime
  field :temporary_token, type: String # Used for email confirmations and password resets
  field :password_salt, type: String
  field :password_digest, type: String # Encrypted password to compare with encrypted login password
  field :persistence_token, type: String # Session token that is cleared only when the user explicitly signs out
  field :access_token
  field :city
  field :country
  field :role, default: 'user'
  field :notification_messages_or_invitations, type: Boolean, default: true
  field :notification_mailing, type: Boolean, default: true
  field :notification_surveys, type: Boolean, default: true
  field :notification_auction_approval, type: Boolean, default: true
  field :notification_auction_restart, type: Boolean, default: true
  field :stripe_customer_token

  # Associations
  has_many :profiles
  has_many :auction_watchlists
  embeds_one :payoneer_info
  # has_many :user_legal_infos

  # Indexes
  index persistence_token: 1 # Because we are retrieving all users by persistence_token
  index({ email: 1 }, { unique: true})
  index access_token: 1

  # Validations
  validates :email,
            :presence => {:message => "Email cannot be blank"},
            :uniqueness => {:message => "Email was already registered"},
            :format => { :with => /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i, :message => "Invalid email" }
  # validates_presence_of :password, on: :create

  before_create :generate_access_token
  after_update :flush_cache # TODO: Should be after_commit, but there is an error with sidekiq

  def password=(password_str)
    @password = password_str
    self.password_salt   = BCrypt::Engine.generate_salt
    self.password_digest = BCrypt::Engine.hash_secret(password_str, password_salt)
  end

  def authenticate(password)
    password.present? && password_digest.present? && password_digest == BCrypt::Engine.hash_secret(password, password_salt)
  end

  def password_required?
    (!password_digest.blank?)
  end

  def payoneer_payee_url
    if self.payoneer_info.nil?
      payoneer_status = Payoneer::System.status
      if payoneer_status.ok?
        response = Payoneer::Payee.signup_url(self.id.to_s)
        signup_url = response.body if response.ok?
        self.create_payoneer_info(sign_up_url: signup_url)
      end
    else
      signup_url = self.payoneer_info.sign_up_url
    end
    return signup_url
  end

  def default_profile
    return self.profiles.where(is_default: true).first
  end

  # Roles
  def has_role?(role_name)
    self.roles.include?(role_name.to_s)
  end

  def is_admin?
    self.role == "admin" rescue false
  end

  def is_owner?(profile_id)
    self.profiles.where(id: profile_id).exists? rescue false
  end

  def grant_role(role_name)
    self.roles << role_name.to_s unless self.has_role?(role_name.to_s)
  end

  def revoke_role(role_name)
    self.roles.delete(role_name.to_s)
  end

  def full_name
    [first_name, last_name].join(' ')
  end

  def full_name=(name)
    split = name.split(' ', 2)
    self.first_name = split.first
    self.last_name = split.last
  end

  def avatar(size)

  end

  # Caching
  def self.cached_find(id)
    Rails.cache.fetch([name, id], expires_in: 5.minutes) { find(id) }
  end

  def self.cached_find_by_access_token(access_token)
    Rails.cache.fetch([name, access_token], expires_in: 5.minutes) { find_by(access_token: access_token) }
  end

  def flush_cache
    Rails.cache.delete([self.class.name, access_token])
  end

  private

  def generate_access_token
    begin
      self.access_token = SecureRandom.hex
    end while self.class.where(access_token: access_token).exists?
  end
end
