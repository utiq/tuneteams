require 'elasticsearch/model'

class Widget
  include Mongoid::Document
  include Elasticsearch::Model
  include Elasticsearch::Model::Callbacks

  # Attributes
  field :type # types: [overview, spotify_music, youtube_video, people, discography]

  # Associations
  belongs_to :section
  belongs_to :profile

  # Widgets
  embeds_one :widget_overview
  embeds_one :widget_interest
  embeds_many :widget_youtubes
  embeds_many :widget_spotifies
  embeds_many :widget_peoples
  embeds_many :widget_discographies

  after_touch() { profile.__elasticsearch__.index_document }
end
