class WidgetDiscography
  include Mongoid::Document

  # Attributes
  field :name
  field :external_url

  # Associations
  embedded_in :widget
end
