class WidgetInterest
  include Mongoid::Document

  # Attributes
  field :interests, type: Array
  field :countries, type: Array

  # Associations
  embedded_in :widget

  def self.attribute_names
    fields.keys | ["country_names", "interest_names"]
  end

  # ElasticSearch Indexing
  after_save do
    widget.touch
  end
end
