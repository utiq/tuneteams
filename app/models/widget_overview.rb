class WidgetOverview
  include Mongoid::Document

  # Attributes
  field :description

  # Associations
  embedded_in :widget

end
