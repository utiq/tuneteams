class WidgetPeople
  include Mongoid::Document

  # Attributes
  field :profile_id
  field :name
  field :role
  field :avatar_url_copy
  field :profile_permalink
  field :added_manually, type: Boolean

  # Associations
  embedded_in :widget

  # Uploaders
  mount_uploader :avatar, ProfileAvatarUploader, mount_on: :avatar_filename

  # ElasticSearch Indexing
  after_save do
    widget.touch
  end
end
