class WidgetSpotify
  include Mongoid::Document

  # Attributes
  field :url

  # Associations
  embedded_in :widget
end
