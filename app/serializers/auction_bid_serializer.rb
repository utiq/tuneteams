include ActionView::Helpers::DateHelper

class AuctionBidSerializer < ActiveModel::Serializer
  attributes :amount,
             :amount_publishing,
             :profile,
             :auction,
             :bid_time_ago_in_words,
             :second_offer_sent,
             :second_offer_status,
             :reserve_met

  def id
    object._id.to_s
  end

  def profile
    object.auction_bidder.profile.as_json(:only => [:name, :permalink], methods: [:default_avatar])
  end

  def second_offer_sent
    #
    # bidder = object.auction.auction_bids.where(auction_bidder: object.auction_bidder).order_by('amount DESC').limit(1).first
    # if bidder.exists?
    #   last_amount = bidder.amount
    #   return last_amount == object.amount && object.auction_bidder.second_offer_sent
    # else
    #   return false
    # end

    last_amount = object.auction.auction_bids.where(auction_bidder: object.auction_bidder).order_by('amount DESC').limit(1).first.amount
    return last_amount == object.amount && object.auction_bidder.second_offer_sent
  end

  def second_offer_status
    object.auction_bidder.second_offer_status
  end

  def auction
    object.auction.as_json(:only => [:name, :permalink])
  end

  def bid_time_ago_in_words
    time_ago_in_words(object.created_at)
  end

# super( only: [:amount, :amount_publishing],
#        include: { auction_bidder: {only: [:created_at],
#                                 include: { profile: { only: [:name, :permalink], methods: [:default_avatar] },
#                                            auction: { only: [:name, :permalink]}
#                                          }
#                                   }
#                 })

end
