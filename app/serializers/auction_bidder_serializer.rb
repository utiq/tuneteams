class AuctionBidderSerializer < ActiveModel::Serializer
  attributes :id,
             :requester_profile,
             :auction,
             :last_amount,
             :second_offer_sent,
             :status

  def id
    object._id.to_s
  end

  def requester_profile
    object.profile.as_json(:only => [:name, :permalink])
  end

  def auction
    object.auction.as_json(:only => [:name, :permalink])
  end

  def last_amount
    last_amount = object.auction.auction_bids.where(auction_bidder: object).order_by('amount DESC').limit(1).first
    if not last_amount.nil?
      return last_amount.amount
    else
      return 0
    end
  end

# super( only: [:amount, :amount_publishing],
#        include: { auction_bidder: {only: [:created_at],
#                                 include: { profile: { only: [:name, :permalink], methods: [:default_avatar] },
#                                            auction: { only: [:name, :permalink]}
#                                          }
#                                   }
#                 })

end
