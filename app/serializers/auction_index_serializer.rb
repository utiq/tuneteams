class AuctionIndexSerializer < ActiveModel::Serializer
  attributes :id,
             :name,
             :permalink,
             :description,
             :illustration,
             :default_illustration,
             :genre_ids,
             :country_ids,
             :bids_count,
             :profile_owner,
             :status,
             :notes,
             :bid_status,
             :auction_type,
             :auction_type_name,
             :status,
             :countries,
             :published_date,
             :created_at

  has_many :genres
  # has_many :countries

  def id
    object._id.to_s
  end

  def countries
    new_countries = []
    object.countries.each do |country|
      # I had to hardcode this part because the (Global Rights) text
      if country.id.to_s == '5420a193636573852c000000'
        country.name = "All Countries (Global Rights)"
      end
      new_countries << CountrySerializer.new(country, scope: @current_user)
    end
    return new_countries
  end

  def profile_owner
    object.profile.as_json(:only => [:name, :permalink])
  end

  def higher_bid
  	rand(1000..30000)
  end

  def bids_count
  	object.bids_count
  end

  def bid_status
    # if scope
    #   if object.auction_bidders.where(profile: scope.default_profile).first.is_winner
    #     return "buyed"
    #   else
    #     return "open"
    #   end
    # else
    #   return "open"
    # end
  end

  def status
    object.auction_status
  end

  def auction_type_name
    object.auction_type_name
  end

  def published_date
    if scope && scope.is_admin?
      object.publishing_date.to_time
    end
  end

  def notes
    if scope && scope.is_admin? == "admin"
      if object.notes.nil?
        return ""
      else
        return object.notes rescue ""
      end
    else
      return ""
    end
  else
    return ""
  end

end
