class AuctionSerializer < ActiveModel::Serializer
  attributes :id,
             :name,
             :permalink,
             :description,
             :default_illustration,
             :illustration,
             :genre_ids,
             :country_ids,
             :validations,
             :is_watching,
             :competitive_term,
             :competitive_term_name,
             :auction_type,
             :auction_type_name,
             :auction_length,
             :recording_term,
             :publishing_term,
             :publishing_agreement_type,
             :required_advance_or_royalty_rate,
             :required_advance_or_royalty_rate_publishing,
             :reserve_on_competitive_term,
             :reserve_on_competitive_term_publishing,
             :licensor_legal_name,
             :licensor_legal_address_line1,
             :licensor_legal_address_line2,
             :licensor_legal_address_country,
             :licensor_legal_address_country_name,
             :licensor_phone_number,
             :licensor_calling_code,
             :licensor_phone_number_verified,
             :licensor_signer_name,
             :licensor_contact_email,
             :recording_publishing_different_info,
             :publisher_legal_name,
             :publisher_legal_address_line1,
             :publisher_legal_address_line2,
             :publisher_legal_address_country,
             :publisher_legal_address_country_name,
             :publisher_phone_number,
             :publisher_calling_code,
             :publisher_signer_name,
             :publisher_contact_email,
             :publisher_contact_email_confirmed,
             :applicable_law_for_license,
             :applicable_law_for_license_name,
             :place_of_court,
             :due_date,
             :time_remaining,
             :bids,
             :bidders,
             :second_chance_sent,
             :reserve_was_met,
             :current_bidder_profile,
             :is_owner,
             :is_admin,
             :profile_owner,
             :notes,
             :status,
             :bidder_status,
             :can_bid,
             :second_licensor_contact_email,
             :second_licensor_contact_email_confirmed,
             :bidder_has_second_signer,
             :bid_before,
             :last_maximum_amount,
             :client_id,
             :total,
             :signers,
             :countries,
             :related_countries,
             :facebook_page,
             :twitter_username,
             :spotify_monthly_listeners,
             :spotify_total_plays,
             :soundcloud_profile,
             :nbs_artist_id,
             :nbs_artist_name,
             :youtube_channel,
             :youtube_video_views,
             :youtube_video_views_info,
             :show_private_info

  has_many :genres
  # has_many :countries
  has_many :auction_songs
  # has_many :auction_signers, key: :signers
  has_many :auction_videos, key: :videos
  # has_one :applicable_law_for_license

  def id
    object._id.to_s
  end

  def profile_owner
    object.profile.as_json(:only => [:name, :permalink])
  end

  def is_owner
    if scope
      scope.is_owner? object.profile.id
    else
      return false
    end
  end

  def is_admin
    if scope
      scope.is_admin?
    else
      return false
    end
  end

  def countries
    new_countries = []
    object.countries.each do |country|
      # I had to hardcode this part because the (Global Rights) text
      if country.id.to_s == '5420a193636573852c000000'
        country.name = "All Countries (Global Rights)"
      end
      new_countries << CountrySerializer.new(country, scope: @current_user)
    end
    return new_countries
  end

  # List of related countries if the user created a copy
  def related_countries
    object.related_countries
  end

  def signers
    if scope
      object.auction_signers
    else
      return []
    end
  end

  def reserve_on_competitive_term
    if scope
      if scope.is_admin? || scope.is_owner?(object.profile.id) || object.bidder_status(scope.default_profile) == 'accepted'
        object.reserve_on_competitive_term
      else
        if object.show_private_info
          object.reserve_on_competitive_term
        else
          return nil
        end
      end
    else
      if object.show_private_info
        object.reserve_on_competitive_term
      else
        return nil
      end
    end
  end

  def reserve_on_competitive_term_publishing
    if scope
      if scope.is_admin? || scope.is_owner?(object.profile.id) || object.bidder_status(scope.default_profile) == 'accepted'
        object.reserve_on_competitive_term_publishing
      else
        if object.show_private_info
          object.reserve_on_competitive_term_publishing
        else
          return nil
        end
      end
    else
      if object.show_private_info
        object.reserve_on_competitive_term_publishing
      else
        return nil
      end
    end
  end

  def required_advance_or_royalty_rate
    if scope
      if scope.is_admin? || scope.is_owner?(object.profile.id) || object.bidder_status(scope.default_profile) == 'accepted'
        object.required_advance_or_royalty_rate
      else
        if object.show_private_info
          object.required_advance_or_royalty_rate
        else
          return nil
        end
      end
    else
      if object.show_private_info
        object.required_advance_or_royalty_rate
      else
        return nil
      end
    end
  end

  def required_advance_or_royalty_rate_publishing
    if scope
      if scope.is_admin? || scope.is_owner?(object.profile.id) || object.bidder_status(scope.default_profile) == 'accepted'
        object.required_advance_or_royalty_rate_publishing
      else
        if object.show_private_info
          object.required_advance_or_royalty_rate_publishing
        else
          return nil
        end
      end
    else
      if object.show_private_info
        object.required_advance_or_royalty_rate_publishing
      else
        return nil
      end
    end
  end

  def recording_term
    if scope
      if scope.is_admin? || scope.is_owner?(object.profile.id) || object.bidder_status(scope.default_profile) == 'accepted'
        object.recording_term
      else
        if object.show_private_info
          object.recording_term
        else
          return nil
        end
      end
    else
      if object.show_private_info
        object.recording_term
      else
        return nil
      end
    end
  end

  # Fields related with signature
  def licensor_legal_name
    if scope && (scope.is_owner?(object.profile.id) || scope.is_admin?)
      object.licensor_legal_name
    end
  end

  def licensor_legal_address_line1
    if scope && (scope.is_owner?(object.profile.id) || scope.is_admin?)
      object.licensor_legal_address_line1
    end
  end

  def licensor_legal_address_line2
    if scope && (scope.is_owner?(object.profile.id) || scope.is_admin?)
      object.licensor_legal_address_line2
    end
  end

  def licensor_legal_address_country
    if scope && (scope.is_owner?(object.profile.id) || scope.is_admin?)
      object.licensor_legal_address_country
    end
  end

  def licensor_legal_address_country_name
    if not object.licensor_legal_address_country.nil?
      if scope && (scope.is_owner?(object.profile.id) || scope.is_admin?)
        Country.find(object.licensor_legal_address_country).name
      end
    end
  end

  def licensor_phone_number
      if scope && (scope.is_owner?(object.profile.id) || scope.is_admin?)
        object.licensor_phone_number
      end
  end

  def licensor_calling_code
    if scope && (scope.is_owner?(object.profile.id) || scope.is_admin?)
      object.licensor_calling_code
    end
  end

  def licensor_signer_name
    if scope && (scope.is_owner?(object.profile.id) || scope.is_admin?)
      object.licensor_signer_name
    end
  end

  def licensor_contact_email
    if scope && (scope.is_owner?(object.profile.id) || scope.is_admin?)
      object.licensor_contact_email
    end
  end

  def publisher_legal_name
    if scope && (scope.is_owner?(object.profile.id) || scope.is_admin?)
      object.publisher_legal_name
    end
  end

  def publisher_legal_address_line1
    if scope && (scope.is_owner?(object.profile.id) || scope.is_admin?)
      object.publisher_legal_address_line1
    end
  end

  def publisher_legal_address_line2
    if scope && (scope.is_owner?(object.profile.id) || scope.is_admin?)
      object.publisher_legal_address_line2
    end
  end

  def publisher_legal_address_country
    if scope && (scope.is_owner?(object.profile.id) || scope.is_admin?)
      object.publisher_legal_address_country
    end
  end

  def publisher_legal_address_country_name
    if not object.publisher_legal_address_country.nil?
      if scope && (scope.is_owner?(object.profile.id) || scope.is_admin?)
        Country.find(object.publisher_legal_address_country).name
      end
    end
  end

  def publisher_phone_number
    if scope && (scope.is_owner?(object.profile.id) || scope.is_admin?)
      object.publisher_phone_number
    end
  end

  def publisher_calling_code
    if scope && (scope.is_owner?(object.profile.id) || scope.is_admin?)
      object.publisher_calling_code
    end
  end

  def publisher_signer_name
    if scope && (scope.is_owner?(object.profile.id) || scope.is_admin?)
      object.publisher_signer_name
    end
  end

  def publisher_contact_email
    if scope && (scope.is_owner?(object.profile.id) || scope.is_admin?)
      object.publisher_contact_email
    end
  end

  def publisher_contact_email_confirmed
    if scope && (scope.is_owner?(object.profile.id) || scope.is_admin?)
      object.publisher_contact_email_confirmed
    end
  end

  def second_licensor_contact_email
    if scope
      scope.default_profile.second_licensor_contact_email rescue nil
    end
  end

  def second_licensor_contact_email_confirmed
    if scope
      scope.default_profile.second_licensor_contact_email_confirmed rescue false
    end
  end

  def bidder_has_second_signer
    if scope
      object.auction_bidders.find_by(profile: scope.default_profile).has_second_signer rescue false
    else
      return false
    end
  end

  def applicable_law_for_license
    object.country.id.to_s rescue nil
  end

  def applicable_law_for_license_name
    object.country.name rescue nil
  end

  def time_remaining
    (object.due_date - Time.now).to_i rescue nil
  end

  def due_date
    if not object.due_date.nil?
      return {date: object.due_date, stamp: (object.due_date.to_i * 1000)}
    else
      return {date: nil, stamp: nil}
    end
  end

  def recording_term
    if object.recording_term.nil?
      return "0"
    else
      return object.recording_term.to_s
    end
  end

  def publishing_term
    if object.publishing_term.nil?
      return "0"
    else
      return object.publishing_term.to_s
    end
  end

  def is_watching
    if scope
      scope.auction_watchlists.where(auction: object).exists?
    else
      return false
    end
  end

  def competitive_term_name
    object.competitive_term.gsub("_"," ").split(/(\W)/).map(&:capitalize).join rescue nil
  end

  def validations
    object.validations
  end

  def bidder_status
    if scope
      object.bidder_status(scope.default_profile)
    else
      return "guest"
    end
  end

  def auction_type_name
    object.auction_type_name
  end

  def status
    if object.due_date.nil?
      return object.auction_status
    else
      if Time.zone.now > object.due_date
        if object.auction_status[:code] == 1
          return object.auction_status
        elsif object.auction_status[:code] > 4
          return { name: "Ended", raw: "ended", code: 100 }
        end
      else
        return object.auction_status
      end
    end
  end

  def can_bid
    if scope
      bidder = object.auction_bidders.find_by(profile: scope.default_profile)
      if bidder.nil?
        return false
      else
        if bidder.status == 'accepted'
          return true
        end
      end
    else
      return false
    end
  end

  # Check if this user is a past bidder in this auction
  def bid_before
    if scope
      bidder = object.auction_bidders.find_by(profile: scope.default_profile)
      if bidder.nil?
        return false
      else
        if bidder.auction_bids.length > 0
          return true
        end
      end
    else
      return false
    end

  end

  def last_maximum_amount
    last_bidder_bid_amount = 0
    if scope
      bidder = object.auction_bidders.find_by(profile: scope.default_profile)
      last_bidder_bid_amount = object.auction_bids.where(:auction_bidder => @auction_bidder).last.maximum_amount rescue 0
    end
    return last_bidder_bid_amount
  end

  def client_id
    return ENV['HELLOSIGN_CLIENT_ID']
  end

  def notes
    if scope
      if scope.role == "admin"
        return object.notes
      else
        return ""
      end
    else
      return ""
    end
  end

  def bids
    # return []
    if object.auction_bids.count == 0
      increment_money = 0
      increment_percentage = 0
    else
      increment_money = 25
      increment_percentage = 1
    end

    if object.auction_bidders.pluck(:id).count > 0
      @auction_bids = object.auction_bids #AuctionBid.where(:auction_bidder_id.in => object.auction_bidders.pluck(:id))

      if @auction_bids.count > 0
        total = @auction_bids.count rescue 0
        # if object.auction_type == 'recording_publishing_rights'
        #   current = (@auction_bids.last.amount + @auction_bids.last.amount_publishing) rescue 0
        # else
        #   current = @auction_bids.last.amount rescue 0
        # end
        current = {recording: @auction_bids.last.amount, publishing: @auction_bids.last.amount_publishing} rescue {recording: 0, publishing: 0}
        @auction_bidder = AuctionBidder.find(@auction_bids.last.auction_bidder_id)
        profile = @auction_bidder.profile rescue nil
        if scope
          # Get the last bid of the current profile that is logged and viewing the auction
          @auction_bidder = object.auction_bidders.where(profile: scope.default_profile).last
          if @auction_bidder.nil?
            last_bid_profile = {recording: 0, publishing: 0}
          else
            @auction_bid = object.auction_bids.where(auction_bidder: @auction_bidder).last
            if @auction_bid.nil?
              last_bid_profile = {recording: 0, publishing: 0}
            else
              last_bid_profile = {recording: @auction_bid.amount, publishing: @auction_bid.amount_publishing}#@auction_bid.amount rescue 0
            end

          end
        else
          last_bid_profile = {recording: 0, publishing: 0}
        end
        # TODO: change for a Serializer
        # bidders = @auction_bids.as_json
        bidders = ActiveModel::ArraySerializer.new(@auction_bids, each_serializer: AuctionBidSerializer, scope: scope).serializable_array
      else
        total = 0
        current = {recording: 0, publishing: 0}
        profile = nil
        last_bid_profile = {recording: 0, publishing: 0}
        bidders = []
      end

      higher_profile_name = profile.name rescue nil
      higher_profile_permalink = profile.permalink rescue nil
      higher_profile_avatar = profile.default_avatar rescue nil

      return {current: current, total: total, last_bid_profile: last_bid_profile, higher: {name: higher_profile_name, permalink: higher_profile_permalink, avatar: {thumb: higher_profile_avatar}}, bidders: bidders, increment_money: increment_money, increment_percentage: increment_percentage}
    else
      # return []
      last_bid_profile = {recording: 0, publishing: 0}
      return {current: 0, total: 0, last_bid_profile: last_bid_profile, higher: {name: nil, permalink: nil, avatar: {thumb: ''}}, bidders: [], increment_money: increment_money, increment_percentage: increment_percentage}
    end

  end

  def bidders
    ActiveModel::ArraySerializer.new(object.auction_bidders.where(status: 'accepted'), each_serializer: AuctionBidderSerializer, scope: scope).serializable_array

  end

  def second_chance_sent
    object.auction_bidders.where(second_offer_sent: true).exists?
  end

  def reserve_was_met
    reserve_was_met = false
    object.auction_bids.each do |bid|
      if bid.amount > object.reserve_on_competitive_term
        reserve_was_met = true
      end
    end
    return reserve_was_met
  end

  def current_bidder_profile
    if scope
      scope.default_profile.as_json(only: [:licensor_legal_name,
                                           :licensor_legal_address_line1,
                                           :licensor_legal_address_line2,
                                           :licensor_legal_address_country,
                                           :licensor_signer_name,
                                           :licensor_contact_email,
                                           :licensor_phone_number,
                                           :licensor_calling_code,
                                           :licensor_phone_number_verified],
                                 methods: [:phone_number_complete])
    end
  end

  def total
    # TODO: move the rescue to the model
    sub_total = object.sub_total rescue 0
    tuneteams_commision = object.tuneteams_commision rescue 0

    fee = object.transaction_fee rescue 0
    return {sub_total: object.sub_total, fee: fee, tuneteams_commision: object.tuneteams_commision}

  end

end
