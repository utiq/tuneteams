class AuctionSignerSerializer < ActiveModel::Serializer
  attributes :name,
             :email,
             :email_confirmed

end
