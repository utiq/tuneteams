class AuctionSongSerializer < ActiveModel::Serializer
  attributes :id,
             :name,
             :writer,
             :publisher,
             :recording_rights,
             :publishing_rights,
             :url

  def id
    object._id.to_s
  end

  def url
    object.audio.url
  end

end
