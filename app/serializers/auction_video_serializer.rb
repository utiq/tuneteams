class AuctionVideoSerializer < ActiveModel::Serializer
  attributes :_id,
             :name,
             :duration,
             :position,
             :url,
             :youtube_id

end
