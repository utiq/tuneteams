class ClaimSerializer < ActiveModel::Serializer
  attributes :id, :email, :profile, :created_at

  def id
    object._id.to_s
  end

  def profile
    object.profile.as_json(:only => [:_id, :name, :avatar, :permalink], :include => :industry_role_profile)
  end
end
