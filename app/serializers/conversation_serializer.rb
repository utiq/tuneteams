class ConversationSerializer < ActiveModel::Serializer
  attributes :id, :subject, :sender, :message_id, :created_at, :body, :avatar

  def id
    object._id.to_s
  end

  def subject
    object.message.subject
  end

  def sender
    # User.find(object.sender_id).profiles.first.as_json(:only => [:_id, :name, :avatar], :include => :industry_role_profile)
    object.sender.as_json(:only => [:_id, :name], :include => :industry_role_profile)
  end

  def avatar
    Rails.logger.debug object.sender
    object.sender.default_avatar
  end

  # def last_message
  #   object.body
  # end

  def message_id
    object.message_id.to_s
  end

  # def created_at
  #   rand(6)
  # end
end
