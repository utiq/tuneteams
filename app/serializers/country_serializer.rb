class CountrySerializer < ActiveModel::Serializer
  attributes :id, :name, :calling_code

  def id
    object._id.to_s
  end
end
