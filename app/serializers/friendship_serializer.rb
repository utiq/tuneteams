class FriendshipSerializer < ActiveModel::Serializer
  attributes :id, :message, :status, :profile, :avatar, :created_at

  def id
    object._id.to_s
  end

  def message
    object.invitation_message
  end

  def profile
    object.owner.as_json(:only => [:_id, :name, :permalink], :include => :industry_role_profile)
  end

  def avatar
    object.owner.default_avatar
  end
end
