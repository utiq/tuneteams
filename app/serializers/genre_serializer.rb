class GenreSerializer < ActiveModel::Serializer
  attributes :id, :name, :name_length

  def id
    object._id.to_s
  end
end
# ActiveModel::Serializer.setup do |config|
#   config.namespace = true # this should be renamed too.
# end
