class IndustryRoleProfileSerializer < ActiveModel::Serializer
  attributes :id, :name, :industry_role, :industry_role_id
  # has_many :sections
  # has_many :profiles
  # has_many :industry_role
  def id
    object.id.to_s
  end

  def industry_role
    object.industry_role.as_json(:only => [:_id, :name])
  end

  def industry_role_id
    object.industry_role.id.to_s
  end
end
