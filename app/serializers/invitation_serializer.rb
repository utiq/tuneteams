class InvitationSerializer < ActiveModel::Serializer
  attributes :id, :email, :industry_role, :invitation_sent, :city, :country, :token, :category

  def id
    object._id.to_s
  end

  def industry_role
    object.industry_role_profile.name rescue nil
  end
end
