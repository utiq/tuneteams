class MessageSerializer < ActiveModel::Serializer
  attributes :id, :subject, :last_message
  has_many :conversations

  def id
    object._id.to_s
  end

  def last_message
    # object.conversations.last.body
    "pop"
  end
end
