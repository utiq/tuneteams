class ProfileIndexSerializer < ActiveModel::Serializer
  attributes :id,
             :name,
             :permalink,
             :country,
             :web_page,
             :industry_role_profile_id,
             :industry_role,
             :default_avatar,
             :default_cover,
             :is_default,
             :rating

  has_many :genres

  def id
    object._id.to_s
  end

  def industry_role_profile_id
    object.industry_role_profile_id.to_s
  end

  def industry_role
    IndustryRoleProfile.find(object.industry_role_profile_id).name
  end

  def country
    object.country.as_json(:only => [:_id, :name])
  end

  def avatar
    object.default_avatar
  end

end
