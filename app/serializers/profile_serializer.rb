class ProfileSerializer < ActiveModel::Serializer
  attributes :id,
             :name,
             :permalink,
             :country,
             :industry_role_profile_id,
             :avatar,
             :cover,
             :web_page,
             :is_owner,
             :is_executive,
             :frienship_status,
             :are_friends,
             :is_blocked,
             :has_owner,
             :genres_ids,
             :connections_count,
             :show_connections_list,
             :who_can_see_connection_list,
             :profile_working_at,
             :cover_type,
             :avatar_type,
             :tour_viewed
  attribute :sections, key: :sections
  has_many :genres
  # embeds_one :profile_working_at

  def sections
    @sections = Section.where(:industry_role_profile_id => object.industry_role_profile_id).order_by('position ASC').as_json(:include => :widgets)

    @sections.each do |section|
      section["widgets"].delete_if{ |widget| widget["profile_id"].to_s != object.id.to_s }

      if section["content_type"] == 'interest'
        # Get the countries name
        country_names = []
        countries = section["widgets"].first["widget_interest"]["countries"]
        if not countries.nil?
          countries.each do |country|
            country_names << Country.find(country).name
          end
        end

        # Get the interests name
        interest_names = []
        interests = section["widgets"].first["widget_interest"]["interests"]
        if not interests.nil?
          interests.each do |interest|
            interest_names << Section.find(section["id"]).industry_role_profile.industry_role_profile_interests.find(interest).alias
          end
        end

        section["widgets"].first["widget_interest"]["country_names"] = country_names
        section["widgets"].first["widget_interest"]["interest_names"] = interest_names
      end
    end
  end

  def industry_role_profile_id
    object.industry_role_profile_id.to_s
  end

  def is_executive
    object.industry_role_profile.is_executive
  end

  def country
    object.country.as_json(:only => [:_id, :name])
  end

  def is_owner
    if scope
      scope.profiles.where(id: object.id).exists?
    else
      return false
    end
  end

  def avatar
    if object.avatar_filename == nil
      @default_avatar_name = object.industry_role_profile.industry_role.default_avatar
      default_avatar = "{\"avatar\": {\"medium\": {\"url\": \"/assets/icons/#{@default_avatar_name}.png\"}}, \"is_default\": true}"
      return JSON.parse(default_avatar)
    else
      return object.as_json(:only => [:avatar])
    end
  end

  def frienship_status
    status = nil
    if scope
      @owner = scope.profiles.where(is_default: true).first
      if @owner
        if @owner.id != object.id
          # Rails.logger.debug Friendship.where(owner: @owner, friend: object).inspect
          friendship = Friendship.where(owner: @owner, friend: object).first
          if not friendship.nil?
            status = friendship.status
          end
        end
      end
    end
    return status
  end

  def are_friends
    # false
    if scope
      @default_visitor_profile = scope.profiles.where(is_default: true).first
      if @default_visitor_profile
        return @default_visitor_profile.are_friends?(object.id.to_s)
      else
        return false
      end
    else
      return false
    end
  end

  def is_blocked
    if scope
      @default_visitor_profile = scope.profiles.where(is_default: true).first
      if @default_visitor_profile
        connection = object.connections.find_by(profile_id: @default_visitor_profile.id)
        if connection.nil?
          return false
        else
          if connection.status == 'active'
            return false
          else
            return true
          end
        end
      else
        return false
      end
    else
      return false
    end
  end

  def genres_ids
    genre_ids = Array.new
    object.genres.each do |genre|
      genre_ids << genre.id.to_s
    end
    return genre_ids
  end

  def connections_count
    object.connections.count
  end

  def show_connections_list
    if scope
      if scope.profiles.where(id: object.id).exists?
        return object.show_connections_list
      else
        if object.show_connections_list
          if object.who_can_see_connection_list == 'all'
            return object.show_connections_list
          elsif object.who_can_see_connection_list == 'friends'
            if scope
              @default_visitor_profile = scope.profiles.where(is_default: true).first
              if @default_visitor_profile.nil?
                  return false
              else
                return object.are_friends?(@default_visitor_profile.id.to_s)
              end
            end
          end
        else
          return object.show_connections_list
        end
      end
    else
      return false
    end
  end
end
