class SectionSerializer < ActiveModel::Serializer
  attributes :_id, :title
  has_many :widgets
end