class UserSerializer < ActiveModel::Serializer
  attributes :email,
             :role,
             :messages_count,
             :connections_count,
             :bidders_count,
             :profiles,
             :auctions,
             :current_profile,
             :avatar,
             :access_token,
             :email_confirmed,
             :notification_mailing,
             :notification_messages_or_invitations,
             :notification_surveys,
             :notification_auction_approval,
             :notification_auction_restart

  def id
    object._id.to_s
  end

  def role
    # TODO: Improve this
    if object.role == "user"
      JSON.parse("{\"title\": \"#{object.role}\", \"bitMask\": 2}")
    elsif object.role == "admin"
      JSON.parse("{\"title\": \"#{object.role}\", \"bitMask\": 4}")
    end
  end

  def profiles
    object.profiles.order_by("created_at ASC").as_json(:only => [:_id, :name, :permalink, :is_default])
  end

  def auctions
    @auctions = Auction.where(:profile_id.in => object.profiles.distinct(:id), status: 'approved').order_by("created_at DESC").as_json(:only => [:_id, :name, :permalink])
  end

  def current_profile
    default_profile = object.profiles.find_by(is_default: true).as_json(:only => [:name, :permalink])
  end

  def messages_count
    count = 0
    default_profile = object.profiles.find_by(is_default: true)
    if not default_profile.nil?
      count = default_profile.received_messages.where(is_read: false).count
    end
    return count
  end

  def connections_count
    count = 0
    default_profile = object.profiles.find_by(is_default: true)
    if not default_profile.nil?
      count = Friendship.where(friend: default_profile, status: 'pending').count
    end
    return count
  end

  def bidders_count
    count = 0
    default_profile = object.profiles.find_by(is_default: true)
    if not default_profile.nil?
      # TODO: Change this for a map
      default_profile.auctions.each do |auction|
        if auction.status == 'approved'
          count += auction.auction_bidders.where(status: 'pending').count
        end
      end
    end
    return count
  end

  def avatar
    default_avatar = "{\"small\": \"/assets/default-avatar.jpg\"}"
    @default_profile = object.profiles.where(is_default: true).first
    if @default_profile.nil?
      return JSON.parse(default_avatar)
    else
      @default_profile.default_avatar
    end
  end
end
