class WidgetSerializer < ActiveModel::Serializer
  attributes :id

  def id
    object._id.to_s
  end
end