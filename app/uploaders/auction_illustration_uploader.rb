# encoding: utf-8

class AuctionIllustrationUploader < CarrierWave::Uploader::Base
  include CarrierWave::MiniMagick

  # TODO: Implement direct uploading to S3
  storage :fog

  # Sets mimetype in case it is incorrect
  include CarrierWave::MimeTypes
  process :set_content_type

  # def store_dir
  #   "uploads/profiles_images/avatars/#{model.id}"
  # end

  def store_dir
    "uploads/#{model.class.to_s.underscore}/#{mounted_as}/#{model.id}"
  end

  def filename
    "#{secure_token}.jpg" if original_filename.present?
  end

  version :thumb, :if => :image? do
    process :efficient_conversion => [75, 75]
  end

  version :small, :if => :image? do
    process :efficient_conversion => [250, 250]
  end

  version :medium, :if => :image? do
    process :efficient_conversion => [500, 500]
  end

  version :large, :if => :image? do
    process :efficient_conversion => [1000, 1000]
  end

  def efficient_conversion(width, height)
    # Resize to Fill manipulation
    manipulate! do |img|
      cols, rows = img[:dimensions]
      img.combine_options do |cmd|
        if width != cols || height != rows
          scale = [width/cols.to_f, height/rows.to_f].max
          cols = (scale * (cols + 0.5)).round
          rows = (scale * (rows + 0.5)).round
          cmd.resize "#{cols}x#{rows}"
        end
        cmd.gravity 'Center'
        cmd.background "rgba(255,255,255,0.0)"
        cmd.extent "#{width}x#{height}" if cols != width || rows != height
      end
      img = yield(img) if block_given?
      img.flatten
      img.quality('80')
      img.format('jpg')
      img
    end
  end

  def extension_white_list
    %w(jpg jpeg gif png bmp)
  end

  protected

  def image?(new_file)
    new_file.content_type.include? 'image'
  end

  def secure_token
    var = :"@#{mounted_as}_secure_token"
    model.instance_variable_get(var) or model.instance_variable_set(var, SecureRandom.uuid)
  end
end
