# encoding: utf-8

class AuctionSongUploader < CarrierWave::Uploader::Base
  include CarrierWave::MiniMagick

  # TODO: Implement direct uploading to S3
  storage :fog

  # Sets mimetype in case it is incorrect
  include CarrierWave::MimeTypes
  process :set_content_type

  # def store_dir
  #   "uploads/profiles_images/avatars/#{model.id}"
  # end

  def store_dir
    "uploads/#{model.class.to_s.underscore}/#{mounted_as}/#{model.id}"
  end

  def filename
    "#{secure_token}.mp3" if original_filename.present?
  end

  def extension_white_list
    %w(mp3)
  end

  protected

  def image?(new_file)
    new_file.content_type.include? 'image'
  end

  def secure_token
    var = :"@#{mounted_as}_secure_token"
    model.instance_variable_get(var) or model.instance_variable_set(var, SecureRandom.uuid)
  end
end
