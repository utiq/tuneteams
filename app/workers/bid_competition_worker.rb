class BidCompetitionWorker
  include Sidekiq::Worker
  sidekiq_options retry: false, queue: "often"


  def perform(permalink = nil)
    begin
      sleep 5
      if permalink.nil?
        auctions = Auction.where(:due_date.gte => Time.zone.now, status: 'approved')
      else
        auctions = Auction.where(:due_date.gte => Time.zone.now, permalink: permalink)
      end

      auctions.each do |auction|
        if auction.auction_bids.count > 0
          # max_bid = auction.auction_bids.desc(:maximum_amount).limit(1).first
          # puts "Max bid: #{max_bid.inspect}"
          last_bid = auction.auction_bids.desc(:created_at).limit(1).first
          check_auctions(auction, last_bid)
        end
      end

      auctions.each do |auction|
        if auction.auction_bids.count > 0
          # max_bid = auction.auction_bids.desc(:maximum_amount).limit(1).first
          # puts "Max bid: #{max_bid.inspect}"
          last_bid = auction.auction_bids.desc(:created_at).limit(1).first
          puts "Last bid: #{last_bid.inspect}"
          check_auctions_same_maximum(auction, last_bid)
        end
      end
    rescue StandardError => e
      ExceptionNotifier.notify_exception(e)
      Rails.logger.debug { e }
    end
  end

  # Chech if there is somebody else willing to outbid the current bid. And it's not the current bidder
  def check_auctions(auction, last_bid)

    comparative_increment = 1
    if auction.competitive_term == 'advance'
      comparative_increment = 25
    end

    auction.auction_bids.where(:auction_bidder.ne => last_bid.auction_bidder, :maximum_amount.gte => last_bid.maximum_amount + comparative_increment).order_by('automated_bids_count ASC').each do |bid|

      puts "There is an auction bidder"
      puts bid.auction_bidder.profile.name
      puts bid.automated_bids_count
      puts bid.inspect

      # # Comparitive variables
      times_automated_bid = auction.auction_bids.where(:auction_bidder => bid.auction_bidder, auction: auction).count

      @auction_bid = AuctionBid.create(auction: auction,
                                      auction_bidder: bid.auction_bidder,
                                      amount: last_bid.maximum_amount + comparative_increment,
                                      maximum_amount: bid.maximum_amount,
                                      amount_publishing: bid.amount_publishing,
                                      maximum_amount_publishing: bid.maximum_amount_publishing,
                                      automated_bids_count: times_automated_bid + 1)

      OutbidWorker.perform_async(@auction_bid.id.to_s)
      break

    end
  end

  # Chech if there is somebody else willing to outbid the current bid. And it's not the current bidder
  def check_auctions_same_maximum(auction, last_bid)
    puts "Enter check_auctions_same_maximum"
    auction.auction_bids.where(:auction_bidder.ne => last_bid.auction_bidder, :maximum_amount => last_bid.maximum_amount).order_by('automated_bids_count ASC').each do |bid|

      puts "There is an auction bidder"
      puts bid.auction_bidder.profile.name
      puts bid.automated_bids_count
      puts bid.inspect

      # # Comparitive variables
      times_automated_bid = auction.auction_bids.where(:auction_bidder => bid.auction_bidder, auction: auction).count

      @auction_bid = AuctionBid.create(auction: auction,
                                      auction_bidder: bid.auction_bidder,
                                      amount: last_bid.maximum_amount,
                                      maximum_amount: bid.maximum_amount,
                                      amount_publishing: bid.amount_publishing,
                                      maximum_amount_publishing: bid.maximum_amount_publishing,
                                      automated_bids_count: times_automated_bid + 1)

      OutbidWorker.perform_async(@auction_bid.id.to_s)
      break

    end
  end
end
