class CancelAuctionWorker
  include Sidekiq::Worker
  sidekiq_options retry: false

  def perform(auction_id)
    @auction = Auction.find(auction_id)
    @auction.auction_bidders.each do |bidder|
      user = bidder.profile.user
      AuctionsMailer.cancel(@auction.name, user.email).deliver
    end
  end
end
