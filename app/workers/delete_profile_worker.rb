class DeleteProfileWorker
  include Sidekiq::Worker
  sidekiq_options retry: false

  def perform(profile_id)
    #TODO: update the elastic search index
    profile = Profile.find(profile_id)

    # Delete the profile if is a connection in another profile
    profile_connections = Profile.where("connections.profile_id" => profile_id)
    if not profile_connections.nil?
      profile_connections.each do |profile_connection|
        connection = profile_connection.connections.find_by("profile_id" => profile_id)
        connection.destroy
      end
    end

    # Delete the profile avatar in member people widget
    profile_widget_peoples = Widget.where("widget_peoples.profile_id" => profile_id)
    if not profile_widget_peoples.nil?
      profile_widget_peoples.each do |profile_widget_people|
        profile_widget_people = profile_widget_people.widget_peoples.find_by("profile_id" => profile_id)
        profile_widget_people.destroy
      end
    end

    # Delete messages
    if not profile.sent_messages.first.nil?
      # One more comment
      if not profile.sent_messages.first.message.nil?
        profile.sent_messages.first.message.destroy
      end
    end

    if not profile.received_messages.first.nil?
      if not profile.received_messages.first.message.nil?
        profile.received_messages.first.message.destroy
      end
    end

    profile.sent_messages.delete_all
    profile.received_messages.delete_all

    # Delete the profile
    profile.widgets.delete_all
    profile.destroy

  end
end
