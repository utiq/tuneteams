class ExportEmailsWorker
  include Sidekiq::Worker
  sidekiq_options retry: false

  def perform()
    begin
      file_bin = "Name, Email, Country, Industry Role, Countries interest, Deals interest"
      path = ''
      profiles = Profile.all#.limit(10)
      profiles.no_timeout.each do |profile|

        country_names = []
        interest_names = []

        @sections = Section.where(:industry_role_profile_id => profile.industry_role_profile_id).order_by('position ASC').as_json(:include => :widgets)

        @sections.each do |section|
          section["widgets"].delete_if{ |widget| widget["profile_id"].to_s != profile.id.to_s }

          if section["content_type"] == 'interest'
            countries = section["widgets"].first["widget_interest"]["countries"]
            if not countries.nil?
              countries.each do |country|
                country_names << Country.find(country).name
              end
            end

            interests = section["widgets"].first["widget_interest"]["interests"]
            if not interests.nil?
              interests.each do |interest|
                interest_names << Section.find(section["id"]).industry_role_profile.industry_role_profile_interests.find(interest).alias
              end
            end

            # section["widgets"].first["widget_interest"]["country_names"] = country_names
            # section["widgets"].first["widget_interest"]["interest_names"] = interest_names
          end
        end

        file_bin += "\"#{profile.name.gsub('"', '\"').force_encoding('UTF-8')}\", #{profile.user.email}, #{profile.country.name}, #{profile.industry_role_profile.name}, #{country_names.join(';')}, #{interest_names.join(';')}\n"

        path = "uploads/tmp/profile_export.csv"

        open("public/#{path}", "wb") do |file|
          file.write(file_bin)
        end
      end

      if ENV['CURRENT_ENV'] == 'development' || ENV['CURRENT_ENV'] == 'staging'
        email = "cesguty@gmail.com"
      else
        email = "julia@tuneteams.com"
      end

      ProfilesMailer.emails_exported(email, path).deliver
    rescue StandardError => e
      ExceptionNotifier.notify_exception(e)
    end
  end
end
