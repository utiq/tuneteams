class FriendAcceptWorker
  include Sidekiq::Worker
  sidekiq_options retry: false

  # TODO: kill mongoid processes https://github.com/mongoid/kiqstand

  def perform(owner_id, friend_id)
    @owner = Profile.find(owner_id)
    @friend = Profile.find(friend_id)

    if not @friend.connections.where(profile_id: @owner.id).exists?
      genres = []
      @owner.genres.all.each do |genre|
        genres << genre.name
      end
      owner_connection = Connection.new(profile_id: @owner.id,
                                   name: @owner.name,
                                   default_avatar: @owner.default_avatar,
                                   permalink: @owner.permalink,
                                   industry_role: @owner.industry_role_profile.name,
                                   country: @owner.country.name,
                                   genres: genres,
                                   status: 'active')
      @friend.connections.push(owner_connection)
    end

    if not @owner.connections.where(profile_id: @friend.id).exists?
      genres = []
      @friend.genres.all.each do |genre|
        genres << genre.name
      end
      friend_connection = Connection.new(profile_id: @friend.id,
                                   name: @friend.name,
                                   default_avatar: @friend.default_avatar,
                                   permalink: @friend.permalink,
                                   industry_role: @friend.industry_role_profile.name,
                                   country: @friend.country.name,
                                   genres: genres,
                                   status: 'active')
      @owner.connections.push(friend_connection)
    end

    FriendshipsMailer.accept_invitation(@owner.name, @owner.user.email, @friend.user.email, @friend.name, @friend.permalink).deliver

  end
end
