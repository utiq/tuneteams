class GenerateLicenseWorker
  include Sidekiq::Worker
  sidekiq_options retry: false

  def perform(auction_id)
    @auction = Auction.find(auction_id)
    begin
      @auction.generate_license(@auction.id)
    rescue StandardError => e
      ExceptionNotifier.notify_exception(e)
    end
  end
end
