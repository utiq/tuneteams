class KissmetricsWorker
  include Sidekiq::Worker
  sidekiq_options retry: false, queue: :often

  def perform(identifier, event, properties)
    KMTS.record(identifier, event, properties)
  end
end
