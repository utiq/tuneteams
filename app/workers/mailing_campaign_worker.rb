class MailingCampaignWorker
  include Sidekiq::Worker
  sidekiq_options retry: false

  def perform(mailing_campaign_id)
    @mailing_campaign = MailingCampaign.find(mailing_campaign_id)
    @users = User.all
    @users.each do |user|
      if user.notification_mailing
        MailingCampaignsMailer.send_mailing(@mailing_campaign.subject, @mailing_campaign.body, user.email).deliver
      end
    end

  end
end
