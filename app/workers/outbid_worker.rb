class OutbidWorker
  include Sidekiq::Worker
  sidekiq_options retry: false, queue: :often

  # Sends an email to all the bidders that are lower than the new bid

  def perform(auction_bid_id)
    @auction_bid = AuctionBid.find(auction_bid_id)
    the_amount = @auction_bid.amount
    new_amount = @auction_bid.maximum_amount
    new_amount_publishing = @auction_bid.amount_publishing
    lower_bidders = AuctionBidder.where(:id.in => AuctionBid.where(:maximum_amount.lt => new_amount, :auction => @auction_bid.auction).distinct(:auction_bidder_id))
    lower_bidders.each do |lower_bidder|
      if lower_bidder.profile.id != @auction_bid.auction_bidder.profile.id
        BidsMailer.outbid(lower_bidder.auction.name,
                          lower_bidder.auction.permalink,
                          @auction_bid.auction_bidder.profile.name,
                          @auction_bid.auction_bidder.profile.permalink,
                          the_amount,
                          new_amount_publishing,
                          lower_bidder.profile.user.email,
                          lower_bidder.auction.due_date).deliver
      end
    end
    # TODO: check the case when is a recording and publishing rights
  end
end
