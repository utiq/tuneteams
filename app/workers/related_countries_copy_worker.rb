class RelatedCountriesCopyWorker
  include Sidekiq::Worker
  sidekiq_options retry: false, queue: :often

  def perform(auction_id = nil)
    begin
      status = ['published', 'approved']

      if auction_id == nil
        auctions = Auction.where(:status.in => status)
      else
        auctions = Auction.where(id: auction_id)
      end

      auctions.each do |auction|
        array_parent = []
        auction.update_attributes(related_countries: [])
        auction_countries = auction.countries

        # Get parent
        if auction.copy_from
          related_parent(auction, array_parent)

          # Get parent children
          parent = Auction.find(auction.copy_from)
          if not parent.nil?
            parent_sons = Auction.where(copy_from: parent.id, :status.in => status)
            related_countries(auction, parent_sons, array_parent, status)
          end
        end

        # Get brothers
        auction_brothers = Auction.where(copy_from: auction.id, :status.in => status)
        related_countries(auction, auction_brothers, array_parent, status)

        # puts array_parent
        auction.update_attributes(related_countries: array_parent)
      end
    rescue StandardError => e
      ExceptionNotifier.notify_exception(e)
    end

  end

  def related_countries(auction, auctions, array_parent, status)

    auctions.each do |parent_son2|
      # Add the countries related
      parent_son2.countries.each do |country2|
        if country2.id != '5420a193636573852c000000' # All countries
          if parent_son2.permalink != auction.permalink
            if !auction.countries.where(id: country2.id).exists?
              if !array_parent.select{ |x| x['country_name'] == country2.name }.any?
                array_parent << {'country_name' => country2.name, 'permalink' => parent_son2.permalink}
              end
            end
          end
        end
      end
      new_auctions = Auction.where(copy_from: parent_son2.id, :status.in => status)
      related_countries(auction, new_auctions, array_parent, status)
    end
  end

  def related_parent(auction, array_parent)
    parent = Auction.find(auction.copy_from)
    # Add the countries related with the parent
    if not parent.nil?
      parent.countries.each do |country|
        if country.id != '5420a193636573852c000000' # All countries
          if parent.permalink != auction.permalink
            if !auction.countries.where(id: country.id).exists?
              if !array_parent.select{ |x| x['country_name'] == country.name }.any?
                array_parent << {'country_name' => country.name, 'permalink' => parent.permalink}
              end
            end
          end
        end
      end

      if parent.copy_from
        related_parent(parent, array_parent)
      end
    end
  end
end
