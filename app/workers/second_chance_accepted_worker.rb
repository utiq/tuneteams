class SecondChanceAcceptedWorker
  include Sidekiq::Worker
  sidekiq_options retry: false

  def perform(auction_id, auction_bidder_id)
    auction = Auction.find(auction_id)
    auction_bidder = AuctionBidder.find(auction_bidder_id)

    max_amount = auction.auction_bids.where(auction_bidder: auction_bidder).order_by('amount DESC').limit(1).first

    auction.buid_license_contract(auction, max_amount, auction_bidder)
    auction.update_attributes(due_date: Time.zone.now)
  end
end
