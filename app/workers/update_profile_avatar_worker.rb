class UpdateProfileAvatarWorker
  include Sidekiq::Worker
  sidekiq_options retry: false

  def perform(profile_id)
    profile = Profile.find(profile_id)

    # Update the profile avatar in each connection
    profile_connections = Profile.where("connections.profile_id" => profile_id)
    if not profile_connections.nil?
      profile_connections.each do |profile_connection|
        connection = profile_connection.connections.find_by("profile_id" => profile_id)
        connection.update_attributes(default_avatar: profile.default_avatar)
      end
    end

    # Update the profile avatar in member people widget
    profile_widget_peoples = Widget.where("widget_peoples.profile_id" => profile_id)
    if not profile_widget_peoples.nil?
      profile_widget_peoples.each do |profile_widget_people|
        profile_widget_people = profile_widget_people.widget_peoples.find_by("profile_id" => profile_id)
        profile_widget_people.update_attributes(avatar_url_copy: profile.default_avatar["small"])
      end
    end

  end

end
