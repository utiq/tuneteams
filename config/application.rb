require File.expand_path('../boot', __FILE__)

# Pick the frameworks you want:
# require "active_record/railtie"
require "action_controller/railtie"
require "action_mailer/railtie"
require "sprockets/railtie"
require "rails/test_unit/railtie"

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(:default, Rails.env)

module Tuneteams
  class Application < Rails::Application
    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.

    # Set Time.zone default to the specified zone and make Active Record auto-convert to this zone.
    # Run "rake -D time" for a list of tasks for finding time zone names. Default is UTC.
    # config.time_zone = 'Central Time (US & Canada)'

    # The default locale is :en and all translations from config/locales/*.rb,yml are auto loaded.
    # config.i18n.load_path += Dir[Rails.root.join('my', 'locales', '*.{rb,yml}').to_s]
    # config.i18n.default_locale = :de

    config.autoload_paths += Dir["#{config.root}/lib/**/"]
    config.action_controller.allow_forgery_protection = false

    # Rails environment variables
    config.before_configuration do
      env_file = File.join(Rails.root, 'config', 'local_env.yml')
      YAML.load(File.open(env_file)).each do |key, value|
        ENV[key.to_s] = value
      end if File.exists?(env_file)
    end

    # Angular configuration

    # include Bower components in compiled assets
    config.assets.paths << Rails.root.join('vendor', 'assets', 'components', 'javascripts')

    # via https://gist.github.com/afeld/5704079

    # We don't want the default of everything that isn't js or css, because it pulls too many things in
    config.assets.precompile.shift

    # Explicitly register the extensions we are interested in compiling
    config.assets.precompile.push(Proc.new do |path|
      File.extname(path).in? [
       '.html', '.erb', '.haml',
       '.png',  '.gif', '.jpg', '.jpeg', '.svg',
       '.eot',  '.otf', '.svc', '.woff', '.ttf',
      ]
    end)

    # Serializer gem without root
    ActiveModel::Serializer.root = false
    ActiveModel::ArraySerializer.root = false

    config.middleware.use Rack::Prerender, prerender_token: 'C1LgiEZVyU1R3Bh39tAl'

    # config.action_dispatch.default_headers = {
    #   'X-Frame-Options' => 'ALLOWALL'
    # }

  end
end

# module PatchedAwesomePrint
#   require 'awesome_print'
#   ::Moped::BSON = ::BSON
# end
