default['app']               = 'tuneteams'

default['nodejs']['dir']     = '/usr/local'
default['nodejs']['version'] = '0.12.5'

default['ruby']['version']   = '2.1.7'
default['redis']['version']  = '3.0.2'

default['mongodb']['package_version'] = '2.4.2'
# default['elasticsearch']['version'] = '1.7.2'
