monitrc "monit_nginx" do
  template_cookbook "tuneteams"
  template_source "monit/nginx.conf.erb"
end

monitrc "redis" do
  template_cookbook "tuneteams"
  template_source "monit/redis.conf.erb"
end

monitrc "sidekiq" do
  template_cookbook "tuneteams"
  template_source "monit/sidekiq.conf.erb"
end
