# require "bundler/capistrano"

# config valid only for Capistrano 3.1
lock '3.4.0'

set :application, 'tuneteams'
set :repo_url, 'git@bitbucket.org:utiq/tuneteams.git'

set :linked_files, %w{config/local_env.yml config/mongoid.yml config/private_pub.yml config/sidekiq.yml public/blog/wp-config.php}
set :linked_dirs, %w{log tmp/pids public/blog/wp-content/uploads config/private_pub public/pdfs/licenses public/pdfs/contracts public/uploads/tmp }

# # Default branch is :master
# # ask :branch, proc { `git rev-parse --abbrev-ref HEAD`.chomp }.call

# # Default deploy_to directory is /var/www/my_app
set :deploy_to, '/var/www/tuneteams'

# Default value for :pty is false
set :pty, true

# Sidekiq configuration
set :sidekiq_default_hooks, true
set :sidekiq_config, -> {"#{shared_path}/config/sidekiq.yml"}
set :sidekiq_pid, -> {"#{shared_path}/tmp/pids/sidekiq.pid"}
# set :sidekiq_monit_templates_path, 'lib/capistrano/tasks/templates/monit'
set :sidekiq_monit_use_sudo, true
set :private_pub_pid, -> { "#{release_path}/tmp/pids/private_pub.pid" }
set :whenever_identifier, -> { "#{fetch(:application)}_#{fetch(:stage)}" }

SSHKit.config.command_map[:rake] = "bundle exec rake"

# set :tmp_dir, "#{fetch(:home)}/tmp"
set :tmp_dir, "/home/deployer/tmp"

set :ssh_options, {
  forward_agent: true,
  auth_methods: %w(publickey),
  keys: "/Users/cesar/Development/Freelance/Tuneteams/tuneteams.pem",
  user: 'deployer'
}

set :rbenv_type, :user # or :system, depends on your rbenv setup
set :rbenv_ruby, '2.1.7'

# in case you want to set ruby version from the file:
# set :rbenv_ruby, File.read('.ruby-version').strip

set :rbenv_prefix, "RBENV_ROOT=#{fetch(:rbenv_path)} RBENV_VERSION=#{fetch(:rbenv_ruby)} #{fetch(:rbenv_path)}/bin/rbenv exec"
set :rbenv_map_bins, %w{rake gem bundle ruby rails}
set :rbenv_roles, :all # default value

namespace :deploy do

  # before :deploy, "deploy:check_revision"
  after :deploy, "deploy:restart"

  desc 'Precompile assets'
  task :precompile do
    on roles(:app), in: :sequence, wait: 5 do
      # Your restart mechanism here, for example:
      execute :rake, 'assets:precompile'
    end
  end

  after :publishing, :restart
  # after :precompile, :restart

  desc 'Restart application'
  task :restart do
    on roles(:app), in: :sequence, wait: 5 do
      execute :mkdir, '-p', "#{ release_path }/tmp"
      execute :touch, release_path.join('tmp/restart.txt')
    end
  end

  desc "Makes sure local git is in sync with remote."
  task :check_revision do
    unless `git rev-parse HEAD` == `git rev-parse origin/master`
      puts "WARNING: HEAD is not the same as origin/master"
      puts "Run `git push` to sync changes."
      exit
    end
  end

  %w[start stop restart].each do |command|
    desc "#{command} Unicorn server."
    task command do
      on roles(:app) do
        execute "/etc/init.d/unicorn_#{fetch(:application)} #{command}"
      end
    end
  end

  # #TODO: It's not working, I did it manual in the server
  # desc 'Blog permissions'
  # task :blog_permissions do
  #   on roles(:app), in: :sequence, wait: 5 do
  #     execute :sudo ,:chmod, "-R 777 #{shared_path}/public/blog/wp-content"
  #   end
  # end
  #
  desc 'Restart Sidekiq'
  task :sidekiq do
    on roles(:app), in: :sequence, wait: 5 do
      # Your restart mechanism here, for example:
      execute 'bundle exec sidekiq -d -e production -C config/sidekiq.yml'
    end
  end

  after :restart, :clear_cache do
    on roles(:web), in: :groups, limit: 3, wait: 10 do
      # Here we can do anything such as:
      # within release_path do
      #   execute :rake, 'cache:clear'
      # end
    end
  end

  desc "Update crontab with whenever"
  task :update_cron do
    on roles(:app) do
      within current_path do
        execute :bundle, :exec, "whenever --update-crontab #{fetch(:whenever_identifier)}"
      end
    end
  end

  after :finishing, 'deploy:update_cron'

end

namespace :private_pub do
  desc "Start private_pub server"
  task :start do
    on roles(:app) do
      within release_path do
        with rails_env: fetch(:stage) do
          execute :bundle, "exec thin -C config/private_pub/thin_#{fetch(:stage)}.yml -d -P #{fetch(:private_pub_pid)} start"
        end
      end
    end
  end

  desc "Stop private_pub server"
  task :stop do
    on roles(:app) do
      within release_path do
        execute "if [ -f #{fetch(:private_pub_pid)} ] && [ -e /proc/$(cat #{fetch(:private_pub_pid)}) ]; then kill -9 `cat #{fetch(:private_pub_pid)}`; fi"
      end
    end
  end

  desc "Restart private_pub server"
  task :restart do
    on roles(:app) do
      invoke 'private_pub:stop'
      invoke 'private_pub:start'
    end
  end


end


after 'deploy:restart', 'private_pub:restart'
