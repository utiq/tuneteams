Tuneteams::Application.configure do
  # Settings specified here will take precedence over those in config/application.rb.

  # In the development environment your application's code is reloaded on
  # every request. This slows down response time but is perfect for development
  # since you don't have to restart the web server when you make code changes.
  config.cache_classes = false

  # Do not eager load code on boot.
  config.eager_load = false

  # Show full error reports and disable caching.
  config.consider_all_requests_local       = true
  config.action_controller.perform_caching = false

  # Don't care if the mailer can't send.
  config.action_mailer.raise_delivery_errors = false

  # Print deprecation notices to the Rails logger.
  config.active_support.deprecation = :log

  # Debug mode disables concatenation and preprocessing of assets.
  # This option may cause significant delays in view rendering with a large
  # number of complex assets.
  config.assets.debug = true

  # Amazon SES configuration
  config.action_mailer.default_url_options = { :host => 'localhost:3020' }
  config.action_mailer.delivery_method = :ses
  config.action_mailer.perform_deliveries = true
  config.action_mailer.raise_delivery_errors = true

  ::TWILIO = Twilio::REST::Client.new ENV['TWILIO_ACCOUNT_SID'], ENV['TWILIO_AUTH_TOKEN']

  Rails.application.config.assets.precompile += [
    "teaspoon.css",
    "teaspoon-mocha.js",
    "mocha/1.17.1.js"
  ]
end
