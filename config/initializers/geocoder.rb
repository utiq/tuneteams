Geocoder.configure(
  # geocoding service (see below for supported options):
  #:lookup => :geocoder_ca
  #:lookup => :bing
  :lookup => :google,

  # to use an API key:
  #:api_key = ENV["BING_MAPS_KEY"]
  #:api_key => "...",

  # geocoding service request timeout, in seconds (default 3):
  :timeout => 15,

  # set default units to kilometers:
  :units => :miles,

  # caching (see below for details):
  # :cache => Redis.connect(:url => ENV["OPENREDIS_URL"])
)

#test changes