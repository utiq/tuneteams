require 'sidekiq/web'

Tuneteams::Application.routes.draw do
  # root to: 'pages#home'
  match "/licenses/:id" => "licenses#show", via: :get
  match "/embed/auctions/:permalink" => "embeds#auctions", via: :get
  match "/embed/test" => "embeds#test", via: :get


  namespace :api, defaults: {format: 'json'} do
    namespace :v1 do
      match "/admin/reports/dashboard" => "admin_reports#dashboard", via: :get
      resources :industry_roles
      match "/industry_role_profiles/:id/interests" => "industry_role_profiles#interests", via: :get
      match "/auctions/:id/pay" => "auctions#pay", via: :post
      match "/auctions/:id/agreement/:signer_id" => "auction_agreements#show", via: :get
      resources :auctions do
        collection do
          get 'search'
          get 'promoted'
          get 'related'
          post 'set_illustration'
          post ':id/copy' => 'auctions#copy'
          post 'publish' => 'auctions#publish'
          post 'restart'
          get ':id/cancel_restart/:token' => 'auctions#cancel_restart'
          get 'recommended'
          get ':permalink/facebook_feed' => 'auctions#facebook_feed'
          get ':permalink/twitter_timeline' => 'auctions#twitter_timeline'
          get ':permalink/soundcloud_info' => 'auctions#soundcloud_info'
          get ':permalink/next_big_sound_metrics' => 'auctions#next_big_sound_metrics'
          get 'next_big_sound_search_artist'
        end
      end
      resources :auction_songs do
        collection do
          post 'set_audio'
        end
      end
      resources :auction_signers do
        collection do
          post 'send_validation_email'
          post 'validate_second_signer'
        end
      end
      resources :second_chances do
        collection do
          post 'accept/:auction_id/:auction_bidder_id' => 'second_chances#accept'
          post 'decline/:auction_id/:auction_bidder_id' => 'second_chances#decline'
        end
      end
      resources :auction_videos
      resources :auction_watchlists
      resources :auction_bidders
      resources :auction_bids
      match "/auctions/hellosign_callback" => "auction_agreements#hellosign_callback", via: :post
      resources :auction_agreements do
        collection do
          get 'download'
          post 'hellosign_callback'
        end
      end
      resources :auction_management do
        collection do
          post 'cancel'
          post 'materials_received'
        end
      end
      resources :payoneer_settings do
        collection do
          get 'payoneer_listener'
        end
      end
      resources :admin_auctions do
        collection do
          post 'approve'
          post 'reject'
          post 'disapprove'
          post 'payout'
          post 'return_to_draft'
        end
      end

      resources :admin_profiles do
        collection do
          get 'download_profiles'
        end
      end

      resources :auction_verifications do
        collection do
          post 'verify'
        end
      end
      resources :auction_positions
      resources :profile_positions
      resources :industry_role_profiles
      resources :sessions
      resources :user_password_resets
      resources :notifications
      match "/claims/assign/:id" => "claims#assign", via: :post
      match "/claims/reject/:id" => "claims#reject", via: :post
      resources :claims
      resources :messages
      match "/messages/reply/:id" => "messages#reply", via: :post
      match "/messages/archive/:id" => "messages#archive", via: :post
      resources :friendships
      match "/profiles/:permalink/switch" => "profiles#switch", via: :put
      match "/profiles/:permalink/connections" => "profiles#get_connections", via: :get
      match "/profiles/:permalink/connections/:id/status/:status" => "connections#update_status", via: :post
      resources :profiles do
        collection do
          get 'search'
          post 'set_profile_picture'
          post 'set_profile_cover'
          get 'promoted'
          get 'recommended'
        end
      end
      resources :profile_working_ats
      resources :invitations
      match "/invitations/invite/:id" => "invitations#invite", via: :post
      match "/invitations/personal" => "invitations#personal", via: :post
      match "/invitations/personal" => "invitations#personal", via: :post
      resources :countries
      resources :genres
      resources :widgets_content
      match "/payments/bluesnap_response" => "payments#bluesnap_response", via: :post
      resources :payments
      match "/users/recover_password", via: :post
      match "/users/confirm_email", via: :post
      # match "/admin/mailing/campaigns" => "mailing_campaigns#index", via: :get
      # match "/admin/mailing_list/sync" => "mailing_list#sync", via: :get
      resources :mailing_campaigns
      resources :users
      get 'login', to: 'sessions#create', as: :login
    end
  end

  mount Sidekiq::Web, at: "/admin/sidekiq"

  # To generate the PDF license contract
  resources :licenses
  # match "/auctions/license" => "auctions#license", via: :get

  # Only to test emails format
  match "/mailers/invitation" => "mailers#invitation", via: :get
  match "/mailers/connect" => "mailers#connect", via: :get

  # Angular configuration
  get '/' => "pages#home"
  get '*path' => "pages#home"
end
