# Use this file to easily define all of your cron jobs.
#
# It's helpful, but not entirely necessary to understand cron before proceeding.
# http://en.wikipedia.org/wiki/Cron

# Example:
#
# set :output, "/path/to/my/cron_log.log"
#
# every 2.hours do
#   command "/usr/bin/some_great_command"
#   runner "MyModel.some_method"
#   rake "some:great:rake:task"
# end
#
# every 4.days do
#   runner "AnotherModel.prune_old_records"
# end

# Learn more: http://github.com/javan/whenever

# set :environment, ENV['CURRENT_ENV']
set :output, {:error => 'log/cron_error.log', :standard => 'log/cron.log'}
# env :PATH, if @environment == 'production' || @environment == 'staging' then ENV['PATH'] else "/Users/cesar/.rvm/gems/ruby-2.1.0/bin:/Users/cesar/.rvm/gems/ruby-2.1.0@global/bin:/Users/cesar/.rvm/rubies/ruby-2.1.0/bin:/Users/cesar/.rvm/bin:/opt/local/bin:/opt/local/sbin:/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin:/opt/X11/bin:/Applications/MAMP/Library/bin:/usr/local/lib/node_modules:/usr/local/lib/coffee:/applications/development/android-sdk/platform-tools:/applications/development/android-sdk/tools:/usr/share/ant/bin" end
# env :PATH, ENV['PATH']
env 'PATH', ENV['PATH']
# job_type :runner_with_rvm, 'source /etc/profile.d/rvm.sh; cd :path;rvm 2.0@gemset do bundle exec script/rails runner -e :environment ':task' :output'
# job_type :runner, "cd #{path} && RAILS_ENV=development /Users/cesar/.rvm/gems/ruby-2.1.0@global/bundle exec rails runner ':task' :output"
job_type :runner_with_rvm, "cd #{path};rvm use ruby-2.1.0@rails403 && bin/rails runner -e :environment ':task' :output"

case @environment
when 'production', 'staging'
  every 1.minute do
    runner "Auction.check_winner"
    runner "Auction.check_signatures"
    runner "Auction.check_restart"
    runner "Auction.check_materials_received"
    runner "AutomatedBids.automated_bid"
  end
  every :day, :at => '12:05am' do
    runner "RelatedCountriesCopyWorker.perform_async"
    runner "RecommendedIndex.recommended_profiles"
    runner "RecommendedIndex.recommended_auctions"
  end
  every 0.1.minutes do

  end

when 'development'
  every 1.minute do
    runner_with_rvm "Auction.check_winner"
    runner_with_rvm "Auction.check_signatures"
    runner_with_rvm "Auction.check_restart"
    runner_with_rvm "Auction.check_materials_received"
    runner_with_rvm "AutomatedBids.automated_bid"
  end
end



# every 1.minute do
#   command "whoami"
# end

# every 1.minute do
#   command "which ruby"
# end
