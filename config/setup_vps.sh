#!/bin/sh

# check for correct number of arguments
if [ $# -ne 3 ]; then
  echo "Usage: $0 <user> <ip> <port>"
  exit 1
fi

# set variables
USER=$1
IP=$2
#IP=ec2-52.20.107.153.compute-1.amazonaws.com
PORT=$3

# install chef
cd chef && knife solo prepare ubuntu@$IP

# execute the run list
knife solo cook ubuntu@$IP

# upload key for user
ssh-copy-id -i ~/.ssh/id_rsa.pub -p $PORT $USER@$IP

# upload app
# cd ../.. && cap production setup:all

# restart nginx
ssh -p $PORT -t $USER@$IP 'sudo service nginx restart'
