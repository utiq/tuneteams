module AutomatedBids
  extend self

  def automated_bid(permalink = nil)
    # auction = Auction.find("563772a6636573b9e9060000")
    begin
      # if permalink.nil?
      #   auctions = Auction.where(:due_date.gte => Time.zone.now, status: 'approved')
      # else
      #   auctions = Auction.where(:due_date.gte => Time.zone.now, permalink: permalink)
      # end
      #
      # auctions.each do |auction|
      #   if auction.auction_bids.count > 0
      #     max_bid = auction.auction_bids.desc(:amount).limit(1).first
      #     puts "Max bid: #{max_bid.amount}"
      #     check_auctions(auction, max_bid)
      #   end
      # end
    rescue StandardError => e
      ExceptionNotifier.notify_exception(e)
      Rails.logger.debug { e }
    end
  end

  # Chech if there is somebody else willing to outbid the current bid. And it's not the current bidder
  def check_auctions(auction, max_bid)
    comparative_increment = 1
    if auction.competitive_term == 'advance'
      comparative_increment = 25
    end

    auction.auction_bids.where(:auction_bidder.ne => max_bid.auction_bidder, :maximum_amount.gte => max_bid.amount + comparative_increment).order_by('automated_bids_count ASC').each do |bid|

      puts "There is an auction bidder"
      puts bid.auction_bidder.profile.name
      puts bid.automated_bids_count
      puts bid.inspect

      # # Comparitive variables
      times_automated_bid = auction.auction_bids.where(:auction_bidder => bid.auction_bidder, auction: auction).count
      # last_bid_time_difference = ((Time.zone.now - max_bid.created_at) / 60).to_i
      # create_new_bid = false
      #
      # # puts ((auction.due_date - Time.zone.now.to_datetime) * 60).to_f
      #
      # # Increase value of the bids according time
      # if ((auction.due_date.to_time - Time.zone.now) / 60 ) <= 10 && last_bid_time_difference >= 1
      #   # if the difference between due_date and current date is less than 10 minutes, an increment of the bid will be perform every 1 minute
      #   create_new_bid = true
      # elsif ((auction.due_date.to_time - Time.zone.now) / 60 / 60) <= 1 && last_bid_time_difference >= 2
      #   # if the difference between due_date and current date is less than 1 hour, an increment of the bid will be perform every 2 minutes
      #   create_new_bid = true
      # elsif ((auction.due_date.to_time - Time.zone.now) / 60 / 60) <= 2 && last_bid_time_difference >= 10
      #   # if the difference between due_date and current date is less than 2 hours, an increment of the bid will be perform every 10 minutes
      #   create_new_bid = true
      # elsif ((auction.due_date.to_time - Time.zone.now) / 60 / 60) <= 6 && last_bid_time_difference >= 30
      #   # if the difference between due_date and current date is less than 6 hours, an increment of the bid will be perform every 30 minutes
      #   create_new_bid = true
      # elsif ((auction.due_date.to_time - Time.zone.now) / 60 / 60 / 60) <= 1 && last_bid_time_difference >= 60
      #   # if the difference between due_date and current date is less than 1 day, a increment of the bid will be perform every 1 hour = 60 minutes
      #   create_new_bid = true
      # elsif ((auction.due_date.to_time - Time.zone.now) / 60 / 60 / 60) <= 3 && last_bid_time_difference >= 180
      #   # if the difference between due_date and current date is less than 3 days, a increment of the bid will be perform every 3 hours = 180 minutes
      #   create_new_bid = true
      # elsif ((auction.due_date.to_time - Time.zone.now) / 60 / 60 / 60) <= 7 && last_bid_time_difference >= 360
      #   # if the difference between due_date and current date is less than 7 days, a increment of the bid will be perform every 6 hours = 360 minutes
      #   create_new_bid = true
      # elsif ((auction.due_date.to_time - Time.zone.now) / 60 / 60 / 60) <= 14 && last_bid_time_difference >= 720
      #   # if the difference between due_date and current date is less than 14 days, a increment of the bid will be perform every 12 hours = 720 minutes
      #   create_new_bid = true
      # end
      #
      #

      # if create_new_bid || instant
      @auction_bid = AuctionBid.create(auction: auction,
                                      auction_bidder: bid.auction_bidder,
                                      amount: max_bid.amount + comparative_increment,
                                      maximum_amount: bid.maximum_amount,
                                      amount_publishing: bid.amount_publishing,
                                      maximum_amount_publishing: bid.maximum_amount_publishing,
                                      automated_bids_count: times_automated_bid + 1)
      # end

      break

    end
  end

end
