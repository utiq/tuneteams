def template(from, to)
  erb = File.read(File.expand_path("../templates/#{from}", __FILE__))
  # put ERB.new(erb).result(binding), to
  upload! ERB.new(erb).result(binding), to
  # on roles(:all) do
  #   upload! erb, to
  # end
end
