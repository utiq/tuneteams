namespace :monit do
  %w(start stop restart).each do |task_name|
    desc "#{task_name} Monit"
    task task_name do
      on roles(:app), in: :sequence, wait: 5 do
        sudo "service monit #{task_name}"
      end
    end
  end

  desc "Setup all Monit configuration"
  task :setup do
    monit_config "monitrc", "/usr/local/etc/monitrc"
    invoke "monit:nginx"
    invoke "monit:private_pub"
    # postgresql
    # unicorn
    # syntax
    # reload
  end

  desc "Reload Monit"
  task 'reload' do
    on roles(:app), in: :sequence, wait: 5 do
      sudo "monit reload"
    end
  end

  after 'setup', 'reload'

  desc "Copy nginx"
  task :nginx do
    on roles(:web) do
      monit_config "nginx"
    end
  end

  desc "Copy Private_pub"
  task :private_pub do
    on roles(:app) do
      monit_config "private_pub"
    end
  end

  desc "Copy Sidekiq"
  task :sidekiq do
    on roles(:app) do
      monit_config "sidekiq"
    end
  end
  # task(:nginx, roles: :web) { monit_config "nginx" }
end


def monit_config(name, destination = nil)
  # destination ||= "/etc/monit/conf.d/#{name}.conf"
  destination ||= "/usr/lib/monit-5.14/conf/#{name}.conf"
  on roles(:app) do
    erb = File.read(File.expand_path("../templates/monit/#{name}.erb", __FILE__))
    lol = ERB.new(erb).result(binding)
    upload! StringIO.new(lol), "#{current_path}/tmp/monit_#{name}"
    # # template "monit/#{name}.erb", "/home/deployer/tmp/monit_#{name}"
    execute :sudo, :mv, "#{current_path}/tmp/monit_#{name} #{destination}"
    execute :sudo, :chown, "root #{destination}"
    execute :sudo, :chmod, "600 #{destination}"
    # run "#{sudo} mv #{current_path}/tmp/monit_#{name} #{destination}"
    # run "#{sudo} chown root #{destination}"
    # run "#{sudo} chmod 600 #{destination}"
  end

end
