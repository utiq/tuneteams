# encoding: utf-8

module ConsoleResults
  extend self

  def list_profile_interests
    IndustryRoleProfile.all.each do |industry_role_profile|
      puts "- " + industry_role_profile.name
      industry_role_profile.industry_role_profile_interests.each do |irpi|
        puts "  |_" + irpi.alias
        irpi.related_roles.each do |related_role|
          puts "    |_" + IndustryRoleProfile.find(related_role).name
        end
      end
    end
  end

  def list_sections
    IndustryRoleProfile.all.each do |industry_role_profile|
      puts "- " + industry_role_profile.name
      industry_role_profile.sections.order_by("position asc").each do |section|
        puts "  |_ #{section.position} #{section.title}"
      end
    end
  end

  # Generate the widget "interests" to all the profiles previously created
  def generate_interests_widget
    Profile.all.each do |profile|
      # puts profile.industry_role_profile_id
      section = Section.where(:industry_role_profile_id => profile.industry_role_profile_id, content_type: "interest").first
      widget = Widget.new(profile_id: profile.id, section_id: section.id)
      widget.widget_interest = WidgetInterest.new()
      puts widget.save
    end
  end

  def show_profiles_connections
    # Profile.where("connections.name" => "Red Hot Chili Peppers")
    #   puts.profile.name
    # end
  end

  def update_countries_position
    Country.all.order_by("name ASC").each_with_index do |country, index|
      country.update_attributes(position: index + 1)
    end
  end

  def update_profile_cover_default
    Profile.all.each do |profile|
      if profile.cover_filename.nil?
        profile.update_attributes(cover_type: "default")
      else
        profile.update_attributes(cover_type: "customized")
      end
    end
  end

  def migrate_invitations
    Invitation.create(email: 'ihenderson@gmail.com', current_ip: '37.186.201.178', city:'', country:'IT', industry_role_profile_id:'537262126365732c40010000', token: '95e40c', invitation_sent: false, invitation_signed_up:false)
    Invitation.create(email: 'ben.mcewen@prsformusic.com', current_ip: '95.144.105.181', city:'Shipston On Stour', country:'GB', industry_role_profile_id:'537262126365732c40010000', token: '06f8df', invitation_sent: false, invitation_signed_up:false)
    Invitation.create(email: 'cgannettrun@gmail.com', current_ip: '8.12.99.4', city:'New York', country:'US', industry_role_profile_id:'537262126365732c40010000', token: '227c49', invitation_sent: false, invitation_signed_up:false)
    Invitation.create(email: 'eduard.razum@gmail.com', current_ip: '84.58.251.186', city:'Wetzlar', country:'DE', industry_role_profile_id:'537262126365732c40010000', token: 'b6de05', invitation_sent: false, invitation_signed_up:false)
    Invitation.create(email: 'sharique.husain@googlemail.com', current_ip: '95.91.232.102', city:'Dachau', country:'DE', industry_role_profile_id:'537262126365732c40010000', token: 'ba9156', invitation_sent: false, invitation_signed_up:false)
    Invitation.create(email: 'lasse.westi@sonymusic.com', current_ip: '89.150.153.219', city:'RA dovre', country:'DK', industry_role_profile_id:'537262126365732c40010000', token: '2da368', invitation_sent: false, invitation_signed_up:false)
    Invitation.create(email: 'gruzdev@hotmail.com', current_ip: '5.228.17.26', city:'', country:'RU', industry_role_profile_id:'537262126365732c40010000', token: '7ff82f', invitation_sent: false, invitation_signed_up:false)
    Invitation.create(email: 'alasdair.spurr@gmail.com', current_ip: '82.11.82.121', city:'London', country:'GB', industry_role_profile_id:'537262126365732c40010000', token: '4e8852', invitation_sent: false, invitation_signed_up:false)
    Invitation.create(email: 'ulrich.jaerkel@btinternet.com', current_ip: '109.84.0.189', city:'Bad Soden', country:'DE', industry_role_profile_id:'537262126365732c40010000', token: '243f44', invitation_sent: false, invitation_signed_up:false)
    Invitation.create(email: 'kevin@openaura.com', current_ip: '76.103.8.18', city:'San Francisco', country:'US', industry_role_profile_id:'537262126365732c40010000', token: 'c9d29c', invitation_sent: false, invitation_signed_up:false)
    Invitation.create(email: 'vc3krh@gmail.com', current_ip: '99.67.236.85', city:'Austin', country:'US', industry_role_profile_id:'537262126365732c40010000', token: '1b55ad', invitation_sent: false, invitation_signed_up:false)
    Invitation.create(email: 'lisa.ohlin@gmail.com', current_ip: '151.231.155.180', city:'', country:'GB', industry_role_profile_id:'537262126365732c40010000', token: '4e0fbc', invitation_sent: false, invitation_signed_up:false)
    Invitation.create(email: 'goran.andersson@me.com', current_ip: '201.75.175.172', city:'São José dos Campos', country:'BR', industry_role_profile_id:'537262126365732c40010000', token: '5ee83e', invitation_sent: false, invitation_signed_up:false)
    Invitation.create(email: 'tom@resplendentmanagement.com', current_ip: '2.101.147.135', city:'', country:'GB', industry_role_profile_id:'537262126365732c40010000', token: '4fc952', invitation_sent: false, invitation_signed_up:false)
    Invitation.create(email: 'g@tinkertailor.com', current_ip: '108.12.140.253', city:'Elmhurst', country:'US', industry_role_profile_id:'537262126365732c40010000', token: 'af5a54', invitation_sent: false, invitation_signed_up:false)
    Invitation.create(email: 'duncan.collins@ministryofsound.com.au', current_ip: '220.233.233.18', city:'Sydney', country:'AU', industry_role_profile_id:'537262126365732c40010000', token: 'cf9082', invitation_sent: false, invitation_signed_up:false)
    Invitation.create(email: 'rob.marshall@sonymusic.com', current_ip: '86.16.71.224', city:'Fareham', country:'GB', industry_role_profile_id:'537262126365732c40010000', token: 'f7380d', invitation_sent: false, invitation_signed_up:false)
    Invitation.create(email: 'matthias.lumm@web.de', current_ip: '213.161.89.30', city:'', country:'GB', industry_role_profile_id:'537262126365732c40010000', token: '1d698d', invitation_sent: false, invitation_signed_up:false)
    Invitation.create(email: 'nina.bell@ymail.com', current_ip: '149.254.49.241', city:'Beckenham', country:'GB', industry_role_profile_id:'537262126365732c40010000', token: '3409c0', invitation_sent: false, invitation_signed_up:false)
    Invitation.create(email: 'stuart.levene@sonymusic.com', current_ip: '83.244.137.162', city:'London', country:'GB', industry_role_profile_id:'537262126365732c40010000', token: '0f63da', invitation_sent: false, invitation_signed_up:false)
    Invitation.create(email: 'nadir.contractor@umusic.com', current_ip: '77.103.132.39', city:'London', country:'GB', industry_role_profile_id:'537262126365732c40010000', token: '4c1308', invitation_sent: false, invitation_signed_up:false)
    Invitation.create(email: 'ozgeergen@gmail.com', current_ip: '68.203.3.223', city:'Cedar Park', country:'US', industry_role_profile_id:'537262126365732c40010000', token: 'd5be52', invitation_sent: false, invitation_signed_up:false)
    Invitation.create(email: 'carolh@telemark.com', current_ip: '67.189.83.254', city:'Vancouver', country:'US', industry_role_profile_id:'537262126365732c40010000', token: '347527', invitation_sent: false, invitation_signed_up:false)
    Invitation.create(email: 'cesguty@gmail.com', current_ip: '174.44.215.227', city:'Brooklyn', country:'US', industry_role_profile_id:'5372621a6365732c40020000', token: '339f6a', invitation_sent: false, invitation_signed_up:false)
    Invitation.create(email: 'dpereira@mit.edu', current_ip: '50.138.182.32', city:'Somerville', country:'US', industry_role_profile_id:'537262bf6365732c400b0000', token: '980238', invitation_sent: false, invitation_signed_up:false)
    Invitation.create(email: 'miguel.birra@sonymusic.com', current_ip: '62.48.218.222', city:'', country:'PT', industry_role_profile_id:'537262886365732c40070000', token: '05be84', invitation_sent: false, invitation_signed_up:false)
    Invitation.create(email: 'sovietrecordings@mail.ru', current_ip: '80.84.122.211', city:'Moscow', country:'RU', industry_role_profile_id:'537262886365732c40070000', token: 'f2515f', invitation_sent: false, invitation_signed_up:false)
    Invitation.create(email: 'helga-21@yandex.com', current_ip: '217.19.208.105', city:'Bendery', country:'MD', industry_role_profile_id:'537262126365732c40010000', token: 'db9847', invitation_sent: false, invitation_signed_up:false)
    Invitation.create(email: 'jeanette@inkandpixeldesign.com', current_ip: '74.88.82.34', city:'Brooklyn', country:'US', industry_role_profile_id:'537262bf6365732c400b0000', token: '7ae731', invitation_sent: false, invitation_signed_up:false)
    Invitation.create(email: 'fomenkoo@gmail.com', current_ip: '87.113.110.38', city:'', country:'GB', industry_role_profile_id:'5372621a6365732c40020000', token: '8f17b6', invitation_sent: false, invitation_signed_up:false)
    Invitation.create(email: 'free.bird.in.a.free.cage@gmail.com', current_ip: '109.185.136.229', city:'Chisinau', country:'MD', industry_role_profile_id:'537262126365732c40010000', token: '4d6dde', invitation_sent: false, invitation_signed_up:false)
    Invitation.create(email: 'cbennett@spotify.com', current_ip: '80.239.169.203', city:'Stockholm', country:'SE', industry_role_profile_id:'537262eb6365732c40100000', token: '6f0fbc', invitation_sent: false, invitation_signed_up:false)
    Invitation.create(email: 'g@cozmopolitanmedia.com', current_ip: '86.147.119.215', city:'London', country:'GB', industry_role_profile_id:'537262126365732c40010000', token: 'fb744f', invitation_sent: false, invitation_signed_up:false)
    Invitation.create(email: 'jedsell@gmail.com', current_ip: '72.54.21.194', city:'Fort Worth', country:'US', industry_role_profile_id:'537262bf6365732c400b0000', token: '22ad98', invitation_sent: false, invitation_signed_up:false)
    Invitation.create(email: 'greenlanternmusic@hotmail.com', current_ip: '74.178.15.67', city:'Mount Pleasant', country:'US', industry_role_profile_id:'5372621a6365732c40020000', token: '5c72dc', invitation_sent: false, invitation_signed_up:false)
    Invitation.create(email: 'yulia.p.henderson@gmail.com', current_ip: '188.242.249.152', city:'Saint Petersburg', country:'RU', industry_role_profile_id:'537262bf6365732c400b0000', token: 'cfe9c0', invitation_sent: false, invitation_signed_up:false)
    Invitation.create(email: 'teamvegaspromotions@gmail.com', current_ip: '68.224.90.130', city:'', country:'RD', industry_role_profile_id:'537262516365732c40050000', token: '431758', invitation_sent: false, invitation_signed_up:false)
    Invitation.create(email: 'bruno.crolot@reedmidem.com', current_ip: '62.23.102.40', city:'', country:'GB', industry_role_profile_id:'537262bf6365732c400b0000', token: '7e676a', invitation_sent: false, invitation_signed_up:false)
    Invitation.create(email: 'bonniewalker692@gmail.com', current_ip: '67.189.83.254', city:'Vancouver', country:'US', industry_role_profile_id:'537262bf6365732c400b0000', token: 'b5bb92', invitation_sent: false, invitation_signed_up:false)
    Invitation.create(email: 'jstegfellner@gmail.com', current_ip: '85.199.49.213', city:'Wals', country:'AT', industry_role_profile_id:'537262bf6365732c400b0000', token: 'aa110e', invitation_sent: false, invitation_signed_up:false)
    Invitation.create(email: 'badpandarecords@gmail.com', current_ip: '79.53.16.81', city:'Pomezia', country:'IT', industry_role_profile_id:'537262886365732c40070000', token: '0fdbbc', invitation_sent: false, invitation_signed_up:false)
    Invitation.create(email: 'lgs7@yandex.ru', current_ip: '176.193.5.161', city:'', country:'RU', industry_role_profile_id:'537262886365732c40070000', token: '4a2a85', invitation_sent: false, invitation_signed_up:false)
    Invitation.create(email: 'support@standingoproject.com', current_ip: '198.72.157.127', city:'West Hollywood', country:'US', industry_role_profile_id:'537263086365732c40110000', token: '98e54a', invitation_sent: false, invitation_signed_up:false)
    Invitation.create(email: 'gianmaria.girardi@gmail.com', current_ip: '151.95.199.112', city:'Silea', country:'IT', industry_role_profile_id:'537262bf6365732c400b0000', token: '997d95', invitation_sent: false, invitation_signed_up:false)
    Invitation.create(email: 'sunjiking@yahoo.com.au', current_ip: '89.168.217.148', city:'London', country:'GB', industry_role_profile_id:'537262bf6365732c400b0000', token: '339ed8', invitation_sent: false, invitation_signed_up:false)
    Invitation.create(email: 'djgbrown@gmail.com', current_ip: '71.125.218.109', city:'Brooklyn', country:'US', industry_role_profile_id:'537261e36365732c40000000', token: '6efacb', invitation_sent: false, invitation_signed_up:false)
    Invitation.create(email: 'pianomusic@albertoferro.com', current_ip: '79.41.101.138', city:'Brescia', country:'IT', industry_role_profile_id:'537262126365732c40010000', token: 'e475af', invitation_sent: false, invitation_signed_up:false)
    Invitation.create(email: 'remco.mercey@gmail.com', current_ip: '83.80.217.216', city:'Capelle Aan Den Ijssel', country:'NL', industry_role_profile_id:'537262bf6365732c400b0000', token: '3712d8', invitation_sent: false, invitation_signed_up:false)
    Invitation.create(email: 'victoria.dowling@intentmedia.co.uk', current_ip: '87.83.31.242', city:'London', country:'GB', industry_role_profile_id:'5372632b6365732c40130000', token: 'f71097', invitation_sent: false, invitation_signed_up:false)
    Invitation.create(email: 'andrea.rosi@sonymusic.com', current_ip: '88.60.42.83', city:'Collegno', country:'IT', industry_role_profile_id:'5372628f6365732c40080000', token: 'e13d45', invitation_sent: false, invitation_signed_up:false)
    Invitation.create(email: 'walkejas@mail.gvsu.edu', current_ip: '66.87.115.193', city:'', country:'US', industry_role_profile_id:'537262c86365732c400c0000', token: '2a0d6f', invitation_sent: false, invitation_signed_up:false)
    Invitation.create(email: 'ulrich.jaerkel@gimo-management.com', current_ip: '109.84.3.30', city:'Frankfurt Am Main', country:'DE', industry_role_profile_id:'537262e26365732c400f0000', token: '1cc691', invitation_sent: false, invitation_signed_up:false)
    Invitation.create(email: 'Denisfm@list.ru', current_ip: '212.33.7.41', city:'Elektrostal', country:'RU', industry_role_profile_id:'5372632b6365732c40130000', token: 'c23cfb', invitation_sent: false, invitation_signed_up:false)
    Invitation.create(email: 'montique.willis@gmail.com', current_ip: '50.194.186.125', city:'League City', country:'US', industry_role_profile_id:'537261e36365732c40000000', token: '05f968', invitation_sent: false, invitation_signed_up:false)
    Invitation.create(email: 'gurpcitydigital@gmail.com', current_ip: '208.76.28.230', city:'Berkeley', country:'US', industry_role_profile_id:'537262886365732c40070000', token: '29bee4', invitation_sent: false, invitation_signed_up:false)
    Invitation.create(email: 'thefounder@detroitrails.co', current_ip: '199.16.190.194', city:'Farmington', country:'US', industry_role_profile_id:'537262126365732c40010000', token: '96d150', invitation_sent: false, invitation_signed_up:false)
    Invitation.create(email: 'araceliyegros@gmail', current_ip: '190.104.162.48', city:'Asunción', country:'PY', industry_role_profile_id:'5372623e6365732c40030000', token: 'e31a25', invitation_sent: false, invitation_signed_up:false)
    Invitation.create(email: 'gpryor@reedsmith.com', current_ip: '149.255.41.5', city:'London', country:'GB', industry_role_profile_id:'537262eb6365732c40100000', token: 'da4826', invitation_sent: false, invitation_signed_up:false)
    Invitation.create(email: 'antony.d@mac.com', current_ip: '216.194.27.154', city:'New York', country:'US', industry_role_profile_id:'537263336365732c40140000', token: '3b8dad', invitation_sent: false, invitation_signed_up:false)
    Invitation.create(email: 'thimios@wantedpixel.com', current_ip: '92.195.43.128', city:'', country:'DE', industry_role_profile_id:'537262126365732c40010000', token: '876b49', invitation_sent: false, invitation_signed_up:false)
    
  end

end