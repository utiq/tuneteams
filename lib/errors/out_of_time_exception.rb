class OutOfTimeException < StandardError
  def initialize(data)
    @data = data
  end
end