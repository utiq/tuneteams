require "net/http"
require "uri"
# encoding: utf-8

module RecommendedIndex
  extend self

  def recommended_profiles(permalink = nil)
    if permalink.nil?
      profiles = Profile.all
    else
      profiles = Profile.where(permalink: permalink)
    end

    begin

      profiles.no_timeout.each do |profile|
        suggested_profiles = []
        profile.update_attributes(related_profiles: suggested_profiles)
        if true#profile.related_profiles.count == 0
          # same_gender_profiles = Profile.any_in(:genre_ids => profile.genres.map { |x| x.id.to_s }).order_by('rating DESC')
          # name,
          same_gender_profiles = Profile.where(:genre_ids.in => profile.genres.map { |x| x.id.to_s }, :rating.gte => 5)

          same_gender_profiles.no_timeout.shuffle.each do |same_gender_profile|
            # same_gender_profile.genres.each do |gender_profile|
            #   if gender_profile.id
            # end
            #Top Match:	Deal Type/Profile Type AND Country AND Genre
            related_profile_widget_interest = same_gender_profile.widgets.where(:widget_interest.ne => nil).first.widget_interest

            if (!related_profile_widget_interest.interests.nil? && related_profile_widget_interest.interests.include?(profile.industry_role_profile.id.to_s)) && (!related_profile_widget_interest.countries.nil? && related_profile_widget_interest.countries.include?(profile.country.id.to_s))
              if not profile.related_profiles.include? same_gender_profile.id.to_s
                puts same_gender_profile.name
                if not profile.connections.where(profile_id: same_gender_profile.id.to_s).exists?
                  if not profile.user.profiles.where(id: same_gender_profile.id.to_s).exists?
                    if profile.id.to_s != same_gender_profile.id.to_s
                      suggested_profiles.push(same_gender_profile.id.to_s)
                    end
                  end
                end
              end
            end

            if (!related_profile_widget_interest.interests.nil? && related_profile_widget_interest.interests.include?(profile.industry_role_profile.id.to_s))
              if not profile.related_profiles.include? same_gender_profile.id.to_s
                puts same_gender_profile.name
                if not profile.connections.where(profile_id: same_gender_profile.id.to_s).exists?
                  if not profile.user.profiles.where(id: same_gender_profile.id.to_s).exists?
                    if profile.id.to_s != same_gender_profile.id.to_s
                      suggested_profiles.push(same_gender_profile.id.to_s)
                    end
                  end
                end
              end
            end

            if (!related_profile_widget_interest.countries.nil? && related_profile_widget_interest.countries.include?(profile.country.id.to_s))
              puts same_gender_profile.id.to_s
              if not profile.related_profiles.include? same_gender_profile.id.to_s
                puts same_gender_profile.name
                if not profile.connections.where(profile_id: same_gender_profile.id.to_s).exists?
                  if not profile.user.profiles.where(id: same_gender_profile.id.to_s).exists?
                    if profile.id.to_s != same_gender_profile.id.to_s
                      suggested_profiles.push(same_gender_profile.id.to_s)
                    end
                  end
                end
              end
            end
          end
          profile.update_attributes(related_profiles: suggested_profiles)
        end
      end

    rescue StandardError => e
      ExceptionNotifier.notify_exception(e)
    end
  end

  def recommended_auctions(profile_permalink = nil)
    # Match order:
    # 1. Country and Genre
    # 2. Country
    # 3. Genre

    if profile_permalink.nil?
      profiles = Profile.all
    else
      profiles = Profile.where(permalink: profile_permalink)
    end

    begin

      profiles.no_timeout.each do |profile|
        suggested_auctions = []
        profile.update_attributes(related_auctions: suggested_auctions)

        same_country_and_gender_auctions = Auction.any_in(:genre_ids => profile.genres.map { |x| x.id.to_s }, :country_ids => profile.country.id.to_s, status: 'approved').order_by('position ASC')
        same_country_and_gender_auctions.each do |same_country_and_gender_auction|
          if not profile.related_auctions.include? same_country_and_gender_auction.id.to_s
            suggested_auctions.push(same_country_and_gender_auction.id.to_s)
          end
        end

        same_country_auctions = Auction.any_in(:country_ids => profile.country.id.to_s, status: 'approved').order_by('position ASC')
        same_country_auctions.each do |same_country_auction|
          if not profile.related_auctions.include? same_country_auction.id.to_s
            suggested_auctions.push(same_country_auction.id.to_s)
          end
        end

        same_gender_auctions = Auction.any_in(:genre_ids => profile.genres.map { |x| x.id.to_s }, status: 'approved').order_by('position ASC')
        same_gender_auctions.each do |same_gender_auction|
          if not profile.related_auctions.include? same_gender_auction.id.to_s
            suggested_auctions.push(same_gender_auction.id.to_s)
          end
        end

        profile.update_attributes(related_auctions: suggested_auctions)
      end
    rescue StandardError => e
      ExceptionNotifier.notify_exception(e)
    end
  end

end
