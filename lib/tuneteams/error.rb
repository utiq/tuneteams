module Tuneteams
  module Error
    # Custom error class for rescuing from all Tuneteams errors.
    class Error < StandardError; end

    # Raise when copy of the auction has same countries
    class CopyHaveSameCountries < Error; end

    # Raise when the auction to have has the Global rights selected
    class CopyGlobalRightsSelected < Error; end

    class NoProfileConfiguredException < Error; end

    class EmailNotConfirmed < Error; end

  end

end
