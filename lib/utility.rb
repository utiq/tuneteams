require "net/http"
require "uri"
# encoding: utf-8

module Utility
  extend self

  def create_token
    SecureRandom.urlsafe_base64
  end

  def create_permalink(string, separator = '-', max_size = 160)
    ignore_words = ['a', 'an', 'the']
    ignore_re = String.new
    ignore_words.each{ |word| ignore_re << word + '\b|\b' }
    ignore_re = '\b' + ignore_re + '\b'
    permalink = string.gsub("'", separator)
    permalink.gsub!(ignore_re, '')
    permalink = permalink.parameterize
    permalink.gsub!(/[^a-z0-9]+/, separator)
    permalink = permalink.to(max_size)
    return permalink.gsub(/^\\#{separator}+|\\#{separator}+$/, '')
  end

  def genre_length
    Genre.all.each do |genre|
      genre.update_attributes(name_length: genre.name.length)
      puts genre.name.length
    end
  end

  def days_or_minutes number_days
    if ENV["CURRENT_ENV"] == 'production'
      return number_days.days
    else
      return number_days.minutes
    end
  end

  def country_code

    Country.where(:position.gt => 0).each do |country|

      uri = URI.parse("https://restcountries.eu/rest/v1/alpha/#{country.iso2}")
      http = Net::HTTP.new(uri.host, uri.port)
      http.use_ssl = true
      response = http.request(Net::HTTP::Get.new(uri.request_uri))
      json = JSON.parse response.body
      country_code = json["callingCodes"].first rescue "-1"
      puts country_code
      country.update_attributes(calling_code: country_code)
      sleep 1
      # puts country.name
    end

  end

  def update_auction_positions
    Auction.all.each_with_index do |auction, index|
      auction.update_attributes(position: index+1)
    end
  end

  def update_profile_positions
    Profile.all.each_with_index do |profile, index|
      profile.update_attributes(position: index+1)
    end
  end

  def generate_all_contracts
    Auction.where(status: 'published').each_with_index do |auction, index|
      auction.generate_license(auction.id)
    end
  end

  def delete_auction(auction_permalink)
    @auction = Auction.find_by(permalink: auction_permalink)
    @auction.auction_bids.delete_all
    @auction.auction_payouts.delete_all
    @auction.auction_bidders.delete_all
    @auction.auction_watchlist.delete_all
    @auction.delete
  end

  def lowercase_emails
    User.all.each do |user|
      lowercased = user.email.downcase
      user.update_attributes(email: lowercased)
    end
  end

end
