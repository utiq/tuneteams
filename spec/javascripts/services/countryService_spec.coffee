describe 'CountryService', ->
  beforeEach angular.mock.module('tuneTeams.services')

  # service = undefined
  $httpBackend = undefined
  response = undefined
  url = '/api/v1/countries/a365cc3520c7a70a553e95ee354670264'
  beforeEach angular.mock.inject((_$httpBackend_, $injector, _Country_) ->
    $httpBackend = _$httpBackend_
    @service = _Country_#$injector.get('Country')
    return
  )
  afterEach ->
    $httpBackend.verifyNoOutstandingExpectation()
    $httpBackend.verifyNoOutstandingRequest()
    return
  #tests
  it 'Country should be defined', ->
    angular.mock.expect(@service).toBeDefined()
    return
  it 'should send a ping and return the response', ->
    res = @service.get(id: 'a365cc3520c7a70a553e95ee354670264')
    $httpBackend.whenGET(url).respond status: 200
    #explicitly flushes pending requests
    $httpBackend.flush()
    expect(res.status).toEqual 200
    return
  return



# describe 'Country', ->
#   countryResource = $httpBackend = undefined
#   beforeEach -> 
#     module('tuneTeams.services')

#     inject ($injector) ->
#       $httpBackend = $injector.get('$httpBackend');
#       countryResource = $injector.get('Country')
#   # angular.module('tuneTeams.services')

#   # it 'is injectable', ->
#   #   expect(new Country).to.be.ok()

#   describe 'query', ->
#     it 'should return the list of valid countries', inject((Country) ->
#       # $httpBackend.expect('GET', '/api/v1/countries').respond(200, 'success');
#       $httpBackend.whenGET('/api/v1/countries').respond({status: 200});
#       countries = countryResource.query({})
#       $httpBackend.flush()
#       expect(countries.length).toEqual(3)

#     )
