describe 'Genre', ->

  beforeEach angular.mock.module('tuneTeams.services')
  $httpBackend = service = undefined

  beforeEach inject ($injector) ->
    $httpBackend = $injector.get('$httpBackend');
    service = $injector.get 'Genre'

  afterEach ->
    # @service = undefined
    $httpBackend.verifyNoOutstandingExpectation();
    $httpBackend.verifyNoOutstandingRequest();

  it 'is injectable', ->
    expect(service).to.be.ok()
    # expect(@service).toBeDefined();

  it 'should send a ping and return the response', ->
    
    # $httpBackend.whenGET('/api/v1/genres/1').respond status: 200
    res = service.get(id: '1')
    $httpBackend.expectGET("/api/v1/genres/1").respond status: 200
    
    #explicitly flushes pending requests
    $httpBackend.flush()
    console.log res
    expect(res.status).to.be 200